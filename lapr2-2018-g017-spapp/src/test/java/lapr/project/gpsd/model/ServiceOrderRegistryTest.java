/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;


import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author DEI aluno
 */
public class ServiceOrderRegistryTest {
    

    /**
     * Test of addOrder method, of class ServiceOrderRegistry.
     */
    @Test
    public void testAddOrder() {
        System.out.println("addOrder");
        ServiceOrder order = new ServiceOrder("01","joao","1km","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        ServiceOrderRegistry instance = ServiceOrderRegistry.getInstance();
        instance.addOrder(order);
        boolean result=instance.getServiceOrderList().contains(order);
        boolean expResult=true;
        assertEquals(expResult, result);
    }

    /**
     * Test of getServiceOrderList method, of class ServiceOrderRegistry.
     */
    @Test
    public void testGetServiceOrderList() {
        System.out.println("getServiceOrderList");
        ServiceOrderRegistry instance = ServiceOrderRegistry.getInstance();
        List<ServiceOrder> expResult = new ArrayList<ServiceOrder>();
        ServiceOrder order = new ServiceOrder("01","joao","1km","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        instance.addOrder(order);
        expResult.add(order);
        List<ServiceOrder> result = instance.getServiceOrderList();
        assertEquals(expResult.contains(order), result.contains(order));
    }
    
}
