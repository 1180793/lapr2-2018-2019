/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;


import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author DEI aluno
 */
public class ServiceOrderTest {
    

    /**
     * Test of getServiceOrder method, of class ServiceOrder.
     */
    @Test
    public void testGetServiceOrder() {
        System.out.println("getServiceOrder");
       ServiceOrder instance = new ServiceOrder("01","joao","1","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        ServiceOrder expResult = new ServiceOrder("01","joao","1","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        ServiceOrder result = instance.getServiceOrder();
        assertEquals(expResult.postalCode, result.postalCode);
    }
    
     @Test
    public void testGetNum() {
        System.out.println("getNum");
       ServiceOrder instance = new ServiceOrder("01","joao","1","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "01";
        String result = instance.getNum();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetName() {
        System.out.println("getName");
        ServiceOrder instance = new ServiceOrder("01","joao","1","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "joao";
        String result = instance.getName();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetDistance() {
        System.out.println("getDistance");
      ServiceOrder instance = new ServiceOrder("01","joao","1","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "1";
        String result = instance.getDistance();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCategory() {
        System.out.println("getCategory");
       ServiceOrder instance = new ServiceOrder("01","joao","1","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "gardening";
        String result = instance.getCategory();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetType() {
        System.out.println("getType");
        ServiceOrder instance = new ServiceOrder("01","joao","1km","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "limited";
        String result = instance.getType();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetStartDate() {
        System.out.println("getStartDate");
        ServiceOrder instance = new ServiceOrder("01","joao","1km","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "13/04/2019";
        String result = instance.getStartDate();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetStartTime() {
        System.out.println("getStartTime");
        ServiceOrder instance = new ServiceOrder("01","joao","1km","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "13:30";
        String result = instance.getStartTime();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPostalAddress() {
        System.out.println("getPostalAddress");
        ServiceOrder instance = new ServiceOrder("01","joao","1km","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "Rua Pedro Hispano";
        String result = instance.getPostalAddress();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetLocality() {
        System.out.println("getLocality");
        ServiceOrder instance = new ServiceOrder("01","joao","1km","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "Porto";
        String result = instance.getLocality();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetPostalCode() {
        System.out.println("getPostalCode");
        ServiceOrder instance = new ServiceOrder("01","joao","1km","gardening","limited","13/04/2019","13:30","Rua Pedro Hispano", "Porto", "4150-362");
        String expResult = "4150-362";
        String result = instance.getPostalCode();
        assertEquals(expResult, result);
    }
    
}
