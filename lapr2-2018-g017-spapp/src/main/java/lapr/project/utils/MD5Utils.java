package lapr.project.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class MD5Utils {
    
    private MD5Utils() {}
    
    /**
     * Method to convert password.
     * 
     * @param password: password to be converted
     * @return pass: if MessageDigest isn't null
     * or null: if the MessageDigest is null
     */
    public static String convert(String password) {
        try {
            String pass;
            MessageDigest md;
            md = MessageDigest.getInstance("MD5");
            if (md != null) {
                BigInteger hash = new BigInteger(1, md.digest(password.getBytes()));
                pass = hash.toString(16);
                return pass;
            } else {
                return null;
            }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(MD5Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
	}
}
