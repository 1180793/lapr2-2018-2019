package lapr.project.utils;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import lapr.project.gpsd.MainApp;


/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class FxUtils {
    
        private static final String TITLE = "AppGPSD";
        private static final String ERROR_WINDOW = "Error Window";
        private static final String DASHBOARD_TITLE = "Dashboard - ";
        
        private FxUtils() {}

	public static void switchScene(ActionEvent event, String fileName, String title) throws IOException {

		BorderPane tableViewParent;

		Parent topBar = FXMLLoader.load(MainApp.class.getResource("/fxml/TopBar.fxml"));

		tableViewParent = FXMLLoader.load(MainApp.class.getResource("/fxml/" + fileName + ".fxml"));
		tableViewParent.setTop(topBar);

                Scene tableViewScene = new Scene(tableViewParent);

		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

		window.setScene(tableViewScene);
		window.setTitle(TITLE + " | " + title);
		window.setResizable(false);
		window.centerOnScreen();
		window.show();

		addWindowListener(window);
	}


        private static void addWindowListener(Stage window) {
                window.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        
                        alert.setTitle(TITLE);
                        alert.setHeaderText("Confirm Exit");
                        alert.setContentText("Do you want to exit AGPSD?");
                        
                        if (alert.showAndWait().get() == ButtonType.CANCEL) {
                                event.consume();
                        }
                }
        });
    }

}