package lapr.project.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.Alert;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceOrderRegistry;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Utils {
    
        public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.00");

	public static final String[] LIST_OF_DAYS = { "Sunday", "Monday", "Tuesday", "Wednesday",
			"Thursday", "Friday", "Saturday" };

	public static final String[] LIST_OF_TIMES = { "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00",
			"09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00",
			"15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00",
			"21:30", "22:00", "22:30", "23:00", "23:30" , "00:00" };

	public static final String[] LIST_OF_DURATIONS = { "00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30",
			"04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30",
			"10:00", "10:30", "11:00", "11:30", "12:00" };

        public static final Integer[] LIST_OF_RATINGS = { 1, 2, 3, 4, 5};
        
        private Utils() {}
        
        /*
        Method that opens an alert window 
        @param title: title of the alert window
        @param headerText: header text of the alert window
        @param contentText: content of the alert window
        @param alertType: alertType of the alert window
        */
	public static void openAlert(String title, String headerText, String contentText, Alert.AlertType alertType) {
		Alert alert = new Alert(alertType);
		alert.setTitle(title);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.showAndWait();
	}
/*
        Method to convert an inserted duration
        @param strDuration: duration to be converted
        @return firsDigit: if the duration ends in 0
        @return firstDigit + 0.5: if the duration ends in 30
        */
	public static double convertDuration(String strDuration) {
		String[] temp = strDuration.split(":");
		int firstDigit = Integer.parseInt(temp[0]);
		int secondDigit = Integer.parseInt(temp[1]);
                switch (secondDigit) {
                        case 0:
                                return firstDigit;
                        case 30:
                                return firstDigit + 0.5;
                        default:
                                return 0;
            }
	}
        /*
        Method to know the distance of two different places according to the latitude and longitude
        @param lat1: latitude of the first place
        @param long1: longitude of the first place
        @param lat2: latitude of the second place
        @param long2: longitude of the second place
        @return d: distance in meters of the two places
        */
        public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
		final double R = 6371e3;
		double theta1 = Math.toRadians(lat1);
		double theta2 = Math.toRadians(lat2);
		double deltaTheta = Math.toRadians(lat2 - lat1);
		double deltaLambda = Math.toRadians(lon2 - lon1);
		double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2)
				+ Math.cos(theta1) * Math.cos(theta2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		return R * c;
	}
        /*
        Method to know the week day of a certain date (yyyy/mm/dd)
        @param strDate: date to know the day of the week
        @return weekDay: week day of the date passed through parameter
        */
        public static String getWeekDay(String strDate) throws ParseException {
		Calendar c = Calendar.getInstance();
		Date date = new SimpleDateFormat("yyyy-MM-dd").parse(strDate);
		c.setTime(date);
		int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
		return LIST_OF_DAYS[dayOfWeek];
	}
        /*
        Method to add one day to a certain week day
        @param strWeekDay: day of the week 
        @return strNewWeekDay: week day after adding a day
        */
        public static String addWeekDay(String strWeekDay) {
            int index = 0;
            String strNewWeekDay;
            for (int i = 0; i < LIST_OF_DAYS.length; i++) {
                if(strWeekDay.equalsIgnoreCase(LIST_OF_DAYS[i])) {
                    index = i;
                }
            }
            if (index == 0 && !strWeekDay.equalsIgnoreCase(LIST_OF_DAYS[0])) {
                return "ERROR";
            }
            try {
                strNewWeekDay = LIST_OF_DAYS[index + 1];
            } catch (ArrayIndexOutOfBoundsException ex) {
                strNewWeekDay = LIST_OF_DAYS[0];
            } 
            return strNewWeekDay;
        }
        
        /**
        Method to know which hour passed by parameter is later
        @param strHour1: hour to compare
        @param strHour2: hour to compare
        @return true: if strHour1 is after strHour2
                false: if strHour2 is after strHour1
        */
        public static boolean isAfter(String strHour1, String strHour2) {
            int indexStart = -1;
            int indexEnd = -1;
            for (int i = 0; i < Utils.LIST_OF_TIMES.length; i++) {
                if (Utils.LIST_OF_TIMES[i].equalsIgnoreCase(strHour1)) {
                    indexStart = i;
                }
                if (Utils.LIST_OF_TIMES[i].equalsIgnoreCase(strHour2)) {
                    indexEnd = i;
                }
            }
            return indexStart > indexEnd;
        }
        
        /**
        Method to know which hour passed by parameter is later
        @param strHour1: hour to compare
        @param strHour2: hour to compare
        @return true: if strHour1 is after strHour2
                false: if strHour2 is after strHour1
        */
        public static boolean isBefore(String strHour1, String strHour2) {
            int indexStart = -1;
            int indexEnd = -1;
            for (int i = 0; i < Utils.LIST_OF_TIMES.length; i++) {
                if (Utils.LIST_OF_TIMES[i].equalsIgnoreCase(strHour1)) {
                    indexStart = i;
                }
                if (Utils.LIST_OF_TIMES[i].equalsIgnoreCase(strHour2)) {
                    indexEnd = i;
                }
            }
            return indexStart < indexEnd;
        }
        
   
     public static void saveOrders(){   
                    ObjectOutputStream out = null;
        try {
            List<ServiceOrder> savedOrders = ServiceOrderRegistry.getInstance().getServiceOrderList();
            out = new ObjectOutputStream(new FileOutputStream(new File("savedOrders.txt"), false));
            out.writeObject(savedOrders);
            out.close();
            System.out.println("Orders Saved");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ServiceOrderRegistry.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("file not found");
        } catch (IOException ex) {
            Logger.getLogger(ServiceOrderRegistry.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("IOException");
        } finally {
            try {
                out.close();
            } catch (IOException ex) {
                Logger.getLogger(ServiceOrderRegistry.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
     
     
     public static void importOrderFileExcel(File file){
        List orderList = ServiceOrderRegistry.getInstance().getServiceOrderList();
        FileInputStream stream = null;
        DataFormatter df = new DataFormatter();
        try {            
            stream = new FileInputStream(file.getAbsolutePath());    
            Workbook workbook = new XSSFWorkbook(stream);
                Sheet sheet = workbook.getSheetAt(0);
                Iterator rowIterator = sheet.iterator();                
                while (rowIterator.hasNext()) {
                    ServiceOrder order = new ServiceOrder();
                    Row row = (Row) rowIterator.next();
                    if(row.getRowNum()==0){
                        continue;
                    }
                    Iterator cellIterator = row.cellIterator();
                    
                    while (cellIterator.hasNext()) {
 
                        Cell cell = (Cell) cellIterator.next();
                            switch (cell.getColumnIndex()) {
                                case 0:
                                    String valueNum = df.formatCellValue(cell);
                                    order.setNum(valueNum);
                                    break;
                                case 1:
                                    order.setName(String.valueOf(cell.getStringCellValue()));
                                    break;
                                case 2:
                                    String valueDist = df.formatCellValue(cell);
                                    order.setDistance(valueDist);
                                    break;
                                case 3:
                                    order.setCategory(String.valueOf(cell.getStringCellValue()));
                                    break;
                                case 4:
                                    order.setType(String.valueOf(cell.getStringCellValue()));
                                    break;
                                case 5:
                                    String valueDate = df.formatCellValue(cell);
                                    order.setStartDate(valueDate);
                                    break;
                                case 6:
                                    String valueTime = df.formatCellValue(cell);
                                    order.setStartTime(valueTime);
                                    break;
                                case 7:
                                    order.setPostalAddress(String.valueOf(cell.getStringCellValue()));
                                    break;
                                case 8:
                                    order.setLocality(String.valueOf(cell.getStringCellValue()));
                                    break;
                                case 9:
                                    String valueCode = df.formatCellValue(cell);
                                    order.setPostalCode(valueCode);
                                    break;
                                default:
                                    break;
                            }                      
                    }
                    orderList.add(order);
                }
            stream.close();
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        }
    }
 
     public static void importOrderFileCSV(File file) throws FileNotFoundException{
        List orderList = ServiceOrderRegistry.getInstance().getServiceOrderList();
            try (Scanner scanner = new Scanner(new File(file.getAbsolutePath()))) {
                String line = "";
                String split = ";";
                scanner.nextLine();
                while (scanner.hasNextLine()) {
                    String[] orderString = scanner.nextLine().split(split);
                    ServiceOrder order = new ServiceOrder();
                    order.setNum(orderString[0]);
                    order.setName(orderString[1]);
                    order.setDistance(orderString[2]);
                    order.setCategory(orderString[3]);
                    order.setType(orderString[4]);
                    order.setStartDate(orderString[5]);
                    order.setStartTime(orderString[6]);
                    order.setPostalAddress(orderString[7]);
                    order.setLocality(orderString[8]);
                    order.setPostalCode(orderString[9]);
                    orderList.add(order);
                }
            }
            }
     
     
  }
     

     
     
