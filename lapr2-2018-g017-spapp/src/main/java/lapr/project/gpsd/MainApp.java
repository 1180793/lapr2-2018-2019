package lapr.project.gpsd;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.List;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceOrderRegistry;
import lapr.project.utils.Utils;

/**
 * @author Simao
 */
public class MainApp extends Application {

	@Override
	public void start(Stage stage) throws Exception {
                
            try{
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("savedOrders.txt"));
            List<ServiceOrder> listOrders =(List<ServiceOrder>) in.readObject();
            for(ServiceOrder order : listOrders){
                ServiceOrderRegistry.getInstance().getServiceOrderList().add(order);
                System.out.println("added");
            }
            }catch(FileNotFoundException ex){
            }catch(IOException ex){
            }catch(ClassNotFoundException ex){
            }
            
           Runtime.getRuntime().addShutdownHook(new Thread(() -> {
               Utils.saveOrders();
            }, "Shutdown-thread"));

            
                Parent root = FXMLLoader.load(getClass().getResource("/fxml/CheckOrdersUI.fxml"));

		Scene scene = new Scene(root);
		scene.getStylesheets().add("/styles/Styles.css");

		stage.setTitle("SPApp | Check Orders");
		stage.setResizable(false);
		stage.setScene(scene);
		stage.show();
	}

	/**
	 * The main() method is ignored in correctly deployed JavaFX application. main()
	 * serves only as fallback in case the application can not be launched through
	 * deployment artifacts, e.g., in IDEs with limited FX support. NetBeans ignores
	 * main().
	 *
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

}

