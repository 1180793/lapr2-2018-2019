/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceOrderRegistry;

/**
 *
 * @author Bodynho Oliveira
 */
public class CheckHistoricalServicesController implements Initializable {

  //  List<ServiceOrder> doneOrderList = ServiceOrderRegistry.getInstance().getDoneServiceOrderList();
    
    @FXML
    private Label lblSPapp;
    @FXML
    private TableView<ServiceOrder> tblViewPane;
    @FXML
    private TableColumn<ServiceOrder, String> numColumn;
    @FXML
    private TableColumn<ServiceOrder, String> nameColumn;
    @FXML
    private TableColumn<ServiceOrder, String> distanceColumn;
    @FXML
    private TableColumn<ServiceOrder, String> categoryColumn;
    @FXML
    private TableColumn<ServiceOrder, String> typeColumn;
    @FXML
    private TableColumn<ServiceOrder, String> startDateColumn;
    @FXML
    private TableColumn<ServiceOrder, String> startTimeColumn;
    @FXML
    private TableColumn<ServiceOrder, String> addressColumn;
    @FXML
    private TableColumn<ServiceOrder, String> localityColumn;
    @FXML
    private TableColumn<ServiceOrder, String> codeColumn;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        populateDoneOrderForClientTable();
       
    }

    private void populateDoneOrderForClientTable() {
                numColumn.setCellValueFactory(new PropertyValueFactory<>("num"));
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		distanceColumn.setCellValueFactory(new PropertyValueFactory<>("distance"));
		categoryColumn.setCellValueFactory(new PropertyValueFactory<>("category"));
                typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
                startDateColumn.setCellValueFactory(new PropertyValueFactory<>("startDate"));
                startTimeColumn.setCellValueFactory(new PropertyValueFactory<>("startTime"));
                addressColumn.setCellValueFactory(new PropertyValueFactory<>("postalAddress"));
                localityColumn.setCellValueFactory(new PropertyValueFactory<>("locality"));
                codeColumn.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
                
		//ObservableList<ServiceOrder> list = FXCollections.observableArrayList(doneOrderList);
                
		//tblViewPane.setItems(list);
        
    }
    
}
