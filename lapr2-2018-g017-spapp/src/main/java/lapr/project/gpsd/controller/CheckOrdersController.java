/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceOrderRegistry;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;
import static lapr.project.utils.Utils.importOrderFileCSV;
import static lapr.project.utils.Utils.importOrderFileExcel;

/**
 * FXML Controller class
 *
 * @author Simao
 */
public class CheckOrdersController implements Initializable {

    List<ServiceOrder> orderList=ServiceOrderRegistry.getInstance().getServiceOrderList();
    
    @FXML
    private Label lblSPapp;
    @FXML
    private Button importOrdersButton;
    @FXML
    private Button checkHistoryButton;
    @FXML
    private TableView<ServiceOrder> tblViewPane;
    @FXML
    private TableColumn<ServiceOrder, String> nameColumn;
    @FXML
    private TableColumn<ServiceOrder, String> distanceColumn;
    @FXML
    private TableColumn<ServiceOrder, String> categoryColumn;
    @FXML
    private TableColumn<ServiceOrder, String> typeColumn;
    @FXML
    private TableColumn<ServiceOrder, String> addressColumn;
    @FXML
    private TableColumn<ServiceOrder, String> numColumn;
    @FXML
    private TableColumn<ServiceOrder, String> startDateColumn;
    @FXML
    private TableColumn<ServiceOrder, String> startTimeColumn;
    @FXML
    private TableColumn<ServiceOrder, String> localityColumn;
    @FXML
    private TableColumn<ServiceOrder, String> codeColumn;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        populateOrderTable();
    }    

    public void populateOrderTable(){
                numColumn.setCellValueFactory(new PropertyValueFactory<>("num"));
		nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		distanceColumn.setCellValueFactory(new PropertyValueFactory<>("distance"));
		categoryColumn.setCellValueFactory(new PropertyValueFactory<>("category"));
                typeColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
                startDateColumn.setCellValueFactory(new PropertyValueFactory<>("startDate"));
                startTimeColumn.setCellValueFactory(new PropertyValueFactory<>("startTime"));
                addressColumn.setCellValueFactory(new PropertyValueFactory<>("postalAddress"));
                localityColumn.setCellValueFactory(new PropertyValueFactory<>("locality"));
                codeColumn.setCellValueFactory(new PropertyValueFactory<>("postalCode"));
		ObservableList<ServiceOrder> list = FXCollections.observableArrayList(orderList);
		tblViewPane.setItems(list);
    }
    
    public void updateOrderTable(){
        tblViewPane.getItems().clear();
        ObservableList<ServiceOrder> list = FXCollections.observableArrayList(orderList);
	tblViewPane.setItems(list);
    }
    
    @FXML
    private void handleImportOrders(ActionEvent event) throws FileNotFoundException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.setInitialDirectory(new File("."));
        fileChooser.getExtensionFilters().addAll(
        new ExtensionFilter("CSV files", "*.csv"),
        new ExtensionFilter("XLSX files", "*.xlsx"),
        new ExtensionFilter("XML files", "*.xml"));
        File selectedFile = fileChooser.showOpenDialog(null);
        if(selectedFile != null){
            if(selectedFile.getName().substring(selectedFile.getName().lastIndexOf(".")).equals(".xlsx")){
            importOrderFileExcel(selectedFile);
        }
            if(selectedFile.getName().substring(selectedFile.getName().lastIndexOf(".")).equals(".csv")){
             importOrderFileCSV(selectedFile);
            }   
        updateOrderTable();
        Utils.saveOrders();
    }
    }

    @FXML
    private void handleCheckHistory(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "CheckHistoricalUI", "Check Historical Services");
        updateOrderTable();
    }
    
}
