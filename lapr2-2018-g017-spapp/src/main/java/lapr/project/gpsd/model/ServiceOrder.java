/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Simao
 */
public class ServiceOrder implements Serializable{
    String num;
    String name;
    String distance;
    String category;
    String type;
    String startDate;
    String startTime;
    String postalAddress;
    String locality;
    String postalCode;
    
    public ServiceOrder(String num, String name, String distance, String category, String type, String startDate, String startTime, String postalAddress, String locality, String postalCode){
        this.num=num;
        this.name=name;
        this.distance=distance;
        this.category=category;
        this.type=type;
        this.startDate=startDate;
        this.startTime=startTime;
        this.postalAddress=postalAddress;
        this.locality=locality;
        this.postalCode=postalCode;
    }
    public ServiceOrder(ServiceOrder order){
        this.name=order.name;
        this.distance=order.distance;
        this.category=order.category;
        this.type=order.type;
        this.startDate=order.startDate;
        this.startTime=order.startTime;
        this.postalAddress=order.postalAddress;
        this.postalCode=order.postalCode;
    }
        public ServiceOrder(){
    } 
        
    public ServiceOrder getServiceOrder(){
        return new ServiceOrder(this);
    }
    
    public String getNum(){
        return this.num;
    }
    
    public String getName(){
        return this.name;
    }
    public String getDistance(){
        return this.distance;
    }
    public String getCategory(){
        return this.category;
    }
    public String getType(){
        return this.type;
    }
    public String getStartDate(){
        return this.startDate;
    }
    public String getStartTime(){
        return this.startTime;
    }
    public String getPostalAddress(){
        return this.postalAddress;
    }
    public String getLocality(){
        return this.locality;
    }
    public String getPostalCode(){
        return this.postalCode;
    }
        
    public void setNum(String num){
      this.num=num;
    }
      
    public void setName(String name){
      this.name=name;
    }
          
    public void setDistance(String distance){
      this.distance=distance;
    }
          
    public void setCategory(String category){
      this.category=category;
    }
          
    public void setType(String type){
      this.type=type;
    }
          
    public void setStartDate(String startDate){
      this.startDate=startDate;
    }
          
    public void setStartTime(String startTime){
      this.startTime=startTime;
    }
    
    public void setPostalAddress(String postalAddress){
      this.postalAddress=postalAddress;
    }
    
    public void setLocality(String locality){
      this.locality=locality;
    }
    
    public void setPostalCode(String postalCode){
      this.postalCode=postalCode;
    }
}
