/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Simao
 */
public class ServiceOrderRegistry implements Serializable{
    
    private static List<ServiceOrder> ServiceOrderList = new ArrayList<>();
    private static ServiceOrderRegistry registry = null;
    
    private ServiceOrderRegistry(){
        
    }
    
    public static ServiceOrderRegistry getInstance(){
        if(registry == null){
            registry = new ServiceOrderRegistry();
        }
        return registry;
    }
    
    public void addOrder(ServiceOrder order){
        this.ServiceOrderList.add(order);
    }
    
    public List<ServiceOrder> getServiceOrderList(){
        return this.ServiceOrderList;
    }
}
