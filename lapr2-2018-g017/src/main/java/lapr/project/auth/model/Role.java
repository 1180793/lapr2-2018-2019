package lapr.project.auth.model;

import java.util.Objects;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Role {

	private final String strRole;
	private final String strDescription;

	public Role(String strPapel) {
		if ((strPapel == null) || (strPapel.isEmpty()))
			throw new IllegalArgumentException("Role name can't be empty or null.");

		this.strRole = strPapel;
		this.strDescription = strPapel;
	}

	public Role(String strRole, String strDescription) {
		if ((strRole == null) || (strDescription == null) || (strRole.isEmpty()) || (strDescription.isEmpty()))
			throw new IllegalArgumentException("Role arguments can't be empty or null.");

		this.strRole = strRole;
		this.strDescription = strDescription;
	}

	public String getRole() {
		return this.strRole;
	}

	public String getDescription() {
		return this.strDescription;
	}

	public boolean hasId(String strId) {
		return this.strRole.equals(strId);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 23 * hash + Objects.hashCode(this.strRole);
		return hash;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		Role obj = (Role) o;
		return Objects.equals(strRole, obj.strRole);
	}

	@Override
	public String toString() {
		if (this.strRole.equalsIgnoreCase(this.strDescription)) {
			return String.format("%s", this.strRole);
		} else {
			return String.format("%s - %s", this.strRole, this.strDescription);
		}
	}
	
}
