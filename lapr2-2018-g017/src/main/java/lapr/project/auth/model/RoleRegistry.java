package lapr.project.auth.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class RoleRegistry {

	private final Set<Role> lstRoles = new HashSet<>();

	public Role newRole(String strRole) {
		return new Role(strRole);
	}

	public Role newRole(String strRole, String strDescription) {
		return new Role(strRole, strDescription);
	}

	public boolean addRole(Role role) {
		if (role != null)
			return this.lstRoles.add(role);
		return false;
	}

	public boolean removeRole(Role role) {
		if (role != null)
			return this.lstRoles.remove(role);
		return false;
	}

	public Role getRole(String strRole) {
		for (Role role : this.lstRoles) {
			if (role.hasId(strRole))
				return role;
		}
		return null;
	}

	public boolean hasRole(String strRole) {
		Role role = getRole(strRole);
		if (role != null)
			return this.lstRoles.contains(role);
		return false;
	}

	public boolean hasRole(Role role) {
		return this.lstRoles.contains(role);
	}
}
