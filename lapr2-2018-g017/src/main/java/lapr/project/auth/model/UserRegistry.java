package lapr.project.auth.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserRegistry {

	private final Set<User> lstUsers = new HashSet<>();

	public User newUser(String strName, String strEmail, String strPassword) {
		return new User(strName, strEmail, strPassword);
	}

	public Set<User> getUsers() {
		return this.lstUsers;
	}

	public boolean addUser(User user) {
		if (user != null)
			return this.lstUsers.add(user);
		return false;
	}

	public boolean removeUser(User user) {
		if (user != null)
			return this.lstUsers.remove(user);
		return false;
	}

	public User getUser(String strId) {
		for (User user : this.lstUsers) {
			if (user.hasId(strId))
				return user;
		}
		return null;
	}

	public boolean hasUser(String strId) {
		User user = getUser(strId);
		if (user != null)
			return this.lstUsers.contains(user);
		return false;
	}

	public boolean hasUser(User user) {
		return this.lstUsers.contains(user);
	}
}
