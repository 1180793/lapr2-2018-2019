package lapr.project.auth.model;

import java.util.List;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserSession {

	private User oUser = null;

	public UserSession(User oUser) {
		if (oUser == null)
			throw new IllegalArgumentException("UserSession arguments can't be empty or null.");
		this.oUser = oUser;
	}

	public void doLogout() {
		this.oUser = null;
	}

	public boolean isLoggedIn() {
		return this.oUser != null;
	}

	public boolean isLoggedInWithRole(String strRole) {
		if (isLoggedIn()) {
			return this.oUser.hasRole(strRole);
		}
		return false;
	}

	public User getUser() {
		return this.oUser;
	}

	public String getUserName() {
		if (isLoggedIn())
			return this.oUser.getName();
		return null;
	}

	public String getUserEmail() {
		if (isLoggedIn())
			return this.oUser.getEmail();
		return null;
	}

	public List<Role> getUserRoles() {
		return this.oUser.getRoles();
	}

        @Override
	public String toString() {
		return this.oUser.toString();
	}
}
