package lapr.project.auth.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import lapr.project.utils.MD5Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class User {
    
        private final String strName;
	private final String strEmail;
	private final String strPassword; // Stores the encripted password
	private final Set<Role> lstRoles = new HashSet<>();

	public User(String strName, String strEmail, String strPassword) {

		if ((strName == null) || (strEmail == null) || (strPassword == null) || (strName.isEmpty())
				|| (strEmail.isEmpty()) || (strPassword.isEmpty()))
			throw new IllegalArgumentException("User arguments can't be empty or null.");

		this.strName = strName;
		this.strEmail = strEmail;
		this.strPassword = MD5Utils.convert(strPassword);

	}

	public String getName() {
		return this.strName;
	}

	public String getEmail() {
		return this.strEmail;
	}

	public String getEncriptedPassword() {
		return this.strPassword;
	}

	public boolean hasId(String strId) {
		return this.strEmail.equalsIgnoreCase(strId);
	}

	public boolean hasPassword(String strPwd) {
		return this.strPassword.equals(MD5Utils.convert(strPwd));
	}

	public boolean addRole(Role role) {
		if (role != null)
			return this.lstRoles.add(role);
		return false;
	}

	public boolean removeRole(Role role) {
		if (role != null)
			return this.lstRoles.remove(role);
		return false;
	}

	public boolean hasRole(Role role) {
		return this.lstRoles.contains(role);
	}

	public boolean hasRole(String strRole) {
		for (Role role : this.lstRoles) {
			if (role.hasId(strRole))
				return true;
		}
		return false;
	}

	public List<Role> getRoles() {
		List<Role> list = new ArrayList<>();
		for (Role role : this.lstRoles)
			list.add(role);
		return list;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 23 * hash + Objects.hashCode(this.strEmail);
		return hash;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		User obj = (User) o;
		return Objects.equals(strEmail, obj.strEmail);
	}

	@Override
	public String toString() {
		return String.format("%s - %s", this.strName, this.strEmail);
	}
}
