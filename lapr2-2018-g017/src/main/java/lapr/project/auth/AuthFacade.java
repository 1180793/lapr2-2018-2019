package lapr.project.auth;

import lapr.project.auth.model.Role;
import lapr.project.auth.model.RoleRegistry;
import lapr.project.auth.model.User;
import lapr.project.auth.model.UserRegistry;
import lapr.project.auth.model.UserSession;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AuthFacade {

	private UserSession oSession = null;

	private final RoleRegistry oRoles = new RoleRegistry();
	private final UserRegistry oUsers = new UserRegistry();

	public UserRegistry getUserRegistry() {
		return this.oUsers;
	}
	
	public RoleRegistry getRoleRegistry() {
		return this.oRoles;
	}

	public boolean registerRole(String strRole) {
		Role role = this.oRoles.newRole(strRole);
		return this.oRoles.addRole(role);
	}

	public boolean registerRole(String strRole, String strDescription) {
		Role role = this.oRoles.newRole(strRole, strDescription);
		return this.oRoles.addRole(role);
	}

	public boolean registerUser(String strName, String strEmail, String strPassword) {
		User user = this.oUsers.newUser(strName, strEmail, strPassword);
		return this.oUsers.addUser(user);
	}

	public boolean registerUserWithRole(String strName, String strEmail, String strPassword, String strRole) {
		Role role = this.oRoles.getRole(strRole);
		User user = this.oUsers.newUser(strName, strEmail, strPassword);
		user.addRole(role);
		return this.oUsers.addUser(user);
	}

	public boolean registerUserWithRoles(String strName, String strEmail, String strPassword, String[] roles) {
		User user = this.oUsers.newUser(strName, strEmail, strPassword);
		for (String strRole : roles) {
			Role role = this.oRoles.getRole(strRole);
			user.addRole(role);
		}

		return this.oUsers.addUser(user);
	}

	public boolean hasUser(String strId) {
		return this.oUsers.hasUser(strId);
	}

	public UserSession doLogin(String strId, String strPwd) {
		User user = this.oUsers.getUser(strId);
		if (user != null && user.hasPassword(strPwd)) {
                        this.oSession = new UserSession(user);
		}
		return getCurrentSession();
	}

	public UserSession getCurrentSession() {
		return this.oSession;
	}

	public void doLogout() {
		if (this.oSession != null)
			this.oSession.doLogout();
		this.oSession = null;
	}
}
