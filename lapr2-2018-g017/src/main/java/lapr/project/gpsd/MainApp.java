package lapr.project.gpsd;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lapr.project.gpsd.controller.AppGPSD;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class MainApp extends Application {

	@Override
	public void start(Stage stage) throws Exception {
		AppGPSD.getInstance().setWindow(stage);
                Parent root = FXMLLoader.load(getClass().getResource("/fxml/LoginUI.fxml"));

		Scene scene = new Scene(root);
		scene.getStylesheets().add("/styles/Styles.css");

		stage.setTitle("AGPSD | Login");
		stage.setResizable(false);
		stage.setScene(scene);
		stage.show();
                
//                PromptedServiceDescription oDesc = new PromptedServiceDescription(AppGPSD.getInstance().getCompany().getServiceRegistry().getServiceById("002"), "test", "04:30");
//                oDesc.assignProvider(AppGPSD.getInstance().getCompany().getServiceProviderRegistry().getServiceProviderByEmail("hfa@isep.ipp.pt"));
//                PromptedServiceDescription oDesc2 = new PromptedServiceDescription(AppGPSD.getInstance().getCompany().getServiceRegistry().getServiceById("001"), "test2", "02:30");
//                oDesc2.assignProvider(AppGPSD.getInstance().getCompany().getServiceProviderRegistry().getServiceProviderByEmail("hfa@isep.ipp.pt"));
//                AppGPSD.getInstance().getCompany().getServiceRatingRegistry().registerServiceRating(2, oDesc, AppGPSD.getInstance().getCompany().getClientRegistry().getClientByEmail("cli2@esoft.pt"));
//                AppGPSD.getInstance().getCompany().getServiceRatingRegistry().registerServiceRating(5, oDesc2, AppGPSD.getInstance().getCompany().getClientRegistry().getClientByEmail("cli2@esoft.pt"));
	}


	/**
	 * The main() method is ignored in correctly deployed JavaFX application. main()
	 * serves only as fallback in case the application can not be launched through
	 * deployment artifacts, e.g., in IDEs with limited FX support. NetBeans ignores
	 * main().
	 *
	 * @param args the command line arguments
	 */
	public static void main(String[] args) {
		launch(args);
	}

}

