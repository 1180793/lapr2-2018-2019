package lapr.project.gpsd.controller;

import java.util.List;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.utils.Utils;
/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AssociatePostalAddressController {

        private final AppGPSD oApp;
        private final Company oCompany;
        private final UserSession oSession;
        private final ClientRegistry oClients;
        private final String strEmail;
        private final Client oClient;

        public AssociatePostalAddressController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oSession = oApp.getCurrentSession();
                this.oClients = oCompany.getClientRegistry();
                this.strEmail = oSession.getUserEmail();
                this.oClient = oClients.getClientByEmail(oSession.getUserEmail());
        }

        public AppGPSD getApp() {
                return this.oApp;
        }
        
        public String getRole() {
                return this.oApp.getRole();
        }
        
        public String getEmail() {
                return this.oSession.getUserEmail();
        }

        public List<Address> getAddresses() {
                return this.oClient.getAddresses();
        }
        
        public Address fetchAddressByPostalCode(String strPostalCode) {
                if (strPostalCode == null || strPostalCode.isEmpty()) {
                        Utils.openAlert("Error Fetching Address", "Error Fetching Address", "Please enter a Postal Code", Alert.AlertType.ERROR);
                        return null;
                } else {
                        Address oAddress = Utils.getAddressFromPostalCode(strPostalCode);
                        if (oAddress != null) {
                                return oAddress;
                        } else {
                                Utils.openAlert("Error Fetching Address", "Error Fetching Address", "Invalid Postal Code", Alert.AlertType.ERROR);
                                return null;
                        }
                }
        }

        public boolean addAddress(String strStreet, String strPostalCode, String strLocality) {
                if (!((strStreet == null) || (strPostalCode == null) || (strLocality == null)
                        || (strStreet.isEmpty()) || (strPostalCode.isEmpty()) || (strLocality.isEmpty()))) {
                        PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                        if (oPostalCode == null) {
                                Utils.openAlert("Unable to add Address", "Unable to add Address", "Invalid Postal Code", Alert.AlertType.ERROR);
                                return false;
                        }
                        Address address = new Address(strStreet, oPostalCode, strLocality);
                        if (validateAddress(address)) {
                                return oClient.registerAddress(address);
                        } else {
                                Utils.openAlert("Unable to add Address", "Unable to add Address", "Duplicated Address", Alert.AlertType.ERROR);
                                return false;
                        }
                } else {
                        Utils.openAlert("Unable to add Address", "Unable to add Address", "Please fill all the Address fields", Alert.AlertType.ERROR);
                        return false;
                }
        }
        
        private boolean validateAddress(Address oAddress) {
                for (Address address : oClient.getAddresses()) {
                        if (address.equals(oAddress)) {
                                return false;
                        }
                }
                return true;
        }
        
        public boolean removeAddress(Address oAddress) {
                if (oAddress != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Remove Address");
                        alert.setHeaderText("Do you want to remove the selected Address?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && (result.get() == ButtonType.OK)) {
                                return oClient.removeAddress(oAddress);
                        }
                } else {
                        Utils.openAlert("Error trying to remove Address", "Error trying to remove Address",
                                "Select an Address to remove", Alert.AlertType.ERROR);
                }
                return false;
        }
}
