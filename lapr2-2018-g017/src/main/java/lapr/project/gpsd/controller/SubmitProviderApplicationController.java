package lapr.project.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Alert;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.AcademicQualification;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.gpsd.model.ProfessionalQualification;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class SubmitProviderApplicationController {

        private final AppGPSD oApp;
        private final Company oCompany;
        private List<Category> lstCategories = new ArrayList<>();
        private List<ProfessionalQualification> lstProQual = new ArrayList<>();
        private List<AcademicQualification> lstAcaQual = new ArrayList<>();
        private final String REGISTER_ERROR = "Error";

        public SubmitProviderApplicationController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public List<Category> getCategories() {
                return oCompany.getCategoryRegistry().getCategories();
        }

        public List<Category> getCategoryList() {
                return lstCategories;
        }

        public List<ProfessionalQualification> getProQual() {
                return lstProQual;
        }

        public List<AcademicQualification> getAcaQual() {
                return lstAcaQual;
        }

        public boolean submitApplication(String strName, String strNif, String strTel, String strEmail, String strStreet, String strPostalCode, String strLocality) {
                Application oApplication = validateApplicationInfo(strName, strNif, strTel, strEmail, strStreet, strPostalCode, strLocality);
                if (oApplication != null) {
                        if (!lstCategories.isEmpty()) {
                                if (!lstProQual.isEmpty()) {
                                        if (!lstAcaQual.isEmpty()) {
                                                for (Category cat : lstCategories) {
                                                        oApplication.addCategory(cat);
                                                }
                                                for (ProfessionalQualification oProQual : lstProQual) {
                                                        oApplication.addProfessionalQualification(oProQual.getDescription());
                                                }
                                                for (AcademicQualification oAcaQual : lstAcaQual) {
                                                        oApplication.addProfessionalQualification(oAcaQual.getDesignation());
                                                }
                                                return this.oCompany.getApplicationRegistry().addApplication(oApplication);
                                        } else {
                                                Utils.openAlert(REGISTER_ERROR, "Please indicate, at least, one academic qualification ", "", Alert.AlertType.ERROR);
                                        }
                                } else {
                                        Utils.openAlert(REGISTER_ERROR, "Please indicate, at least, one professional qualification ", "", Alert.AlertType.ERROR);
                                }
                        } else {
                               Utils.openAlert(REGISTER_ERROR, "Please choose, at least, one category ", "", Alert.AlertType.ERROR);
                        }
                }
                return false;
        }

        public Application validateApplicationInfo(String strName, String strNif, String strTel, String strEmail, String strStreet, String strPostalCode, String strLocality) {
                if (!((strName == null) || (strNif == null) || (strTel == null) || (strEmail == null) || (strStreet == null) || (strPostalCode == null) || (strLocality == null)
                        || (strName.isEmpty()) || (strNif.isEmpty()) || (strTel.isEmpty()) || (strEmail.isEmpty()) || (strStreet.isEmpty()) || (strPostalCode.isEmpty()) || (strLocality.isEmpty()))) {
                        if (Utils.isValidEmailAddress(strEmail)) {
                                if (Utils.isValidNif(strNif)) {
                                        PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                                        if (oPostalCode != null) {
                                                Address oAddress = new Address(strStreet, oPostalCode, strLocality);
                                                return new Application(strName, strNif, strTel, strEmail, oAddress);
                                        } else {
                                                Utils.openAlert(REGISTER_ERROR, "Invalid PostalCode", "Please provide a valid Postal Code", Alert.AlertType.ERROR);
                                        }
                                } else {
                                        Utils.openAlert(REGISTER_ERROR, "Invalid NIF", "Please provide a valid NIF", Alert.AlertType.ERROR);
                                }
                        } else {
                                Utils.openAlert(REGISTER_ERROR, "Invalid Email", "Please provide a valid email", Alert.AlertType.ERROR);
                        }
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Please fill the necessary information ", "Application information not filled", Alert.AlertType.ERROR);
                }
                return null;
        }

        public boolean addCategory(Category oCategory) {
                if (validateCategory(oCategory)) {
                        return lstCategories.add(oCategory);
                }
                return false;
        }

        public boolean validateCategory(Category oCategory) {
                if (oCategory != null) {
                        for (Category cat : lstCategories) {
                                if (cat.equals(oCategory)) {
                                        Utils.openAlert(REGISTER_ERROR, "Category already added", "", Alert.AlertType.ERROR);
                                        return false;
                                }
                        }
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Choose a Category to add", "", Alert.AlertType.ERROR);
                        return false;
                }
                return true;
        }

        public boolean removeCategory(Category oCategory) {
                if (oCategory != null) {
                        return lstCategories.remove(oCategory);
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Choose a Category to remove", "", Alert.AlertType.ERROR);
                        return false;
                }
        }

        public boolean addProQualification(String strText) {
                if (strText != null && !strText.isEmpty()) {
                        ProfessionalQualification oQual = new ProfessionalQualification(strText);
                        if (validateProQualification(oQual)) {
                                return lstProQual.add(oQual);
                        }
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Fill in the Professional Qualification field", "", Alert.AlertType.ERROR);
                }
                return false;
        }

        public boolean validateProQualification(ProfessionalQualification oQual) {
                for (ProfessionalQualification cat : lstProQual) {
                        if (cat.equals(oQual)) {
                                Utils.openAlert(REGISTER_ERROR, "Professional Qualification already added", "", Alert.AlertType.ERROR);
                                return false;
                        }
                }
                return true;
        }
        
        public boolean addAcaQualification(String strText) {
                if (strText != null && !strText.isEmpty()) {
                        AcademicQualification oQual = new AcademicQualification(strText);
                        if (validateAcaQualification(oQual)) {
                                return lstAcaQual.add(oQual);
                        }
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Fill in the Professional Qualification field", "", Alert.AlertType.ERROR);
                }
                return false;
        }

        public boolean validateAcaQualification(AcademicQualification oQual) {
                for (AcademicQualification cat : lstAcaQual) {
                        if (cat.equals(oQual)) {
                                Utils.openAlert(REGISTER_ERROR, "Academic Qualification already added", "", Alert.AlertType.ERROR);
                                return false;
                        }
                }
                return true;
        }
        
        public boolean removeProQualification(ProfessionalQualification oQual) {
                if (oQual != null) {
                        return lstProQual.remove(oQual);
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Choose an Professional Qualification to remove", "", Alert.AlertType.ERROR);
                        return false;
                }
        }
        
        public boolean removeAcaQualification(AcademicQualification oQual) {
                if (oQual != null) {
                        return lstAcaQual.remove(oQual);
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Choose an Academic Qualification to remove", "", Alert.AlertType.ERROR);
                        return false;
                }
        }
}
