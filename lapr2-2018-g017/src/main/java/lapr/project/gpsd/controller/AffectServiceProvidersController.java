package lapr.project.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.Attribuition;
import lapr.project.gpsd.model.Availability;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.GeographicArea;
import lapr.project.gpsd.model.PromptedServiceDescription;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProviderEvaluation;
import lapr.project.gpsd.model.ServiceProvisionRequest;
import lapr.project.utils.Utils;

/**
 * Controller of UC10, affection of the Service Provider to an RequestedService.
 * 
 * @author Bodynho Oliveira
 */
public class AffectServiceProvidersController {
    
    /**
     * The Service Provider.
     * 
     */
    private ServiceProvider serviceProvider;
    
    /**
     * The Company.
     * 
     */
    private final Company company;
    
    /**
     * The Attribuition 
     */
    private Attribuition attribuition;

    /**
     * Controll the affectation.
     * 
     * @param company: the company to controll
     */
    public AffectServiceProvidersController(Company company) {
        this.company = company;
       
    }

    /**
     * Method to get the Service Provider with the Service Provision Request 
     * attribuited.
     * 
     * @param serviceProvisionRequest: the Service Provision Request attribuited
     * to the Service Provider
     * @param description: the description of the  Prompted Service
     * @return 
     */
//    public ServiceProvider getProvider(ServiceProvisionRequest serviceProvisionRequest, PromptedServiceDescription description) {
//        
//        
//        this.serviceProvider = getTheCloserProvider(serviceProvisionRequest, description);
//        
//        return this.serviceProvider;
//    }

    private ServiceProvider getProviderByRating(ServiceProvider provider, ServiceProviderEvaluation evaluation){
     
        String providerRated = evaluation.getEvaluation();
       
        return null; 
    }
    
    /**
     * Method to get the closest provider of the clients that made the request.
     * First criterion 
     * 
     * @param request: the request of the Client
     * @param description: the description of the Prompted Service
     * @return 
     */
//    private ServiceProvider getTheCloserProvider(ServiceProvisionRequest request, PromptedServiceDescription description) {
//
//        List<ServiceProvider> providerAvailable = hasAvailability(request, description);
//        
//        if (!providerAvailable.isEmpty()) {
//            ServiceProvider nearestProvider = providerAvailable.get(0);
//            
//            GeographicArea initialArea = nearestProvider.getGeographicAreas().get(0);
//            
//            double minDistance = Utils.getDistance(initialArea.getPostalCode().getLatitude(), initialArea.getPostalCode().getLongitude(), request.getAddress().getPostalCode().getLatitude(), request.getAddress().getPostalCode().getLongitude());
//           
//            for (ServiceProvider provider : providerAvailable) {
//                for (GeographicArea areaProvider : provider.getGeographicAreas()) {
//                    double distance = Utils.getDistance(areaProvider.getPostalCode().getLatitude(), areaProvider.getPostalCode().getLongitude(), request.getAddress().getPostalCode().getLatitude(), request.getAddress().getPostalCode().getLongitude());
//                    if (distance < minDistance) {
//                        minDistance = distance;
//                        nearestProvider = provider;
//                    }
//                }
//            }
//            
//            return nearestProvider;
//        }
//        return null;
//    }
    
    /**
     * Method to return the list of  Service Providers availables according to
     * the category and availabilities
     * 
     * @param request: the Service Provision Request in cause
     * @param description: the description of the PromptedService in cause
     * @return 
     */
//    private List<ServiceProvider> hasAvailability(ServiceProvisionRequest request, PromptedServiceDescription description) {
//       
//        List<ServiceProvider> providerWithCategory = hasCategory(description);
//        
//        List<ServiceProvider> providerWithAvailability = new ArrayList<ServiceProvider>();
//        
//        for (ServiceProvider provider: providerWithCategory) {
//            for (SchedulePreference preference : request.getSchedules()) {
//                for (Availability availability : provider.getAvailabilities()) {
//                    if (availability != null) {
//                        attribuition = new  Attribuition(provider, request);
//                        if (attribuition.hasAvailability(availability, preference, provider));
//                        providerWithAvailability.add(provider);
//                    }
//                }
//            }
//        }
//
//        return providerWithAvailability;
//    }
    
    /**
     * Method to return the provider with category by receiving the 
     * Prompted Service.
     * 
     * @param description
     * @return 
     */
    private List<ServiceProvider> hasCategory(PromptedServiceDescription description){
        
        List<ServiceProvider> providersWithCategory = new ArrayList<ServiceProvider>();
        
        for(ServiceProvider provider: company.getServiceProviderRegistry().getProviders()){
            Service serviceDescription = description.getService();
            for (Category categoryProvider: provider.getCategories()){

                if(serviceDescription.getCategory().equals(categoryProvider.getId())){
                    providersWithCategory.add(provider);
                }
            }
        }
        return providersWithCategory;
    }
    
    /**
     * Method to set the an attribuition receiving by parameter the Service 
     * Provider and the Service Provision Request.
     * 
     * @param provider: the service provider to set
     * @param request: the service provision request to set
     * @return the attribuiton with the new sets
     */
//    public Attribuition setAttribuition(ServiceProvider provider, ServiceProvisionRequest request){
//        attribuition.setRequestedService(request);
//        attribuition.setServiceProvider(provider);
//        
//        return attribuition;
//    }
    
    /**
     * Method to check if the attribuition is registered and add it.
     * 
     * @param attribuition: the attribuition to register
     * @return true: if the attribuition is resgistered
     * or false: if the attribuition isn't resgistered
     */
//    public boolean registerAttribuition(Attribuition attribuition) {
//        if (!company.getAttribuitionRegistry().getAttribuition().contains(attribuition)) {
//            company.getAttribuitionRegistry().getAttribuition().add(attribuition);
//        }
//        return false;
//    }
    
    
}
