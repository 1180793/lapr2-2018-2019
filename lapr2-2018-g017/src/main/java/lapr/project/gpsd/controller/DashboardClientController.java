package lapr.project.gpsd.controller;

import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceProvisionRequestRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DashboardClientController {

    private final AppGPSD oApp;
    private final Company oCompany;
    private final ClientRegistry oClients;
    private final UserSession oSession;
    private final String strEmail;
    private final ServiceProvisionRequestRegistry oRequests;

        public DashboardClientController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oClients =  oCompany.getClientRegistry();
                this.oSession = oApp.getCurrentSession();
                this.strEmail = oSession.getUserEmail();
                this.oRequests = oCompany.getRequestRegistry();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }
        
        public boolean clientHasRequests() {
                return this.oRequests.getRequestsByClient(oClients.getClientByEmail(strEmail)).isEmpty();
        }
}

