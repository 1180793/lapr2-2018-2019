package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ClientsController {

        private final AppGPSD oApp;
        private final Company oCompany;
        private final UserSession oSession;
        private final ClientRegistry oClients;

        public ClientsController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oSession = oApp.getCurrentSession();
                this.oClients = oCompany.getClientRegistry();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }
        
        public String getRole() {
                return this.oApp.getRole();
        }
        
        public String getEmail() {
                return this.oSession.getUserEmail();
        }
        
        public List<Client> getClients() {
                return this.oClients.getClients();
        }
}
