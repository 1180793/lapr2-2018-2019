package lapr.project.gpsd.controller;

import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ExternalService;
import lapr.project.gpsd.model.GeographicArea;
import lapr.project.gpsd.model.GeographicAreaRegistry;
import lapr.project.gpsd.model.PostalCode;

/**
 *
 * @author ZDPessoa
 */
public class GeographicalAreaSpecificationController {
    
    private AppGPSD app;
    private Company company;
    
    private GeographicAreaRegistry GAreaReg;
    private GeographicArea GeoArea;
    private ExternalService EService;
    
    public AppGPSD getApp(){
        
        return app;
    }
    
    public void startGeoArea() throws InvalidAvailabilityException{
        
        app = AppGPSD.getInstance();
        company = app.getCompany();
    }
    
    public void newGeographicalArea(String desig, PostalCode posCode, double cost, double radius){
        
        EService = company.getExternalService();
        
        GAreaReg = company.getGeographicAreaRegistry();
        
        GeoArea = GAreaReg.newGeographicArea(desig, posCode, cost, radius, EService);
        
    }
    
    public boolean validate(){
        
        return GAreaReg.validateGeographicArea(GeoArea);
    }
    
    public boolean addGeographicalArea(){
        
        return GAreaReg.addGeographicArea(GeoArea);
    }
    
}
