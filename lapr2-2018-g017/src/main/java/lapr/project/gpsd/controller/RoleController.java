package lapr.project.gpsd.controller;

import java.util.List;

import lapr.project.auth.model.Role;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class RoleController {

        private final AppGPSD oApp;
        private final UserSession oSession;
        private final String strEmail;

        public RoleController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oSession = oApp.getCurrentSession();
                this.strEmail = oSession.getUserEmail();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public List<Role> getUserRoles() {
                return oSession.getUserRoles();
        }

        public String getEmail() {
                return this.strEmail;
        }

        public void setRole(String strRole) {
                this.oApp.setRole(strRole);
        }
}
