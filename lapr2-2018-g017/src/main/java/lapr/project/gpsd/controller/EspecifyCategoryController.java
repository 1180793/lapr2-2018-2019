/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.gpsd.exception.DuplicatedIDException;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.CategoryRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Constants;
import lapr.project.utils.Utils;

/**
 * Represents the controller of the Use Case - Especify Category
 * 
 * @author Bodynho Oliveira
 */
public class EspecifyCategoryController {
   
    /**
     * The Company.
     * 
     */
    public Company oCompany;
    
    /**
     * The Category.
     * 
     */
    private Category oCategory;
    
    /**
     * The Category Registry.
     * 
     */
    private CategoryRegistry oCategoryRegistry;
   
    /**
     * Build the controller getting the session and the user role and defining
     * the company using the application.
     * 
         * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
     */
    public EspecifyCategoryController() throws InvalidAvailabilityException
    {
        if(!AppGPSD.getInstance().getCurrentSession().isLoggedInWithRole(Constants.ROLE_SP))
            throw new IllegalStateException("Utilizador não Autorizado");
        this.oCompany = AppGPSD.getInstance().getCompany();
    }
    
    /**
     * Method to check the new category created receiving by parameter the id 
     * and description of the category.
     * 
     * @param strId: id of the category
     * @param strDescription: description of the category
     * @return the new category created receiving by parameter the id 
     * and description of the category.
     * @throws RuntimeException: returning false if there is no category in null
     */
    public boolean newCategory(String strId, String strDescription)
    {
        try
        {
            this.oCategory = this.oCategoryRegistry.newCategory(strId, strDescription);
            return this.oCategoryRegistry.validateCategory(this.oCategory);
        }
        catch(RuntimeException ex)
        {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            this.oCategory = null;
            return false;
        }
    }
     
    /**
     * Method to check the category to register.
     * 
     * @return the category registered to category registry
     * @throws DuplicatedIDException: if the category id already exist.
     */
    public boolean registerCategory() throws DuplicatedIDException {
        if (this.oCategoryRegistry.registerCategory(this.oCategory)) {
			return true;
		}
			throw new DuplicatedIDException("Duplicated Category ID");
    }

    /**
     * Method to get the category as string
     * 
     * @return the category as string
     */
    public String getCategoryString(){
        return this.oCategory.toString();
    }
}
