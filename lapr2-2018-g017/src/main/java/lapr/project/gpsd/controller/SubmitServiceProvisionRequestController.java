package lapr.project.gpsd.controller;

import java.text.ParseException;
import java.time.format.DateTimeFormatter;
import java.util.List;
import javafx.scene.control.Alert;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.CategoryRegistry;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Constants;
import lapr.project.gpsd.model.PromptedServiceDescription;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceProvisionRequest;
import lapr.project.gpsd.model.ServiceProvisionRequestRegistry;
import lapr.project.gpsd.model.ServiceRegistry;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class SubmitServiceProvisionRequestController {

        private final AppGPSD oApp;
        private final Company oCompany;
        private final UserSession oSession;
        private final ClientRegistry oClients;
        private final CategoryRegistry oCategories;
        private final ServiceRegistry oServices;
        private final ServiceProvisionRequestRegistry oRequests;
        private final String strEmail;
        private final String strRole;
        private final Client oClient;

        private ServiceProvisionRequest oRequest;

        private final String REQUEST_TITLE = "Service Provision Request";
        private final String SERVICE_ERROR = "Error adding Service to the Request";
        private final String SCHEDULE_ERROR = "Error adding Schedule to the Request";

        public SubmitServiceProvisionRequestController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oSession = oApp.getCurrentSession();
                this.oClients = oCompany.getClientRegistry();
                this.oCategories = oCompany.getCategoryRegistry();
                this.oServices = oCompany.getServiceRegistry();
                this.oRequests = oCompany.getRequestRegistry();
                this.strEmail = oApp.getCurrentSession().getUserEmail();
                this.strRole = oApp.getRole();
                this.oClient = oClients.getClientByEmail(strEmail);
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public String getRole() {
                return this.strRole;
        }

        public String getUserEmail() {
                return this.strEmail;
        }

        public Client getClient() {
                return this.oClient;
        }

        public String getClientName() {
                return this.oClient.getName();
        }

        public String getClientNif() {
                return this.oClient.getNif();
        }

        public String getClientTelephone() {
                return this.oClient.getTelephone();
        }

        public List<Category> getCategories() {
                return this.oCategories.getCategories();
        }

        public ServiceProvisionRequest getRequest() {
                return this.oRequest;
        }

        public void setRequest(Address oAddress) {
                if (oAddress == null) {
                        this.oRequest = null;
                } else {
                        ServiceProvisionRequest newRequest = new ServiceProvisionRequest(oClient, oAddress);
                        this.oRequest = newRequest;
                }
        }

        public List<Service> getServices(Category oCategory) {
                return oServices.getServicesByCategory(oCategory);
        }

        public String[] getDurations() {
                return Utils.LIST_OF_DURATIONS;
        }

        public String[] getHours() {
                return Utils.LIST_OF_TIMES;
        }

        public List<Address> getAddresses() {
                return oClient.getAddresses();
        }

        public PromptedServiceDescription validateService(Service oService, String strDuration, String strDescription) {
                if (oRequest == null) {
                        Utils.openAlert(SERVICE_ERROR, SERVICE_ERROR,
                                "Select an Address", Alert.AlertType.ERROR);
                        return null;
                }
                if (oService == null) {
                        Utils.openAlert(SERVICE_ERROR, SERVICE_ERROR,
                                "Select a Service", Alert.AlertType.ERROR);
                        return null;
                }
                if (!oService.hasOtherAtributes() && (strDuration == null || strDuration.isEmpty())) {
                        Utils.openAlert(SERVICE_ERROR, SERVICE_ERROR,
                                "Select a Duration for the Service", Alert.AlertType.ERROR);
                        return null;
                }
                if (strDescription == null || strDescription.isEmpty()) {
                        Utils.openAlert(SERVICE_ERROR, SERVICE_ERROR,
                                "The Service's description has to be filled in", Alert.AlertType.ERROR);
                        return null;
                }
                if (oService.hasOtherAtributes()) {
                        strDuration = oService.getOtherAtributes();
                }
                PromptedServiceDescription oPromptedServiceDescription = new PromptedServiceDescription(oService, strDescription, strDuration);
                return oPromptedServiceDescription;
        }

        public boolean addServiceToRequest(PromptedServiceDescription oService) {
                if (this.oRequest != null) {
                        return oRequest.addServiceToRequest(oService);
                }
                return false;
        }

        public SchedulePreference validateSchedule(String strDate, String strHour) {
                if (oRequest == null) {
                        Utils.openAlert(SCHEDULE_ERROR, SCHEDULE_ERROR,
                                "Select an Address", Alert.AlertType.ERROR);
                        return null;
                }
                if (strDate == null || strDate.isEmpty()) {
                        Utils.openAlert(SCHEDULE_ERROR, SCHEDULE_ERROR,
                                "Select a Date", Alert.AlertType.ERROR);
                        return null;
                }
                if (strHour == null || strHour.isEmpty()) {
                        Utils.openAlert(SCHEDULE_ERROR, SCHEDULE_ERROR,
                                "Select an Hour", Alert.AlertType.ERROR);
                        return null;
                }
                return this.oRequest.createSchedule(strDate, strHour);
        }

        public boolean addScheduleToRequest(SchedulePreference oSchedule) {
                if (this.oRequest != null) {
                        return oRequest.addSchedule(oSchedule);
                }
                return false;
        }

        public double calculateCost() throws InvalidAvailabilityException {
                if (oRequest != null) {
                        return oRequest.calculateCost();
                }
                return 0;
        }

        public boolean validateRequest() {
                if (oRequests.validateRequest(oRequest)) {
                        return true;
                } else {
                        Utils.openAlert(REQUEST_TITLE, "Invalid Service Provision Request",
                                "Make sure you have filled in all the fields required to complete the Request.",
                                Alert.AlertType.ERROR);
                        return false;
                }
        }

        public boolean registerRequest() throws InvalidAvailabilityException {
                if (oRequests.registerRequest(oRequest)) {
                        return true;
                } else {
                        return false;
                }
        }
}
