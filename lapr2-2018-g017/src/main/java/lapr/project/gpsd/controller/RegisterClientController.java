package lapr.project.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import lapr.project.gpsd.exception.DuplicatedIDException;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.utils.Utils;

/**
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class RegisterClientController {

        private final AppGPSD oApp;
        private final String strRole;
        private final Company oCompany;
        private final ClientRegistry oClients;
        List<Address> lstAddresses = new ArrayList<>();
        Address oAddress;
        private final String REGISTER_ERROR = "Error";

        public RegisterClientController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.strRole = this.oApp.getRole();
                this.oCompany = this.oApp.getCompany();
                this.oClients = this.oCompany.getClientRegistry();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public String getRole() {
                return this.strRole;
        }

        public List<Address> getAddresses() {
                return this.lstAddresses;
        }

        private boolean validatePostalCode(String strPostalCode) {
                if (strPostalCode == null || strPostalCode.isEmpty()) {
                        return false;
                }
                String[] sPostalCode = strPostalCode.split("-");
                return sPostalCode.length == 2;
        }

        private boolean validateAddress(Address oAddress) {
                for (Address address : lstAddresses) {
                        if (address.equals(oAddress)) {
                                return false;
                        }
                }
                return true;
        }

        public Address getFetchedAddress() {
                return this.oAddress;
        }

        public boolean getAddress(String strPostalCode) {
                if (validatePostalCode(strPostalCode)) {
                        Address address = Utils.getAddressFromPostalCode(strPostalCode);
                        if (address != null) {
                                this.oAddress = address;
                                return true;
                        } else {
                                Utils.openAlert(REGISTER_ERROR, "Unable to Fetch Address", "Postal Code not found", Alert.AlertType.ERROR);
                                return false;
                        }
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Unable to Fetch Address", "Invalid Postal Code", Alert.AlertType.ERROR);
                        return false;
                }
        }

        public boolean addAddress(String strStreet, String strPostalCode, String strLocality) {
                if (!((strStreet == null) || (strPostalCode == null) || (strLocality == null)
                        || (strStreet.isEmpty()) || (strPostalCode.isEmpty()) || (strLocality.isEmpty()))) {
                        PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                        if (oPostalCode == null) {
                                Utils.openAlert(REGISTER_ERROR, "Unable to add Address", "Invalid Postal Code", Alert.AlertType.ERROR);
                                return false;
                        }
                        Address address = new Address(strStreet, oPostalCode, strLocality);
                        if (validateAddress(address)) {
                                return lstAddresses.add(oAddress);
                        } else {
                                Utils.openAlert(REGISTER_ERROR, "Unable to add Address", "Duplicated Address", Alert.AlertType.ERROR);
                                return false;
                        }
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Unable to add Address", "Please fill all the Address fields", Alert.AlertType.ERROR);
                        return false;
                }
        }

        public boolean removeAddress(Address oAddress) {
                if (oAddress != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Remove Address");
                        alert.setHeaderText("Do you want to remove the selected Address?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && (result.get() == ButtonType.OK)) {
                                lstAddresses.remove(oAddress);
                                Utils.openAlert("Address Removed", "The selected Address was removed successfully.", "", Alert.AlertType.INFORMATION);
                                return true;
                        }
                } else {
                        Utils.openAlert("Error trying to remove Address", "Error trying to remove Address",
                                "Select an Address to remove", Alert.AlertType.ERROR);
                }
                return false;
        }

        public void cancel() {
                this.lstAddresses.clear();
                this.oAddress = null;
        }

        public Client submitRegistration(String strName, String strNif, String strTel, String strEmail, String strPassword, String strPasswordConfirmation) {
                if (validateClientInfo(strName, strNif, strTel, strEmail, strPassword, strPasswordConfirmation)) {
                        Client oClient = oClients.newClient(strName, strNif, strTel, strEmail, lstAddresses.get(0));
                        if (lstAddresses.size() > 1) {
                                for (int i = 1; i < lstAddresses.size(); i++) {
                                        oClient.registerAddress(lstAddresses.get(i));
                                }
                        }
                        System.out.println(oClient.getAddressString());
                        return oClient;
                }
                return null;
        }

        public boolean validateClientInfo(String strName, String strNif, String strTel, String strEmail, String strPassword, String strPasswordConfirmation) {
                if (!((strName == null) || (strNif == null) || (strTel == null) || (strEmail == null) || (strPassword == null) || (strPasswordConfirmation == null)
                        || (strName.isEmpty()) || (strNif.isEmpty()) || (strTel.isEmpty()) || (strEmail.isEmpty())
                        || (strPasswordConfirmation.isEmpty()) || (strPassword.isEmpty()))) {
                        if (Utils.isValidEmailAddress(strEmail)) {
                                if (Utils.isValidNif(strNif)) {
                                        if (strPassword.equals(strPasswordConfirmation)) {
                                                if (!this.lstAddresses.isEmpty()) {
                                                        return true;
                                                } else {
                                                        Utils.openAlert(REGISTER_ERROR, "No addresses added", "Please add an address", Alert.AlertType.ERROR);
                                                }
                                        } else {
                                                Utils.openAlert(REGISTER_ERROR, "Passwords do not match", "Please re-enter the password", Alert.AlertType.ERROR);
                                        }
                                } else {
                                        Utils.openAlert(REGISTER_ERROR, "Invalid NIF", "Please provide a valid NIF", Alert.AlertType.ERROR);
                                }
                        } else {
                                Utils.openAlert(REGISTER_ERROR, "Invalid Email", "Please provide a valid email", Alert.AlertType.ERROR);
                        }
                } else {
                        Utils.openAlert(REGISTER_ERROR, "Please fill the necessary information ", "Client information not filled", Alert.AlertType.ERROR);
                }
                return false;
        }

        public boolean confirmRegistration(Client oClient, String strPassword) {
                try {
                        if (this.oClients.registerClient(oClient, strPassword)) {
                                Utils.openAlert("Client Registered", "The client was successfuly registered", "", Alert.AlertType.INFORMATION);
                                return true;
                        } else {
                                Utils.openAlert(REGISTER_ERROR, "NIF or Email are already registered", "Duplicated NIF/Email", Alert.AlertType.ERROR);
                                return false;
                        }
                } catch (DuplicatedIDException ex) {
                        Utils.openAlert(REGISTER_ERROR, "NIF or Email are already registered", "Duplicated NIF/Email", Alert.AlertType.ERROR);
                        return false;
                }
        }
}
