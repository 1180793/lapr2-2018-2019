package lapr.project.gpsd.controller;

import java.util.List;
import javafx.scene.control.Alert;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Availability;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProviderRegistry;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DailyAvailabilityController {

        private final AppGPSD oApp;
        private final Company oCompany;
        private final UserSession oSession;
        private final String strEmail;
        private final ServiceProviderRegistry oProviders;
        private final ServiceProvider oProvider;
        private final String AVAILABILITY_ERROR = "Error trying to add Availability";

        ;

        public DailyAvailabilityController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oSession = oApp.getCurrentSession();
                this.strEmail = oSession.getUserEmail();
                this.oProviders = oCompany.getServiceProviderRegistry();
                this.oProvider = oProviders.getServiceProviderByEmail(strEmail);
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public String getRole() {
                return this.oApp.getRole();
        }

        public String getEmail() {
                return this.strEmail;
        }

        public List<Availability> getAvailabilities() {
                return oProvider.getAvailabilities();
        }

        public String[] getHours() {
                return Utils.LIST_OF_TIMES;
        }

        public Availability newAvailability(String strStartDate, String strStartHour, String strEndDate, String strEndHour) {
                if (!((strStartDate == null) || (strStartHour == null) || (strEndDate == null) || (strEndHour == null)
                        || (strStartDate.isEmpty()) || (strStartHour.isEmpty()) || (strEndDate.isEmpty()) || (strEndHour.isEmpty()))) {
                        try {
                                return new Availability(strStartDate, strStartHour, strEndDate, strEndHour);
                        } catch (InvalidAvailabilityException ex) {
                                Utils.openAlert(AVAILABILITY_ERROR, AVAILABILITY_ERROR,
                                        "Check Start and End Hours", Alert.AlertType.ERROR);
                                return null;
                        }
                } else {
                        Utils.openAlert(AVAILABILITY_ERROR, AVAILABILITY_ERROR,
                                "Select an End Date", Alert.AlertType.ERROR);
                        return null;
                }
        }

        public boolean registerAvailability(Availability oAvailability) {
                if (oProvider.validateAvailability(oAvailability)) {
                        return this.oProvider.addAvailability(oAvailability);
                } else {
                        Utils.openAlert(AVAILABILITY_ERROR, AVAILABILITY_ERROR,
                                "Invalid Availability. There is Availability Overlap", Alert.AlertType.ERROR);
                        return false;
                }
        }
        
        public boolean removeAvailability(Availability oAvailability) {
                return oProvider.removeAvailability(oAvailability);
        }
}
