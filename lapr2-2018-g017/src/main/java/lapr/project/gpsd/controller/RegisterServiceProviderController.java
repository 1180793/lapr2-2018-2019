package lapr.project.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import lapr.project.gpsd.exception.DuplicatedIDException;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.ApplicationRegistry;
import lapr.project.gpsd.model.ApplicationStatus;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.CategoryRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.GeographicArea;
import lapr.project.gpsd.model.GeographicAreaRegistry;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProviderRegistry;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class RegisterServiceProviderController {

        private final AppGPSD oApp;
        private final String strUserEmail;
        private final String strRole;
        private final Company oCompany;
        private final CategoryRegistry oCategories;
        private final GeographicAreaRegistry oGeographicAreas;
        private final ApplicationRegistry oApplications;
        private final ServiceProviderRegistry oProviders;
        private Application oApplication;
        private final List<Category> lstCategories;
        private final List<GeographicArea> lstGeographicAreas;
        private final String ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER = "Error trying to register Service Provider";

        public RegisterServiceProviderController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.strUserEmail = this.oApp.getCurrentSession().getUserEmail();
                this.strRole = this.oApp.getRole();
                this.oCompany = this.oApp.getCompany();
                this.oCategories = this.oCompany.getCategoryRegistry();
                this.oGeographicAreas = this.oCompany.getGeographicAreaRegistry();
                this.oApplications = this.oCompany.getApplicationRegistry();
                this.oProviders = this.oCompany.getServiceProviderRegistry();
                this.oApplication = null;
                this.lstCategories = new ArrayList<>();
                this.lstGeographicAreas = new ArrayList<>();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public String getRole() {
                return this.strRole;
        }

        public String getUserEmail() {
                return this.strUserEmail;
        }

        public List<Category> getCategories() {
                return this.oCategories.getCategories();
        }

        public List<GeographicArea> getGeographicAreas() {
                return this.oGeographicAreas.getGeographicAreas();
        }

        public boolean getApplication(String strNif) {
                if (strNif != null && !strNif.isEmpty()) {
                        Application application = this.oApplications.getApplicationByNif(strNif);
                        if (application != null) {
                                if (application.getStatus() != ApplicationStatus.REJECTED) {
                                        this.oApplication = application;
                                        Utils.openAlert("Application Found and Associated with the Registration", "The provided Nif matches an Application", "Application Found and Associated with the Registration", Alert.AlertType.INFORMATION);
                                        return true;
                                } else {
                                        Utils.openAlert("Error Fetching Application", "The provided Nif matches a Rejected Application", "", Alert.AlertType.ERROR);
                                        return false;
                                }
                        } else {
                                Utils.openAlert("Error Fetching Application", "There aren't any Accepted or Pending Application found for the provided Nif", "", Alert.AlertType.ERROR);
                                return false;
                        }
                } else {
                        Utils.openAlert("Error Fetching Application", "Please enter a Nif", "", Alert.AlertType.ERROR);
                        return false;
                }
        }

        public Application getApplication() {
                return this.oApplication;
        }

        public void addCategoryFromApplication() {
                if (this.oApplication != null) {
                        for (Category oCat : this.oApplication.getCategories()) {
                                this.lstCategories.add(oCat);
                        }
                }
        }

        public List<Category> getProviderCategories() {
                return this.lstCategories;
        }

        public List<GeographicArea> getProviderGeographicAreas() {
                return this.lstGeographicAreas;
        }

        public boolean addCategory(Category oCategory) {
                if (validateCategory(oCategory)) {
                        lstCategories.add(oCategory);
                        return true;
                } else {
                        return false;
                }
        }

        public boolean validateCategory(Category oCategory) {
                if (oCategory == null) {
                        Utils.openAlert("Error Adding Category", "Error Adding Category", "Choose a Category to add", Alert.AlertType.ERROR);
                        return false;
                } else {
                        for (Category category : lstCategories) {
                                if (oCategory.equals(category)) {
                                        Utils.openAlert("Error Adding Category", "Error Adding Category", "Category already added", Alert.AlertType.ERROR);
                                        return false;
                                }
                        }
                }
                return true;
        }

        public boolean removeCategory(Category oCategory) {
                if (oCategory != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Remove Category");
                        alert.setHeaderText("Do you want to remove the selected Category?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && (result.get() == ButtonType.OK)) {
                                lstCategories.remove(oCategory);
                                Utils.openAlert("Category Removed", "Category Removed", "The selected Category was removed successfully.", Alert.AlertType.INFORMATION);
                                return true;
                        }
                } else {
                        Utils.openAlert("Error trying to remove Category", "Error trying to remove Category",
                                "Select a Category to remove", Alert.AlertType.ERROR);
                }
                return false;
        }

        public boolean addGeographicArea(GeographicArea oGeographicArea) {
                if (validateGeographicArea(oGeographicArea)) {
                        lstGeographicAreas.add(oGeographicArea);
                        return true;
                } else {
                        return false;
                }
        }

        public boolean validateGeographicArea(GeographicArea oGeographicArea) {
                if (oGeographicArea == null) {
                        Utils.openAlert("Error Adding Geographic Area", "Error Adding Geographic Area", "Choose a Geographic Area to add", Alert.AlertType.ERROR);
                        return false;
                } else {
                        for (GeographicArea geoArea : lstGeographicAreas) {
                                if (oGeographicArea.equals(geoArea)) {
                                        Utils.openAlert("Error Adding Geographic Area", "Error Adding Geographic Area", "Geographic Area already added", Alert.AlertType.ERROR);
                                        return false;
                                }
                        }
                }
                return true;
        }

        public boolean removeGeographicArea(GeographicArea oGeographicArea) {
                if (oGeographicArea != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Remove Geographic Area");
                        alert.setHeaderText("Do you want to remove the selected Geographic Area?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && (result.get() == ButtonType.OK)) {
                                lstGeographicAreas.remove(oGeographicArea);
                                Utils.openAlert("Geographic Area Removed", "Geographic Area Removed", "The selected Geographic Area was removed successfully.", Alert.AlertType.INFORMATION);
                                return true;
                        }
                } else {
                        Utils.openAlert("Error trying to remove Geographic Area", "Error trying to remove Geographic Area",
                                "Select a Geographic Area to remove", Alert.AlertType.ERROR);
                }
                return false;
        }

        public void cancel() {
                this.lstCategories.clear();
                this.lstGeographicAreas.clear();
                this.oApplication = null;
        }

        public Address getAddress(String strStreet, String strLocality, String strPostalCode) {
                if (strPostalCode == null || strPostalCode.isEmpty()) {
                        return null;
                } else if (strStreet == null || strLocality == null || strStreet.isEmpty() || strLocality.isEmpty()) {
                        return Utils.getAddressFromPostalCode(strPostalCode);
                } else {
                        PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                        return new Address(strStreet, oPostalCode, strLocality);
                }
        }

        public ServiceProvider submitRegistration(String strPersonnelNumber, String strNif, String strFullName, String strAbbreviatedName, String strEmail, String strTel, Address oAddress) {
                if (validateProviderInfo(strPersonnelNumber, strNif, strFullName, strAbbreviatedName, strEmail, strTel, oAddress)) {
                        ServiceProvider oProvider = oProviders.newProvider(strPersonnelNumber, strNif, strFullName, strAbbreviatedName, strEmail, strTel, lstCategories, lstGeographicAreas);
                        oProvider.setAddress(oAddress);
                        return oProvider;
                }
                return null;
        }

        public boolean validateProviderInfo(String strPersonnelNumber, String strNif, String strFullName, String strAbbreviatedName, String strEmail, String strTel, Address oAddress) {
                if (!((strPersonnelNumber == null) || (strNif == null) || (strFullName == null) || (strAbbreviatedName == null) || (strEmail == null)
                        || (strTel == null) || (strPersonnelNumber.isEmpty()) || (strNif.isEmpty()) || (strFullName.isEmpty())
                        || (strAbbreviatedName.isEmpty()) || (strEmail.isEmpty()) || (strTel.isEmpty()))) {
                        if (Utils.isValidEmailAddress(strEmail)) {
                                if (Utils.isValidNif(strNif)) {
                                        if (oAddress != null) {
                                                if (!this.lstCategories.isEmpty()) {
                                                        if (!this.lstGeographicAreas.isEmpty()) {
                                                                return true;
                                                        } else {
                                                                Utils.openAlert(ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, "You must add, at least, one Geographic Area", Alert.AlertType.ERROR);
                                                        }
                                                } else {
                                                        Utils.openAlert(ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, "You must add, at least, one Geographic Area", Alert.AlertType.ERROR);
                                                }
                                        } else {
                                                Utils.openAlert(ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, "Invalid Address", Alert.AlertType.ERROR);
                                        }
                                } else {
                                        Utils.openAlert(ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, "Invalid NIF", "Please provide a valid NIF", Alert.AlertType.ERROR);
                                }
                        } else {
                                Utils.openAlert(ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, "Invalid Email", "Please provide a valid email", Alert.AlertType.ERROR);
                        }
                } else {
                        Utils.openAlert(ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, "Fill all the Service Provider's Information fields", Alert.AlertType.ERROR);
                }
                return false;
        }

        public boolean confirmRegistration(ServiceProvider oProvider) {
                try {
                        String strPassword = this.oProviders.generatePassword();
                        if (this.oProviders.registerProvider(oProvider, strPassword)) {
                                Utils.openAlert("Service Provider Registered Successfully", "Service Provider Registered Successfully", "Service Provider Registered Successfully\nGenerated Password: " + strPassword, Alert.AlertType.INFORMATION);
                                return true;
                        } else {
                                Utils.openAlert(ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, "A User with that Email and/or Nif is already registered", Alert.AlertType.ERROR);
                                return false;
                        }
                } catch (DuplicatedIDException ex) {
                        Utils.openAlert(ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, ERROR_TRYING_TO_REGISTER__SERVICE__PROVIDER, "A User with that Email and/or Nif is already registered", Alert.AlertType.ERROR);
                        return false;
                }
        }

}
