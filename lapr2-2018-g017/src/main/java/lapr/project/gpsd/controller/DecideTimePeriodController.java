/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import javafx.scene.control.Alert;
import lapr.project.auth.model.Role;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Attribuition;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.utils.Utils;

/**
 *
 * @author Lenovo
 */
public class DecideTimePeriodController {

    private final AppGPSD oApp;
    private final Company oCompany;
    private final UserSession oSession;
    private final ClientRegistry oClients;
    private final Client oClient;

    public DecideTimePeriodController() throws InvalidAvailabilityException {
        this.oApp = AppGPSD.getInstance();
        this.oCompany = oApp.getCompany();
        this.oClients = oCompany.getClientRegistry();
        this.oSession = oApp.getCurrentSession();
        this.oClient = oClients.getClientByEmail(this.oSession.getUserEmail());
    }

    public AppGPSD getApp() {
        return this.oApp;
    }

    public String getRole() {
        return this.oApp.getRole();
    }

    public String getEmail() {
        return this.oSession.getUserEmail();
    }

    public List<Attribuition> getAttribuitionList() {
        return oCompany.getAttribuitionRegistry().getPendingAttribuitionByClient(oClient);

    }

    public ServiceOrder acceptAttribuition(Attribuition att) {
        if (att != null) {
            att.setDecision(true);

            ServiceOrder order = oCompany.getServiceOrderRegistry().newServiceOrder(att);
            if (oCompany.getServiceOrderRegistry().registerServiceOrder(order)) {
                return order;
            }

        } else {
            Utils.openAlert("Error", "Please select a Service to decide on", "", Alert.AlertType.ERROR);
        }
        return null;
    }

    public boolean rejectAttribuition(Attribuition att) {
        if (att != null) {
            att.setDecision(false);
            return true;
        } else {
            Utils.openAlert("Error", "Please select a Service to decide on", "", Alert.AlertType.ERROR);
        }
        return false;
    }
}
