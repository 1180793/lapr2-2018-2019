package lapr.project.gpsd.controller;

import javafx.stage.Window;
import lapr.project.auth.AuthFacade;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class TopBarController {

	private final AppGPSD oApp;
        private final Company oCompany;
        private final AuthFacade oAuth;

        public TopBarController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oAuth = oCompany.getAuthFacade();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }
        
        public void doLogout() {
                this.oApp.doLogout();
        }
        
        public Window getStage() {
                return this.oApp.getStage();
        }

        public boolean hasMoreThanOneRole () {
                return this.oApp.getCurrentSession().getUserRoles().size() > 1;
        }

}
