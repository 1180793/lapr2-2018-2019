package lapr.project.gpsd.controller;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javafx.stage.Stage;
import lapr.project.auth.AuthFacade;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Constants;
import lapr.project.utils.XLSUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AppGPSD {

	private final Company oCompany;
	private final AuthFacade oAuth;
        private Stage oStage;
	public String strRole;

        /**
         * Private Constructor
         */
	private AppGPSD() throws InvalidAvailabilityException {
		Properties props = getProperties();
                if (props != null) {
                        this.oCompany = new Company(props.getProperty(Constants.PARAMS_COMPANY_DESIGNATION),
                        props.getProperty(Constants.PARAMS_COMPANY_NIF));
                        this.oAuth = this.oCompany.getAuthFacade();
                        this.oStage = null;
                } else {
                        this.oCompany = new Company("Default Lda.", "Default NIF");
                        this.oAuth = this.oCompany.getAuthFacade();
                        this.oStage = null;
                }
                bootstrap();
        }

	public Company getCompany() {
		return this.oCompany;
	}

	public UserSession getCurrentSession() {
		return this.oAuth.getCurrentSession();
	}
	
	public String getRole() {
		return this.strRole;
	}
	
	public void setRole(String strRole) {
		this.strRole = strRole;
	}

        /**
         * Logs the User into the App
         * @param strId User Email
         * @param strPwd User Password
         * @return true if oAuth authenticates the user
         */
	public boolean doLogin(String strId, String strPwd) {
		return this.oAuth.doLogin(strId, strPwd) != null;
	}

	public void doLogout() {
		this.oAuth.doLogout();
                this.strRole = "";
	}

	private Properties getProperties() {
		Properties props = new Properties();

		// Add properties and values by omission
		props.setProperty(Constants.PARAMS_COMPANY_DESIGNATION, "Default Lda.");
		props.setProperty(Constants.PARAMS_COMPANY_NIF, "Default NIF");

		// Reads properties and established values
		try {
			InputStream in = new FileInputStream(Constants.PARAMS_FILE_CONFIG);
			props.load(in);
			in.close();
		} catch (IOException ex) {
			return null;
		}
		return props;
	}

        /**
         * Bootstrap to register Admins and Human Resources Officers
         */
	private void bootstrap() throws InvalidAvailabilityException {
		this.oAuth.registerRole(Constants.ROLE_ADMIN);
		this.oAuth.registerRole(Constants.ROLE_CLIENT);
		this.oAuth.registerRole(Constants.ROLE_HRO);
		this.oAuth.registerRole(Constants.ROLE_SP);

		this.oAuth.registerUserWithRole("Admin 1", "adm1@esoft.pt", "entrar",
				Constants.ROLE_ADMIN);
		this.oAuth.registerUserWithRole("Admin 2", "adm2@esoft.pt", "654321",
				Constants.ROLE_ADMIN);

		this.oAuth.registerUserWithRole("Human Resources 1", "hr1@esoft.pt", "entrar", Constants.ROLE_HRO);
		this.oAuth.registerUserWithRole("Human Resources 2", "hr2@esoft.pt", "password", Constants.ROLE_HRO);

		this.oAuth.registerUserWithRoles("Martim", "martim@esoft.pt", "12345",
				new String[] { Constants.ROLE_HRO, Constants.ROLE_ADMIN });
                
                XLSUtils.loadClients(this);
                XLSUtils.loadCategories(this);
                XLSUtils.loadServices(this);
                XLSUtils.loadGeographicAreas(this);
                XLSUtils.loadServiceProviders(this);
                XLSUtils.loadRequests(this);
                XLSUtils.loadAvailabilities(this);
                XLSUtils.loadApplications(this);
                XLSUtils.loadServiceOrders(this);
                XLSUtils.loadRatings(this);
                XLSUtils.loadWorkReports(this);
	}

	// Inspired in
	// https://www.javaworld.com/article/2073352/core-java/core-java-simply-singleton.html?page=2
	private static AppGPSD singleton = null;

	public static AppGPSD getInstance() throws InvalidAvailabilityException {
		if (singleton == null) {
			synchronized (AppGPSD.class) {
				singleton = new AppGPSD();
			}
		}
		return singleton;
	}

        public Stage getStage() {
                return this.oStage;
        }
        
        public void setWindow(Stage stage) {
                this.oStage = stage;
        }

}
