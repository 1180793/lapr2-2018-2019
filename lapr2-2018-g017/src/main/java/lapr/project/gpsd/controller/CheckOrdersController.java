/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.io.IOException;
import java.util.List;
import java.util.Properties;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Export;
import lapr.project.gpsd.model.ExportList;
import lapr.project.gpsd.model.ExportType;
import lapr.project.gpsd.model.ExportTypeRegistry;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceOrderRegistry;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProviderRegistry;

/**
 *
 * @author ZDPessoa
 */
public class CheckOrdersController {

    private AppGPSD app;
    private UserSession session;
    private Company company;
    private String strEmail;
    private ServiceProviderRegistry SProviderReg;
    private ServiceProvider ServiceProvider;
    private ServiceOrderRegistry SOrderReg;
    private List<ServiceOrder> ServiceOrderList;
    private ExportTypeRegistry ETypeReg;
    private ExportList ExportList;
    private Export Export;
    private String dateI;
    private String dateF;
    
    public AppGPSD getApp(){
        
        return app;
    }

    public List<ExportType> checkOrders() throws IOException, InvalidAvailabilityException {
        
        List<ExportType> ExportListAux;
        Properties props;

        app = AppGPSD.getInstance();
        company = app.getCompany();
        session = app.getCurrentSession();
        strEmail = session.getUserEmail();
        SProviderReg = company.getServiceProviderRegistry();
        ServiceProvider = SProviderReg.getServiceProviderByEmail(strEmail);
        
        ETypeReg = company.getExportTypeRegistry();
        props = ETypeReg.getConfigVals();
        ExportListAux = ETypeReg.getExportTypes(props);
        
        return ExportListAux;
    }
    
    public List<ServiceOrder> newCheck(String dateI, String dateF) throws IOException{
        
        this.dateF = dateF;
        this.dateI = dateI;
        
        SOrderReg = company.getServiceOrderRegistry();
        
        ServiceOrderList = SOrderReg.getOrderList(dateI, dateF, ServiceProvider);
        
        return ServiceOrderList;
    
    }
    
    public void newExport(ExportType expType){
        
        String fileName = ServiceProvider.generateFileName();
        
        Export = expType.newExport(fileName, ServiceOrderList, dateI, dateF);
        
    }
    
    public boolean validate(){
        
        ExportList = ServiceProvider.getExportList();
        
        return ExportList.validateExport(Export);
    }
    
    public boolean exportFile(){
        
        return ExportList.registerFile(Export);
    }
    

}
