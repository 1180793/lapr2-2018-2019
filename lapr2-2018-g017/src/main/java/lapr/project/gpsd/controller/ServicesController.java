package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServicesController {

        private final AppGPSD oApp;
        private final Company oCompany;
        private final UserSession oSession;
        private final ServiceRegistry oServices;

        public ServicesController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oSession = oApp.getCurrentSession();
                this.oServices = oCompany.getServiceRegistry();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }
        
        public String getRole() {
                return this.oApp.getRole();
        }
        
        public String getEmail() {
                return this.oSession.getUserEmail();
        }
        
        public List<Service> getServices() {
                return this.oServices.getServices();
        }
}

