package lapr.project.gpsd.controller;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Alert;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceOrderRegistry;
import lapr.project.gpsd.model.ServiceRating;
import lapr.project.gpsd.model.ServiceRatingRegistry;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class GiveRatingController {

        private final AppGPSD oApp;
        private final Company oCompany;
        private final UserSession oSession;
        private final ClientRegistry oClients;
        private final ServiceOrderRegistry oOrders;
        private final ServiceRatingRegistry oRatings;
        private final String strEmail;
        private final Client oClient;
        private static final String RATING_ERROR = "Error with the service rating";

        public GiveRatingController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oSession = oApp.getCurrentSession();
                this.oClients = oCompany.getClientRegistry();
                this.oOrders = oCompany.getServiceOrderRegistry();
                this.oRatings = oCompany.getServiceRatingRegistry();
                this.strEmail = oSession.getUserEmail();
                this.oClient = oClients.getClientByEmail(strEmail);
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public String getRole() {
                return this.oApp.getRole();
        }

        public String getEmail() {
                return this.oSession.getUserEmail();
        }

        public Client getClient() {
                return this.oClient;
        }

        public String getClientName() {
                return this.oClient.getName();
        }

        public String getClientNif() {
                return this.oClient.getNif();
        }

        public String getClientTelephone() {
                return this.oClient.getTelephone();
        }

        public List<ServiceOrder> getOrders() {
                return this.oOrders.getClientOrders(this.oClient);
        }

        public Integer[] getRatings() {
                return Utils.LIST_OF_RATINGS;
        }

        public List<ServiceRating> getClientRating() {
                return this.oRatings.getServiceRatingsByClient(oClient);
        }

        public ServiceRating submitRating(ServiceOrder oOrder, int intRate) {
                if (oOrder != null) {
                        if (oRatings.validateRate(intRate)) {
                                if (oRatings.getRating(oOrder) == null) {
                                        ServiceRating oRating = new ServiceRating(intRate, oOrder);
                                        return oRating;
                                } else {
                                        Utils.openAlert(RATING_ERROR, "That Service Order is already rated.", "", Alert.AlertType.ERROR);
                                }
                        } else {
                                Utils.openAlert(RATING_ERROR, "Please select a Rating", "", Alert.AlertType.ERROR);
                        }
                } else {
                        Utils.openAlert(RATING_ERROR, "Please Select a Service Order to rate", "", Alert.AlertType.ERROR);
                }
                return null;
        }

        public boolean registerRating(ServiceRating oRating) {
                return oRatings.registerServiceRating(oRating);
        }

        public ServiceRating removeRating(ServiceOrder oOrder) {
                if (oOrder != null) {
                        ServiceRating oRating = oRatings.getRating(oOrder);
                        if (oRating != null) {
                                return oRating;
                        } else {
                                Utils.openAlert(RATING_ERROR, "That Service Order does not have a Rating", "", Alert.AlertType.ERROR);
                        }
                } else {
                        Utils.openAlert(RATING_ERROR, "Please select a Service Order to remove rate", "", Alert.AlertType.ERROR);
                }
                return null;
        }
        
        public boolean confirmRemoveRating(ServiceRating oRating) {
                return oRatings.removeServiceRating(oRating);
        }

}
