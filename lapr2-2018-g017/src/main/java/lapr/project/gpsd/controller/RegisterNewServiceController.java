/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import lapr.project.gpsd.exception.DuplicatedIDException;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.CategoryRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceRegistry;
import lapr.project.gpsd.model.ServiceType;
import lapr.project.gpsd.model.ServiceTypeRegistry;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * FXML Controller class
 *
 * @author Simao
 */
public class RegisterNewServiceController implements Initializable {

    private final AppGPSD oApp;
    private final Company oCompany;
    private final CategoryRegistry oCategories;
    private final ServiceRegistry oServices;
    private final ServiceTypeRegistry oServiceTypes;
    @FXML
    private Label lblTitle;
    @FXML
    private ChoiceBox<Category> cbxCategory;
    @FXML
    private TextField txtIdentifier;
    @FXML
    private TextField txtCostHour;
    @FXML
    private TextField txtBriefDesc;
    @FXML
    private TextField txtFullDesc;
    @FXML
    private ChoiceBox<ServiceType> cbxServiceType;
    @FXML
    private TextField txtAddInfo;
    @FXML
    private Button btnAddService;
    @FXML
    private Button btnDashboard;

        public RegisterNewServiceController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oServiceTypes = oCompany.getServiceTypeRegistry();
                this.oServices = oCompany.getServiceRegistry();
                this.oCategories = oCompany.getCategoryRegistry();
                
        }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
       populateTypes();
       populateCategory();
    }    

    private void populateTypes(){
    List<ServiceType> lstTypes = oServiceTypes.getServiceTypes();
    cbxServiceType.setItems(FXCollections.observableArrayList(lstTypes));
    }
    
    private void populateCategory(){
    List<Category> lstCategories = oCategories.getCategories();
    cbxCategory.setItems(FXCollections.observableArrayList(lstCategories));
    }
    
    private void successAlert(){
        Utils.openAlert("Service Added to the Request successfully", "Service Details",
	  "Service(ID:" + txtIdentifier.getText() + ") - " + txtBriefDesc.getText() + "\nCategory: " + cbxCategory.getValue(),
	  Alert.AlertType.INFORMATION);
    }
    
   private boolean validateService() {
		if (txtIdentifier.getText().isEmpty()) {
			Utils.openAlert("Error", "Invalid Identifier",
					"Please enter a valid identifier", Alert.AlertType.ERROR);
                        return false;
		}
		if (txtBriefDesc.getText().isEmpty()) {
			Utils.openAlert("Error", "Invalid Brief Description",
					"Please enter a brief description", Alert.AlertType.ERROR);
                        return false;
		}
		if (txtFullDesc.getText().isEmpty()) {
			Utils.openAlert("Error", "Invalid Full Description",
					"Please enter a full description", Alert.AlertType.ERROR);
                        return false;
		}
                if (txtCostHour.getText().isEmpty()) {
			Utils.openAlert("Error", "Invalid Cost/Hour",
					"Please enter a valid cost/hour", Alert.AlertType.ERROR);
                        return false;
		}
                if (cbxCategory.getSelectionModel().isEmpty()) {
			Utils.openAlert("Error", "Invalid Category",
					"Please choose a valid category", Alert.AlertType.ERROR);
                        return false;
		}
                if (cbxServiceType.getSelectionModel().isEmpty()) {
			Utils.openAlert("Error", "Invalid Service Type",
					"Please choose a valid service type", Alert.AlertType.ERROR);
                        return false;
                }
                if (cbxServiceType.getSelectionModel().getSelectedItem().getId().isEmpty()){
                         Utils.openAlert("Error", "Invalid Type",
                                        "Please enter a valid service type", Alert.AlertType.ERROR);
                        return false;
		}
                if (cbxServiceType.getSelectionModel().getSelectedItem().getId().equals("Stationary") && (txtAddInfo.getText().isEmpty())) {
			Utils.openAlert("Error", "Invalid Duration",
					"Please enter a valid duration", Alert.AlertType.ERROR);
                        return false;
		}
                return true;
	}
    
    
    @FXML
    private void handleAddService(ActionEvent event) throws IOException, InvalidAvailabilityException {
         if(validateService()==true){
        try{
        if(((ServiceType) cbxServiceType.getValue()).toString().equals("Stationary")){
            Service service = (ServiceType.STATIONARY.newService(txtIdentifier.getText(), txtBriefDesc.getText(), txtFullDesc.getText(), Double.parseDouble(txtCostHour.getText()),(Category) cbxCategory.getValue()));
            try{
            Double.parseDouble(txtAddInfo.getText());
            service.setOtherAtributes(txtAddInfo.getText());
             try{
            oServices.registerService(service);
            String strRole = oApp.getRole();
            successAlert();
            FxUtils.switchDashboard(event, strRole, oApp.getCurrentSession().getUserEmail());
            }catch(DuplicatedIDException ex){
                Utils.openAlert("Error", "Duplicate Service", "A service with this identifier already exists", Alert.AlertType.ERROR);
            }
            }catch(NumberFormatException ex){
                Utils.openAlert("Error", "Invalid Format", "Format in the additional information field is invalid", Alert.AlertType.ERROR);
            }
        }
        if(((ServiceType) cbxServiceType.getValue()).toString().equals("Limited")){
            Service service = (ServiceType.LIMITED.newService(txtIdentifier.getText(), txtBriefDesc.getText(), txtFullDesc.getText(),  Double.parseDouble(txtCostHour.getText()),(Category) cbxCategory.getValue()));
             try{
            oServices.registerService(service);
            String strRole = oApp.getRole();
            successAlert();
            FxUtils.switchDashboard(event, strRole, oApp.getCurrentSession().getUserEmail());
            }catch(DuplicatedIDException ex){
                Utils.openAlert("Error", "Duplicate Service", "A service with this identifier already exists", Alert.AlertType.ERROR);
            }
        }
        if(((ServiceType) cbxServiceType.getValue()).toString().equals("Expandable")){
            Service service = (ServiceType.EXPANDABLE.newService(txtIdentifier.getText(), txtBriefDesc.getText(), txtFullDesc.getText(),  Double.parseDouble(txtCostHour.getText()),(Category) cbxCategory.getValue()));
            try{
            oServices.registerService(service);
            String strRole = oApp.getRole();
            successAlert();
            FxUtils.switchDashboard(event, strRole, oApp.getCurrentSession().getUserEmail());
            }catch(DuplicatedIDException ex){
                Utils.openAlert("Error", "Duplicate Service", "A service with this identifier already exists", Alert.AlertType.ERROR);
            }
        }
        
        }catch(NumberFormatException ex){
            Utils.openAlert("Errro", "Invalid Format", "Format in the cost/hour field is invalid", Alert.AlertType.ERROR);
        }
    }
    }
    
    @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, oApp.getRole(), oApp.getCurrentSession().getUserEmail());
        }
    
}
