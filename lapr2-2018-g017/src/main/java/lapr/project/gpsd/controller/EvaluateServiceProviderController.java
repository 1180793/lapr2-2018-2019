package lapr.project.gpsd.controller;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import javafx.scene.control.Alert;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProviderEvaluation;
import lapr.project.gpsd.model.ServiceProviderRegistry;
import lapr.project.gpsd.model.ServiceRatingRegistry;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class EvaluateServiceProviderController {

        private final AppGPSD oApp;
        private final String strUserEmail;
        private final String strRole;
        private final Company oCompany;
        private final ServiceProviderRegistry oProviders;
        private final ServiceRatingRegistry oRatings;
        private final NumberFormat FORMATTER = new DecimalFormat("#0.00");     

        public EvaluateServiceProviderController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.strUserEmail = this.oApp.getCurrentSession().getUserEmail();
                this.strRole = this.oApp.getRole();
                this.oCompany = this.oApp.getCompany();
                this.oProviders = this.oCompany.getServiceProviderRegistry();
                this.oRatings = this.oCompany.getServiceRatingRegistry();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public String getRole() {
                return this.strRole;
        }

        public String getUserEmail() {
                return this.strUserEmail;
        }

        public List<ServiceProvider> getProviders() {
                return this.oProviders.getProviders();
        }

        public String getGlobalProviderRatingMean() {
                return FORMATTER.format(oRatings.getGlobalProviderRatingMean());
        }

        public String getGlobalProviderRatingDeviation() {
                return FORMATTER.format(oRatings.getGlobalProviderRatingDeviation());
        }

        public int getProviderServiceRatingsByRate(ServiceProvider oProvider, int intRate) {
                return oRatings.getProviderServiceRatingsByRate(oProvider, intRate).size();
        }

        public String getProviderRatingDeviation(ServiceProvider oProvider) {
                return FORMATTER.format(oRatings.getProviderRatingDeviation(oProvider));
        }

        public String getProviderRatingAverage(ServiceProvider oProvider) {
                return FORMATTER.format(oRatings.getProviderRatingAverage(oProvider));
        }

        public boolean validateProvider(ServiceProvider oProvider) {
                if (oProvider != null) {
                        return true;
                } else {
                        Utils.openAlert("Error Evaluating Service Provider", "Error Evaluating Service Provider", "Please choose a Service Provider to evaluate.", Alert.AlertType.ERROR);
                }
                return false;
        }

        public ServiceProviderEvaluation getServiceProviderEvaluation(ServiceProvider oProvider) {
                return this.oRatings.getServiceProviderEvaluation(oProvider);
        }

        public void acceptSystemEvaluation(ServiceProvider oProvider) {
                oProvider.setEvaluation(oRatings.getServiceProviderEvaluation(oProvider));
                Utils.openAlert("Service Provider Evaluated", oProvider.getFullName() + " was Evaluated Successfully",
                        oProvider.getAbbreviatedName() + " was evaluated with: " + oProvider.getEvaluation().getEvaluation(), Alert.AlertType.INFORMATION);
        }

        public List<ServiceProviderEvaluation> getEvaluations() {
                return new ArrayList<>(EnumSet.allOf(ServiceProviderEvaluation.class));
        }

        public void evaluateProvider(ServiceProvider oProvider, ServiceProviderEvaluation oEvaluation) {
                oProvider.setEvaluation(oEvaluation);
                Utils.openAlert("Service Provider Evaluated", oProvider.getFullName()+ " was Evaluated Successfully", 
                        oProvider.getAbbreviatedName()+ " was evaluated with: " + oEvaluation.getEvaluation(), Alert.AlertType.INFORMATION);

        }
}
