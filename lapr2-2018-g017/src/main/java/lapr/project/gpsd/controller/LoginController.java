package lapr.project.gpsd.controller;

import java.io.IOException;
import java.util.List;
import javafx.scene.control.Alert;
import lapr.project.auth.AuthFacade;
import lapr.project.auth.model.Role;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class LoginController {

        private final AppGPSD oApp;
        private final Company oCompany;
        private final AuthFacade oAuth;

        public LoginController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oAuth = oCompany.getAuthFacade();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }

        public boolean onLogin(String strEmail, String strPwd) throws IOException {
                if (strEmail == null || strPwd == null || strEmail.isEmpty() || strPwd.isEmpty()) {
                        return false;
                }
                boolean isValid = this.oApp.doLogin(strEmail, strPwd);
                if (!isValid) {
                        Utils.openAlert("Authentication Error", "Invalid Credentials", "Invalid email and/or password.",
                                Alert.AlertType.ERROR);
                        return false;
                } else {
                        Utils.openAlert("Authentication Successful", "Authenticated Successfully",
                                "Press OK to proceed to the application.", Alert.AlertType.INFORMATION);
                        List<Role> lstRoles = this.oApp.getCurrentSession().getUserRoles();
                        Role oRole = null;
                        if (lstRoles.size() == 1) {
                                oRole = lstRoles.get(0);
                        }
                        if (oRole != null) {
                                this.oApp.setRole(oRole.getRole());
                        }
                        return true;
                }
        }
}
