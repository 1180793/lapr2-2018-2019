/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PromptedServiceDescription;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceOrderRegistry;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProviderRegistry;
import lapr.project.gpsd.model.ServiceProvisionRequest;
import lapr.project.gpsd.model.WorkReport;
import lapr.project.gpsd.model.WorkReportRegistry;


/**
 *
 * @author ZDPessoa
 */
public class EndOfReportController {
    
    
    private AppGPSD app;
    private UserSession session;
    private Company company;
    private String strEmail;
    private ServiceProviderRegistry SProviderReg;
    private ServiceProvider ServiceProvider;
    private ServiceOrderRegistry SOrderReg;
    private List<ServiceOrder> ServiceOrderList;
    private WorkReportRegistry WorkReportReg;
    private WorkReport Report;
    
    public AppGPSD getApp(){
        
        return app;
    }
    
    public List<ServiceOrder> startReport() throws InvalidAvailabilityException{
        
        app = AppGPSD.getInstance();
        company = app.getCompany();
        session = app.getCurrentSession();
        strEmail = session.getUserEmail();
        SProviderReg = company.getServiceProviderRegistry();
        ServiceProvider = SProviderReg.getServiceProviderByEmail(strEmail);
        
        SOrderReg = company.getServiceOrderRegistry();
        
        ServiceOrderList = SOrderReg.getServiceOrderListByProvider(ServiceProvider);
        
        return ServiceOrderList;
        
    }
    
    public void newReport(int orderNumber, String problem, String strategy, int extraExecHours){
        
        WorkReportReg = company.getWorkReportRegistry();
        
        Report = WorkReportReg.newReport(orderNumber, problem, strategy, extraExecHours);
    }
    
    public boolean validate(){
        
       return WorkReportReg.validateReport(Report);
    }
    
    public boolean RegisterReport(ServiceOrder so){

       so.setDone(true);
       return WorkReportReg.registerReport(Report);
    }
    
    
}
