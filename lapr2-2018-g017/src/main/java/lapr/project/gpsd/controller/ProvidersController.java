package lapr.project.gpsd.controller;

import java.util.List;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProviderRegistry;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ProvidersController {
 
        private final AppGPSD oApp;
        private final Company oCompany;
        private final UserSession oSession;
        private final ServiceProviderRegistry oProviders;

        public ProvidersController() throws InvalidAvailabilityException {
                this.oApp = AppGPSD.getInstance();
                this.oCompany = oApp.getCompany();
                this.oSession = oApp.getCurrentSession();
                this.oProviders = oCompany.getServiceProviderRegistry();
        }

        public AppGPSD getApp() {
                return this.oApp;
        }
        
        public String getRole() {
                return this.oApp.getRole();
        }
        
        public String getEmail() {
                return this.oSession.getUserEmail();
        }
        
        public List<ServiceProvider> getProviders() {
                return this.oProviders.getProviders();
        }
}
