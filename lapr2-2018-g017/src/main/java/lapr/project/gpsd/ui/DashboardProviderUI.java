package lapr.project.gpsd.ui;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DashboardProviderUI {

    @FXML
    private Button btnDailyAvailability;

    @FXML
    private Button btnWorkReport;

    @FXML
    private Button btnCheckExecutionOrders;

    @FXML
    private Button btnAvailableServices;

    @FXML
    private void handleDailyAvailability(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "DailyAvailabilityUI",
				"Indicate Daily Availability", true);
    }

    @FXML
    private void handleWorkReport(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "EndOfWorkScene",
				"End Of Work Report", true);
    }

    @FXML
    private void handleCheckExecutionOrders(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "CheckOrdersScene",
				"Check Service Orders", true);
    }

    @FXML
    private void handleAvailableServices(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "AvailableServicesUI",
				"Available Services", true);
    }

}
