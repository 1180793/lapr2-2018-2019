package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import lapr.project.gpsd.controller.RegisterClientController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Client;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class RegisterClientUI implements Initializable {

        private RegisterClientController oController;

        @FXML
        private Label lblTitle;

        @FXML
        private TextField txtName;

        @FXML
        private TextField txtNif;

        @FXML
        private TextField txtTel;

        @FXML
        private TextField txtEmail;

        @FXML
        private PasswordField txtPassword;

        @FXML
        private PasswordField txtPasswordConfirmation;

        @FXML
        private TextField txtPostalCode;

        @FXML
        private Button btnFetchPostalCode;

        @FXML
        private TextField txtStreet;

        @FXML
        private TextField txtLocality;

        @FXML
        private Button btnAddAddress;

        @FXML
        private Button btnRemoveAddress;

        @FXML
        private TableView<Address> tblAddresses;

        @FXML
        private TableColumn<Address, String> colStreet;

        @FXML
        private TableColumn<Address, String> colLocality;

        @FXML
        private TableColumn<Address, String> colPostalCode;

        @FXML
        private Button btnCancel;

        @FXML
        private Button btnSubmit;

        @Override
        public void initialize(URL url, ResourceBundle rb) {
                try {
                        this.oController = new RegisterClientController();
                } catch (InvalidAvailabilityException ex) {
                }

        }

        @FXML
        private void handleFetchPostalCode(ActionEvent event) {
                String strPostalCode = txtPostalCode.getText();
                if (this.oController.getAddress(strPostalCode)) {
                        txtStreet.setText(this.oController.getFetchedAddress().getStreet());
                        txtLocality.setText(this.oController.getFetchedAddress().getLocality());
                }
        }

        @FXML
        private void handleAddAddress(ActionEvent event) {
                String strStreet = txtStreet.getText();
                String strPostalCode = txtPostalCode.getText();
                String strLocality = txtLocality.getText();
                if (this.oController.addAddress(strStreet, strPostalCode, strLocality)) {
                        Utils.openAlert("Address Added", "The specified Address was added successfully.", "", Alert.AlertType.INFORMATION);
                        populateAddresses();
                }
        }

        @FXML
        private void handleRemoveAddress(ActionEvent event) {
                Address oAddress = tblAddresses.getSelectionModel().getSelectedItem();
                if (this.oController.removeAddress(oAddress)) {
                        Utils.openAlert("Address Removed", "The selected Address was removed successfully.", "", Alert.AlertType.INFORMATION);
                        populateAddresses();
                }
        }

        private void populateAddresses() {
                colStreet.setCellValueFactory(new PropertyValueFactory<>("street"));
                colLocality.setCellValueFactory(new PropertyValueFactory<>("locality"));
                colPostalCode.setCellValueFactory(new PropertyValueFactory<>("postalCodeString"));
                ObservableList<Address> list = FXCollections.observableArrayList(this.oController.getAddresses());
                tblAddresses.setItems(list);
        }

        @FXML
        private void handleCancel(ActionEvent event) throws IOException, InvalidAvailabilityException {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Are you sure you want to cancel?");
                alert.setHeaderText("Do you want to cancel the registration?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.isPresent()) {
                        if (result.get() == ButtonType.OK) {
                                this.oController.cancel();
                                FxUtils.switchLogin();
                        }
                }
        }

        @FXML
        private void handleSubmit(ActionEvent event) throws IOException, InvalidAvailabilityException {
                String strName = txtName.getText();
                String strNif = txtNif.getText();
                String strTel = txtTel.getText();
                String strEmail = txtEmail.getText();
                String strPassword = txtPassword.getText();
                String strPasswordConfirmation = txtPasswordConfirmation.getText();
                Client oClient = this.oController.submitRegistration(strName, strNif, strTel, strEmail, strPassword, strPasswordConfirmation);
                if (oClient != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Are you sure you want to submit registration?");
                        alert.setHeaderText("Do you want to submit the registration?");
                        alert.setContentText("Name: " + strName 
                                + "\nNIF: " + strNif 
                                + "\nTelephone: " + strTel 
                                + "\nEmail: " + strEmail);
                        
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent()) {
                                if (result.get() == ButtonType.OK) {
                                        if (this.oController.confirmRegistration(oClient, strPassword)) {
                                                FxUtils.switchLogin();
                                        }
                                }
                        }
                }
        }

}
