/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.controller.EndOfReportController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.utils.FxUtils;

/**
 *
 * @author ZDPessoa
 */
public class EndOfWrokReportUI implements Initializable {

    private EndOfReportController controller;

    private ServiceOrder SelectedSO;

    private static final String EXPANDABLE_SERV_CLASS = "class lapr.project.gpsd.model.ExpandableService";

    @FXML
    private ComboBox<ServiceOrder> WR_ComboBox;

    @FXML
    private ComboBox<String> WR_ComboBox2;

    @FXML
    private Label WR_Label;

    @FXML
    private Label WR_Label2;

    @FXML
    private TextArea WR_TArea;

    @FXML
    private TextArea WR_TArea2;

    ObservableList<ServiceOrder> ServiceOrderList;

    ObservableList<String> Hours = FXCollections.observableArrayList("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12");

    public EndOfWrokReportUI() {

        this.controller = new EndOfReportController();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        List<ServiceOrder> SOrderListAux = new ArrayList<>();

        try {
            SOrderListAux = controller.startReport();
        } catch (InvalidAvailabilityException ex) {
            Logger.getLogger(EndOfWrokReportUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        ServiceOrderList = FXCollections.observableArrayList(SOrderListAux);

        WR_ComboBox.setItems(ServiceOrderList);

        WR_ComboBox2.setItems(Hours);

    }

    @FXML
    private void handleServiceOrderComboBox(ActionEvent event) {

        SelectedSO = WR_ComboBox.getValue();

        if (SelectedSO.getServiceType().equals(EXPANDABLE_SERV_CLASS)) {

            WR_Label2.setVisible(true);
            WR_ComboBox2.setVisible(true);
        } else {
            WR_Label2.setVisible(false);
            WR_ComboBox2.setVisible(false);
        }

        WR_Label.setText(SelectedSO.DisplaytoString());
    }

    @FXML
    private void handleConfirm(ActionEvent event) throws IOException, InvalidAvailabilityException {

        if (!(WR_ComboBox.getValue() == null)) {
            
            String problem = WR_TArea.getText();
            String strategy = WR_TArea2.getText();
            String SeHours = WR_ComboBox2.getValue();
            int eHours;

            if (SeHours == null) {
                eHours = 0;
            } else {
                eHours = Integer.parseInt(SeHours);
            }

            controller.newReport(SelectedSO.getSequentialNumber(), problem, strategy, eHours);

            if (controller.validate()) {
                controller.RegisterReport(SelectedSO);
            }
        }
        exit(event);
    }

    @FXML
    private void handleBack(ActionEvent event) throws IOException, InvalidAvailabilityException {

        exit(event);
    }

    private void exit(ActionEvent event) throws IOException, InvalidAvailabilityException {

        AppGPSD app = controller.getApp();

        String strRole = app.getRole();
        FxUtils.switchDashboard(event, strRole, app.getCurrentSession().getUserEmail());
    }
}
