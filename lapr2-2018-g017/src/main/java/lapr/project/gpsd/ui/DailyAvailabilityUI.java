package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lapr.project.gpsd.controller.DailyAvailabilityController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Availability;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DailyAvailabilityUI implements Initializable {

        private DailyAvailabilityController oController;

        @FXML
        private Label lblTitle;

        @FXML
        private DatePicker dpStartDate;

        @FXML
        private ChoiceBox<String> cbxStartHour;

        @FXML
        private DatePicker dpEndDate;

        @FXML
        private ChoiceBox<String> cbxEndHour;

        @FXML
        private Button btnAddAvailability;

        @FXML
        private Button btnRemoveAvailability;

        @FXML
        private TableView<Availability> tableAvailability;

        @FXML
        private TableColumn<Availability, String> colStartDate;

        @FXML
        private TableColumn<Availability, String> colStartHour;

        @FXML
        private TableColumn<Availability, String> colEndDate;

        @FXML
        private TableColumn<Availability, String> colEndHour;

        @FXML
        private Button btnDashboard;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new DailyAvailabilityController();
                } catch (InvalidAvailabilityException ex) {
                }
                populateAvailabilities();
                listenerDatePickers();
                populateHours();
        }

        private void populateAvailabilities() {
                colStartDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
                colStartHour.setCellValueFactory(new PropertyValueFactory<>("startHour"));
                colEndDate.setCellValueFactory(new PropertyValueFactory<>("endDate"));
                colEndHour.setCellValueFactory(new PropertyValueFactory<>("endHour"));
                tableAvailability.setItems(FXCollections.observableArrayList(this.oController.getAvailabilities()));
        }

        private void listenerDatePickers() {
                dpStartDate.setDayCellFactory(picker -> new DateCell() {
                        @Override
                        public void updateItem(LocalDate date, boolean empty) {
                                super.updateItem(date, empty);
                                LocalDate today = LocalDate.now();
                                setDisable(empty || date.compareTo(today) < 0);
                        }
                });
                dpEndDate.setDayCellFactory(picker -> new DateCell() {
                        @Override
                        public void updateItem(LocalDate date, boolean empty) {
                                super.updateItem(date, empty);
                                LocalDate today = LocalDate.now();
                                setDisable(empty || date.compareTo(today) < 0);
                        }
                });
        }

        private void populateHours() {
                cbxStartHour.setItems(FXCollections.observableArrayList(oController.getHours()));
                cbxEndHour.setItems(FXCollections.observableArrayList(oController.getHours()));
        }

        @FXML
        private void handleAddAvailability(ActionEvent event) throws ParseException {
                String strStartDate = "";
                if (dpStartDate.getValue() != null) {
                        strStartDate = dpStartDate.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                }
                String strStartHour = cbxStartHour.getValue();
                String strEndDate = "";
                if (dpEndDate.getValue() != null) {
                        strEndDate = dpEndDate.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                }
                String strEndHour = cbxEndHour.getValue();
                Availability oAvailability = this.oController.newAvailability(strStartDate, strStartHour, strEndDate, strEndHour);
                if (oAvailability != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Add Availability");
                        alert.setHeaderText("Do you want to add the specified Availability?");
                        alert.setContentText("Start Date: " + oAvailability.getStartDate() + " ("
                                + Utils.getWeekDay(oAvailability.getStartDate()) + ")\nStart Hour: "
                                + oAvailability.getStartHour() + "h\nEnd Date: " + oAvailability.getEndDate()
                                + " (" + Utils.getWeekDay(oAvailability.getEndDate()) + ")\nEnd Hour: "
                                + oAvailability.getEndHour() + "h");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && (result.get() == ButtonType.OK)) {
                                if (this.oController.registerAvailability(oAvailability)) {
                                        dpStartDate.setValue(null);
                                        cbxStartHour.setValue(null);
                                        dpEndDate.setValue(null);
                                        cbxEndHour.setValue(null);
                                        populateAvailabilities();
                                        Utils.openAlert("Availability Added Successfully", "Availability Details",
                                                "Start Date: " + oAvailability.getStartDate() + " ("
                                                + Utils.getWeekDay(oAvailability.getStartDate()) + ")\nStart Hour: "
                                                + oAvailability.getStartHour() + "h\nEnd Date: " + oAvailability.getEndDate()
                                                + " (" + Utils.getWeekDay(oAvailability.getEndDate()) + ")\nEnd Hour: "
                                                + oAvailability.getEndHour() + "h",
                                                Alert.AlertType.INFORMATION);
                                }
                        }
                }
        }

        @FXML
        private void handleRemoveAvailability(ActionEvent event) {
                Availability oAvailability = tableAvailability.getSelectionModel().getSelectedItem();
                if (oAvailability != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Remove Availability");
                        alert.setHeaderText("Do you want to remove the selected Availability?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && (result.get() == ButtonType.OK)) {
                                if (this.oController.removeAvailability(oAvailability)) {
                                        populateAvailabilities();
                                        Utils.openAlert("Availability Removed", "The selected Availability was removed successfully.", "", Alert.AlertType.INFORMATION);
                                } else {
                                        Utils.openAlert("Error trying to remove Availability", "Error trying to remove Availability",
                                                "Select an Availability to remove", Alert.AlertType.ERROR);
                                }
                        }
                } else {
                        Utils.openAlert("Error trying to remove Availability", "Error trying to remove Availability",
                                "Select an Availability to remove", Alert.AlertType.ERROR);
                }
        }

        @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, oController.getRole(), oController.getEmail());
        }

}
