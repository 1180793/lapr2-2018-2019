package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import lapr.project.gpsd.controller.MyServiceRequestsController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.PromptedServiceDescription;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.ServiceProvisionRequest;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class MyServiceRequestsUI implements Initializable {

        private MyServiceRequestsController oController;

        @FXML
        private Label lblTitle;

        @FXML
        private ChoiceBox<ServiceProvisionRequest> cbxRequest;

        @FXML
        private TextField txtSequencialNumber;

        @FXML
        private TextField txtAddress;

        @FXML
        private TextField txtStatus;

        @FXML
        private TableView<PromptedServiceDescription> tableServices;

        @FXML
        private TableColumn<PromptedServiceDescription, String> colService;

        @FXML
        private TableColumn<PromptedServiceDescription, String> colDuration;

        @FXML
        private TableColumn<PromptedServiceDescription, String> colDescription;

        @FXML
        private TableView<SchedulePreference> tableSchedules;

        @FXML
        private TableColumn<SchedulePreference, Integer> colOrder;

        @FXML
        private TableColumn<SchedulePreference, String> colDate;

        @FXML
        private TableColumn<SchedulePreference, String> colHour;

        @FXML
        private Button btnDashboard;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new MyServiceRequestsController();
                } catch (InvalidAvailabilityException ex) {
                        Logger.getLogger(MyServiceRequestsUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                populateRequests();
                listenerRequests();
        }

        private void populateRequests() {
                cbxRequest.setItems(FXCollections.observableArrayList(this.oController.getClientRequests()));
        }

        private void listenerRequests() {
                cbxRequest.valueProperty().addListener((obs, oldValue, newValue) -> {
                        if (newValue == null) {
                                clearRequest();
                                clearTables();
                        } else {
                                populateRequest(newValue);
                                populateTables(newValue);
                        }
                });
        }

        private void clearTables() {
                clearServicesTable();
                clearSchedulesTable();
        }

        private void clearRequest() {
                txtSequencialNumber.clear();
                txtAddress.clear();
                txtStatus.clear();
        }

        private void clearServicesTable() {
                tableServices.getItems().clear();
        }

        private void clearSchedulesTable() {
                tableSchedules.getItems().clear();
        }

        private void populateRequest(ServiceProvisionRequest oRequest) {
                if (oRequest != null) {
                        txtSequencialNumber.setText(oRequest.toString());
                        txtAddress.setText(oRequest.getAddressString());
                        txtStatus.setText(oRequest.getStatus());
                }
        }

        private void populateTables(ServiceProvisionRequest oRequest) {
                populateServicesTable(oRequest);
                populateSchedulesTable(oRequest);
        }

        private void populateServicesTable(ServiceProvisionRequest oRequest) {
                colService.setCellValueFactory(new PropertyValueFactory<>("serviceString"));
                colDuration.setCellValueFactory(new PropertyValueFactory<>("duration"));
                colDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
                ObservableList<PromptedServiceDescription> list = FXCollections
                        .observableArrayList(oRequest.getRequestedServices());
                tableServices.setItems(list);
        }

        private void populateSchedulesTable(ServiceProvisionRequest oRequest) {
                colOrder.setCellValueFactory(new PropertyValueFactory<>("order"));
                colDate.setCellValueFactory(new PropertyValueFactory<>("dateString"));
                colHour.setCellValueFactory(new PropertyValueFactory<>("hour"));
                ObservableList<SchedulePreference> list = FXCollections.observableArrayList(oRequest.getSchedules());
                tableSchedules.setItems(list);
        }

        @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, oController.getRole(), oController.getEmail());
        }

}
