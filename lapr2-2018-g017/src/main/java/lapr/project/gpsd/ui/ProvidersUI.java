package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import lapr.project.gpsd.controller.ProvidersController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ProvidersUI implements Initializable {

	private ProvidersController oController;
	
	@FXML
        private Label lblTitle;

        @FXML
        private TableView<ServiceProvider> tableProviders;

        @FXML
        private TableColumn<ServiceProvider, String> colPersonnelNumber;

        @FXML
        private TableColumn<ServiceProvider, String> colName;
    
        @FXML
        private TableColumn<ServiceProvider, String> colAbbrevName;

        @FXML
        private TableColumn<ServiceProvider, String> colEmail;
    
        @FXML
        private TableColumn<ServiceProvider, String> colCategories;
    
        @FXML
        private TableColumn<ServiceProvider, String> colGeographicAreas;

        @FXML
        private Button btnDashboard;

        @Override
	public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new ProvidersController();
                } catch (InvalidAvailabilityException ex) {
                }
		populateProviders();
	}
        
        @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, oController.getRole(), oController.getEmail());
        }

        private void populateProviders() {
                colPersonnelNumber.setCellValueFactory(new PropertyValueFactory<>("personnelNumber"));
                colName.setCellValueFactory(new PropertyValueFactory<>("fullName"));
                colAbbrevName.setCellValueFactory(new PropertyValueFactory<>("abbreviatedName"));
                colEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
                colCategories.setCellValueFactory(new PropertyValueFactory<>("categoriesString"));
                colGeographicAreas.setCellValueFactory(new PropertyValueFactory<>("geographicAreasString"));
		ObservableList<ServiceProvider> list = FXCollections.observableArrayList(this.oController.getProviders());
		tableProviders.setItems(list);
        }
}

