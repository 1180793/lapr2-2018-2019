package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import lapr.project.gpsd.controller.ClientsController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Client;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ClientsUI implements Initializable {

	private ClientsController oController;

	@FXML
	private TableView<Client> tableClients;

	@FXML
	private TableColumn<Client, String> colName;

	@FXML
	private TableColumn<Client, String> colNif;

	@FXML
	private TableColumn<Client, String> colEmail;

	@FXML
	private TableColumn<Client, String> colTelephone;

	@FXML
	private TableColumn<Client, String> colAddress;

	@FXML
	private TableColumn<Client, String> colPassword;

	@FXML
        private Button btnDashboard;

        @Override
	public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new ClientsController();
                } catch (InvalidAvailabilityException ex) {
                }
		populateClients();
	}
        
        @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, oController.getRole(), oController.getEmail());
        }

	private void populateClients() {
		colName.setCellValueFactory(new PropertyValueFactory<>("name"));
		colNif.setCellValueFactory(new PropertyValueFactory<>("nif"));
		colEmail.setCellValueFactory(new PropertyValueFactory<>("email"));
		colTelephone.setCellValueFactory(new PropertyValueFactory<>("telephone"));
		colAddress.setCellValueFactory(new PropertyValueFactory<>("addressString"));
		colPassword.setCellValueFactory(new PropertyValueFactory<>("password"));
		ObservableList<Client> list = FXCollections.observableArrayList(this.oController.getClients());
		tableClients.setItems(list);
	}

}