package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import lapr.project.gpsd.controller.AssociatePostalAddressController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AssociatePostalAddressUI implements Initializable {

        private AssociatePostalAddressController oController;
        
        @FXML
        private Label lblTitle;
        
        @FXML
        private TextField txtStreetAddress;

        @FXML
        private TextField txtLocality;
        
        @FXML
        private TextField txtPostalCode;

        @FXML
        private Button btnFetchAddress;

        @FXML
        private Button btnAddAddress;

        @FXML
        private Button btnRemoveAddress;
        
        @FXML
        private TableView<Address> tableAddresses;
        
        @FXML
        private TableColumn<Address, String> colStreet;

        @FXML
        private TableColumn<Address, String> colLocality;
        
        @FXML
        private TableColumn<Address, String> colPostalCode;

        @FXML
        private Button btnDashboard;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new AssociatePostalAddressController();
                } catch (InvalidAvailabilityException ex) {
                }
                populateAddresses();
        }
        
        private void populateAddresses() {
                colStreet.setCellValueFactory(new PropertyValueFactory<>("street"));
                colLocality.setCellValueFactory(new PropertyValueFactory<>("locality"));
                colPostalCode.setCellValueFactory(new PropertyValueFactory<>("postalCodeString"));
                ObservableList<Address> list = FXCollections.observableArrayList(this.oController.getAddresses());
                tableAddresses.setItems(list);
        }
        
        @FXML
        private void handleFetchAddress(ActionEvent event) {
                String strPostalCode = txtPostalCode.getText();
                Address oAdress = this.oController.fetchAddressByPostalCode(strPostalCode);
                if (oAdress != null) {
                        txtStreetAddress.setText(oAdress.getStreet());
                        txtLocality.setText(oAdress.getLocality());
                }
        }

        @FXML
        private void handleAddAddress(ActionEvent event) {
                String strStreet = txtStreetAddress.getText();
                String strPostalCode = txtPostalCode.getText();
                String strLocality = txtLocality.getText();
                if (this.oController.addAddress(strStreet, strPostalCode, strLocality)) {
                        Utils.openAlert("Address Added", "The specified Address was added successfully.", "", Alert.AlertType.INFORMATION);
                        populateAddresses();
                }
        }

        @FXML
        private void handleRemoveAddress(ActionEvent event) {
                Address oAddress = tableAddresses.getSelectionModel().getSelectedItem();
                if (this.oController.removeAddress(oAddress)) {
                        Utils.openAlert("Address Removed", "The selected Address was removed successfully.", "", Alert.AlertType.INFORMATION);
                        populateAddresses();
                }
        }

        @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, oController.getRole(), oController.getEmail());
        }

}
