/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.controller.GeographicalAreaSpecificationController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 *
 * @author ZDPessoa
 */
public class GeographicalAreaSpecificationUI implements Initializable {

    private final GeographicalAreaSpecificationController controller;

    @FXML
    private TextArea GS_TArea;

    @FXML
    private TextArea GS_TArea2;

    @FXML
    private TextArea GS_TArea3;

    @FXML
    private TextArea GS_TArea4;
    
    @FXML
    private Button GS_Button;
    
    @FXML
    private Button GS_Button2;

    public GeographicalAreaSpecificationUI() {

        this.controller = new GeographicalAreaSpecificationController();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            controller.startGeoArea();
        } catch (InvalidAvailabilityException ex) {
            Logger.getLogger(GeographicalAreaSpecificationUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void handleConfirm(ActionEvent event) throws IOException, InvalidAvailabilityException {
        
        String desig = GS_TArea.getText();
        String sPostalCode = GS_TArea2.getText();
        String radiusS = GS_TArea3.getText();
        String tCostsS = GS_TArea4.getText();
       
        if (!(desig.isEmpty() || sPostalCode.isEmpty() || radiusS.isEmpty() || tCostsS.isEmpty())) {
            if (Utils.getPostalCode(sPostalCode) != null) {

                PostalCode PostalCode = Utils.getPostalCode(sPostalCode);

                double radius = Double.parseDouble(radiusS);

                double travelCosts = Double.parseDouble(tCostsS);

                controller.newGeographicalArea(desig, PostalCode, radius, travelCosts);

                if (controller.validate()) {
                    controller.addGeographicalArea();
                }

                exit(event);

            } else {
                Utils.openAlert("Postal Code Error", "Error Selecting the Postal Code",
                        "Please Select an existing Postal Code", Alert.AlertType.ERROR);
            }
        }else{
            Utils.openAlert("Mandatory Field Error", "Error Missing informartion",
                        "Please fill all the Mandatory Fields", Alert.AlertType.ERROR);
        }
    }

    @FXML
    private void handleBack(ActionEvent event) throws IOException, InvalidAvailabilityException {

        exit(event);
    }

    private void exit(ActionEvent event) throws IOException, InvalidAvailabilityException {

        AppGPSD app = controller.getApp();

        String strRole = app.getRole();
        FxUtils.switchDashboard(event, strRole, app.getCurrentSession().getUserEmail());
    }

}
