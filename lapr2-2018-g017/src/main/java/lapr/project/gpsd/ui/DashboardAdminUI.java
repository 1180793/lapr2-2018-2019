package lapr.project.gpsd.ui;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DashboardAdminUI {

    @FXML
    private Button btnSpecifyCategory;

    @FXML
    private Button btnSpecifyService;

    @FXML
    private Button btnSpecifyGeographicArea;
    
    @FXML
    private Button btnAvailableServices;

    @FXML
    private void handleSpecifyCategory(ActionEvent event) {
        Utils.openAlert("Error trying to Specify Category", "Error trying to Specify Category", "Not implemented yet", Alert.AlertType.ERROR);
    }

    @FXML
    private void handleSpecifyService(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "RegisterNewServiceUI", "Register New Service", true);
    }

    @FXML
    private void handleSpecifyGeographicArea(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "GeographicalAreaSpecificationScene",
				"Geographical Area Specification", true);
    }

    @FXML
    private void handleAvailableServices(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "AvailableServicesUI",
				"Available Services", true);
    }

}

