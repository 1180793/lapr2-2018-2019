package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import lapr.project.gpsd.controller.LoginController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class LoginUI implements Initializable {

        private LoginController oController;

        /**
         * Scene title
         */
        @FXML
        private Label lblTitle;

        /**
         * Email text field
         */
        @FXML
        private TextField txtEmail;

        /**
         * Password field
         */
        @FXML
        private PasswordField txtPassword;

        /**
         * Login Button
         */
        @FXML
        private Button btnLogin;

        /**
         * Register as Client Button
         */
        @FXML
        private Button btnRegisterClient;

        /**
         * Submit Application Button
         */
        @FXML
        private Button btnSubmitApplication;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new LoginController();
                } catch (InvalidAvailabilityException ex) {
                }
        }

        /**
         * Method that authenticates a User into the App
         *
         * @param event Action Event
         * @throws IOException if the UI for Role Switching doesn't exist in the resources/fxml folder
         */
        @FXML
        private void onLogin(ActionEvent event) throws IOException, InvalidAvailabilityException {
                String strEmail = txtEmail.getText();
                String strPwd = txtPassword.getText();
                if (this.oController.onLogin(strEmail, strPwd)) {
                        if (this.oController.getApp().getRole() == null || this.oController.getApp().getRole().isEmpty()) {
                                FxUtils.switchRole();
                        } else {
                                FxUtils.switchDashboard(event, this.oController.getApp().getRole(), strEmail);
                        }
                }
        }

        /**
         * Method that switches from login pane to client registration window
         *
         * @param event Action event
         * @throws IOException if 'RegisterClientUI' doesn't exist in the resources/fxml folder
         */
        @FXML
        private void handleRegisterClient(ActionEvent event) throws IOException {
                FxUtils.switchScene(event, "RegisterClientUI",
                        "Client Registration", false);
        }

        /**
         * Method that switches from login pane to service provider application window
         *
         * @param event Action event
         * @throws IOException if 'RegisterClientUI' doesn't exist in the resources/fxml folder
         */
        @FXML
        private void handleSubmitApplication(ActionEvent event) throws IOException {
                FxUtils.switchScene(event, "SubmitProviderApplication",
                        "Service Provider Application", false);
        }

}
