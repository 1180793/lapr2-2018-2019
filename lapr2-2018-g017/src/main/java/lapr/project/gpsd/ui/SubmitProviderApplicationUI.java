package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import lapr.project.gpsd.controller.SubmitProviderApplicationController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.AcademicQualification;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.ProfessionalQualification;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class SubmitProviderApplicationUI implements Initializable {

        private SubmitProviderApplicationController oController;

        @FXML
        private Label lblTitle;

        @FXML
        private TextField txtName;

        @FXML
        private TextField txtNif;

        @FXML
        private TextField txtTel;

        @FXML
        private TextField txtEmail;

        @FXML
        private TextField txtStreet;

        @FXML
        private TextField txtLocality;

        @FXML
        private TextField txtPostalCode;

        @FXML
        private ChoiceBox<Category> cbxCategory;

        @FXML
        private Button bntAddCategory;

        @FXML
        private Button bntnRemoveCategory;

        @FXML
        private TextField txtProQualification;

        @FXML
        private Button bntAddProQualification;

        @FXML
        private Button bntRemoveProQualification;

        @FXML
        private TextField txtAcademicQualification;

        @FXML
        private Button bntAddAcademicQualification;

        @FXML
        private Button bntRemoveAcademicQualification;

        @FXML
        private TableView<Category> tblCategories;

        @FXML
        private TableColumn<Category, String> colId;

        @FXML
        private TableColumn<Category, String> colDescription;

        @FXML
        private TableView<ProfessionalQualification> tblProQualification;

        @FXML
        private TableColumn<Category, String> colProQualification;

        @FXML
        private TableView<AcademicQualification> tblAcademicQualification;

        @FXML
        private TableColumn<Category, String> colAcademicQualification;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new SubmitProviderApplicationController();
                } catch (InvalidAvailabilityException ex) {
                }
                populateCategories();
        }

        private void populateCategories() {
                cbxCategory.setItems(FXCollections.observableArrayList(oController.getCategories()));
        }

        private void updateCategoriesTable() {
                colId.setCellValueFactory(new PropertyValueFactory<>("id"));
                colDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
                ObservableList<Category> list = FXCollections.observableArrayList(this.oController.getCategoryList());
                tblCategories.setItems(list);
        }

        private void updateQualificationsTable() {
                colProQualification.setCellValueFactory(new PropertyValueFactory<>("description"));
                ObservableList<ProfessionalQualification> list = FXCollections.observableArrayList(this.oController.getProQual());
                tblProQualification.setItems(list);
                colAcademicQualification.setCellValueFactory(new PropertyValueFactory<>("designation"));
                ObservableList<AcademicQualification> list2 = FXCollections.observableArrayList(this.oController.getAcaQual());
                tblAcademicQualification.setItems(list2);
        }

        @FXML
        private void handleAddCategory() {
                Category oCategory = cbxCategory.getValue();
                if (oController.addCategory(oCategory)) {
                        Utils.openAlert("Category added", "You sucessfuly added a category", "", Alert.AlertType.INFORMATION);
                        updateCategoriesTable();
                }
        }

        @FXML
        private void handleRemoveCategory() {
                Category oCategory = cbxCategory.getValue();
                if (oController.removeCategory(oCategory)) {
                        Utils.openAlert("Category removed", "You sucessfuly removed a category", "", Alert.AlertType.INFORMATION);
                        updateCategoriesTable();
                }
        }

        @FXML
        private void handleAddProQualification() {
                String strText = txtProQualification.getText();
                if (oController.addProQualification(strText)) {
                        Utils.openAlert("Professional Qualification added", "You sucessfuly added a Professional Qualification", "", Alert.AlertType.INFORMATION);
                        updateQualificationsTable();
                }
        }

        @FXML
        private void handleRemoveProQualification() {
                ProfessionalQualification oQualification = tblProQualification.getSelectionModel().getSelectedItem();
                if (oController.removeProQualification(oQualification)) {
                        Utils.openAlert("Professional Qualification removed", "You sucessfuly removed a Professional Qualification", "", Alert.AlertType.INFORMATION);
                        updateQualificationsTable();
                }
        }

        @FXML
        private void handleAddAcademicQualification() {
                String strText = txtAcademicQualification.getText();
                if (oController.addAcaQualification(strText)) {
                        Utils.openAlert("Academic Qualification added", "You sucessfuly added an Academic Qualification", "", Alert.AlertType.INFORMATION);
                        updateQualificationsTable();
                }
        }

        @FXML
        private void handleRemoveAcademicQualification() {
                AcademicQualification oQualification = tblAcademicQualification.getSelectionModel().getSelectedItem();
                if (oController.removeAcaQualification(oQualification)) {
                        Utils.openAlert("Academic Qualification removed", "You sucessfuly removed an Academic Qualification", "", Alert.AlertType.INFORMATION);
                        updateQualificationsTable();
                }
        }
        
        @FXML
        private void handleSubmit(ActionEvent event) throws IOException, InvalidAvailabilityException {
                String strName = txtName.getText();
                String strEmail = txtEmail.getText();
                String strTel = txtTel.getText();
                String strNif = txtNif.getText();
                String strStreet = txtStreet.getText();
                String strLocality = txtLocality.getText();
                String strPostalCode = txtPostalCode.getText();
                if (oController.submitApplication(strName, strNif, strTel, strEmail, strStreet, strPostalCode, strLocality)) {
                        Utils.openAlert("Application Submited", "Application Submited", "", Alert.AlertType.INFORMATION);
                        FxUtils.switchLogin();
                }
        }

        @FXML
        private void handleCancel(ActionEvent event) throws IOException, InvalidAvailabilityException {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Are you sure you want to cancel?");
                alert.setHeaderText("Do you want to cancel the application?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.isPresent()) {
                        if (result.get() == ButtonType.OK) {
                                FxUtils.switchLogin();
                        }
                }
        }
}
