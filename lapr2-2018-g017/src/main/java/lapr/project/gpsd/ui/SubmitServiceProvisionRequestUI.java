package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import lapr.project.gpsd.controller.SubmitServiceProvisionRequestController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.PromptedServiceDescription;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.Service;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class SubmitServiceProvisionRequestUI implements Initializable {

        private SubmitServiceProvisionRequestController oController;

        @FXML
        private Label lblTitle;

        @FXML
        private TextField txtName;

        @FXML
        private TextField txtNif;

        @FXML
        private TextField txtTel;

        @FXML
        private TextField txtEmail;

        @FXML
        private ChoiceBox<Address> cbxAddress;

        @FXML
        private ChoiceBox<Category> cbxCategory;

        @FXML
        private ChoiceBox<Service> cbxService;

        @FXML
        private ChoiceBox<String> cbxDuration;

        @FXML
        private TextArea txtDescription;

        @FXML
        private Button btnAddService;

        @FXML
        private DatePicker dpDate;

        @FXML
        private ChoiceBox<String> cbxHour;

        @FXML
        private Button btnAddSchedule;

        @FXML
        private TableView<PromptedServiceDescription> tblServices;

        @FXML
        private TableColumn<PromptedServiceDescription, String> colService;

        @FXML
        private TableColumn<PromptedServiceDescription, String> colDuration;

        @FXML
        private TableColumn<PromptedServiceDescription, String> colDescription;

        @FXML
        private TableView<SchedulePreference> tblSchedules;

        @FXML
        private TableColumn<SchedulePreference, Integer> colOrder;

        @FXML
        private TableColumn<SchedulePreference, String> colDate;

        @FXML
        private TableColumn<SchedulePreference, String> colHour;

        @FXML
        private Button btnCancel;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new SubmitServiceProvisionRequestController();
                } catch (InvalidAvailabilityException ex) {
                }
                populateClient();
                populateAddresses();
                populateHour();
                listenerAddresses();
                listenerServices();
                listenerDuration();
                listenerDate();
        }

        private void populateClient() {
                txtName.setText(this.oController.getClientName());
                txtNif.setText(this.oController.getClientNif());
                txtTel.setText(this.oController.getClientTelephone());
                txtEmail.setText(this.oController.getUserEmail());
        }

        private void listenerAddresses() {
                cbxAddress.valueProperty().addListener((obs, oldAddress, newAddress) -> {
                        if (newAddress == null) {
                                cbxCategory.getItems().clear();
                                cbxCategory.setDisable(true);
                                cbxService.getItems().clear();
                                cbxService.setDisable(true);
                                cbxDuration.getItems().clear();
                                cbxDuration.setDisable(true);
                                tblServices.getItems().clear();
                                tblSchedules.getItems().clear();
                                this.oController.setRequest(null);
                        } else {
                                cbxCategory.getItems().clear();
                                cbxCategory.setDisable(false);
                                cbxCategory.getSelectionModel().clearSelection();
                                populateCategory();
                                cbxService.getItems().clear();
                                cbxService.getSelectionModel().clearSelection();
                                cbxService.setDisable(false);
                                cbxDuration.getItems().clear();
                                cbxDuration.setDisable(false);
                                cbxDuration.getSelectionModel().clearSelection();
                                this.oController.setRequest(newAddress);
                                updateRequestPreviewTables();
                                updateSchedulePreviewTables();
                        }
                });
        }

        private void listenerServices() {
                cbxCategory.valueProperty().addListener((obs, oldCategory, newCategory) -> {
                        if (newCategory == null) {
                                cbxService.getItems().clear();
                                cbxService.setDisable(true);
                                cbxDuration.getItems().clear();
                                cbxDuration.setDisable(true);
                        } else {
                                cbxService.getItems().setAll(this.oController.getServices(newCategory));
                                cbxService.getSelectionModel().clearSelection();
                                cbxService.setDisable(false);
                                cbxDuration.getItems().clear();
                                cbxDuration.setDisable(false);
                                cbxDuration.getSelectionModel().clearSelection();
                        }
                });
        }

        private void listenerDuration() {
                cbxService.valueProperty().addListener((obs, oldService, newService) -> {
                        if (newService == null) {
                                cbxDuration.setItems(FXCollections.observableArrayList(this.oController.getDurations()));
                                cbxDuration.setDisable(true);
                        } else if (newService.hasOtherAtributes()) {
                                cbxDuration.setItems(FXCollections.observableArrayList(this.oController.getDurations()));
                                cbxDuration.setValue(newService.getOtherAtributes());
                                cbxDuration.setDisable(true);
                        } else {
                                cbxDuration.setItems(FXCollections.observableArrayList(this.oController.getDurations()));
                                cbxDuration.setDisable(false);
                        }
                });
        }

        private void listenerDate() {
                dpDate.setDayCellFactory(picker -> new DateCell() {
                        @Override
                        public void updateItem(LocalDate date, boolean empty) {
                                super.updateItem(date, empty);
                                LocalDate today = LocalDate.now();
                                setDisable(empty || date.compareTo(today) < 0);
                        }
                });
        }

        private void populateAddresses() {
                cbxAddress.setItems(FXCollections.observableArrayList(this.oController.getAddresses()));
        }

        private void populateCategory() {
                cbxCategory.setItems(FXCollections.observableArrayList(this.oController.getCategories()));
        }

        private void populateHour() {
                cbxHour.setItems(FXCollections.observableArrayList(this.oController.getHours()));
        }

        private void updateRequestPreviewTables() {
                if (this.oController.getRequest() != null) {
                        colService.setCellValueFactory(new PropertyValueFactory<>("serviceString"));
                        colDuration.setCellValueFactory(new PropertyValueFactory<>("duration"));
                        colDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
                        ObservableList<PromptedServiceDescription> list = FXCollections
                                .observableArrayList(this.oController.getRequest().getRequestedServices());
                        tblServices.setItems(list);
                }
        }

        private void updateSchedulePreviewTables() {
                if (this.oController.getRequest() != null) {
                        colOrder.setCellValueFactory(new PropertyValueFactory<>("order"));
                        colDate.setCellValueFactory(new PropertyValueFactory<>("date"));
                        colHour.setCellValueFactory(new PropertyValueFactory<>("hour"));
                        ObservableList<SchedulePreference> list = FXCollections.observableArrayList(this.oController.getRequest().getSchedules());
                        tblSchedules.setItems(list);
                }
        }

        @FXML
        private void handleAddService(ActionEvent event) {
                Service oService = cbxService.getSelectionModel().getSelectedItem();
                String strDuration = cbxDuration.getSelectionModel().getSelectedItem();
                String strDescription = txtDescription.getText();
                PromptedServiceDescription oPromptedService = this.oController.validateService(oService, strDuration, strDescription);
                if (oPromptedService != null) {
                        if (this.oController.addServiceToRequest(oPromptedService)) {
                                Utils.openAlert("Service Added to the Request successfully", "Service Details",
                                        "Service(ID:" + oService.getId() + ") - " + oService.getBriefDescription() + "\nDuration: " + strDuration
                                        + "h\nEstimated Cost: " + Utils.DECIMAL_FORMAT.format(oPromptedService.getCost())
                                        + "€\n\n\nOBS: Estimated Cost does not include Travel Expenses.",
                                        Alert.AlertType.INFORMATION);
                                cbxCategory.setValue(null);
                                cbxService.setValue(null);
                                cbxDuration.setValue(null);
                                txtDescription.setText(null);
                                updateRequestPreviewTables();
                        }
                }
        }

        @FXML
        private void handleAddSchedule(ActionEvent event) throws ParseException {
                String strDate = dpDate.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                String strHour = cbxHour.getValue();
                SchedulePreference oSchedule = this.oController.validateSchedule(strDate, strHour);
                if (oSchedule != null) {
                        if (this.oController.addScheduleToRequest(oSchedule)) {
                                Utils.openAlert("Schedule Added to the Request successfully", "Schedule Details",
                                        "Order - " + oSchedule.getOrder() + "\nDate: " + strDate + " (" + Utils.getWeekDay(strDate) + ")"
                                        + "\nHour: " + oSchedule.getHour() + "h",
                                        Alert.AlertType.INFORMATION);
                                dpDate.setValue(null);
                                cbxHour.setValue(null);
                                updateSchedulePreviewTables();
                        }
                }
        }

        @FXML
        private void handleCancel(ActionEvent event) throws IOException, InvalidAvailabilityException {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Service Provision Request");
                alert.setHeaderText("Do you want to cancel the request?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.isPresent() && result.get() == ButtonType.OK) {
                        this.oController.setRequest(null);
                        FxUtils.switchDashboard(event, this.oController.getRole(), this.oController.getUserEmail());
                }
        }

        @FXML
        private void handleSubmit(ActionEvent event) throws IOException, InvalidAvailabilityException {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Service Provision Request");
                alert.setHeaderText("Do you want to Submit the Request?");
                alert.setContentText("Total Cost: " + this.oController.calculateCost() + "€");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.isPresent() && (result.get() == ButtonType.OK)) {
                        if (this.oController.validateRequest()) {
                                this.oController.registerRequest();
                                FxUtils.switchDashboard(event, this.oController.getRole(), this.oController.getUserEmail());
                        }
                }
        }
}
