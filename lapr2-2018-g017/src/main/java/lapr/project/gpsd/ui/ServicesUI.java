package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lapr.project.gpsd.controller.ServicesController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceType;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServicesUI implements Initializable {
    
	private ServicesController oController;
	
	@FXML
        private Label lblTitle;
	
        @FXML
        private TableView<Service> tableServices;

        @FXML
        private TableColumn<Service, String> colId;

        @FXML
        private TableColumn<Service, String> colBriefDesc;
    
        @FXML
        private TableColumn<Service, String> colCompleteDesc;

        @FXML
        private TableColumn<Service, String> colCostHour;

        @FXML
        private TableColumn<Service, Category> colCategory;
    
        @FXML
        private TableColumn<Service, ServiceType> colServiceType;
    
        @FXML
        private Button btnDashboard;

        @Override
	public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new ServicesController();
                } catch (InvalidAvailabilityException ex) {
                }
		populateServices();
	}

        @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, oController.getRole(), oController.getEmail());
        }
    
        private void populateServices() {
                colId.setCellValueFactory(new PropertyValueFactory<>("id"));
                colBriefDesc.setCellValueFactory(new PropertyValueFactory<>("briefDescription"));
                colCompleteDesc.setCellValueFactory(new PropertyValueFactory<>("completeDescription"));
                colCostHour.setCellValueFactory(new PropertyValueFactory<>("costHour"));
                colCategory.setCellValueFactory(new PropertyValueFactory<>("category"));
		colServiceType.setCellValueFactory(new PropertyValueFactory<>("serviceType"));
		ObservableList<Service> list = FXCollections.observableArrayList(this.oController.getServices());
		tableServices.setItems(list);
        }

	
}