package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import lapr.project.auth.model.Role;
import lapr.project.gpsd.controller.RoleController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class RoleUI implements Initializable {

        private RoleController oController;

        @FXML
        private ChoiceBox<Role> cbxRoles;

        @FXML
        private Button btnContinue;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new RoleController();
                } catch (InvalidAvailabilityException ex) {
                }
                populateRoles();
        }

        private void populateRoles() {
                List<Role> lstRoles = this.oController.getUserRoles();
                cbxRoles.setItems(FXCollections.observableArrayList(lstRoles));
        }

        @FXML
        private void handleChooseRole(ActionEvent event) throws IOException, InvalidAvailabilityException {
                if (cbxRoles.getValue() != null) {
                        Role oRole = cbxRoles.getValue();
                        String strRole = oRole.getRole();
                        String strEmail = this.oController.getEmail();
                        FxUtils.switchDashboard(event, strRole, strEmail);
                        oController.setRole(strRole);
                } else {
                        Utils.openAlert("Role Choosing", "Role Choosing",
                                "Choose a Role to proceed to the Dashboard.", AlertType.ERROR);
                }
        }
}
