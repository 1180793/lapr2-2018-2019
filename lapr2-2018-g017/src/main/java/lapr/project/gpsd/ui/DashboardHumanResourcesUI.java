package lapr.project.gpsd.ui;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DashboardHumanResourcesUI {

    @FXML
    private Button btnRegisterServiceProvider;

    @FXML
    private Button btnRegisteredClients;

    @FXML
    private Button btnEvaluateServiceProvider;
    
    @FXML
    private Button btnRegisteredProviders;

    @FXML
    private void handleRegisterServiceProvider(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "RegisterServiceProviderUI",
				"Register a Service Provider", true);
    }

    @FXML
    private void handleRegisteredClients(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "RegisteredClientsUI",
				"Registered Clients", true);
    }

    @FXML
    private void handleEvaluateServiceProvider(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "EvaluateServiceProvider",
				"Service Provider Evaluation", true);
    }

    @FXML
    private void handleRegisteredProviders(ActionEvent event) throws IOException {
        FxUtils.switchScene(event, "RegisteredProvidersUI",
				"Registered Service Providers", true);
    }

}

