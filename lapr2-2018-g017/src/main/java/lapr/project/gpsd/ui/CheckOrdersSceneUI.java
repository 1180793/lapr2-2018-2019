/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListView;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.controller.CheckOrdersController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.ExportType;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 *
 * @author ZDPessoa
 */
public class CheckOrdersSceneUI implements Initializable {

    private final CheckOrdersController controller;

    private List<ExportType> ExportList;

    private ExportType ExportType;

    ExportType ExportTypeDefault = new ExportType();

    @FXML
    private DatePicker CO_Date1;

    @FXML
    private DatePicker CO_Date2;

    @FXML
    private Button CO_Button1;

    @FXML
    private ComboBox<ExportType> CO_ComboBox;

    @FXML
    private ListView<String> CO_ListView;

    ObservableList<ExportType> ExportTypeList;
    ObservableList<String> ServiceOrderList = FXCollections.observableArrayList();

    public CheckOrdersSceneUI() {

        this.controller = new CheckOrdersController();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            ExportList = controller.checkOrders();
        } catch (IOException ex) {
            Logger.getLogger(CheckOrdersSceneUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidAvailabilityException ex) {
            Logger.getLogger(CheckOrdersSceneUI.class.getName()).log(Level.SEVERE, null, ex);
        }

        ExportTypeList = FXCollections.observableArrayList(ExportList);

        CO_ComboBox.setItems(ExportTypeList);
    }

    @FXML
    private void handleCheckOrders(ActionEvent event) throws IOException {

        List<ServiceOrder> ServiceOrderListAux;

        if (CO_Date1.getValue() != null && CO_Date2.getValue() != null) {

            CO_ListView.getItems().clear();
            LocalDate lDate1 = CO_Date1.getValue();
            LocalDate lDate2 = CO_Date2.getValue();

            if (lDate1.isBefore(lDate2)) {

                ServiceOrderListAux = controller.newCheck(lDate1.toString(), lDate2.toString());

                if (!ServiceOrderListAux.isEmpty()) {

                    for (ServiceOrder so : ServiceOrderListAux) {

                        ServiceOrderList.add(so.DisplaytoString());

                    }
                }

                CO_ListView.setItems(ServiceOrderList);

            } else {
                Utils.openAlert("Date Error", "Error Selecting the Dates",
                        "Starting Date can't be higher than Ending Date", Alert.AlertType.ERROR);
            }
        } else {
            Utils.openAlert("Date Error", "Error Selecting the Dates",
                    "Please Select both a Start and End Date", Alert.AlertType.ERROR);
        }

    }

    @FXML
    private void handleExportComboBox(ActionEvent event) {

        if (!CO_ComboBox.getItems().contains(ExportTypeDefault)) {
            CO_ComboBox.getItems().addAll(ExportTypeDefault);
        }

        ExportType = CO_ComboBox.getValue();

    }

    @FXML
    private void handleConfirm(ActionEvent event) throws IOException, InvalidAvailabilityException {

        if (!ServiceOrderList.isEmpty()) {
            if (!(ExportType == null || ExportType.equals(ExportTypeDefault))) {
                controller.newExport(ExportType);
                if (controller.validate()) {
                    controller.exportFile();
                }
            }
        }
        exit(event);
    }

    @FXML
    private void handleBack(ActionEvent event) throws IOException, InvalidAvailabilityException {

        exit(event);
    }

    private void exit(ActionEvent event) throws IOException, InvalidAvailabilityException {

        AppGPSD app = controller.getApp();

        String strRole = app.getRole();
        FxUtils.switchDashboard(event, strRole, app.getCurrentSession().getUserEmail());
    }

}
