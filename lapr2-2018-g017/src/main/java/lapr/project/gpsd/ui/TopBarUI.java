package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.stage.Window;
import javafx.stage.WindowEvent;

import lapr.project.gpsd.controller.TopBarController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class TopBarUI implements Initializable {

	private TopBarController oController;

	@FXML
	private MenuItem itmSwitchRole;

	@FXML
	private void handleSwitchRole(ActionEvent event) throws IOException, InvalidAvailabilityException {
		FxUtils.switchRole();
	}

	@FXML
	private void doLogout(ActionEvent event) throws IOException, InvalidAvailabilityException {
		oController.doLogout();
		FxUtils.switchLogin();
	}

	@FXML
	private void closeApp(ActionEvent event) {
		Window window = this.oController.getStage();
                window.fireEvent(new WindowEvent(window, WindowEvent.WINDOW_CLOSE_REQUEST));
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new TopBarController();
                } catch (InvalidAvailabilityException ex) {
                }
		if (!this.oController.hasMoreThanOneRole()) {
			itmSwitchRole.setDisable(true);
		}
	}

}
