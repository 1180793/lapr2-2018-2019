package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import lapr.project.gpsd.controller.DashboardClientController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DashboardClientUI implements Initializable {

        private DashboardClientController oController;

        @FXML
        private Button btnSubmitRequests;

        @FXML
        private Button btnAvailableServices;

        @FXML
        private Button btnNewAddress;

        @FXML
        private Button btnMyRequests;

        @FXML
        private Button btnRateService;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new DashboardClientController();
                } catch (InvalidAvailabilityException ex) {
                }
        }

        @FXML
        private void handleSubmitRequest(ActionEvent event) throws IOException {
                FxUtils.switchScene(event, "SubmitServiceProvisionRequestUI",
                        "Submit Service Provision Request", true);
        }

        @FXML
        private void handleNewAddress(ActionEvent event) throws IOException {
                FxUtils.switchScene(event, "AssociatePostalAddress",
                        "Associate new Address", true);
        }

        @FXML
        private void handleMyRequests(ActionEvent event) throws IOException {
                if (this.oController.clientHasRequests()) {
                        Utils.openAlert("Error Opening Service Provision Requests", "Error Opening Service Provision Requests",
                                "You don't have any Service Provision Requests", Alert.AlertType.ERROR);
                } else {
                        FxUtils.switchScene(event, "MyServiceRequestsUI",
                                "My Service Provision Requests", true);
                }
        }

        @FXML
        private void handleAvailableServices(ActionEvent event) throws IOException {
                FxUtils.switchScene(event, "AvailableServicesUI",
                        "Available Services", true);
        }

        @FXML
        private void handleRateService(ActionEvent event) throws IOException {
                FxUtils.switchScene(event, "ServiceRateUI", "Service Rating", true);
        }

}
