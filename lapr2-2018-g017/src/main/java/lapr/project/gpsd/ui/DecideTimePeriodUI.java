/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.controller.DecideTimePeriodController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Attribuition;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * FXML Controller class
 *
 * @author Lenovo
 */
public class DecideTimePeriodUI implements Initializable {

    private DecideTimePeriodController oController;

    @FXML
    private Label lblTitle;
    @FXML
    private ChoiceBox<Attribuition> cbxServices;
    @FXML
    private Button btnAccept;
    @FXML
    private Button btnDecline;
    @FXML
    private TextArea txtAttribuition;
    @FXML
    private Button btnDashboard;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            this.oController = new DecideTimePeriodController();
            populateServices();
            listenerAttribuitions();
        } catch (InvalidAvailabilityException ex) {

        }
    }

    public void populateServices() {
        cbxServices.setItems(FXCollections.observableArrayList(this.oController.getAttribuitionList()));

    }

    private void listenerAttribuitions() {
        cbxServices.valueProperty().addListener((obs, oldAttribuition, newAttribuition) -> {
            if (newAttribuition == null) {
                txtAttribuition.setText("");
            } else {
                txtAttribuition.setText(newAttribuition.toString());
            }
        });
    }

    @FXML
    private void handleAccept(ActionEvent event) {

        Attribuition att = cbxServices.getValue();
        ServiceOrder order = oController.acceptAttribuition(att);
        if (order != null) {

            Utils.openAlert("The Service Order was registered with the number " + order.getSequentialNumber(), "The Service Order was registered with the number " + order.getSequentialNumber(), "", Alert.AlertType.ERROR);
            populateServices();
        }

    }

    @FXML
    private void handleDecline(ActionEvent event) {
        Attribuition att = cbxServices.getValue();
        boolean bol = oController.rejectAttribuition(att);
        
    }

    @FXML
    private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
        FxUtils.switchDashboard(event, oController.getRole(), oController.getEmail());
    }

}
