package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import lapr.project.gpsd.controller.GiveRatingController;

import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceRating;
import lapr.project.utils.FxUtils;
import lapr.project.utils.Utils;

/**
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class GiveRatingUI implements Initializable {

        private GiveRatingController oController;

        @FXML
        private Label lblTitle;

        @FXML
        private TextField txtName;

        @FXML
        private TextField txtNif;

        @FXML
        private TextField txtTel;

        @FXML
        private TextField txtEmail;

        @FXML
        private ChoiceBox<ServiceOrder> cbxOrders;

        @FXML
        private ChoiceBox<Integer> cbxRating;

        @FXML
        private Button btnRate;

        @FXML
        private Button btnRemoveRating;

        @FXML
        private TextArea txtInvoice;

        @FXML
        private TableView<ServiceRating> tblRatings;

        @FXML
        private TableColumn<ServiceRating, String> colServiceOrder;

        @FXML
        private TableColumn<ServiceRating, Integer> colRate;

        @FXML
        private Button btnDashboard;

        @Override
        public void initialize(URL url, ResourceBundle rb) {
                try {
                        this.oController = new GiveRatingController();
                } catch (InvalidAvailabilityException ex) {
                }
                populateClient();
                populateOrders();
                populateRatings();
                listenerOrders();
                updateRatingsTable();
        }

        private void populateClient() {
                txtName.setText(this.oController.getClientName());
                txtNif.setText(this.oController.getClientNif());
                txtTel.setText(this.oController.getClientTelephone());
                txtEmail.setText(this.oController.getEmail());
        }

        private void populateOrders() {
                cbxOrders.setItems(FXCollections.observableArrayList(this.oController.getOrders()));
        }

        private void populateRatings() {
                cbxRating.setItems(FXCollections.observableArrayList(this.oController.getRatings()));
        }

        private void updateRatingsTable() {
                colServiceOrder.setCellValueFactory(new PropertyValueFactory<>("serviceOrderString"));
                colRate.setCellValueFactory(new PropertyValueFactory<>("rate"));
                ObservableList<ServiceRating> list = FXCollections.observableArrayList(this.oController.getClientRating());
                tblRatings.setItems(list);
        }

        private void listenerOrders() {
                cbxOrders.valueProperty().addListener((obs, oldValue, newValue) -> {
                        if (newValue == null) {
                                cbxRating.getItems().clear();
                                cbxRating.setDisable(true);
                                txtInvoice.setText("No Service Order Selected");
                                txtInvoice.setDisable(true);

                        } else {
                                txtInvoice.setDisable(false);
                                txtInvoice.setText(newValue.showInvoice());
                                cbxRating.setDisable(false);
                                populateRatings();
                        }
                });
        }

        @FXML
        private void handleRate(ActionEvent event) {
                ServiceOrder oOrder = cbxOrders.getSelectionModel().getSelectedItem();
                int intRate = cbxRating.getValue();
                ServiceRating oRating = this.oController.submitRating(oOrder, intRate);
                if (oRating != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Service Rating");
                        alert.setHeaderText("Do you want to Submit the Rating?");
                        alert.setContentText("Rating: " + cbxRating.getValue());
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && result.get() == ButtonType.OK) {
                                if (this.oController.registerRating(oRating)) {
                                        Utils.openAlert("Service Rating", "Service Order Rated", "The Selected Order was rated.", Alert.AlertType.INFORMATION);
                                        updateRatingsTable();
                                }
                        }
                }
        }

        @FXML
        private void handleRemoveRating(ActionEvent event) throws IOException {
                ServiceOrder oOrder = cbxOrders.getSelectionModel().getSelectedItem();
                ServiceRating oRating = this.oController.removeRating(oOrder);
                if (oRating != null) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Service Rating");
                        alert.setHeaderText("Do you want to remove the rating?");
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && result.get() == ButtonType.OK) {
                                if (this.oController.confirmRemoveRating(oRating)) {
                                        Utils.openAlert("Service Rating", "Service Order Rating Removed", "The Rating for the Selected Order was removed.", Alert.AlertType.INFORMATION);
                                        updateRatingsTable();
                                }
                        }
                }
        }

        @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, this.oController.getRole(), this.oController.getEmail());
        }

}
