package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import lapr.project.gpsd.controller.RegisterServiceProviderController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.GeographicArea;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class RegisterServiceProviderUI implements Initializable {

        private RegisterServiceProviderController oController;

        @FXML
        private Label lblTitle;

        @FXML
        private TextField txtNif;

        @FXML
        private Button btnFetchApplication;

        @FXML
        private TextField txtFullName;

        @FXML
        private TextField txtAbbreviatedName;

        @FXML
        private TextField txtPersonnelNumber;

        @FXML
        private TextField txtEmail;

        @FXML
        private TextField txtTel;

        @FXML
        private TextField txtStreet;

        @FXML
        private TextField txtLocality;

        @FXML
        private TextField txtPostalCode;

        @FXML
        private ChoiceBox<Category> cbxCategory;

        @FXML
        private Button btnAddCategory;

        @FXML
        private Button btnRemoveCategory;

        @FXML
        private ChoiceBox<GeographicArea> cbxGeographicArea;

        @FXML
        private Button btnAddGeographicArea;

        @FXML
        private Button btnRemoveGeographicArea;

        @FXML
        private TableView<Category> tblCategories;

        @FXML
        private TableColumn<Category, String> colId;

        @FXML
        private TableColumn<Category, String> colDescription;

        @FXML
        private TableView<GeographicArea> tblGeographicAreas;

        @FXML
        private TableColumn<GeographicArea, String> colDesignation;

        @FXML
        private TableColumn<GeographicArea, String> colPostalCode;

        @FXML
        private Button btnSubmit;

        @FXML
        private Button btnCancel;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new RegisterServiceProviderController();
                } catch (InvalidAvailabilityException ex) {
                }
                populateChoiceBoxes();
        }

        private void populateChoiceBoxes() {
                cbxCategory.setItems(FXCollections.observableArrayList(oController.getCategories()));
                cbxGeographicArea.setItems(FXCollections.observableArrayList(oController.getGeographicAreas()));
        }

        @FXML
        private void handleFetchApplication(ActionEvent event) {
                String strNif = txtNif.getText();
                if (oController.getApplication(strNif)) {
                        populateServiceProviderInfoByApplication();
                }
        }

        private void populateServiceProviderInfoByApplication() {
                if (this.oController.getApplication() != null) {
                        txtFullName.setText(this.oController.getApplication().getName());
                        txtTel.setText(this.oController.getApplication().getTelephone());
                        txtEmail.setText(this.oController.getApplication().getEmail());
                        txtStreet.setText(this.oController.getApplication().getAddress().getStreet());
                        txtLocality.setText(this.oController.getApplication().getAddress().getLocality());
                        txtPostalCode.setText(this.oController.getApplication().getAddress().getPostalCodeString());
                        this.oController.addCategoryFromApplication();
                        updateCategoriesTable();
                }
        }

        private void populateAddress(Address oAddress) {
                if (oAddress != null) {
                        txtStreet.setText(oAddress.getStreet());
                        txtLocality.setText(oAddress.getLocality());
                        txtPostalCode.setText(oAddress.getPostalCodeString());
                }
        }

        private void updateCategoriesTable() {
                colId.setCellValueFactory(new PropertyValueFactory<>("id"));
                colDescription.setCellValueFactory(new PropertyValueFactory<>("description"));
                ObservableList<Category> list = FXCollections.observableArrayList(this.oController.getProviderCategories());
                tblCategories.setItems(list);
        }

        private void updateGeographicAreasTable() {
                colDesignation.setCellValueFactory(new PropertyValueFactory<>("designation"));
                colPostalCode.setCellValueFactory(new PropertyValueFactory<>("postalCodeString"));
                ObservableList<GeographicArea> list = FXCollections.observableArrayList(this.oController.getProviderGeographicAreas());
                tblGeographicAreas.setItems(list);
        }

        @FXML
        private void handleAddCategory(ActionEvent event) {
                Category oCategory = cbxCategory.getValue();
                if (this.oController.addCategory(oCategory)) {
                        updateCategoriesTable();
                }
        }

        @FXML
        private void handleRemoveCategory(ActionEvent event) {
                Category oCategory = tblCategories.getSelectionModel().getSelectedItem();
                if (this.oController.removeCategory(oCategory)) {
                        updateCategoriesTable();
                }
        }

        @FXML
        private void handleAddGeographicArea(ActionEvent event) {
                GeographicArea oGeographicArea = cbxGeographicArea.getValue();
                if (this.oController.addGeographicArea(oGeographicArea)) {
                        updateGeographicAreasTable();
                }
        }

        @FXML
        private void handleRemoveGeographicArea(ActionEvent event) {
                GeographicArea oGeographicArea = tblGeographicAreas.getSelectionModel().getSelectedItem();
                if (this.oController.removeGeographicArea(oGeographicArea)) {
                        updateGeographicAreasTable();
                }
        }

        @FXML
        private void handleCancel(ActionEvent event) throws IOException, InvalidAvailabilityException {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("Are you sure you want to cancel?");
                alert.setHeaderText("Do you want to cancel the Service Provider Registration?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.isPresent()) {
                        if (result.get() == ButtonType.OK) {
                                this.oController.cancel();
                                FxUtils.switchDashboard(event, this.oController.getRole(), this.oController.getUserEmail());
                        }
                }
        }

        @FXML
        private void handleSubmit(ActionEvent event) throws IOException, InvalidAvailabilityException {

                String strPersonnelNumber = txtPersonnelNumber.getText();
                String strNif = txtNif.getText();
                String strFullName = txtFullName.getText();
                String strAbbreviatedName = txtAbbreviatedName.getText();
                String strEmail = txtEmail.getText();
                String strTel = txtTel.getText();
                String strStreet = txtStreet.getText();
                String strLocality = txtLocality.getText();
                String strPostalCode = txtPostalCode.getText();

                Address oAddress = this.oController.getAddress(strStreet, strLocality, strPostalCode);
                ServiceProvider oProvider = this.oController.submitRegistration(strPersonnelNumber, strNif, strFullName, strAbbreviatedName, strEmail, strTel, oAddress);
                populateAddress(oAddress);

                if (oProvider != null) {

                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Are you sure you want to register the Service Provider?");
                        alert.setHeaderText("Do you want to submit the registration?");
                        alert.setContentText("Full Name: " + strFullName
                                + "\nAbbreviated Name: " + strAbbreviatedName
                                + "\nNIF: " + strNif + "\tPersonnel Number: " + strPersonnelNumber
                                + "\nEmail: " + strEmail + "\tTelephone: " + strTel
                                + "\nStreet Address: " + oAddress.getStreet()
                                + "\nLocality: " + oAddress.getLocality() + "\tPostal Code: " + oAddress.getPostalCodeString());

                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent()) {
                                if (result.get() == ButtonType.OK) {
                                        if (this.oController.confirmRegistration(oProvider)) {
                                                FxUtils.switchDashboard(event, this.oController.getRole(), this.oController.getUserEmail());
                                        }
                                }
                        }
                }
        }

}
