package lapr.project.gpsd.ui;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;

import lapr.project.gpsd.controller.EvaluateServiceProviderController;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProviderEvaluation;
import lapr.project.utils.FxUtils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class EvaluateServiceProviderUI implements Initializable {

        private EvaluateServiceProviderController oController;

        @FXML
        private Label lblTitle;

        @FXML
        private ChoiceBox<ServiceProvider> cbxProvider;

        @FXML
        private Label lblGlobalRating;

        @FXML
        private Label lblGlobalDeviation;

        @FXML
        private Label lblProviderName;

        @FXML
        private BarChart<?, ?> chartRatings;

        @FXML
        private NumberAxis axisNumberOfRatings;

        @FXML
        private CategoryAxis axisRatings;

        @FXML
        private Label lblPersonalRating;

        @FXML
        private Label lblPersonalDeviation;

        @FXML
        private Button btnEvaluate;

        @FXML
        private Button btnDashboard;

        @Override
        public void initialize(URL location, ResourceBundle resources) {
                try {
                        this.oController = new EvaluateServiceProviderController();
                } catch (InvalidAvailabilityException ex) {
                }
                populateProviders();
                listenerProviders();
                populateGlobalValues();
        }

        private void populateProviders() {
                cbxProvider.setItems(FXCollections.observableArrayList(this.oController.getProviders()));
        }

        private void listenerProviders() {
                cbxProvider.valueProperty().addListener((obs, oldProvider, newProvider) -> {
                        if (newProvider == null) {
                                clearChart();
                                clearProviderValues();
                        } else {
                                populateChart(newProvider);
                                populateProviderValues(newProvider);
                        }
                });
        }

        private void populateGlobalValues() {
                lblGlobalRating.setText(this.oController.getGlobalProviderRatingMean());
                lblGlobalDeviation.setText(this.oController.getGlobalProviderRatingDeviation());
        }

        private void clearChart() {
                XYChart.Series newSeries = new XYChart.Series<>();
                chartRatings.getData().setAll(newSeries);
        }

        private void clearProviderValues() {
                lblPersonalRating.setText("");
                lblPersonalDeviation.setText("");
        }

        private void populateChart(ServiceProvider oProvider) {
                XYChart.Series chartSeries = new XYChart.Series<>();
                chartSeries.getData().add(new XYChart.Data("0", (this.oController.getProviderServiceRatingsByRate(oProvider, 0))));
                chartSeries.getData().add(new XYChart.Data("1", (this.oController.getProviderServiceRatingsByRate(oProvider, 1))));
                chartSeries.getData().add(new XYChart.Data("2", (this.oController.getProviderServiceRatingsByRate(oProvider, 2))));
                chartSeries.getData().add(new XYChart.Data("3", (this.oController.getProviderServiceRatingsByRate(oProvider, 3))));
                chartSeries.getData().add(new XYChart.Data("4", (this.oController.getProviderServiceRatingsByRate(oProvider, 4))));
                chartSeries.getData().add(new XYChart.Data("5", (this.oController.getProviderServiceRatingsByRate(oProvider, 5))));
                chartRatings.getData().setAll(chartSeries);
        }

        private void populateProviderValues(ServiceProvider oProvider) {
                lblProviderName.setText(oProvider.getFullName() + "'s Ratings");
                lblPersonalRating.setText(this.oController.getProviderRatingAverage(oProvider));
                lblPersonalDeviation.setText(this.oController.getProviderRatingDeviation(oProvider));
        }

        @FXML
        private void handleEvaluate(ActionEvent event) {
                ServiceProvider oProvider = cbxProvider.getValue();
                if (this.oController.validateProvider(oProvider)) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        alert.setTitle("Service Provider Evaluation");
                        alert.setHeaderText("Do you accept the System Evaluation?");
                        alert.setContentText("System Evaluation: " + this.oController.getServiceProviderEvaluation(oProvider));
                        ButtonType buttonYes = new ButtonType("Yes", ButtonData.YES);
                        ButtonType buttonNo = new ButtonType("No", ButtonData.NO);
                        ButtonType buttonCancel = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
                        alert.getButtonTypes().setAll(buttonYes, buttonNo, buttonCancel);
                        Optional<ButtonType> result = alert.showAndWait();
                        if (result.isPresent() && (result.get() == buttonYes)) {
                                this.oController.acceptSystemEvaluation(oProvider);
                        } else if (result.isPresent() && (result.get() == buttonNo)) {
                                List<ServiceProviderEvaluation> lstEvaluations = this.oController.getEvaluations();
                                ServiceProviderEvaluation oSystemEvaluation = this.oController.getServiceProviderEvaluation(oProvider);
                                ChoiceDialog<ServiceProviderEvaluation> dialog = new ChoiceDialog<>(oSystemEvaluation, lstEvaluations);
                                dialog.setTitle("Service Provider Evaluation");
                                dialog.setHeaderText("Choose an Evaluation for the Service Provider");
                                dialog.setContentText("Evaluation:");
                                Optional<ServiceProviderEvaluation> result2 = dialog.showAndWait();
                                if (result2.isPresent()) {
                                        ServiceProviderEvaluation oEvaluation = result2.get();
                                        this.oController.evaluateProvider(oProvider, oEvaluation);
                                }
                        }

                }
        }

        @FXML
        private void handleDashboard(ActionEvent event) throws IOException, InvalidAvailabilityException {
                FxUtils.switchDashboard(event, this.oController.getRole(), this.oController.getUserEmail());
        }

}
