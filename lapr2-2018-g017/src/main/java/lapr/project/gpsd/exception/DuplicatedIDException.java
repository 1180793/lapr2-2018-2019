package lapr.project.gpsd.exception;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DuplicatedIDException extends Exception {
        
        public DuplicatedIDException(String message) {
                super(message);
        }
}
