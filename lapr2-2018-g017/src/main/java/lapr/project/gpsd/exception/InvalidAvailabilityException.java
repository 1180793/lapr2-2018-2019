package lapr.project.gpsd.exception;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class InvalidAvailabilityException extends Exception {
	
	public InvalidAvailabilityException(String message) {
		super(message);
	}
}