package lapr.project.gpsd.model;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public enum ApplicationStatus {

	PENDING("Pending Human Resources Officer Decision"), ACCEPTED("Verified and Accepted"), REJECTED("Verified and Rejected");

	private final String strStatus;

	private ApplicationStatus(String strStatus) {
		this.strStatus = strStatus;
	}
        
	public String getStatus() {
		return this.strStatus;
	}

        @Override
	public String toString() {
		return String.format("%s", this.strStatus);
	}
}
