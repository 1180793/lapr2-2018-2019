package lapr.project.gpsd.model;

import java.util.Objects;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ProfessionalQualification {
    
        private final String strDescription;
    
        public ProfessionalQualification(String strDescription) {
                this.strDescription = strDescription;
        }
    
        public String getDescription() {
                return this.strDescription;
        }   
        
        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                ProfessionalQualification obj = (ProfessionalQualification) o;
                return (Objects.equals(strDescription, obj.strDescription));
        }

        @Override
        public int hashCode() {
                int hash = 7;
                hash = 13 * hash + Objects.hashCode(this.strDescription);
                return hash;
        }
        
        @Override
        public String toString() {
                return String.format("%s", this.strDescription);
        }

}
