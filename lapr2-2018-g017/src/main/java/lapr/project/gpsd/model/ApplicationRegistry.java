package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ApplicationRegistry {

        private final List<Application> lstApplications = new ArrayList<>();

        public List<Application> getApplications() {
                return this.lstApplications;
        }

        public Application newApplication(String strName, String strNIF, String strTelephone, String strEmail,
                Address oAddress) {
                return new Application(strName, strNIF, strTelephone, strEmail, oAddress);
        }

        public boolean addApplication(Application oApplication) {
                if (oApplication != null && validateApplication(oApplication)) {
                        return lstApplications.add(oApplication);
                }
                return false;
        }

        public Application getApplicationByNif(String strNif) {
                for (Application oApplication : this.lstApplications) {
                        if (oApplication.hasNif(strNif)) {
                                return oApplication;
                        }
                }
                return null;
        }

        public boolean validateApplication(Application oApplication) {
                boolean bRet = true;
                for (Application app : this.lstApplications) {
                        if (app.getEmail().equalsIgnoreCase(oApplication.getEmail())) {
                                bRet = false;
                        }
                        if (app.getNif().equalsIgnoreCase(oApplication.getNif())) {
                                bRet = false;
                        }
                }
                return bRet;
        }

}
