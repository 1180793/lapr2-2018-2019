package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.exception.DuplicatedIDException;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class GeographicAreaRegistry {

        private final List<GeographicArea> lstGeographicAreas = new ArrayList<>();

        /**
         * Method to get a list of geographic areas
         *
         * @return lstGeographicalAreas: list of geographic areas
         */
        public List<GeographicArea> getGeographicAreas() {
                return this.lstGeographicAreas;
        }

        /**
         * Method to get the geographic area by id
         *
         * @param strId: id to look for
         * @return aGeo: if the id matches a geographic area
         */
        public GeographicArea getGeographicAreaById(String strId) {
                for (GeographicArea aGeo : this.lstGeographicAreas) {
                        if (aGeo.getPostalCode().getPostalCodeString().equalsIgnoreCase(strId)) {
                                return aGeo;
                        }
                }
                return null;
        }
        
        /**
         * Method to get the geographic area by designation
         *
         * @param strDesignation: designation to look for
         * @return aGeo: if the designation matches a geographic area
         */
        public GeographicArea getGeographicAreaByDesignation(String strDesignation) {
                for (GeographicArea aGeo : this.lstGeographicAreas) {
                        if (aGeo.getDesignation().trim().equalsIgnoreCase(strDesignation.trim())) {
                                return aGeo;
                        }
                }
                return null;
        }

        /**
         * Method to register a Geographic area
         *
         * @param oGeographicArea: geographic area to register
         * @throws lapr.project.gpsd.exception.DuplicatedIDException
         * @return adds the geographic area
         */
        public boolean registerGeographicArea(GeographicArea oGeographicArea) throws DuplicatedIDException {
                if (this.validateGeographicArea(oGeographicArea)) {
                        return addGeographicArea(oGeographicArea);
                } else {
                        throw new DuplicatedIDException("Duplicated Geographic Area");
                }
        }

        /**
         * Method to add a geographic area to the list of geographic areas
         *
         * @param oGeographicArea
         * @return true: if the geographic area is added to the list | false: if the geographic area is not added to the list
         */
        public boolean addGeographicArea(GeographicArea oGeographicArea) {
                return lstGeographicAreas.add(oGeographicArea);
        }

        /**
         * Method to validate a geographic area
         *
         * @param oGeographicArea: geographic area to validate
         * @return true: if the area is validated false: if the area already exists
         */
        public boolean validateGeographicArea(GeographicArea oGeographicArea) {
                boolean bRet = true;
                for (GeographicArea geoArea : this.lstGeographicAreas) {
                        if (oGeographicArea.getPostalCode().equals(geoArea.getPostalCode())) {
                                bRet = false;
                        }
                }
                return bRet;
        }

        /**
         * Method to get the closest geographic area according to a postal code
         *
         * @param oPostalCode: postal code to compare
         * @return closesteGeographicArea: geographic area closeste to the postal code
         */
        public GeographicArea getClosestGeographicArea(PostalCode oPostalCode) {
                double minor = Double.MAX_VALUE;
                GeographicArea closesteGeographicArea = null;
                for (GeographicArea geoArea : this.lstGeographicAreas) {
                        PostalCode postalCode = geoArea.getPostalCode();
                        double dblDistance = Utils.getDistance(postalCode.getLatitude(), postalCode.getLongitude(),
                                oPostalCode.getLatitude(), oPostalCode.getLongitude());
                        if (dblDistance < minor) {
                                minor = dblDistance;
                                closesteGeographicArea = geoArea;
                        }
                }
                return closesteGeographicArea;
        }

        /**
         * Creates an Instance of GeographicArea using the designation, Postal Code, travel costs, radius, and the Actuation List
         *
         * @param desig Area Designation
         * @param posCode Postal Code
         * @param cost Travel Costs
         * @param radius Actuation Radius
         * @param es External Service
         * @return GeographicArea
         */
        public GeographicArea newGeographicArea(String desig, PostalCode posCode, double cost, double radius, ExternalService es) {

                List<ActsIn> ActsInAux;

                ActsInAux = es.getAct(posCode, radius);

                return new GeographicArea(desig, posCode, cost, radius, ActsInAux);

        }
}
