/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.List;

/**
 * Schedulind Algorithm Interface to Assign Service Provider
 * 
 * @author Bodynho Oliveira
 */
public interface SchedulingAlgorithm {
    
    List<Attribuition> getAssignedProvider(ServiceProvider provider, ServiceProvisionRequest request);
}
