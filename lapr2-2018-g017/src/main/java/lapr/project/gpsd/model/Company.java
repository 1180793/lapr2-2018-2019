package lapr.project.gpsd.model;

import lapr.project.auth.AuthFacade;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Company {

        /**
         * The Designation of the company
         */
        private final String strDesignation;

        /**
         * The NIF of the Company
         */
        private final String strNIF;

        /**
         * The AuthFacade
         */
        private final AuthFacade oAuth;

        /**
         * The espefications/components of the company
         */
        private final ClientRegistry oClients;
        private final CategoryRegistry oCategories;
        private final ServiceRegistry oServices;
        private final ServiceTypeRegistry oServiceTypes;
        private final GeographicAreaRegistry oGeographicAreas;
        private final ServiceProviderRegistry oProviders;
        private final ServiceProvisionRequestRegistry oRequests;
        private final ServiceRatingRegistry oServiceRating;
        private final ApplicationRegistry oApplications;
        private final AttribuitionRegistry oAttribuition;
        private final ServiceOrderRegistry oServiceOrder;
        private final ExportTypeRegistry oExportType;
        private final WorkReportRegistry oWorkReport;
        private final ExternalService oExternalService;
        

        /**
         * Creates an instance of Company receiving designation and NIF as parameter
         *
         * @param strDesignation: designation of the company
         * @param strNIF: nif of the company
         */
        public Company(String strDesignation, String strNIF) {
                if ((strDesignation == null) || (strNIF == null) || (strDesignation.isEmpty()) || (strNIF.isEmpty())) {
                        throw new IllegalArgumentException("The Company's Designation and NIF can't be null.");
                }

                this.strDesignation = strDesignation;
                this.strNIF = strNIF;

                this.oAuth = new AuthFacade();

                this.oClients = new ClientRegistry(oAuth);
                this.oCategories = new CategoryRegistry();
                this.oServices = new ServiceRegistry();
                this.oServiceTypes = new ServiceTypeRegistry();
                this.oGeographicAreas = new GeographicAreaRegistry();
                this.oProviders = new ServiceProviderRegistry(oAuth);
                this.oRequests = new ServiceProvisionRequestRegistry();
                this.oServiceRating = new ServiceRatingRegistry();
                this.oApplications = new ApplicationRegistry();
                this.oAttribuition = new AttribuitionRegistry();
                this.oServiceOrder = new ServiceOrderRegistry();
                this.oExportType = new ExportTypeRegistry();
                this.oWorkReport = new WorkReportRegistry();
                this.oExternalService = new ExternalServicePorto();
        }

        /**
         * Method to return the Textual information of the Company
         *
         * @return textual information of the Company
         */
        @Override
        public String toString() {
                return String.format("Company - %s %nTax Identification Number:%s", this.strDesignation, this.strNIF);
        }

        // ##############################################################################################################
        // Auth Facade
        public AuthFacade getAuthFacade() {
                return this.oAuth;
        }

        // ##############################################################################################################
        // Client Registry
        public ClientRegistry getClientRegistry() {
                return this.oClients;
        }

        // ##############################################################################################################
        // Geographic Area Registry
        public GeographicAreaRegistry getGeographicAreaRegistry() {
                return this.oGeographicAreas;
        }

        // ##############################################################################################################
        // Category Registry
        public CategoryRegistry getCategoryRegistry() {
                return this.oCategories;
        }

        // ##############################################################################################################
        // Service Registry
        public ServiceRegistry getServiceRegistry() {
                return this.oServices;
        }

        // ##############################################################################################################
        // Service Type Registry
        public ServiceTypeRegistry getServiceTypeRegistry() {
                return this.oServiceTypes;
        }

        // ##############################################################################################################
        // Service Provider Registry
        public ServiceProviderRegistry getServiceProviderRegistry() {
                return this.oProviders;
        }

        // ##############################################################################################################
        // Service Provision Request Registry
        public ServiceProvisionRequestRegistry getRequestRegistry() {
                return this.oRequests;
        }
        //###############################################################################################################
        // Service Rating Registry

        public ServiceRatingRegistry getServiceRatingRegistry() {
                return this.oServiceRating;
        }

        //###############################################################################################################
        // Application Registry
        public ApplicationRegistry getApplicationRegistry() {
                return this.oApplications;
        }

        //###############################################################################################################
        // AttribuitionRegistry
        public AttribuitionRegistry getAttribuitionRegistry() {
                return this.oAttribuition;
        }
        
        /**
         * Returns the Registry with the Service Orders
         * @return ServiceOrderRegistry
         */
        public ServiceOrderRegistry getServiceOrderRegistry(){
            return oServiceOrder;
        }
        
        /**
         * Returns the Export Type Registry
         * @return ExportTypeRegistry
         */
        public ExportTypeRegistry getExportTypeRegistry(){
            
            return oExportType;
        }
        
        /**
         * Returns the Work Report Registry
         * @return WorkReportRegistry
         */
        public WorkReportRegistry getWorkReportRegistry(){
            
            return oWorkReport;
        }

        /**
         * Returns the External Service for Porto
         *
         * @return ExternalService
         */
        public ExternalService getExternalService() {
                return this.oExternalService;
        }
}
