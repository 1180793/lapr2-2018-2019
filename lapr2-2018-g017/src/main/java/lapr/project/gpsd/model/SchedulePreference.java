package lapr.project.gpsd.model;

import java.text.ParseException;
import java.util.Date;
import lapr.project.utils.Utils;

/**
 * Represents the Schedule Preference
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class SchedulePreference {

    /**
     * Order of schedule preference.
     */
    private final int intOrder;

    /**
     * Date of preference.
     */
    private final String strDate;

    /**
     * Hour of preference.
     */
    private final String strHour;

    /**
     * Creates an instance of a Schedule Preference receiving Order, Date and
     * Hour by parameter.
     *
     * @param intOrder: order of preference
     * @param strDate: date of preference
     * @param strHour: hour of preference
     */
    public SchedulePreference(int intOrder, String strDate, String strHour) {
        this.intOrder = intOrder;
        this.strDate = strDate;
        this.strHour = strHour;
    }

    /**
     * Method to return the order of preference of the schedule.
     *
     * @return intOrder +1: order of preference
     */
    public int getOrder() {
        return this.intOrder + 1;
    }

    /**
     * Method to return the date and the day of the week of preference for the
     * service.
     *
     * @return strDate: date of the preference schedule
     * @throws java.text.ParseException
     */
    public String getDateString() throws ParseException {
        return this.strDate + " (" + Utils.getWeekDay(strDate) + ")";
    }

    /**
     * Method to return the date of preference for the service.
     *
     * @return strDate: date of the preference schedule
     */
    public String getDate() {
        return this.strDate;
    }

    /**
     * Method to return the hour of preference for the service.
     *
     * @return strHour: hour of the preference schedule
     */
    public String getHour() {
        return this.strHour;
    }

    /**
     * Verifies the Schedule to see if it's between dateI and dateF
     *
     * @param dateI Initial Date
     * @param dateF Final Date
     * @return True if dateF > date > dateI | False Otherwise
     */
    public boolean verifyDate(String dateI, String dateF) {
        
        String[] sDateI = dateI.split("-");
        String[] sDateF = dateF.split("-");
        
        String[] sDateSchelude = this.strDate.split("/");
        
        Date aDateI = new Date(Integer.parseInt(sDateI[0]), Integer.parseInt(sDateI[1])-1,
                Integer.parseInt(sDateI[2]));
        Date aDateF = new Date(Integer.parseInt(sDateF[0]), Integer.parseInt(sDateF[1])-1,
                Integer.parseInt(sDateF[2]));
        Date aDateS = new Date(Integer.parseInt(sDateSchelude[2]), Integer.parseInt(sDateSchelude[1])-1,
                Integer.parseInt(sDateSchelude[0]));
                
        return aDateI.before(aDateS) && aDateS.before(aDateF);

    }

    /**
     * Method to return the textual representation of the Schedule Preference.
     *
     * @return textual representation of the Schedule Preference
     */
    @Override
    public String toString() {
        return String.format("%s %s", this.strDate, this.strHour);
    }
}
