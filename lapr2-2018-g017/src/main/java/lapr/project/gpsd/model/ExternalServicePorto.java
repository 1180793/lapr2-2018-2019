/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import lapr.project.utils.Utils;

/**
 *
 * @author ZDPessoa
 */
public class ExternalServicePorto implements ExternalService {

    private static final String FILE_NAME = "codigopostal_alt_long.csv";
    private static final int LAT_INDEX = 10;
    private static final int LONG_INDEX = 11;
    private static final int FOUND_ADDRESS_INDEX = 13;
    
    /**
     * Method used by "getAcs" to split a entry from the file
     * @param line Line
     * @return String List
     */
    private static List<String> getRecordFromLine(String line) {
        List<String> values = new ArrayList<>();
        try (Scanner rowScanner = new Scanner(line)) {
            rowScanner.useDelimiter(";");
            while (rowScanner.hasNext()) {
                values.add(rowScanner.next());
            }
        }
        return values;
    }

    /**
     * Gets the actuation List For the Given Postal code and radius
     * @param pc Postal Code
     * @param radius Radius
     * @return 
     */
    @Override
    public List<ActsIn> getAct(PostalCode pc, double radius) {

        List<ActsIn> ActListAux = new ArrayList<>();
        List<String> StringListAux;

        double distance;
        double lat;
        double log;

        try (Scanner scanner = new Scanner(new File(FILE_NAME))) {
            while (scanner.hasNextLine()) {

                StringListAux = getRecordFromLine(scanner.nextLine());

                lat = Double.parseDouble(StringListAux.get(LAT_INDEX));
                log = Double.parseDouble(StringListAux.get(LONG_INDEX));

                distance = Utils.getDistance(pc.getLatitude(), pc.getLongitude(), lat, log);
                
                if(distance < radius){

                    ActsIn ActAux = new ActsIn(new PostalCode(StringListAux.get(FOUND_ADDRESS_INDEX), lat, log), distance);

                    ActListAux.add(ActAux);
                }

            }
        } catch (FileNotFoundException ex) {
            return ActListAux;
        }

        return ActListAux;
    }

}
