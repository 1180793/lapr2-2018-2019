package lapr.project.gpsd.model;

/**
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class ServiceRating {

        private final ServiceOrder oOrder;
        private final int rate;

        /**
         * Creates an instance of Service Rating that receive
         *
         * @param rate: rate of the service
         * @param order: service order
         */
        public ServiceRating(int rate, ServiceOrder order) {
                this.rate = rate;
                this.oOrder = order;
        }

        /**
         * Method to return the rate
         *
         * @return this.rate: rate of the service
         */
        public int getRate() {
                return this.rate;
        }

        /**
         * Method to return the client that requested the service
         *
         * @return this.client: client that requested the service
         */
        public Client getClient() {
                return this.oOrder.getClient();
        }

        /**
         * Method to return the requested service
         *
         * @return this.promptedService: requested service that is goign to be rated
         */
        public PromptedServiceDescription getRequestedService() {
                return this.oOrder.getService();
        }
        
        public ServiceOrder getServiceOrder() {
                return this.oOrder;
        }
        
        public int getServiceOrderNumber() {
                return this.oOrder.getSequentialNumber();
        }
        
        public String getServiceOrderString() {
                return String.format("Service Order no.%d", this.oOrder.getSequentialNumber());
        }

        @Override
        public String toString() {
                return String.format("Service Provided by: %s%nRating: %d", this.oOrder.getServiceProvider().getAbbreviatedName(), this.rate);
        }

}
