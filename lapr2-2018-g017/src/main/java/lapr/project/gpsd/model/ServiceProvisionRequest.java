package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.exception.InvalidAvailabilityException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServiceProvisionRequest {

        private int intNumber;
        private final Client oClient;
        private final Address oAddress;
        private List<PromptedServiceDescription> lstRequestedServices;
        private List<SchedulePreference> lstSchedules;
        private List<OtherCost> lstOtherCosts = new ArrayList<>();
        private boolean bReady;

        /**
         * Method to create an instance of service provision request
         *
         * @param oClient: client that requested the service
         * @param oAddress : address that the client requested to be served
         */
        public ServiceProvisionRequest(Client oClient, Address oAddress) {
                this.oClient = oClient;
                this.oAddress = oAddress;
                this.lstRequestedServices = new ArrayList<>();
                this.lstSchedules = new ArrayList<>();
                this.bReady = false;
        }

        public PromptedServiceDescription getRequestedService(Service oService) {
                for (PromptedServiceDescription prompted : lstRequestedServices) {
                        if (prompted.getService().equals(oService)) {
                                return prompted;
                        }
                }
                return null;
        }

        /**
         * Method to set ready if the service is ready or not
         *
         * @param bReady : true if the service is ready and false if not
         */
        public void setReady(boolean bReady) {
                this.bReady = bReady;
        }

        /**
         * Method to check if the request is done
         *
         * @return true if the request is ready and false if not
         */
        public boolean isReady() {
                return this.bReady;
        }

        /**
         * Method to add a service to a request
         *
         * @param oService: service to add
         * @param strDescription: description of the service to add
         * @param strDuration: duration of the service to add
         * @return true if the service is added to the list and false if not
         */
        public boolean addServiceToRequest(Service oService, String strDescription, String strDuration) {
                PromptedServiceDescription oRequestedService = new PromptedServiceDescription(oService, strDescription, strDuration);
                if (validateServiceRequest(oRequestedService)) {
                        return this.lstRequestedServices.add(oRequestedService);
                }
                return false;
        }

        /**
         * Method to add a service to a request passing a Prompted service description through parameter
         *
         * @param oRequestedService: service to add to the list
         * @return true if the service is added to the list and false if not
         */
        public boolean addServiceToRequest(PromptedServiceDescription oRequestedService) {
                if (validateServiceRequest(oRequestedService)) {
                        return this.lstRequestedServices.add(oRequestedService);
                }
                return false;
        }

        /**
         * Method to validate a service request
         *
         * @param oRequestedService: service to validate
         * @return true if the service is valid and false if not
         */
        public boolean validateServiceRequest(PromptedServiceDescription oRequestedService) {
                boolean bRet = true;
                for (PromptedServiceDescription requests : this.lstRequestedServices) {
                        if (requests.getService().equals(oRequestedService.getService())) {
                                bRet = false;
                        }
                }
                return bRet;
        }

        /**
         * Method to add a schedule preference
         *
         * @param strDate: date of the preference
         * @param strHour: start hour of the preference
         * @return new SchedulePreference with the attributes passed through parameter
         */
        public SchedulePreference createSchedule(String strDate, String strHour) {
                int intOrder = countSchedules();
                return new SchedulePreference(intOrder, strDate, strHour);
        }

        /**
         * Method to add a schedule to the schedule list
         *
         * @param strDate: date of the schedule to add
         * @param strHour: hour of the schedule to add
         * @return true if the schedule is added to the list and false if not
         */
        public boolean addSchedule(String strDate, String strHour) {
                int intOrder = countSchedules();
                SchedulePreference oSchedule = new SchedulePreference(intOrder, strDate, strHour);
                return this.lstSchedules.add(oSchedule);
        }

        /**
         * Method to add a schedule to the schedule list passing a SchedulePreference through parameter
         *
         * @param oSchedule: schedule to add to the list
         * @return true if the schedule is added anda false if not
         */
        public boolean addSchedule(SchedulePreference oSchedule) {
                return this.lstSchedules.add(oSchedule);
        }

        /**
         * Method to count the schedules in the list of schedules
         *
         * @return number of schedules in the list
         */
        private int countSchedules() {
                return this.lstSchedules.size();
        }

        /**
         * Method to get the client that requested the service
         *
         * @return the client that requested the service
         */
        public Client getClient() {
                return this.oClient;
        }

        /**
         * Method to get the address that the service was requested for
         *
         * @return the address of the service
         */
        public Address getAddress() {
                return this.oAddress;
        }

        /**
         * Method to get the status of the service
         *
         * @return "ready for execution" if the service is ready to execute and "waiting for service provider attribuition" if no service provider is yet attributed
         */
        public String getStatus() {
                if (this.bReady) {
                        return "Ready For Execution";
                } else {
                        return "Waiting for Service Provider Atribuition";
                }
        }

        /**
         * Returns the List of Services Requested
         *
         * @return List of Requested Services
         */
        public List<PromptedServiceDescription> getServiceDesciprionList() {

                return this.lstRequestedServices;
        }

        /**
         * Method to calculate the cost of the service
         *
         * @return dblCost: cost of the service
         * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
         */
        public double calculateCost() throws InvalidAvailabilityException {
                double dblCost = 0;
                for (PromptedServiceDescription requests : this.lstRequestedServices) {
                        dblCost = dblCost + requests.getService().getCostForDuration(requests.getDuration());
                }
                GeographicAreaRegistry oGeographicAreas = AppGPSD.getInstance().getCompany().getGeographicAreaRegistry();
                PostalCode oPostalCode = this.oAddress.getPostalCode();
                GeographicArea geoArea = oGeographicAreas.getClosestGeographicArea(oPostalCode);
                double travelExpenses = geoArea.getTravelExpenses();
                OtherCost otherCost = new OtherCost("Travel Expenses", travelExpenses);
                this.lstOtherCosts.add(otherCost);
                for (OtherCost costs : this.lstOtherCosts) {
                        dblCost = dblCost + costs.getCost();
                }
                return dblCost;
        }

        /**
         * Method to get the number of the service
         *
         * @return the number of the service
         */
        public int getNumber() {
                return this.intNumber;
        }

        /**
         * Method to get the address of the requested service
         *
         * @return the address of the requested service
         */
        public String getAddressString() {
                return this.oAddress.toString();
        }

        /**
         * Method to get the requested services
         *
         * @return the list of requested services
         */
        public List<PromptedServiceDescription> getRequestedServices() {
                return this.lstRequestedServices;
        }

        /**
         * Method to get the schedule list
         *
         * @return
         */
        public List<SchedulePreference> getSchedules() {
                return this.lstSchedules;
        }

        /**
         * Method to set a number for the service
         *
         * @param intNumber : number to set for the service
         */
        public void setNumber(int intNumber) {
                this.intNumber = intNumber;
        }

        public boolean hasClient(Client oClient) {
                return oClient.getEmail().equals(this.oClient.getEmail());
        }

        /**
         * Sets The ServiceStatus on the corresponding Service form the Request to DONE
         *
         * @param serv Completed Service
         * @return True if the Service is Set to Done | False Otherwise
         */
        public boolean setServiceStatusDone(Service serv) {

                boolean done = true;

                for (PromptedServiceDescription sd : this.lstRequestedServices) {

                        if (sd.getService().equals(serv)) {
                                sd.setDone(done);
                                return true;
                        }

                }

                return false;
        }

        /**
         * Verifies the request to see if it's between dateI and dateF
         *
         * @param dateI Initial Date
         * @param dateF Final Date
         * @return True if dateF > ServiceRequest > dateI | False Otherwise
         */
        public boolean verifyRequest(String dateI, String dateF) {

                //First Instace from the Schedule List
                int i = 0;

                return lstSchedules.get(i).verifyDate(dateI, dateF);
        }

        @Override
        public String toString() {
                return String.format("%s", this.intNumber);
        }

        public List<PromptedServiceDescription> getDoneServices() {
                List<PromptedServiceDescription> doneRequests = new ArrayList<>();

                for (PromptedServiceDescription servDesc : this.lstRequestedServices) {
                        if (servDesc.isDone()) {
                                doneRequests.add(servDesc);
                        }

                }

                return doneRequests;
        }

}
