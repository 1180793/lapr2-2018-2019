package lapr.project.gpsd.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author ZDPessoa
 */
public class Data implements Serializable{
    
    /**
     * Year in the date
     */
    private int year;
    
    /**
    * Month in the date
    */
    private int month;
     
    /**
    * Day in the date
    */
    private int day;
        
    /**
    * Year by default
    */
    private static final int YEAR_BY_DEFAULT = 1;
    
    /**
    * Month by default
    */
    private static final int MONTH_BY_DEFAULT = 1;
    
    /**
    * Day by default
    */
    private static final int DAY_BY_DEFAULT = 1;
    
    /**
    * Number of days for each month in a year.
    */
    private static int[] daysPerMonth = {  0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 
                                        31, 30, 31};
    
    /**
    * Month names
    */
    private static String[] nameMonth = {"Invalid", "January", "February",
                                       "March", "April", "May", "June",
                                       "July", "August", "September",
                                       "October", "November", "December"};
   
    /**
     * Creates an instance of date using year month and day
     * 
     * @param year year from the date
     * @param month month from the date
     * @param day day from the date
     */
    public Data(int year, int month, int day) {        
        this.year = year;
        this.month = month;
        this.day = day;              
    }

    /**
     * Creates an instance using the values by default
     * 
     */
    public Data() {
        year = YEAR_BY_DEFAULT;
        month = MONTH_BY_DEFAULT;
        day = DAY_BY_DEFAULT;
    }
    
    @Override
    public String toString() {
        return String.format("%d/%s/%d", day, nameMonth[month], year);
    }
    
    /**
     * Returns the date`s year
     * 
     * @return year from the date
     */
    public int getYear() {
        return year;
    }
    
    /**
     * Returns the date`s month
     * 
     * @return month form the date
     */
    public int getMonth() {
        return month;
    }

    /**
     * Returns the date` day
     * 
     * @return day from the date
     */    
    public int getDay() {
        return day;
    }
    
    /**
     * Modifies the date`s year, month and day
     * 
     * @param year new year for the date
     * @param month new month for the date
     * @param day new day for the date
     */    
    public void setData(int year, int month, int day) {
        this.year = year;         
        this.month = month;       
        this.day = day;          
    }
    
    /**
     * Checks if a year is leap year
     * @param year year to validate
     * @return True if the year is a leap year | False Otherwise
     */        
    public static boolean isLeapYear(int year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }
    
    /**
     * Returns the difference in days counting from 1/1/1
     * @return number of days
     */
    public int countDays() {
        
        int totalDays = 0;

        for (int i = 1; i < year; i++) {
            totalDays += isLeapYear(i) ? 366 : 365;
        }
        for (int i = 1; i < month; i++) {
            totalDays += daysPerMonth[i];
        }
        totalDays += (isLeapYear(year) && month > 2) ? 1 : 0;
      
        totalDays += day;
        
        return totalDays;
    }
    
    /**
     * Compares both dates to check which one is bigger
     * @param date2 date to compare
     * @return true if the first date is bigger | False Otherwise
     */
    public boolean isBigger(Data date2) {
        int totalDays = countDays();
        int totalDays1 = date2.countDays();

        return totalDays >= totalDays1;
    }
    
    /**
     * Compares both dates to check which one is smaller
     * @param date2 date to compare
     * @return true if the first date is smaller | False Otherwise
     */
    public boolean isSmaller(Data date2) {
        int totalDays = countDays();
        int totalDays1 = date2.countDays();

        return totalDays <= totalDays1;
    }
    
    /**
     * Calculates the Difference in days between two dates
     * @param date2 date to compare
     * @return difference in days
     */
    public int calcualteDifference(Data date2) {
        int totalDays = countDays();
        int totalDays1 = date2.countDays();

        return  (totalDays - totalDays1);
    }
    
    /**
     * Calculates the difference in hours between two dates a staring hour and an ending hour
     * @param dateI date to compare
     * @param horai starting hour
     * @param horaf ending hour
     * @return number of hours
     */
    public int difDataHora(Data dateI, int horai, int horaf){
        
        int total;
        
        total = calcualteDifference(dateI) * 24;
        
        total -= horai;
        total += horaf;
        
        return total;
        
    }
    
    /**
     * Receives a String representing a Date in the form of YYYY-MM-DD and converts it to "Data"
     * @param strDate Date in form of String
     * @return instance of Data
     */
    public static Data parseDate(String strDate){
        
        int day = Integer.parseInt( strDate.split("-")[0] );
        int month = Integer.parseInt( strDate.split("-")[1] );
        int year =  Integer.parseInt( strDate.split("-")[2] );
        
        Data dateAux = new Data(year, month, day);
        
        return dateAux; 
    }
    
    @Override
    public boolean equals(Object o) {
        
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (getClass() != o.getClass()) {
            return false;
        }
        Data obj = (Data) o;
        return (Objects.equals(day, obj.day) || Objects.equals(month, obj.month) || Objects.equals(year, obj.year));
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.year;
        hash = 29 * hash + this.month;
        hash = 29 * hash + this.day;
        return hash;
    }
    
}
