package lapr.project.gpsd.model;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public interface Service {

        String getId();

        String getBriefDescription();

        String getCompleteDescription();

        boolean hasId(String strId);

        ServiceType getServiceType();

        Category getCategory();

        boolean hasOtherAtributes();

        String getOtherAtributes();

        void setOtherAtributes(String strOtherAtributes);

        double getCostForDuration(String strDuration);

        String getCostHour();

}
