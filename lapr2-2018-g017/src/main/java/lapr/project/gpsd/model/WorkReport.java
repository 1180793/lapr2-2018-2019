package lapr.project.gpsd.model;

/**
 *
 * @author ZDPessoa
 */
public class WorkReport {
    
    /**
     * Order Number
     */
    private int orderNumber;
    /**
     * Problem
     */
    private String problem;
    /**
     * Strategy
     */
    private String strategy;
    /**
     * Extra Hours
     */
    private int extraExecHours;
    /**
     * Global Evaluation
     */
    private String eval; 
    
    /**
     * Problem & Strategy when the service when as expected
     */
    private static final String PROBLEM_STRAT_BY_DEFAULT = "";
    /**
     * Extra Hours By Default
     */
    private static final int E_HOURS_BY_DEFAULT = 0;
    
    /**
     * Creates an instance of WorkReport (Report) using the orderNumber, the problem, strategy, the extra Hours, And the Global Evaluation
     * @param orderNumber Order Number
     * @param problem Problem
     * @param strategy Strategy
     * @param extraExecHours Extra Hours
     * @param eval Global Evaluation
     */
    public WorkReport(int orderNumber, String problem, String strategy, int extraExecHours, String eval){
        
        this.orderNumber = orderNumber;
        this.problem = problem;   
        this.strategy = strategy;
        this.extraExecHours = extraExecHours;
        this.eval = eval;
    }
    
    /**
     * Returns the Order Number used for the Report
     * @return Number
     */
    public int getOrderNumber(){
        
        return orderNumber;
    }
    
    /**
     * Validates the Report
     * @return True - if valid | False - Otherwise
     */
    public boolean validate(){
        
        if (this.orderNumber < 0)
            return false;
        else
            return true;
        
    }
    
    @Override
    public String toString(){
        if(problem.equals(PROBLEM_STRAT_BY_DEFAULT) && strategy.equals(PROBLEM_STRAT_BY_DEFAULT) && extraExecHours == E_HOURS_BY_DEFAULT)
            return String.format("WorkReport Nº: %d\nGlobal Evaluation: %s", orderNumber, eval);
        else if(extraExecHours == E_HOURS_BY_DEFAULT)
            return String.format("WorkReport Nº: %d\nProblem: %s\nStrategy: %s\nGlobal Evaluation: %s", orderNumber, problem, strategy, eval);
        else if(problem.equals(PROBLEM_STRAT_BY_DEFAULT) && strategy.equals(PROBLEM_STRAT_BY_DEFAULT) && extraExecHours != E_HOURS_BY_DEFAULT)
            return String.format("WorkReport Nº: %d\nExtra Hours: %d\nGlobal Evaluation: %s", orderNumber, extraExecHours, eval);
        else
            return String.format("WorkReport Nº: %d\nProblem: %s\nStrategy: %s\nExtra Hours: %d\nGlobal Evaluation: %s", orderNumber, problem, strategy, extraExecHours, eval);
    }
    
    
    
}
