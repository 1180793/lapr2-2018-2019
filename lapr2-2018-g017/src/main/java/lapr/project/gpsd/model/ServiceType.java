package lapr.project.gpsd.model;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public enum ServiceType {

        STATIONARY("Stationary"), LIMITED("Limited"), EXPANDABLE("Expandable");

        private final String strDesignation;

        private ServiceType(String strDesignation) {
                this.strDesignation = strDesignation;
        }

        /**
         * Method to instance a new service
         *
         * @param strId: id of the service
         * @param strBriefDesc: brief description of the service
         * @param strCompleteDesc complete description of the service
         * @param dCost cost of the service
         * @param oCategory category of the service
         * @return new object of Expandable Service with the attributes passed through parameter
         */
        public Service newService(String strId, String strBriefDesc, String strCompleteDesc, double dCost,
                Category oCategory) {
                if (this == ServiceType.STATIONARY) {
                        return new StationaryService(strId, strBriefDesc, strCompleteDesc, dCost, oCategory);
                }
                if (this == ServiceType.LIMITED) {
                        return new LimitedService(strId, strBriefDesc, strCompleteDesc, dCost, oCategory);
                }
                if (this == ServiceType.EXPANDABLE) {
                        return new ExpandableService(strId, strBriefDesc, strCompleteDesc, dCost, oCategory);
                }
                return null;
        }

        /**
         * Method to get the designation of the service type.
         *
         * @return service designation
         */
        public String getId() {
                return this.strDesignation;
        }

        @Override
        public String toString() {
                return String.format("%s", this.strDesignation);
        }
}
