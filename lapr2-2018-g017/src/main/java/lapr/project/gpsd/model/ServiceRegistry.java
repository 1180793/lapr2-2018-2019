package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.exception.DuplicatedIDException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServiceRegistry {

        /**
         * List of services
         */
        private final List<Service> lstServices = new ArrayList<>();

        /**
         * Method to get the service list
         *
         * @return list of services
         */
        public List<Service> getServices() {
                return this.lstServices;
        }

        /**
         * Method to get a service by a id passed through parameter
         *
         * @param strId: id to look for
         * @return serv: the service with the id passed through parameter or null if the id doesn't exist
         */
        public Service getServiceById(String strId) {
                for (Service serv : this.lstServices) {
                        if (serv.hasId(strId)) {
                                return serv;
                        }
                }
                return null;
        }

        /**
         * Method to register a service
         *
         * @param oService: service to register
         * @return true if the service is added to the list of services or false if the service is not false
         * @throws DuplicatedIDException : if the service already exists
         */
        public boolean registerService(Service oService) throws DuplicatedIDException {
                if (this.validateService(oService)) {
                        return addService(oService);
                } else {
                        throw new DuplicatedIDException("Duplicated Service ID");
                }
        }

        /**
         * Method to add a service to the service list
         *
         * @param oService: service to add to the list
         * @return true if the service is added to the list and false if the service is null
         */
        private boolean addService(Service oService) {
                if (oService != null) {
                        return this.lstServices.add(oService);
                }
                return false;
        }

        /**
         * Method to validate a service
         *
         * @param oService: service to validate
         * @return true if the service is valid and false if not
         */
        public boolean validateService(Service oService) {
                boolean bRet = true;
                for (Service serv : this.lstServices) {
                        if (serv.getId().equalsIgnoreCase(oService.getId())) {
                                bRet = false;
                        }
                }
                return bRet;
        }

        /**
         * Method to get a list of service by string category passed through parameter
         *
         * @param strCategory: category to look for
         * @return lstServ : list of service of a certain category
         */
        public List<Service> getServicesByCategory(String strCategory) {
                List<Service> lstServ = new ArrayList<>();
                for (Service serv : this.lstServices) {
                        if (serv.getCategory().getId().equalsIgnoreCase(strCategory)) {
                                lstServ.add(serv);
                        }
                }
                return lstServ;
        }

        /**
         * Method to get a list of service by an object of Category passed through parameter
         *
         * @param oCategory: category object to look for
         * @return lstServ : list of service of a certain category
         */
        public List<Service> getServicesByCategory(Category oCategory) {
                String strCategory = oCategory.getId();
                return getServicesByCategory(strCategory);
        }
}
