package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Alert;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServiceProvisionRequestRegistry {

        /**
         * List of requests
         */
        private List<ServiceProvisionRequest> lstRequests = new ArrayList<>();

        /**
         * Method to get the list of requests
         *
         * @return
         */
        public List<ServiceProvisionRequest> getRequests() {
                return this.lstRequests;
        }

        /**
         * Method to creat a new instance of service provision request
         *
         * @param oClient: client that requested the service
         * @param oAddress: address for the service
         * @return new instance of ServiceProvisionRequest with attributes passed through parameter
         */
        public ServiceProvisionRequest newRequest(Client oClient, Address oAddress) {
                return new ServiceProvisionRequest(oClient, oAddress);
        }

        /**
         * Method to get the requested requests by client
         *
         * @param oClient: client to look for its requests
         * @return list of client requests
         */
        public List<ServiceProvisionRequest> getRequestsByClient(Client oClient) {
                List<ServiceProvisionRequest> lstClientRequests = new ArrayList<>();
                for (ServiceProvisionRequest oRequest : this.lstRequests) {
                        if (oRequest.getClient().equals(oClient)) {
                                lstClientRequests.add(oRequest);
                        }
                }
                return lstClientRequests;
        }
        
        public ServiceProvisionRequest getRequestByNumber(int number) {
                for (ServiceProvisionRequest oRequest : this.lstRequests) {
                        if (oRequest.getNumber() == number) {
                                return oRequest;
                        }
                }
                return null;
        }

        /**
         * Method to generate a request number
         *
         * @return a random number for the request
         */
        public int generateRequestNumber() {
                return 10000 + lstRequests.size();
        }

        /**
         * Method to register a new request
         *
         * @param oRequest : request to register
         * @return true if registered
         */
        public boolean registerRequest(ServiceProvisionRequest oRequest) {
                if (validateRequest(oRequest)) {
                        oRequest.setNumber(generateRequestNumber());
                        return addRequest(oRequest);
                }
                return false;
        }

        private boolean addRequest(ServiceProvisionRequest oRequest) {
                return this.lstRequests.add(oRequest);
        }

        /**
         * Method to notify the client
         *
         * @param oRequest request that is going to be notified
         * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
         */
        public void notifyClient(ServiceProvisionRequest oRequest) throws InvalidAvailabilityException {
                Utils.openAlert("Service Provision Request", "Service Provision Request No" + oRequest.getNumber(),
                        "Your request has been successfully registered.\n\nTotal Cost: "
                        + Utils.DECIMAL_FORMAT.format(oRequest.calculateCost()) + "€",
                        Alert.AlertType.INFORMATION);
        }

        /**
         * Method to validate a request
         *
         * @param oRequest: request to be validated
         * @return true if the request is valid and false if not
         */
        public boolean validateRequest(ServiceProvisionRequest oRequest) {
                if (oRequest == null) {
                        return false;
                } else {
                        if (oRequest.getClient() == null) {
                                return false;
                        }
                        if (oRequest.getAddress() == null) {
                                return false;
                        }
                        if (oRequest.getRequestedServices().isEmpty()) {
                                return false;
                        }
                        if (oRequest.getSchedules().isEmpty()) {
                                return false;
                        }
                        int intHigherDuration = 0;
                        for (PromptedServiceDescription service : oRequest.getRequestedServices()) {
                                for (int i = 0; i < Utils.LIST_OF_DURATIONS.length; i++) {
                                        if (service.getDuration().equalsIgnoreCase(Utils.LIST_OF_DURATIONS[i])) {
                                                if (i > intHigherDuration) {
                                                        intHigherDuration = i;
                                                }
                                        }
                                }
                        }
                        return !oRequest.getSchedules().isEmpty();
                }
        }
}
