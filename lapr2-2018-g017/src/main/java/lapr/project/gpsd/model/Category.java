package lapr.project.gpsd.model;

import java.util.Objects;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Category {

	private final String strId;
	private final String strDescription;

        /**
         * Creats an instance of Category.
         * 
         * @param strId: id of the category
         * @param strDescription: description of the category
        */
	public Category(String strId, String strDescription) {
		if ((strId == null) || (strDescription == null) || (strId.isEmpty()) || (strDescription.isEmpty()))
			throw new IllegalArgumentException("Category arguments can't be empty or null.");

		this.strId = strId;
		this.strDescription = strDescription;
	}

        /**
         * Method to check if a category has an id.
         * 
         * @param strId: category id to check
         * @return true: if the category has id
         *  or false: if the category doesnt have an id
         */
	public boolean hasId(String strId) {
		return this.strId.equalsIgnoreCase(strId);
	}

        /**
         * Method to return the id of the category.
         * 
         * @return strId: id of the category
         */
	public String getId() {
		return this.strId;
	}
        
        /**
         * Method to return the description of the category.
         * 
         * @return strDescription: description of the category
         */
	public String getDescription() {
		return this.strDescription;
	}

        @Override
        public int hashCode() {
                int hash = 7;
                hash = 29 * hash + Objects.hashCode(this.strId);
                return hash;
        }
        
        /**
         * Method to check if a category is equals to another.
         * 
         * @param o: category to compare
         * @return true: if are equals
         * or false: if aren't equals or if the class object aren't equals
         */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null)
			return false;
		if (getClass() != o.getClass())
			return false;
		Category obj = (Category) o;
		return (Objects.equals(strId, obj.strId));
	}

        /**
         * Makes the textual representation of the category id and category description.
         * 
         * @return textual representation of id and description
         */
	@Override
	public String toString() {
		return String.format("%s | %s", this.strId, this.strDescription);
	}

}
