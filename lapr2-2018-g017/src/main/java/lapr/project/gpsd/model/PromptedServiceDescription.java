package lapr.project.gpsd.model;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PromptedServiceDescription {

        private final Service oService;
        private final String strDescription;
        private final String strDuration;
        private ServiceProvider oProvider;
        private boolean isDone;

        /**
         * Creats an instance of Prompted Service Descritpion receiving as parameters
         *
         * @param oService: requested service
         * @param strDescription: description of the service
         * @param strDuration: duration of the service
         */
        public PromptedServiceDescription(Service oService, String strDescription, String strDuration) {
                this.oService = oService;
                this.strDescription = strDescription;
                this.strDuration = strDuration;
                this.oProvider = null;
                this.isDone = false;
        }

        /**
         * Method to get the provider of the service
         *
         * @return oProvider: service provider that is responsable for the service
         *
         */
        public ServiceProvider getProvider() {
                return this.oProvider;
        }

        /**
         * Method to get the name of the service provider
         *
         * @return "Not Assigned": if the service doesnt have a provider or the abreviated name of the provider if the provider exists
         */
        public String getProviderString() {
                if (this.getProvider() == null) {
                        return "Not Assigned";
                } else {
                        return this.oProvider.getAbbreviatedName();
                }
        }

        /**
         * Method to get the service
         *
         * @return oService: requested service
         */
        public Service getService() {
                return this.oService;
        }

        /**
         * Method to get the id of the service
         *
         * @return oService.getId(): gets the id of the service
         */
        public String getServiceString() {
                return "ID: " + this.oService.getId();
        }

        /**
         * Method to get the description of the service
         *
         * @return strDescription: description of the service
         */
        public String getDescription() {
                return this.strDescription;
        }

        /**
         * Method to get the duration of the service
         *
         * @return strDuration: duration of the service
         */
        public String getDuration() {
                if (oService.hasOtherAtributes()) {
                        return oService.getOtherAtributes();
                }
                return this.strDuration;
        }

        /**
         * Method that returns the cost of the service according to the duration
         *
         * @return cost according to the duration of the service
         */
        public double getCost() {
                return this.oService.getCostForDuration(strDuration);
        }

        /**
         * Method to check if a service is done
         *
         * @return true: if the service is done and false if not
         */
        public boolean isDone() {
                return this.isDone;
        }

        /**
         * Method to set if the service is done
         *
         * @param isDone: true if the service is done, false if the service is not done
         */
        public void setDone(boolean isDone) {
                this.isDone = isDone;
        }

        /**
         * Method to assign a service provider
         *
         * @param oProvider: service provider to assign
         */
        public void assignProvider(ServiceProvider oProvider) {
                this.oProvider = oProvider;
        }

        /**
         * Method to remove a service provider from the service
         *
         */
        public void removeProvider() {
                if (this.oProvider != null) {
                        this.oProvider = null;
                }
        }

}
