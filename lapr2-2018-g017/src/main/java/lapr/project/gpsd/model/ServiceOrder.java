package lapr.project.gpsd.model;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import lapr.project.utils.Utils;

/**
 * Represents the Service Order.
 *
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class ServiceOrder {

        private int intNumber;
        private final SchedulePreference oSchedule;
        private final Client oClient;
        private final Address oAddress;
        private final ServiceProvider oProvider;
        private final PromptedServiceDescription oService;
        private Date issueDate;
        private boolean isDone;

        public ServiceOrder(SchedulePreference oSchedule, Client oClient, Address oAddress, ServiceProvider servProvider, PromptedServiceDescription servDescription) {
                this.oClient = oClient;
                this.oAddress = oAddress;
                this.oSchedule = oSchedule;
                this.oProvider = servProvider;
                this.oService = servDescription;
                this.isDone = false;
        }

        public SchedulePreference getSchedulePreference() {
                return oSchedule;
        }

        public Client getClient() {
                return oClient;
        }

        public Address getAddress() {
                return oAddress;
        }

        public int getSequentialNumber() {
                return this.intNumber;
        }

        public PromptedServiceDescription getService() {
                return this.oService;
        }

        public ServiceProvider getServiceProvider() {
                return oProvider;
        }
        
        public Date getIssueDate() {
                return this.issueDate;
        }
        
        public String getServiceType(){
            
            return this.oService.getService().getClass().toString();         
        }

        public void setSequentialNumber(int number) {
                this.intNumber = number;
        }
        
        public void setIssueDate(Date date) {
                this.issueDate = date;
        }
        
        public boolean isDone() {
                return this.isDone;
        }
        
        public void setDone(boolean bDone) {
                this.isDone = bDone;
        }

        /**
         * Generates the Current Date
         *
         * @return System Date
         */
        public String getCurrentDateString() {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
                Date sysDate = new Date(System.currentTimeMillis());
                return String.format("", formatter.format(sysDate));
        }

        public Date getCurrentDate() {
                Date sysDate = new Date(System.currentTimeMillis());
                return sysDate;

        }

        /**
         * Verifies the Order to see if the request is in-between dateI and dateF
         *
         * @param dateI Initial Date
         * @param dateF Final Date
         * @return True if dateF > ServiceOrder > dateI | False Otherwise
         */
        public boolean verifyOrder(String dateI, String dateF) {
                return oSchedule.verifyDate(dateI, dateF);
        }
        
        public String showInvoice() {
                return String.format("Service Provider: %s%n"
                        + "Description of the Requested Service: %s%n"
                        + "Spent Time: %s%n"
                        + "Total Cost: %s€", this.oProvider, this.oService.getDescription(), this.oService.getDuration(), Utils.DECIMAL_FORMAT.format(this.oService.getCost()));
        }

        @Override
        public String toString() {
                return String.format("Service Order Number: %d\nDate: %s", intNumber, oSchedule.toString());
        }
        
        /**
         * toString used for Exports
         * @return String
         */
        public String CSVtoString(){
            
            return String.format("%d;%s;0;%s;%s;%s;%s;%s;%s;%s", intNumber, this.oClient.getName(), this.oService.getService().getCategory().toString(), this.oService.getService().getServiceType(),
                    this.oSchedule.getDate(), this.oSchedule.getHour(), this.oAddress.CSVtoString(), this.oAddress.getLocality(), this.oAddress.getPostalCode().CSVtoString() );
        }
        
        /**
         * toString with additional information
         * @return String
         */
        public String DisplaytoString(){
            
            return String.format("Service Order Number: %d\nDate: %s\nClient: %s\nService: %s", intNumber, oSchedule.toString(), oClient.getName(), oService.getService().getBriefDescription());
        }
        
        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                ServiceOrder obj = (ServiceOrder) o;
                return (Objects.equals(this.intNumber, obj.intNumber));
        }

        @Override
        public int hashCode() {
                int hash = 7;
                hash = 41 * hash + this.intNumber;
                return hash;
        }

}
