package lapr.project.gpsd.model;

/**
 * Represents the Contants to be in other classes.
 * 
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Constants {
    
        private Constants() {}
        
        /**
         * Role Names for the elements of the Company.
         */
        public static final String ROLE_ADMIN = "ADMIN";
        public static final String ROLE_CLIENT = "CLIENT";
        public static final String ROLE_HRO = "HUMAN_RESOURCES";
        public static final String ROLE_SP = "SERVICE_PROVIDER";
    
        /**
         * Values for the properties file, Designation and Nif of the Company.
         */
        public static final String PARAMS_FILE_CONFIG = "config.properties";
        public static final String PARAMS_COMPANY_DESIGNATION = "Company.Designation";
        public static final String PARAMS_COMPANY_NIF = "Company.NIF";
    
        /**
         * Values for the Application status.
         */
        public static final String APPLICATION_STATUS_APPROVED = "Approved";
        public static final String APPLICATION_STATUS_REJECTED = "Rejected";
        public static final String APPLICATION_STATUS_WAITING = "Waiting Admin Review";
        
        /**
         * Strings used to generate passwords.
         */
        public static final int PASSWORD_DEFAULT_LENGTH = 8;
        public static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
        public static final String NUMERIC = "0123456789";
        
        /**
         * Values for omission of type int.
         */
        public static final int DEFAULT_INT = 0;
}
