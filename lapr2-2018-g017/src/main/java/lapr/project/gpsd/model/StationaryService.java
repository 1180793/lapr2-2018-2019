package lapr.project.gpsd.model;

import java.util.Objects;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class StationaryService implements Service {

        private final String strId;
        private final String strBriefDescription;
        private final String strCompleteDescription;
        private final double dCostHour;
        private String strPredefinedDuration;
        private final Category oCategory;

        /**
         * Method to creat an instance of Stationary Service
         *
         * @param strId: id of the service
         * @param strBriefDescription brief description of the serivce
         * @param strCompleteDescription complete description of the service
         * @param dCostHour cost of an hour of the serice
         * @param oCategory category of the service
         */
        public StationaryService(String strId, String strBriefDescription, String strCompleteDescription, double dCostHour,
                Category oCategory) {
                if ((strId == null) || (strBriefDescription == null) || (strCompleteDescription == null) || (dCostHour < 0)
                        || (oCategory == null) || (strId.isEmpty()) || (strBriefDescription.isEmpty())
                        || (strCompleteDescription.isEmpty())) {
                        throw new IllegalArgumentException("Service arguments can't be empty or null.");
                }

                this.strId = strId;
                this.strBriefDescription = strBriefDescription;
                this.strCompleteDescription = strCompleteDescription;
                this.dCostHour = dCostHour;
                this.oCategory = oCategory;
        }

        @Override
        public String getId() {
                return this.strId;
        }

        @Override
        public String getBriefDescription() {
                return this.strBriefDescription;
        }

        @Override
        public String getCompleteDescription() {
                return this.strCompleteDescription;
        }

        @Override
        public ServiceType getServiceType() {
                return ServiceType.STATIONARY;
        }

        @Override
        public Category getCategory() {
                return this.oCategory;
        }

        @Override
        public boolean hasId(String strId) {
                return this.strId.equalsIgnoreCase(strId);
        }

        @Override
        public boolean hasOtherAtributes() {
                return true;
        }

        @Override
        public String getOtherAtributes() {
                return this.strPredefinedDuration;
        }

        @Override
        public void setOtherAtributes(String strOtherAtributes) {
                this.strPredefinedDuration = strOtherAtributes;
        }

        @Override
        public double getCostForDuration(String strDuration) {
                return dCostHour * Utils.convertDuration(strDuration);
        }

        @Override
        public String getCostHour() {
                return this.dCostHour + "€";
        }

        @Override
        public int hashCode() {
                int hash = 7;
                hash = 89 * hash + Objects.hashCode(this.strId);
                return hash;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                StationaryService obj = (StationaryService) o;
                return (Objects.equals(strId, obj.strId));
        }

        @Override
        public String toString() {
                return String.format("%s - %s | %s", this.strId, this.strCompleteDescription, this.getCostHour());
        }
}
