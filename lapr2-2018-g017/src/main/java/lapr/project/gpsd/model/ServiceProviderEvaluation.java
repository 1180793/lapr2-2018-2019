package lapr.project.gpsd.model;

/**
 * Represents the Rating for a Service Provider.
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public enum ServiceProviderEvaluation {

	WORST("Worst Providers"), REGULAR("Regular Providers"), OUTSTANDING("Outstanding Providers");

	private final String strEvaluation;

	private ServiceProviderEvaluation(String strEvaluation) {
		this.strEvaluation = strEvaluation;
	}
        
	public String getEvaluation() {
		return this.strEvaluation;
	}

        @Override
	public String toString() {
		return String.format("%s", this.strEvaluation);
	}
}
