/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ZDPessoa
 */
public class CSVExport implements Export {
 
    /**
     * File Name
     */
    private String fileName;
    /**
     * Order List
     */
    private List<ServiceOrder> orderList = new ArrayList<>();
    /**
     * Starting Date
     */
    private String dateI;
    /**
     * Ending Date
     */
    private String dateF;
    
    /**
     * Date By Default
     */
    private static final String DATE_BY_DEFAULT = "No Date";
    
    /**
     * Creates an instance of CSVExport receiving the file name, List of Service Orders, initial date and ending date
     * @param fileName Name of the File to be exported
     * @param orderList Lists contained in the file
     * @param dateI Initial Date
     * @param dateF Final date
     */
    public CSVExport(String fileName, List<ServiceOrder> orderList,String dateI, String dateF){
        
        this.fileName = fileName;
        this.orderList = orderList;
        this.dateI = dateI;
        this.dateF = dateF;
        
    }
    
    /**
     * Creates an instance of CSVExport receiving the file name, List of Service Orders
     * @param fileName Name of the File to be exported
     * @param orderList Lists contained in the file
     */
    public CSVExport(String fileName, List<ServiceOrder> orderList){
        
        this.fileName = fileName;
        this.orderList = orderList;
        this.dateI = DATE_BY_DEFAULT;
        this.dateF = DATE_BY_DEFAULT;
    }
    
    /**
     * Return the Name of the File
     * @return File Name
     */
    @Override
    public String getFileName(){
        
        return fileName;
    }
    
    /**
     * Modifies the Name of the file
     * @param name new file name
     */
    public void setFileName(String name){
        
        fileName = name;
    }
    
    /**
     * Checks to see if the List of Orders is empty
     * @return True if it has elements | False if empty
     */
    @Override
    public boolean validate(){
        
        if(orderList.isEmpty())
                return false;
        
        return true;
    }
    
    /**
     * Creates the File with the Provider`s Service Orders 
     * @return True if successful | False Otherwise
     */
    @Override
    public boolean ExportFile(){
        
        try(FileWriter csvWriter = new FileWriter("./src/main/resources/exports/" + fileName + ".csv")) {
            
            if(!(dateI.equals(DATE_BY_DEFAULT) || dateF.equals(DATE_BY_DEFAULT))){
                csvWriter.append("Service Orders From " + dateI + " to " + dateF);
                csvWriter.append("\n");
            }
            
            for(ServiceOrder order : orderList ){
                csvWriter.append(order.CSVtoString());
                csvWriter.append("\n");           
            }
            
            csvWriter.flush();
            csvWriter.close();  
        } catch (IOException ex) {
            return false;
        }
        
         return true;
    }
    
}
