package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Application {

        /**
         * The name of the applicant
         */
        private final String strName;

        /**
         * The NIF of the user (Applicant)
         */
        private final String strNIF;

        /**
         * The telephone of the user (Applicant)
         */
        private final String strTelephone;

        /**
         * The email of the user (Applicant)
         */
        private final String strEmail;

        /**
         * The list of address of the user (Applicant)
         *
         */
        private final Address oAddress;
        private final List<Category> lstCategories = new ArrayList<>();
        private final List<AcademicQualification> lstAcademicQualifications = new ArrayList<>();
        private final List<ProfessionalQualification> lstProfessionalQualifications = new ArrayList<>();
        private final ApplicationStatus oStatus;

        /**
         * Creats an instance of Application receiving the name, nif, telephone,email and address of the user.
         *
         * @param strName: name of the user
         * @param strNIF: nif of the user
         * @param strTelephone: telephone of the user
         * @param strEmail: email of the user
         * @param oAddress: address of the user
         */
        public Application(String strName, String strNIF, String strTelephone, String strEmail, Address oAddress) {
                if ((strName == null) || (strNIF == null) || (strTelephone == null) || (strEmail == null) || (strName.isEmpty())
                        || (strNIF.isEmpty()) || (strTelephone.isEmpty()) || (strEmail.isEmpty())) {
                        throw new IllegalArgumentException("Application arguments can't be empty or null.");
                }

                this.strName = strName;
                this.strEmail = strEmail;
                this.strNIF = strNIF;
                this.strTelephone = strTelephone;
                this.oAddress = oAddress;
                this.oStatus = ApplicationStatus.PENDING;
        }

        public String getName() {
                return this.strName;
        }

        public String getEmail() {
                return this.strEmail;
        }

        public String getNif() {
                return this.strNIF;
        }

        public String getTelephone() {
                return this.strTelephone;
        }

        public ApplicationStatus getStatus() {
                return this.oStatus;
        }
        
        public Address getAddress() {
                return this.oAddress;
        }

        public boolean hasNif(String strNIF) {
                return this.strNIF.equalsIgnoreCase(strNIF);
        }

        public Address newAddress(String strStreet, PostalCode oPostalCode, String strLocality) {
                return new Address(strStreet, oPostalCode, strLocality);
        }

        public boolean addAcademicQualification(String strDesignation) {
                AcademicQualification oAcademicQualification = new AcademicQualification(strDesignation);
                if (validateAcademicQualification(oAcademicQualification)) {
                        return lstAcademicQualifications.add(oAcademicQualification);
                }
                return false;
        }

        public boolean validateAcademicQualification(AcademicQualification oAcademicQualification) {
                boolean bRet = true;
                for (AcademicQualification academicQualification : this.lstAcademicQualifications) {
                        if (oAcademicQualification.equals(academicQualification)) {
                                bRet = false;
                        }
                }
                return bRet;
        }

        public boolean addProfessionalQualification(String strDescription) {
                ProfessionalQualification oProfessionalQualification = new ProfessionalQualification(strDescription);
                if (validateProfessionalQualification(oProfessionalQualification)) {
                        return lstProfessionalQualifications.add(oProfessionalQualification);
                }
                return false;
        }

        public boolean validateProfessionalQualification(ProfessionalQualification oProfessionalQualification) {
                boolean bRet = true;
                for (ProfessionalQualification professionalQualification : this.lstProfessionalQualifications) {
                        if (oProfessionalQualification.equals(professionalQualification)) {
                                bRet = false;
                        }
                }
                return bRet;
        }
        
        public List<Category> getCategories() {
                return this.lstCategories;
        }

        public boolean addCategory(Category oCategory) {
                if (validateCategory(oCategory)) {
                        return lstCategories.add(oCategory);
                }
                return false;
        }

        public boolean validateCategory(Category oCategory) {
                boolean bRet = true;
                for (Category category : this.lstCategories) {
                        if (oCategory.equals(category)) {
                                bRet = false;
                        }
                }
                return bRet;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                Application obj = (Application) o;
                return (Objects.equals(strEmail, obj.strEmail) || Objects.equals(strNIF, obj.strNIF));
        }

        @Override
        public int hashCode() {
                int hash = 3;
                hash = 59 * hash + Objects.hashCode(this.strNIF);
                hash = 59 * hash + Objects.hashCode(this.strEmail);
                return hash;
        }

        @Override
        public String toString() {
                return String.format("%s - %s - %s - %s", this.strName, this.strNIF, this.strTelephone,
                        this.strEmail);
        }
}
