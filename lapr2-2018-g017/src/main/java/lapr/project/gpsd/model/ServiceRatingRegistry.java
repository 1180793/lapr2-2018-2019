package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class ServiceRatingRegistry {

        private final List<ServiceRating> ratedServiceList = new ArrayList<>();

        /**
         * Method to get a list of rated services by client.
         *
         * @param oClient
         * @return clientServiceRates: list of the rated services for the oClient
         */
        public List<ServiceRating> getServiceRatingsByClient(Client oClient) {
                List<ServiceRating> clientServiceRates = new ArrayList<>();
                for (ServiceRating servRating : ratedServiceList) {
                        if (servRating.getClient().equals(oClient)) {
                                clientServiceRates.add(servRating);
                        }
                }
                return clientServiceRates;
        }

        public ServiceRating getRating(ServiceOrder oOrder) {
                for (ServiceRating servRating : ratedServiceList) {
                        if (servRating.getServiceOrder().equals(oOrder)) {
                                return servRating;
                        }
                }
                return null;
        }

        /**
         * Method to get a list of rated services by service provider.
         *
         * @param oProvider
         * @return clientServiceRates: list of the rated services for the oClient
         */
        public List<ServiceRating> getProviderServiceRatings(ServiceProvider oProvider) {
                List<ServiceRating> lstProviderRatings = new ArrayList<>();
                for (ServiceRating servRating : this.ratedServiceList) {
                        if (servRating.getServiceOrder().getServiceProvider().equals(oProvider)) {
                                lstProviderRatings.add(servRating);
                        }
                }
                return lstProviderRatings;
        }

        public double getGlobalProviderRatingMean() {
                double n = ratedServiceList.size();
                double total = 0;
                if (n == 0) {
                        return 0;
                }
                for (ServiceRating rating : ratedServiceList) {
                        total = total + rating.getRate();
                }
                return total/n;
        }

        public double getGlobalProviderRatingDeviation() {
                double standardDeviation = 0.0;
                int n = ratedServiceList.size();

                double mean = getGlobalProviderRatingMean();

                for (ServiceRating rate : ratedServiceList) {
                        standardDeviation += Math.pow(rate.getRate() - mean, 2);
                }

                return Math.sqrt(standardDeviation / n);
        }

        public double getProviderRatingAverage(ServiceProvider oProvider) {
                List<ServiceRating> lstProviderRatings = getProviderServiceRatings(oProvider);
                int n = lstProviderRatings.size();
                int total = 0;
                if (n == 0) {
                        return 3;
                }
                for (int i = 0; i < n; i++) {
                        total = +lstProviderRatings.get(i).getRate();
                }
                return total / n;
        }

        public double getProviderRatingDeviation(ServiceProvider oProvider) {
                double standardDeviation = 0.0;
                List<ServiceRating> lstProviderRatings = getProviderServiceRatings(oProvider);
                int n = lstProviderRatings.size();

                double mean = getProviderRatingAverage(oProvider);

                for (ServiceRating rate : lstProviderRatings) {
                        standardDeviation += Math.pow(rate.getRate() - mean, 2);
                }
                if (lstProviderRatings.isEmpty()) {
                        standardDeviation += Math.pow(3 - mean, 2);
                }

                return Math.sqrt(standardDeviation / n);
        }

        public ServiceProviderEvaluation getServiceProviderEvaluation(ServiceProvider oProvider) {
                double mean = getGlobalProviderRatingMean();
                double deviation = getGlobalProviderRatingDeviation();
                double providerMean = getProviderRatingAverage(oProvider);
                double minValue = mean - deviation;
                double maxValue = mean + deviation;
                if (providerMean < minValue) {
                        return ServiceProviderEvaluation.WORST;
                } else if (providerMean > maxValue) {
                        return ServiceProviderEvaluation.OUTSTANDING;
                } else {
                        return ServiceProviderEvaluation.REGULAR;
                }
        }

        /**
         * Method to get a list of service provider rated services by rating.
         *
         * @param oProvider
         * @param intRate
         * @return clientServiceRates: list of the rated services for the oClient
         */
        public List<ServiceRating> getProviderServiceRatingsByRate(ServiceProvider oProvider, int intRate) {
                List<ServiceRating> lstProviderRatings = new ArrayList<>();
                for (ServiceRating servRating : this.getProviderServiceRatings(oProvider)) {
                        if (servRating.getRate() == intRate) {
                                lstProviderRatings.add(servRating);
                        }
                }
                return lstProviderRatings;
        }

        /**
         * Method to register a service rating
         *
         * @param rate: rate of the service rating
         * @param oOrder: service order
         * @return true: if the service rating is added to the rated service list and false if not
         */
        public boolean registerServiceRating(int rate, ServiceOrder oOrder) {
                if (validateRate(rate)) {
                        ServiceRating oRating = new ServiceRating(rate, oOrder);
                        return addServiceRating(oRating);
                }
                return false;
        }

        public boolean registerServiceRating(ServiceRating oRating) {
                if (validateRate(oRating.getRate())) {
                        return addServiceRating(oRating);
                }
                return false;
        }

        public boolean validateRate(int rate) {
                return (rate <= 5 && rate >= 0);
        }

        /**
         * Method to add a service rating to the rated service list
         *
         * @param sr: service rating to add to the list
         * @return true if the service rating id added to the list and false if not
         */
        private boolean addServiceRating(ServiceRating sr) {
                return this.ratedServiceList.add(sr);
        }

        public boolean validateRating(ServiceRating oRating) {
                for (ServiceRating rate : ratedServiceList) {
                        if (rate.getServiceOrder().equals(rate.getServiceOrder())) {
                                return false;
                        }
                }
                return true;
        }

        /**
         * Method to return a list of Service Ratings
         *
         * @return ratedServiceList: list of service ratings
         */
        public List<ServiceRating> getServiceRatings() {
                List<ServiceRating> ratedServiceListCopy = new ArrayList<>(this.ratedServiceList);
                return ratedServiceListCopy;
        }

        public boolean removeServiceRating(ServiceRating serv) {
                return this.ratedServiceList.remove(serv);
        }

}
