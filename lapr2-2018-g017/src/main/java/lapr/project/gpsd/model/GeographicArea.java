package lapr.project.gpsd.model;

import java.util.List;
import java.util.Objects;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class GeographicArea {

        private final String strDesignation;
        private final PostalCode oPostalCode;
        private final double dblTravelExpenses;
        private final double dblActionRange;
        private List<ActsIn> lstActsIn;
        
        /**
     * Creates an instance of geographic area using the designation, Postal
     * Code, Travel Costs, Action Radius and List of Actuation
     *
     * @param strDesignation designation of the geographical area
     * @param oPostalCode Geographical Area´s postal code
     * @param dblTravelExpenses travel expenses
     * @param dblActionRange action range of the geographical area
     * @param lstActsIn List of Postal Codes inside the Geographical Area`s
     * Radius
     */
    public GeographicArea(String strDesignation, PostalCode oPostalCode, double dblTravelExpenses,
            double dblActionRange, List<ActsIn> lstActsIn) {
        this.strDesignation = strDesignation;
        this.oPostalCode = oPostalCode;
        this.dblTravelExpenses = dblTravelExpenses;
        this.dblActionRange = dblActionRange;
        this.lstActsIn = lstActsIn;

    }

        /**
         * Creates an instance of geographic area using the designation, Postal Code, Travel Costs, Action Radius and List of Actuation
         *
         * @param strDesignation designation of the geographical area
         * @param oPostalCode Geographical Area´s postal code
         * @param dblTravelExpenses travel expenses
         * @param dblActionRange action range of the geographical area
         */
        public GeographicArea(String strDesignation, PostalCode oPostalCode, double dblTravelExpenses,
                double dblActionRange) {
                this.strDesignation = strDesignation;
                this.oPostalCode = oPostalCode;
                this.dblTravelExpenses = dblTravelExpenses;
                this.dblActionRange = dblActionRange;
        }

        /**
         * Method to return the designation of the geo area
         *
         * @return strDesignation: designation of the geographical area
         */
        public String getDesignation() {
                return this.strDesignation;
        }

        /**
         * Method to return the postal code of the geographical area
         *
         * @return oPostalCode: postal code of the geographical area
         */
        public PostalCode getPostalCode() {
                return this.oPostalCode;
        }

        /**
         * Method to return the travel expenses to the geographical area
         *
         * @return dblTravelExpenses: cost of the travel deslocation
         */
        public double getTravelExpenses() {
                return this.dblTravelExpenses;
        }

        /**
         * Method to get the action range
         *
         * @return dblActionRange: action range of the geographical area
         */
        public double getActionRange() {
                return this.dblActionRange;
        }

        /**
         * Method to know if a certain address is inside the range of the geographical area
         *
         * @param oAddress: address to check
         * @return true: if the address is inside the range
         */
        public boolean actsIn(Address oAddress) {
                double dblDistance = Utils.getDistance(this.oPostalCode.getLatitude(), this.oPostalCode.getLongitude(),
                        oAddress.getPostalCode().getLatitude(), oAddress.getPostalCode().getLongitude());
                return dblDistance <= (this.dblActionRange * 1000);
        }

        @Override
        public int hashCode() {
                int hash = 7;
                hash = 41 * hash + Objects.hashCode(this.oPostalCode);
                return hash;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                GeographicArea obj = (GeographicArea) o;
                return oPostalCode.equals(obj.oPostalCode);
        }

        @Override
        public String toString() {
                return String.format("%s | %s | %.2f€ | %.2f Km", this.strDesignation,
                        this.oPostalCode.getPostalCodeString(), this.dblTravelExpenses, this.dblActionRange);
        }
}
