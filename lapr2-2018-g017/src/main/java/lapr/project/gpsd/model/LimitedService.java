package lapr.project.gpsd.model;

import java.util.Objects;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class LimitedService implements Service {

        private final String strId;
        private final String strBriefDescription;
        private final String strCompleteDescription;
        private final double dCostHour;
        private final transient Category oCategory;

        /**
         * Creates an instance of Limited Service
         *
         * @param strId: id of limited service
         * @param strBriefDescription: brief description of the limited service
         * @param strCompleteDescription: complete description of the limited service
         * @param dCostHour: cost of an hour for the service
         * @param oCategory: category of the service
         */
        public LimitedService(String strId, String strBriefDescription, String strCompleteDescription, double dCostHour,
                Category oCategory) {
                if ((strId == null) || (strBriefDescription == null) || (strCompleteDescription == null) || (dCostHour < 0)
                        || (oCategory == null) || (strId.isEmpty()) || (strBriefDescription.isEmpty())
                        || (strCompleteDescription.isEmpty())) {
                        throw new IllegalArgumentException("Service arguments can't be empty or null.");
                }

                this.strId = strId;
                this.strBriefDescription = strBriefDescription;
                this.strCompleteDescription = strCompleteDescription;
                this.dCostHour = dCostHour;
                this.oCategory = oCategory;
        }

        /**
         * Method to get the id of the limited service
         *
         * @return strId: id of the service
         */
        @Override
        public String getId() {
                return this.strId;
        }

        /**
         * Method to get the brief description of the service
         *
         * @return strBriefDescription: brief description of the service
         */
        @Override
        public String getBriefDescription() {
                return this.strBriefDescription;
        }

        /**
         * Method to get the complete description of the service
         *
         * @return strCompleteDescription: complete description
         */
        @Override
        public String getCompleteDescription() {
                return this.strCompleteDescription;
        }

        /**
         * Method to get the service type
         *
         * @return LIMITED: limited service
         */
        @Override
        public ServiceType getServiceType() {
                return ServiceType.LIMITED;
        }

        /**
         * Method to get the category of the service
         *
         * @return oCategory: category of the service
         */
        @Override
        public Category getCategory() {
                return this.oCategory;
        }

        /**
         * Method to check if two ids are the same
         *
         * @param strId
         * @return true: if the ids are the same
         */
        @Override
        public boolean hasId(String strId) {
                return this.strId.equalsIgnoreCase(strId);
        }

        /**
         * Method to check if there are other attributes
         *
         * @return false: no other attributes on limited service
         */
        @Override
        public boolean hasOtherAtributes() {
                return false;
        }

        /**
         * Method to get other attributes
         *
         * @return IllegalArgumentException: no other atributtes on limited service
         */
        @Override
        public String getOtherAtributes() {
                throw new IllegalArgumentException("Limited Services don't have other atributes.");
        }

        /**
         * Method to set other attributes
         *
         * @param strOtherAtributes
         */
        @Override
        public void setOtherAtributes(String strOtherAtributes) {
                throw new IllegalArgumentException("Limited Services don't have other atributes.");
        }

        /**
         * Method to get the cost of a service for a certain duration
         *
         * @param strDuration: duration of the service
         * @return resutl: cost of the service for the strDuration
         */
        @Override
        public double getCostForDuration(String strDuration) {
                double dblDuration = Utils.convertDuration(strDuration);
                return dCostHour * dblDuration;
        }

        /**
         * Method to return the cost per hour of the service
         *
         * @return dCostHour: cost per hour
         */
        @Override
        public String getCostHour() {
                return this.dCostHour + "€";
        }

        @Override
        public int hashCode() {
                int hash = 5;
                hash = 61 * hash + Objects.hashCode(this.strId);
                return hash;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                LimitedService obj = (LimitedService) o;
                return (Objects.equals(strId, obj.strId));
        }

        @Override
        public String toString() {
                return String.format("%s - %s | %s", this.strId, this.strCompleteDescription, this.getCostHour());
        }
}
