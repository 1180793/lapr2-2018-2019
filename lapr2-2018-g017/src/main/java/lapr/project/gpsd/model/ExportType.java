/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.lang.reflect.Constructor;
import java.util.List;

/**
 *
 * @author ZDPessoa
 */
public class ExportType {
    
    /**
     * Format to Export
     */
    private String format;
    /**
     * Class corresponding to the Format
     */
    private String exportClass;
    
    /**
     * Format By Default
     */
    private static final String FORMAT_BY_DEFAULT = "Don't Export";
    
    /**
     * Class By Default
     */
    private static final String CLASS_EXPORT_BY_DEFAULT = "NO CLASS";
    
    /**
     * Creates and instance of ExportType using the format and format class
     * @param format format
     * @param cExport Class corresponding to the format
     */
    public ExportType(String format, String cExport){
        
       this.format = format;
       exportClass = cExport;
        
    }
    
    /**
     * Creates and instance of ExportTypes using the default values
     */
    public ExportType(){
        
        format = FORMAT_BY_DEFAULT;
        exportClass = CLASS_EXPORT_BY_DEFAULT;
        
    }
    
    /**
     * Returns the class
     * @return 
     */
    public String getexportClass(){
        
        return exportClass;
    }
    
    /**
     * Creates an Instance of Export using the file name, List of Orders, Staring Date and Ending Date
     * @param fileName File Name
     * @param orderList List of Service Orders
     * @param dateI Starting Date
     * @param dateF Ending Date
     * @return Export
     */
    public Export newExport(String fileName, List<ServiceOrder> orderList, String dateI, String dateF) {
    
    try{
    Class<?> oClass = Class.forName(exportClass);
    Class argsClasses[] = new Class[] { String.class, List.class , String.class, String.class };
    Constructor construct = oClass.getConstructor(argsClasses);
    Object[] argsValues = new Object[] {fileName, orderList, dateI, dateF};
    Export export = (Export) construct.newInstance(argsValues);
    return export;
    
    }catch (Exception e){
        System.out.println("Something went wrong...");
        return null;
        }
     
    }
    
    @Override
    public String toString(){
        
        return String.format("%s", this.format);
    }
    
    
}
