/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ZDPessoa
 */
public class ExportList {
    
    private List<Export> exportList = new ArrayList<>();
    
    public boolean validateExport(Export export){
        
        if(!export.validate())
            return false;
       
        //verify
        
        return true;
    }
    
    
    private boolean exportFile(Export export){
        
        return export.ExportFile();
    }
    
    private void addFile(Export export){
        
        exportList.add(export);
    }
    
    public boolean registerFile(Export export){
        
        if(validateExport(export)){
            
            exportFile(export);
            addFile(export);
            return true;
        }else{
            return false;
        }
        
      
    }
    
}
