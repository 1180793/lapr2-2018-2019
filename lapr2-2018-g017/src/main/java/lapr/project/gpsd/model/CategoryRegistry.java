package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.exception.DuplicatedIDException;

/**
 * Represents the category registry of the company.
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class CategoryRegistry {

        /**
         * List of categories
         */
        private List<Category> lstCategories = new ArrayList<>();

        /**
         * Method to create a new service category.
         *
         * @param strId id of the category
         * @param strDescription description of the category
         * @return Category : new category with id and description
         */
        public Category newCategory(String strId, String strDescription) {
                return new Category(strId, strDescription);
        }

        /**
         * Method to return the list of categories.
         *
         * @return lstCategories: list of categories
         */
        public List<Category> getCategories() {
                return this.lstCategories;
        }

        /**
         * Method to get a category by an id passed through parameter.
         *
         * @param strId: id of the category
         * @return cat: if the id exists or null: if the id doens't exists
         */
        public Category getCategoryById(String strId) {
                for (Category cat : this.lstCategories) {
                        if (cat.hasId(strId)) {
                                return cat;
                        }
                }
                return null;
        }

        /**
         * Method to register a Category.
         *
         * @param oCategory: category to register
         * @return true: if the category is added to list of categories or false: if the category isn't valid
         *
         * @throws DuplicatedIDException: if the category is duplicated
         */
        public boolean registerCategory(Category oCategory) throws DuplicatedIDException {
                if (this.validateCategory(oCategory)) {
                        return this.lstCategories.add(oCategory);
                } else {
                        throw new DuplicatedIDException("Duplicated Category");
                }
        }

        /**
         * Method to validate a category.
         *
         * @param oCategory: category to validate
         * @return return true: if the category is valid or false: if the category isn't valid
         */
        public boolean validateCategory(Category oCategory) {
                boolean bRet = true;
                for (Category cat : this.lstCategories) {
                        if (cat.getId().equalsIgnoreCase(oCategory.getId())) {
                                bRet = false;
                        }
                }
                return bRet;
        }
}
