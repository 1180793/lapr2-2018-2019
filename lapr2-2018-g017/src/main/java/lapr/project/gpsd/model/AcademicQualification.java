package lapr.project.gpsd.model;

import java.util.Objects;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AcademicQualification {

        private final String strDesignation;

        public AcademicQualification(String strDesignation) {
                this.strDesignation = strDesignation;
        }

        public String getDesignation() {
                return this.strDesignation;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                AcademicQualification obj = (AcademicQualification) o;
                return (Objects.equals(strDesignation, obj.strDesignation));
        }

        @Override
        public int hashCode() {
                int hash = 3;
                hash = 53 * hash + Objects.hashCode(this.strDesignation);
                return hash;
        }

        @Override
        public String toString() {
                return String.format("%s", this.strDesignation);
        }
}
