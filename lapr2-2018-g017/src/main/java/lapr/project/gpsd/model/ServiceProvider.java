package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import lapr.project.utils.Utils;

/**
 * Represents the Service Provider of the company.
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServiceProvider {

        private final String strPersonnelNumber;
        private final String strNif;
        private String strTelephone;
        private final String strFullName;
        private final String strAbbrevName;
        private final String strEmail;
        private Address oAddress;
        private ServiceProviderEvaluation oEvaluation = ServiceProviderEvaluation.REGULAR;
        private final List<Category> lstCategories = new ArrayList<>();
        private final List<GeographicArea> lstGeographicAreas = new ArrayList<>();
        private final List<Availability> lstAvailabilities = new ArrayList<>();
        private final ExportList lstExports = new ExportList();

        /**
         * Method that creates an instance of Service Provider.
         *
         * @param strPersonnelNumber: service provider personal number
         * @param strNif: nif of the service provider
         * @param strFullName: full name of the service provider
         * @param strAbbrevName: abbreviated name of the service provider
         * @param strEmail: email of the service provider
         * @param lstCategories: categories that the service provider wants to serve
         * @param lstGeographicAreas : list of geographic areas of the service provider
         */
        public ServiceProvider(String strPersonnelNumber, String strNif, String strFullName, String strAbbrevName, String strEmail,
                List<Category> lstCategories, List<GeographicArea> lstGeographicAreas) {
                if ((strPersonnelNumber == null) || (strNif == null) || (strFullName == null) || (strAbbrevName == null) || (strEmail == null)
                        || (lstCategories == null) || (lstGeographicAreas == null) || (strPersonnelNumber.isEmpty()) || (strNif.isEmpty())
                        || (strFullName.isEmpty()) || (strAbbrevName.isEmpty()) || (strEmail.isEmpty())) {
                        throw new IllegalArgumentException("Service Provider arguments can't be empty or null.");
                }

                this.strPersonnelNumber = strPersonnelNumber;
                this.strNif = strNif;
                this.strEmail = strEmail;
                this.strFullName = strFullName;
                this.strAbbrevName = strAbbrevName;
                for (Category cat : lstCategories) {
                        this.lstCategories.add(cat);
                }
                for (GeographicArea geoArea : lstGeographicAreas) {
                        this.lstGeographicAreas.add(geoArea);
                }
        }

        /**
         * Method to get the personal number of the service provider.
         *
         * @return strPersonnelNumber: personal number of the service provider
         */
        public String getPersonnelNumber() {
                return this.strPersonnelNumber;
        }

        /**
         * Method to get the nif of the service provider.
         *
         * @return strNif: nif of the service provider
         */
        public String getNif() {
                return this.strNif;
        }

        /**
         * Method to get the full name of the service provider.
         *
         * @return strFullName: full name of the service provider
         */
        public String getFullName() {
                return this.strFullName;
        }

        /**
         * Method to get the brief name of the service provider.
         *
         * @return strAbbrevName: abbreviated name of the service provider
         */
        public String getAbbreviatedName() {
                return this.strAbbrevName;
        }

        /**
         * Method to get the email of the service provider.
         *
         * @return strEmail: email of the service provider
         */
        public String getEmail() {
                return this.strEmail;
        }

        /**
         * Method to get the telephone of the service provider.
         *
         * @return strTelephone: telephone of the service provider
         */
        public String getTelephone() {
                return this.strTelephone;
        }
        
        /**
         * Method to get the address of the service provider.
         * 
         * @return oAddress: address of the service provider
         */
        public Address getAddress() {
                return this.oAddress;
        }
        
        /**
         * Method to get the Service Provider Evaluation.
         *
         * @return strTelephone: telephone of the service provider
         */
        public ServiceProviderEvaluation getEvaluation() {
                return this.oEvaluation;
        }
        
        /**
         * Method to set the Service Provider Evaluation.
         * @param oEvaluation
         */
        public void setEvaluation(ServiceProviderEvaluation oEvaluation) {
                this.oEvaluation = oEvaluation;
        }

        /**
         * Method to get the list of categories of the service provider.
         *
         * @return copyLstCategories: list of categories of the service provider
         */
        public List<Category> getCategories() {
                List<Category> copyLstCategories = new ArrayList<>();
                copyLstCategories = this.lstCategories;
                return copyLstCategories;
        }

        /**
         * Method to get the list of geographic areas of the service provider.
         *
         * @return lstGeographicAreasCopy: list of geographic areas of the service provider
         */
        public List<GeographicArea> getGeographicAreas() {
                List<GeographicArea> lstGeographicAreasCopy = new ArrayList<>(this.lstGeographicAreas);
                return lstGeographicAreasCopy;
        }

        /**
         * Method to return the categories of the service provider.
         *
         * @return Categories of the service provider
         */
        public String getCategoriesString() {
                StringBuilder sbReturn = new StringBuilder();
                String strPrefix = "";
                for (Category oCat : this.lstCategories) {
                        sbReturn.append(strPrefix);
                        strPrefix = "; ";
                        sbReturn.append(oCat.getId());
                }
                return sbReturn.toString();
        }

        /**
         * Method to return all the geographic areas of the service provider.
         *
         * @return sbReturn.toString(): all the geographic areas of the service provider
         */
        public String getGeographicAreasString() {
                StringBuilder sbReturn = new StringBuilder();
                String strPrefix = "";
                for (GeographicArea geoArea : this.lstGeographicAreas) {
                        sbReturn.append(strPrefix);
                        strPrefix = "; ";
                        sbReturn.append(geoArea.getDesignation());
                }
                return sbReturn.toString();
        }

        /**
         * Method to get the availabilities of the service provider.
         *
         * @return copyLstAvailabilities: ordered availabilities of the service provider
         */
        public List<Availability> getAvailabilities() {
                Utils.orderAvailabilities(this.lstAvailabilities);
                List<Availability> copyLstAvailabilities = new ArrayList<>(this.lstAvailabilities);
                return copyLstAvailabilities;
        }

        /**
         * Returns the List with the Service Provider`s Exports.
         *
         * @return exportListAux: the list with the Service Provider`s Exports.
         */
        public ExportList getExportList() {

                return this.lstExports;
        }

        /**
         * Method to set a telephone number for the service provider.
         *
         * @param strTelephone: telephone number to set
         */
        public void setTelephone(String strTelephone) {
                this.strTelephone = strTelephone;
        }
        
        /**
         * Method to set an address for the service provider.
         *
         * @param oAddress: address to set
         */
        public void setAddress(Address oAddress) {
                this.oAddress = oAddress;
        }

        /**
         * Method to check if it has an email.
         *
         * @param strEmail: the email to check
         * @return true: if the argument is not null and the Strings are equal, ignoring case;
         */
        public boolean hasEmail(String strEmail) {
                return this.strEmail.equalsIgnoreCase(strEmail);
        }

        /**
         * Method to check if it has a perssonnel number.
         *
         * @param strPersonnelNumber: the personnel number to check
         * @return true if the argument is not null and the Strings are equal, ignoring case;
         */
        public boolean hasPersonnelNumber(String strPersonnelNumber) {
                return this.strPersonnelNumber.equalsIgnoreCase(strPersonnelNumber);
        }

        /**
         * Method to check if a category is added to the list of categories of the service provider.
         *
         * @param oCategory: category to add to the list
         * @return true: if the category is added and false if not
         */
        public boolean addCategory(Category oCategory) {
                if (this.validateCategory(oCategory)) {
                        return this.lstCategories.add(oCategory);
                }
                return false;
        }

        /**
         * Method to add a geographic to the list of geographic areas.
         *
         * @param oGeographicArea: geographic area to add
         * @return true: if the geographic area is added to the list false: if the geographic area is not valid
         */
        public boolean addGeographicArea(GeographicArea oGeographicArea) {
                if (this.validateGeographicArea(oGeographicArea)) {
                        return this.lstGeographicAreas.add(oGeographicArea);
                }
                return false;
        }

        /**
         * Method to add an availability to the list of availabilities.
         *
         * @param oAvailability: availability to add
         * @return true if the availability is added to the list or false if the availability is not valid
         */
        public boolean addAvailability(Availability oAvailability) {
                if (this.validateAvailability(oAvailability)) {
                        return this.lstAvailabilities.add(oAvailability);
                }
                return false;
        }

        /**
         * Method to remove a category from the categories list.
         *
         * @param oCategory: category to remove
         * @return lstCatogories: list of categories without the category passed through parameter
         */
        public boolean removeCategory(Category oCategory) {
                return this.lstCategories.remove(oCategory);
        }

        /**
         * Method to remove a geographic area from the geographic areas list.
         *
         * @param oGeographicArea: geographic area to remove
         * @return lstGeographicAreas: list of geographic areas without the geographic area passed through parameter
         */
        public boolean removeGeographicArea(GeographicArea oGeographicArea) {
                return this.lstGeographicAreas.remove(oGeographicArea);
        }

        /**
         * Method to remove a availability from the availabilties list.
         *
         * @param oAvailability: availability to remove
         * @return lstAvailabilities: list of categories without the availability passed through parameter
         */
        public boolean removeAvailability(Availability oAvailability) {
                return this.lstAvailabilities.remove(oAvailability);
        }

        /**
         * Method to validate a category.
         *
         * @param oCategory: category to validate
         * @return false if the category already exists and true if not
         */
        public boolean validateCategory(Category oCategory) {
                boolean bRet = true;
                for (Category cat : lstCategories) {
                        if (oCategory.equals(cat)) {
                                bRet = false;
                        }
                }
                return bRet;
        }

        /**
         * Method to validate a geographic area.
         *
         * @param oGeographicArea: geographic area to validate
         * @return false: if the geographic area already exists and true if not
         */
        public boolean validateGeographicArea(GeographicArea oGeographicArea) {
                boolean bRet = true;
                for (GeographicArea geoArea : lstGeographicAreas) {
                        if (oGeographicArea.equals(geoArea)) {
                                bRet = false;
                        }
                }
                return bRet;
        }

        /**
         * Method to validate aa availability.
         *
         * @param oAvailability: availability to validate
         * @return false: if the availability already exists and true if not
         */
        public boolean validateAvailability(Availability oAvailability) {
		String[] sStartDate = oAvailability.getStartDate().split("/");
		String[] sEndDate = oAvailability.getEndDate().split("/");
		String[] sStartHour = oAvailability.getStartHour().split(":");
		String[] sEndHour = oAvailability.getEndHour().split(":");

		Date availStart = new Date(Integer.parseInt(sStartDate[2]), Integer.parseInt(sStartDate[1]),
				Integer.parseInt(sStartDate[0]), Integer.parseInt(sStartHour[0]), Integer.parseInt(sStartHour[1]));
		Date availEnd = new Date(Integer.parseInt(sEndDate[2]), Integer.parseInt(sEndDate[1]),
				Integer.parseInt(sEndDate[0]), Integer.parseInt(sEndHour[0]), Integer.parseInt(sEndHour[1]));

		if (lstAvailabilities == null) {
			return true;
		}

		for (Availability o : lstAvailabilities) {
			String[] sStartDate2 = o.getStartDate().split("/");
			String[] sEndDate2 = o.getEndDate().split("/");
			String[] sStartHour2 = o.getStartHour().split(":");
			String[] sEndHour2 = o.getEndHour().split(":");

			Date availStart2 = new Date(Integer.parseInt(sStartDate2[2]), Integer.parseInt(sStartDate2[1]),
					Integer.parseInt(sStartDate2[0]), Integer.parseInt(sStartHour2[0]),
					Integer.parseInt(sStartHour2[1]));
			Date availEnd2 = new Date(Integer.parseInt(sEndDate2[2]), Integer.parseInt(sEndDate2[1]),
					Integer.parseInt(sEndDate2[0]), Integer.parseInt(sEndHour2[0]), Integer.parseInt(sEndHour2[1]));

			if (availStart.before(availEnd2) && availStart2.before(availEnd)) {
				return false;
			}
		}
		return true;
        }

        /**
         * Method to check if the service provider acts in a certain geographic area.
         *
         * @param oGeographicArea: geographic area to check
         * @return true: if the geographic area already exists in the list and false if not
         */
        public boolean actsIn(GeographicArea oGeographicArea) {
                return this.lstGeographicAreas.contains(oGeographicArea);
        }

        /**
         * Creates the name of the file with the Service Provider´s Service Orders according to the name of the Service Provider.
         *
         * @return fileName: the name of the File
         */
        public String generateFileName() {

                String fileName = this.strAbbrevName + " Service Orders";

                return fileName;

        }

        /**
         * Indicates whether some other object is "equal to" this one.
         *
         * @return hash
         */
        @Override
        public int hashCode() {
                int hash = 3;
                hash = 79 * hash + Objects.hashCode(this.strPersonnelNumber);
                hash = 79 * hash + Objects.hashCode(this.strNif);
                hash = 79 * hash + Objects.hashCode(this.strEmail);
                return hash;
        }

        /**
         * Method to compare two objects.
         *
         * @param o: the object to compare with the parameter already created
         * @return
         */
        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                ServiceProvider obj = (ServiceProvider) o;
                return (Objects.equals(strEmail, obj.strEmail)
                        || Objects.equals(strPersonnelNumber, obj.strPersonnelNumber));
        }

        /**
         * Method to return the Service Provider's information has String.
         *
         * @return str: the Service Provider's information
         */
        @Override
        public String toString() {
                return String.format("%s (%s)", this.strFullName, this.strPersonnelNumber);
        }

}
