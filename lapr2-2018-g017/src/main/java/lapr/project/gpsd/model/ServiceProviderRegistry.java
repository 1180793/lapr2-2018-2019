package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import lapr.project.auth.AuthFacade;
import lapr.project.gpsd.exception.DuplicatedIDException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServiceProviderRegistry {

        private final AuthFacade oAuth;
        private final List<ServiceProvider> lstProviders = new ArrayList<>();

        public ServiceProviderRegistry(AuthFacade auth) {
                this.oAuth = auth;
        }

        /**
         * Method to get the list of providers
         *
         * @return this.lstProviders: list of providers
         */
        public List<ServiceProvider> getProviders() {
                return this.lstProviders;
        }

        /**
         * Method to create a new instance of service provider
         *
         * @param strPersonnelNumber: service provider personal number
         * @param strNif: service provider nif
         * @param strFullName: full name of the service provider
         * @param strAbbrevName: abreviated name of the service provider
         * @param strEmail: email of the service provider
         * @param strTelephone: telephone number of the service provider
         * @param lstCategories: list of categories of the service provider
         * @param lstGeographicAreas: list of geographic areas of the service provider
         * @return oProvider: new service provider with the attributes passed through parameter
         */
        public ServiceProvider newProvider(String strPersonnelNumber, String strNif, String strFullName, String strAbbrevName, String strEmail, String strTelephone,
                List<Category> lstCategories, List<GeographicArea> lstGeographicAreas) {
                ServiceProvider oProvider = new ServiceProvider(strPersonnelNumber, strNif, strFullName, strAbbrevName, strEmail,
                        lstCategories, lstGeographicAreas);
                oProvider.setTelephone(strTelephone);
                return oProvider;
        }

        /**
         * Method to add a service provider to the list of service providers
         *
         * @param oProvider: service provider to add to the list
         * @return true if the service provider is added to the list and false if not
         */
        public boolean addProvider(ServiceProvider oProvider) {
                if (oProvider != null) {
                        return lstProviders.add(oProvider);
                }
                return false;
        }

        /**
         * Method to get the service provider through the email
         *
         * @param strEmail: email to look for
         * @return provider if the email exists or null if the email does not exist
         */
        public ServiceProvider getServiceProviderByEmail(String strEmail) {
                for (ServiceProvider provider : this.lstProviders) {
                        if (provider.hasEmail(strEmail)) {
                                return provider;
                        }
                }
                return null;
        }
        
        /**
         * Method to get the service provider through the Personnel Number
         *
         * @param strPersonnelNumber: Personnel Number to look for
         * @return provider if the Personnel Number exists or null if the Personnel Number does not exist
         */
        public ServiceProvider getServiceProviderByPersonnelNumber(String strPersonnelNumber) {
                for (ServiceProvider provider : this.lstProviders) {
                        if (provider.hasPersonnelNumber(strPersonnelNumber)) {
                                return provider;
                        }
                }
                return null;
        }

        /**
         * Method to register a service provider in the list of service provicers
         *
         * @param oProvider: service provider to register
         * @param strPwd: password of the service provider
         * @return true if the service provider if added to the list of service providers and false if it isn't valid
         * @throws DuplicatedIDException
         */
        public boolean registerProvider(ServiceProvider oProvider, String strPwd) throws DuplicatedIDException {
                if (this.validateProvider(oProvider, strPwd)) {
                        if (this.oAuth.registerUserWithRole(oProvider.getFullName(), oProvider.getEmail(),
                                strPwd, Constants.ROLE_SP)) {
                                return addProvider(oProvider);
                        }
                } else {
                        throw new DuplicatedIDException("Email Already registered.");
                }
                return false;
        }

        /**
         * Method to validate a service provider and it's password
         *
         * @param oProvider: service provider to validate
         * @param strPwd: password of the service provider
         * @return true if the service provider and it's password are valid and false if not
         */
        public boolean validateProvider(ServiceProvider oProvider, String strPwd) {
                boolean bRet = true;
                if (strPwd == null || strPwd.isEmpty()) {
                        bRet = false;
                }
                for (ServiceProvider provider : this.lstProviders) {
                        if (provider.getEmail().equalsIgnoreCase(oProvider.getEmail())) {
                                bRet = false;
                        }
                        if (provider.getNif().equalsIgnoreCase(oProvider.getNif())) {

                        }
                }
                if (this.oAuth.hasUser(oProvider.getEmail())) {
                        bRet = false;
                }
                return bRet;
        }

        /**
         * Method that generates a password for the new service provider
         *
         * @return generated password
         */
        public String generatePassword() {
                StringBuilder builder = new StringBuilder(Constants.PASSWORD_DEFAULT_LENGTH);
                String chars = Constants.ALPHA + Constants.ALPHA_CAPS + Constants.NUMERIC;
                Random random = new Random();
                for (int i = 0; i < Constants.PASSWORD_DEFAULT_LENGTH; i++) {
                        int index = random.nextInt(chars.length());
                        builder.append(chars.charAt(index));
                }
                return builder.toString();
        }
}
