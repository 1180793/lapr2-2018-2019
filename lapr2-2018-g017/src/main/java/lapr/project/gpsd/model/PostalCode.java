package lapr.project.gpsd.model;

import java.util.Objects;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class PostalCode {

        private final String strPostalCode;
        private final double dblLatitude;
        private final double dlbLongitude;

        /**
         * Method to instance a Postal Code
         *
         * @param strPostalCode: postal code
         * @param dblLatitude: latitude of the postal code
         * @param dblLongitude: longitude of the postal code
         */
        public PostalCode(String strPostalCode, double dblLatitude, double dblLongitude) {
                this.strPostalCode = strPostalCode;
                this.dblLatitude = dblLatitude;
                this.dlbLongitude = dblLongitude;
        }

        /**
         * Method to return the postal code
         *
         * @return strPostalCode: postal code
         */
        public String getPostalCodeString() {
                return this.strPostalCode;
        }

        /**
         * Method to get the latitude of the postal code
         *
         * @return dblLatitude: latitude of the postal code
         */
        public double getLatitude() {
                return this.dblLatitude;
        }

        /**
         * Method to get the longitude of the postal code
         *
         * @return dblLongitude: longitude
         */
        public double getLongitude() {
                return this.dlbLongitude;
        }

        @Override
        public int hashCode() {
                int hash = 3;
                hash = 23 * hash + Objects.hashCode(this.strPostalCode);
                hash = 23 * hash + (int) (Double.doubleToLongBits(this.dblLatitude) ^ (Double.doubleToLongBits(this.dblLatitude) >>> 32));
                hash = 23 * hash + (int) (Double.doubleToLongBits(this.dlbLongitude) ^ (Double.doubleToLongBits(this.dlbLongitude) >>> 32));
                return hash;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                PostalCode obj = (PostalCode) o;
                return (Objects.equals(strPostalCode, obj.strPostalCode) && Objects.equals(dblLatitude, obj.dblLatitude)
                        && Objects.equals(dlbLongitude, obj.dlbLongitude));
        }

        @Override
        public String toString() {
                return String.format("%s %n%s | %s", this.strPostalCode, this.dblLatitude, this.dlbLongitude);
        }
        
        /**
         * toString used for Exports
         * @return String
         */
        public String CSVtoString(){
            return String.format("%s", strPostalCode);
        }
}
