package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.exception.InvalidAvailabilityException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Client {

        private final String strName;
        private final String strNIF;
        private final String strTelephone;
        private final String strEmail;
        private final List<Address> lstAddresses = new ArrayList<>();

        /**
         * Creats an instance of Client receiving the name, nif,telephone,email and address.
         *
         * @param strName: name of the client
         * @param strNIF: nif of the client
         * @param strTelephone: telephone of the client
         * @param strEmail: email of the client
         * @param oAddress: address of the client
         */
        public Client(String strName, String strNIF, String strTelephone, String strEmail, Address oAddress) {
                if ((strName == null) || (strNIF == null) || (strTelephone == null) || (strEmail == null) || (strName.isEmpty())
                        || (strNIF.isEmpty()) || (strTelephone.isEmpty()) || (strEmail.isEmpty())) {
                        throw new IllegalArgumentException("Client arguments can't be empty or null.");
                }

                this.strName = strName;
                this.strEmail = strEmail;
                this.strNIF = strNIF;
                this.strTelephone = strTelephone;
                lstAddresses.add(oAddress);
        }

        /**
         * Method to get the name of the client.
         *
         * @return strName: name of the client
         */
        public String getName() {
                return this.strName;
        }

        /**
         * Method to get the email of the client.
         *
         * @return strEmail: email of the client
         */
        public String getEmail() {
                return this.strEmail;
        }

        /**
         * Method to get the nif of the client.
         *
         * @return strNIF: nif of the client
         */
        public String getNif() {
                return this.strNIF;
        }

        /**
         * Method to get the telephone number of the client.
         *
         * @return strTelephone: telephone number of the client
         */
        public String getTelephone() {
                return this.strTelephone;
        }

        /**
         * Method to check if two emails are equal.
         *
         * @param strEmail:email to check if
         * @return true if the emails are the same and false if the emails are different
         */
        public boolean hasEmail(String strEmail) {
                return this.strEmail.equalsIgnoreCase(strEmail);
        }

        /**
         * Method to check if two nifs are equal.
         *
         * @param strNIF: nif to
         * @return true if the nifs are the same and false if the nifs are different
         */
        public boolean hasNif(String strNIF) {
                return this.strNIF.equalsIgnoreCase(strNIF);
        }

        /**
         * Method to get the list of addresses.
         *
         * @return this.lstAddresses: list of addresses of the client
         */
        public List<Address> getAddresses() {
                return this.lstAddresses;
        }

        /**
         * Method to return the password of the client.
         *
         * @return password: client´s password
         * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
         */
        public String getPassword() throws InvalidAvailabilityException {
                return AppGPSD.getInstance().getCompany().getAuthFacade().getUserRegistry()
                        .getUser(this.strEmail).getEncriptedPassword();
        }

        /**
         * Method to return the address in a string.
         *
         * @return oAddress: if the address isnt null and null if the address is null
         */
        public String getAddressString() {
                Address oAddress = this.lstAddresses.get(0);
                if (oAddress != null) {
                        return oAddress.toString();
                }
                return null;
        }

        /**
         * Method to register an address to the list of addresses.
         *
         * @param oAddress: address to register
         * @return true: if the address is added to the list and false if the address is not valid
         */
        public boolean registerAddress(Address oAddress) {
                if (validateAddress(oAddress)) {
                        return addAddress(oAddress);
                }
                return false;
        }

        /**
         * Method to validate an address.
         *
         * @param oAddress: address to validate
         * @return true if the address is valid and false if the address already exists
         */
        private boolean validateAddress(Address oAddress) {
                boolean bRet = true;
                for (Address address : this.lstAddresses) {
                        if (oAddress.equals(address)) {
                                bRet = false;
                        }
                }
                return bRet;
        }

        /**
         * Method to add an address to the list of the addresses.
         *
         * @param oAddress: address to add
         * @return true if the address is added and false if the address is not added
         */
        private boolean addAddress(Address oAddress) {
                return this.lstAddresses.add(oAddress);
        }

        /**
         * Method to remove an address from the list of addresses of the client.
         *
         * @param oAddress: address to remove
         * @return true if the address is removed and false if not
         */
        public boolean removeAddress(Address oAddress) {
                return this.lstAddresses.remove(oAddress);
        }

        @Override
        public int hashCode() {
                int hash = 7;
                hash = 23 * hash + Objects.hashCode(this.strEmail);
                return hash;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                Client obj = (Client) o;
                return (Objects.equals(strEmail, obj.strEmail) || Objects.equals(strNIF, obj.strNIF));
        }

        @Override
        public String toString() {
                StringBuilder builder = new StringBuilder();
                String str = String.format("%s - %s - %s - %s", this.strName, this.strNIF, this.strTelephone,
                        this.strEmail);
                builder.append(str);
                for (Address address : this.lstAddresses) {
                        builder.append(address.toString());
                }
                return builder.toString();
        }

}
