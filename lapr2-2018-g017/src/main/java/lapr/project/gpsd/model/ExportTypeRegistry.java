/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 *
 * @author ZDPessoa
 */
public class ExportTypeRegistry {
    
    /**
     * Reads the values form the configurations file
     * @return
     * @throws IOException 
     */
     public Properties getConfigVals() throws IOException {
        
        File configFile = new File("./src/main/resources/files/config.properties");
        FileReader reader = new FileReader(configFile);
        if(reader == null){
            throw new FileNotFoundException("ConfigFile Not Found");
        }
        Properties props = new Properties();
        props.load(reader);
        
        return props;
        
    }
    
     /**
      * Gets all the available Export Types in the system
      * @param props values from the properties file
      * @return List of available Export Types
      * @throws IOException 
      */
    public List<ExportType> getExportTypes(Properties props) throws IOException {
        
    List<ExportType> formatList = new ArrayList<>();
   
    String formatAmount = props.getProperty("AmountOfSupportedFormats");
    int amt = Integer.parseInt(formatAmount);
    
    for (int i=1; i<=amt; i++)
    {
    
        String format = props.getProperty("ExportType."+ i +".Format");
        String cExport = props.getProperty("ExportType." + i + ".Class");
   
        ExportType tipoServ = new ExportType(format, cExport);
  
        formatList.add(tipoServ);
    }   
   
    return formatList;
}
    
    
}
