package lapr.project.gpsd.model;

import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.exception.InvalidAvailabilityException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Attribuition {

    private final ServiceProvider oProvider;
    private final PromptedServiceDescription oService;
    private final ServiceProvisionRequest oRequest;
    private final SchedulePreference oSchedule;
    private boolean bDecision;
    private boolean bAccepted;

    public Attribuition(PromptedServiceDescription oService, ServiceProvisionRequest oRequest, ServiceProvider oProvider, SchedulePreference oSchedule) {
        this.oService = oService;
        this.oRequest = oRequest;
        this.oProvider = oProvider;
        this.oSchedule = oSchedule;
    }

    public ServiceProvider getProvider() {
        return this.oProvider;
    }

    public Client getClient() {
        return this.oRequest.getClient();
    }

    public ServiceProvisionRequest getRequest() {
        return this.oRequest;
    }

    public PromptedServiceDescription getRequestedService() {
        return this.oService;
    }

    public SchedulePreference getShedule() {
        return this.oSchedule;
    }

    public boolean getDecision() {
        return this.bDecision;
    }

    public boolean isAccepted() {
        return this.bAccepted;
    }

    public void setDecision(boolean decision) {
        this.bDecision = true;
        this.bAccepted = decision;
    }

    @Override
    public String toString() {
        try {
            return String.format("Service: %s%nServiceProvider: %s (%s)%nService Provider Rating Average: %.3f", oService.getService().getBriefDescription(), oProvider.getAbbreviatedName(), oProvider.getEvaluation(), AppGPSD.getInstance().getCompany().getServiceRatingRegistry().getProviderRatingAverage(oProvider));
        } catch (InvalidAvailabilityException ex) {

        }
        return "";
    }
}
