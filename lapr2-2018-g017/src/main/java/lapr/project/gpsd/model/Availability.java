package lapr.project.gpsd.model;

import java.util.Date;
import java.util.Objects;
import lapr.project.gpsd.exception.InvalidAvailabilityException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Availability {

        private static final long serialVersionUID = 5068232456821802258L;

        private String strStartDate;
        private String strStartHour;
        private String strEndDate;
        private String strEndHour;

        public Availability(String strStartDate, String strStartHour, String strEndDate, String strEndHour) throws InvalidAvailabilityException {
                if (validateAvailability(strStartDate, strStartHour, strEndDate, strEndHour)) {
                        this.strStartDate = strStartDate;
                        this.strStartHour = strStartHour;
                        this.strEndDate = strEndDate;
                        this.strEndHour = strEndHour;
                } else {
                        throw new InvalidAvailabilityException("Invalid Availability");
                }
        }

        public String getStartDate() {
                return this.strStartDate;
        }

        public String getStartHour() {
                return this.strStartHour;
        }

        public String getEndDate() {
                return this.strEndDate;
        }

        public String getEndHour() {
                return this.strEndHour;
        }

        public static boolean validateAvailability(String strStartDate, String strStartHour, String strEndDate,
                String strEndHour) {
                String[] sDataInicio = strStartDate.split("/");
                String[] sDataFim = strEndDate.split("/");
                String[] sHoraInicio = strStartHour.split(":");
                String[] sHoraFim = strEndHour.split(":");
                Date dataInicio = new Date(Integer.parseInt(sDataInicio[2]), Integer.parseInt(sDataInicio[1]),
                        Integer.parseInt(sDataInicio[0]), Integer.parseInt(sHoraInicio[0]), Integer.parseInt(sHoraInicio[1]));
                Date dataFim = new Date(Integer.parseInt(sDataFim[2]), Integer.parseInt(sDataFim[1]),
                        Integer.parseInt(sDataFim[0]), Integer.parseInt(sHoraFim[0]), Integer.parseInt(sHoraFim[1]));
                return dataInicio.before(dataFim);
        }

        @Override
        public int hashCode() {
                int hash = 3;
                hash = 37 * hash + Objects.hashCode(this.strStartDate);
                hash = 37 * hash + Objects.hashCode(this.strStartHour);
                hash = 37 * hash + Objects.hashCode(this.strEndDate);
                hash = 37 * hash + Objects.hashCode(this.strEndHour);
                return hash;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                Availability obj = (Availability) o;
                return (Objects.equals(strStartDate, obj.strStartDate)
                        && Objects.equals(strStartHour, obj.strStartHour)
                        && Objects.equals(strEndDate, obj.strEndDate) && Objects.equals(strEndHour, obj.strEndHour));
        }

        @Override
        public String toString() {
                return String.format("%s | %s - %s | %s", this.strStartDate, this.strStartHour, this.strEndDate, this.strEndHour);
        }
}