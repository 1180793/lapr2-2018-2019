/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

/**
 *
 * @author ZDPessoa
 */
public interface Export {
    
    /**
     * Returns the name of the File
     * @return 
     */
    public String getFileName();
    
    /**
     * Exports the designated file
     * @return 
     */
    public boolean ExportFile();
    
    /**
     * Validates the File to be exported
     * @return 
     */
    public boolean validate();
    
}
