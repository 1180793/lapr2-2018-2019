package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ZDPessoa
 */
public class WorkReportRegistry {
    
    /**
     * List of WorkReports
     */
    private final List<WorkReport> WorkReportList = new ArrayList<>();
    
    /**
     * Global Evaluation when the service goes as expected
     */
    private static final String EVALUATION_BY_DEFAULT = "As expected";
    /**
     * Global Evaluation when the service does not go as expected
     */
    private static final String EVALUATION_BY_DEFAULT_N = "Not as expected";
    
    /**
     * Creates an instance of Work Report using the orderNumber, the problem, strategy, the extra Hours, And the Global Evaluation
     * @param orderNumber Order Number
     * @param problem Problem
     * @param strategy Strategy
     * @param extraExecHours Extra Hours
     * @return Work Report Instance Created
     */
    public WorkReport newReport(int orderNumber, String problem, String strategy, int extraExecHours){
        
        if(problem.equals("") && strategy.equals(""))
            return new WorkReport(orderNumber, problem, strategy, extraExecHours, EVALUATION_BY_DEFAULT);
        else
            return new WorkReport(orderNumber, problem, strategy, extraExecHours, EVALUATION_BY_DEFAULT_N);
    }
    
    /**
     * Verifies the Report to check if it already exists in the system
     * @param report Work Report
     * @return True - if its unique | False Otherwise
     */
    private boolean verifyReport(WorkReport report){
        
        
        for ( WorkReport wr : WorkReportList){  
            if(report.getOrderNumber() == wr.getOrderNumber())
                return false;
        } 
        
        return true;
        
    }
    
    /**
     * Validates the report by doing the internal and global validation
     * @param report Work Report
     * @return True if it gets validated | False Otherwise
     */
    public boolean validateReport(WorkReport report){
        
        if(report.validate() && verifyReport(report))
            return true;
        else
            return false;
              
    }
    
    /**
     * Registers the report in the system
     * @param report Work Report
     * @return True if it registers | False Otherwise
     */
    public boolean registerReport(WorkReport report){
        
        return WorkReportList.add(report);
        
    }
    
  
}
