package lapr.project.gpsd.model;

import java.util.Objects;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Address {

        private final String strStreet;
        private final PostalCode oPostalCode;
        private final String strLocality;

        /**
         * Creats an instance of Address receiving the street name, postal code, and the locality
         *
         * @param strStreet: street name
         * @param oPostalCode: postal code of the address
         * @param strLocality: locality of the address
         */
        public Address(String strStreet, PostalCode oPostalCode, String strLocality) {
                if ((strStreet == null) || (strLocality == null) || (oPostalCode == null)
                        || (strStreet.isEmpty()) || (strLocality.isEmpty())) {
                        throw new IllegalArgumentException("Address arguments can't be empty or null.");
                }

                this.strStreet = strStreet;
                this.oPostalCode = oPostalCode;
                this.strLocality = strLocality;
        }

        /**
         * Method to return the street
         *
         * @return strStreet: street address
         */
        public String getStreet() {
                return this.strStreet;
        }

        /**
         * Method to return the locality
         *
         * @return strLocality: address locality
         */
        public String getLocality() {
                return this.strLocality;
        }

        /**
         * Method to return the postal code string
         *
         * @return oPostalCode: postal code string of the address
         */
        public String getPostalCodeString() {
                return this.oPostalCode.getPostalCodeString();
        }

        /**
         * Method to return the postal code
         *
         * @return oPostalCode: postal code of the address
         */
        public PostalCode getPostalCode() {
                return this.oPostalCode;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                Address obj = (Address) o;
                return (Objects.equals(strStreet, obj.strStreet) && oPostalCode.equals(obj.oPostalCode)
                        && Objects.equals(strLocality, obj.strLocality));
        }

        @Override
        public int hashCode() {
                int hash = 7;
                hash = 67 * hash + Objects.hashCode(this.strStreet);
                hash = 67 * hash + Objects.hashCode(this.oPostalCode);
                hash = 67 * hash + Objects.hashCode(this.strLocality);
                return hash;
        }

        @Override
        public String toString() {
                return String.format("%s | %s - %s", this.strStreet, this.oPostalCode.getPostalCodeString(),
                        this.strLocality);
        }
        
        /**
         * toString used for Exports
         * @return String
         */
        public String CSVtoString(){
            return String.format("%s", strStreet);
            
        }

}
