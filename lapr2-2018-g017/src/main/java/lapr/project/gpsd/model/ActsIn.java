/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

/**
 *
 * @author ZDPessoa
 */
public class ActsIn {
    
    /**
     * Postal Code
     */
    private PostalCode pc;
    
    /**
     * Distance
     */
    private double distance;
    
    /**
     * Creates an instance of ActsIn receiving a Postal Code and a distance
     * @param pc Postal Code
     * @param distance Distance
     */
    public ActsIn(PostalCode pc, double distance){
        
        this.pc = pc;
        this.distance = distance;
        
    }
    
    
}
