package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the Service Order Registry.
 *
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServiceOrderRegistry {

        private final List<ServiceOrder> lstServiceOrders = new ArrayList<>();
        
        public List<ServiceOrder> getOrders() {
                return lstServiceOrders;
        }

        public ServiceOrder newServiceOrder(SchedulePreference oSchedule, Client oClient, Address oAddress, ServiceProvider oProvider, PromptedServiceDescription oService) {
                return new ServiceOrder(oSchedule, oClient, oAddress, oProvider, oService);
        }
        
        public ServiceOrder newServiceOrder(Attribuition oAttribuition) {
                return new ServiceOrder(oAttribuition.getShedule(), oAttribuition.getRequest().getClient(), oAttribuition.getRequest().getAddress(), oAttribuition.getProvider(), oAttribuition.getRequestedService());
        }
        
        public ServiceOrder getServiceOrderByNumber(int number) {
                for (ServiceOrder oOrder : lstServiceOrders) {
                        if (oOrder.getSequentialNumber() == number) {
                                return oOrder;
                        }
                }
                return null;
        }
        
        public List<ServiceOrder> getClientOrders(Client oClient) {
                List<ServiceOrder> lstClientOrders = new ArrayList<>();
                for (ServiceOrder oOrder : lstServiceOrders) {
                        if (oOrder.getClient().equals(oClient)) {
                                lstClientOrders.add(oOrder);
                        }
                }
                return lstClientOrders;
        }
        
        public boolean registerServiceOrder(ServiceOrder oOrder) {
                if (oOrder != null) {
                        oOrder.setSequentialNumber(generateSequencialNumber());
                        oOrder.setIssueDate(oOrder.getCurrentDate());
                        return lstServiceOrders.add(oOrder);
                }
                return false;
        }
        
        public int generateSequencialNumber() {
                return this.lstServiceOrders.size() + 1;
        }

        /**
         * Filters the Provider`s Service Order List and removes any Orders outside the given date
         *
         * @param dateI Initial Date
         * @param dateF Final Date
         * @param sp Service Provider
         * @return List with the Provider`s Service Orders
         */
        public List<ServiceOrder> getOrderList(String dateI, String dateF, ServiceProvider sp) {
            
                List<ServiceOrder> lstServiceOrderProvider = getProviderOrderList(sp);
                List<ServiceOrder> lstServiceOrderAux = new ArrayList<>();
                
                for (ServiceOrder so : lstServiceOrderProvider) {
                        if (so.verifyOrder(dateI, dateF)) {
                                lstServiceOrderAux.add(so);
                        }
                }
                return lstServiceOrderAux;
        }

        /**
         * Returns A List with all the Service Orders that correspond to a Service Provider
         *
         * @param sp Service Provider
         * @return List with the Provider`s Service Orders
         */
        private List<ServiceOrder> getProviderOrderList(ServiceProvider sp) {
                List<ServiceOrder> lstServiceOrderAux = new ArrayList<>();
                for (ServiceOrder so : lstServiceOrders) {
                        if (so.getServiceProvider().equals(sp)) {
                                lstServiceOrderAux.add(so);
                        }
                }
                return lstServiceOrderAux;

        }

        /**
         * Method that filters the Provider`s Service Order List and removes any Orders that are already done
         *
         * @param sp: service provider
         * @return serviceOrderList: ordered list by provider
         */
        public List<ServiceOrder> getServiceOrderListByProvider(ServiceProvider sp) {

                List<ServiceOrder> ServiceOrderListAux = new ArrayList<>();

                for (ServiceOrder so : lstServiceOrders) {

                        if (so.getServiceProvider().equals(sp) && !so.isDone()) {
                                ServiceOrderListAux.add(so);
                        }

                }

                return ServiceOrderListAux;

        }
        
        /**
         * Sets the order with the designated number to the done status
         * @param orderNumber Order Number
         * @return True if the order is set to Done | False - Otherwise
         */
        public boolean setOrderDone(int orderNumber){
            
            for(ServiceOrder so : this.lstServiceOrders){
                
                if(so.getSequentialNumber() == orderNumber){
                    so.setDone(true);
                    return true;
                }
            }
            
            return false;
        }
}
