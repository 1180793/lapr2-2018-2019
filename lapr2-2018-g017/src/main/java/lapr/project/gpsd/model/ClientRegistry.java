package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.auth.AuthFacade;
import lapr.project.gpsd.exception.DuplicatedIDException;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ClientRegistry {

        private final AuthFacade oAuth;
        private final List<Client> lstClients = new ArrayList<>();

        public ClientRegistry(AuthFacade at) {
                this.oAuth = at;
        }

        /**
         * Method to get the list of clients.
         *
         * @return this.lstClients: list of clients
         */
        public List<Client> getClients() {
                return this.lstClients;
        }

        /**
         * Method to instance a new Client.
         *
         * @param strName: name of the client
         * @param strNIF: nif of the client
         * @param strTelephone: telephone of the client
         * @param strEmail: email of the client
         * @param oAddress: address of the client
         * @return Client with the attributes passed through parameter
         */
        public Client newClient(String strName, String strNIF, String strTelephone, String strEmail,
                Address oAddress) {
                return new Client(strName, strNIF, strTelephone, strEmail, oAddress);
        }

        /**
         * Method to add a client to the client list.
         *
         * @param oClient: client to be added to the list
         * @return true: if the client is added and false if not
         */
        public boolean addClient(Client oClient) {
                if (oClient != null) {
                        return lstClients.add(oClient);
                }
                return false;
        }

        /**
         * Method to get a client by its email.
         *
         * @param strEmail: email to search
         * @return client: if the email is found and null if its null
         *
         */
        public Client getClientByEmail(String strEmail) {
                for (Client client : this.lstClients) {
                        if (client.hasEmail(strEmail)) {
                                return client;
                        }
                }
                return null;
        }
        
        /**
         * Method to get a client by its nif.
         *
         * @param strNif: nif to search
         * @return client: if the nif is found and null if its null
         *
         */
        public Client getClientByNif(String strNif) {
                for (Client client : this.lstClients) {
                        if (client.hasNif(strNif)) {
                                return client;
                        }
                }
                return null;
        }

        /**
         * Method to register a client.
         *
         * @param oClient: client to register
         * @param strPwd: client's password
         * @return true if the client is added to the client list and false the if the client is not valid
         * @throws DuplicatedIDException
         */
        public boolean registerClient(Client oClient, String strPwd) throws DuplicatedIDException {
                if (this.validateClient(oClient, strPwd)) {
                        if (this.oAuth.registerUserWithRole(oClient.getName(), oClient.getEmail(), strPwd,
                                Constants.ROLE_CLIENT)) {
                                return addClient(oClient);
                        }
                } else {
                        throw new DuplicatedIDException("Email or NIF already registered.");
                }
                return false;
        }

        /**
         * Method to validate a client.
         *
         * @param oClient: client to validate
         * @param strPwd: client's passowrd
         * @return true if the client is valid and false if not
         */
        public boolean validateClient(Client oClient, String strPwd) {
                boolean bRet = true;
                if (strPwd == null || strPwd.isEmpty()) {
                        bRet = false;
                }
                for (Client cli : this.lstClients) {
                        if (cli.getEmail().equalsIgnoreCase(oClient.getEmail())) {
                                bRet = false;
                        }
                        if (cli.getNif().equalsIgnoreCase(oClient.getNif())) {
                                bRet = false;
                        }
                }
                if (this.oAuth.hasUser(oClient.getEmail())) {
                        bRet = false;
                }
                return bRet;
        }

}
