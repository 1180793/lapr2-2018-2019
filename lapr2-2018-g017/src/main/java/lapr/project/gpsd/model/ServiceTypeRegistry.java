package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ServiceTypeRegistry {

        /**
         * List of service types
         */
        private final List<ServiceType> oServiceTypes;

        public ServiceTypeRegistry() {
                this.oServiceTypes = new ArrayList<>(EnumSet.allOf(ServiceType.class));
        }

        /**
         * Method to get the service type by id
         *
         * @param strId: id to look for
         * @return type if the id exists and null if not
         */
        public ServiceType getServiceTypeById(String strId) {
                for (ServiceType type : this.oServiceTypes) {
                        if (type.getId().equalsIgnoreCase(strId)) {
                                return type;
                        }
                }
                return null;
        }

        public List<ServiceType> getServiceTypes() {
                return this.oServiceTypes;
        }
}
