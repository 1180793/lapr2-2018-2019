package lapr.project.gpsd.model;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class OtherCost {

    private final String strDesignation;
    private final double dblCost;

    /**
     * Creates an instance of OtherCost
     * @param strDesignation: designation of the other cost
     *   @param dblCost: cost of the additional cost
     */
    public OtherCost(String strDesignation, double dblCost) {
        this.strDesignation = strDesignation;
        this.dblCost = dblCost;
    }

    /**
     * Method to return the designation of other cost
     * @return strDesignation: designation of other cost
     */
    public String getDesignation() {
        return this.strDesignation;
    }

    /**
     * Method to return the value of the other cost
     * @return dblCost: value to be paid for the other cost
     */
    public double getCost() {
        return this.dblCost;
    }

    @Override
    public String toString() {
        return String.format("%s - %.2f", this.strDesignation, this.dblCost);
    }
}
