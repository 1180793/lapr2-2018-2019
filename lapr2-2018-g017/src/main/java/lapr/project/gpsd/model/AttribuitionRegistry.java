package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AttribuitionRegistry {

    private final List<Attribuition> lstAttribuitions = new ArrayList<>();

    /**
     * Method to get the list of attribuition.
     *
     * @return lstAttribuition: the list of attribuition
     */
    public List<Attribuition> getAttribuitions() {
        return this.lstAttribuitions;
    }

    public boolean isRejected(Attribuition att) {
        if (att.getDecision()) {
            if (!att.isAccepted()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to get the list of attribuition rejected.
     *
     * @return lstAttribuition: the list of attribuition
     */
    public List<Attribuition> getRejectedAttribuitions() {
        List<Attribuition> lstRejectedAttribuitions = new ArrayList<>();
        for (Attribuition att : this.lstAttribuitions) {
            if (att.getDecision()) {
                if (!att.isAccepted()) {
                    lstRejectedAttribuitions.add(att);
                }
            }
        }
        return lstRejectedAttribuitions;
    }

    /**
     * Method to add the new attribuition received by parameter to the list of
     * attribuition.
     *
     * @param attribuition: the attribuition to add
     * @return lstAttribuition: if the attribuition isn't null or false: if is
     * null
     */
    public boolean addAttribuition(Attribuition attribuition) {
        if (attribuition != null) {
            return lstAttribuitions.add(attribuition);
        }
        return false;
    }

    /**
     * Method to validate the new attribuition received by parameterto the list
     * of attribuition.
     *
     * @param attribuition: the attiruition to validate
     * @return true: if is valid or false: if isn't valid
     */
    public boolean validateAttribuition(Attribuition attribuition) {
        boolean bRet = true;
        for (Attribuition att : this.lstAttribuitions) {
            if (att.equals(attribuition)) {
                bRet = false;
            }
        }
        return bRet;
    }

    public List<Attribuition> getPendingAttribuitionByClient(Client oClient) {
        List<Attribuition> lstClientAttribuitions = new ArrayList<>();
        for (Attribuition att : lstAttribuitions) {
            if (att.getClient() == oClient && att.getDecision() == false) {
                lstClientAttribuitions.add(att);
            }
        }
        return lstClientAttribuitions;
    }

}
