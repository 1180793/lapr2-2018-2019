package lapr.project.gpsd.model;

import java.util.Objects;
import lapr.project.utils.Utils;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ExpandableService implements Service {

        private final String strId;
        private final String strBriefDescription;
        private final String strCompleteDescription;
        private final double dCostHour;
        private final transient Category oCategory;

        /**
         * Creats an instance of Expandable Service.
         *
         * @param strId: id of the service
         * @param strBriefDescription: brief description of the service
         * @param strCompleteDescription : complete descripiton of the service
         * @param dCostHour: cost of the service by hour
         * @param oCategory: category of the service
         */
        public ExpandableService(String strId, String strBriefDescription, String strCompleteDescription, double dCostHour,
                Category oCategory) {
                if ((strId == null) || (strBriefDescription == null) || (strCompleteDescription == null) || (dCostHour < 0)
                        || (oCategory == null) || (strId.isEmpty()) || (strBriefDescription.isEmpty())
                        || (strCompleteDescription.isEmpty())) {
                        throw new IllegalArgumentException("Service arguments can't be empty or null.");
                }

                this.strId = strId;
                this.strBriefDescription = strBriefDescription;
                this.strCompleteDescription = strCompleteDescription;
                this.dCostHour = dCostHour;
                this.oCategory = oCategory;
        }

        /**
         * Method to get the id of the service.
         *
         * @return this.strId: id of the expandable service
         */
        @Override
        public String getId() {
                return this.strId;
        }

        /**
         * Method to get the brief description of the service.
         *
         * @return this.strBriefDescription: brief descritpion of the service
         */
        @Override
        public String getBriefDescription() {
                return this.strBriefDescription;
        }

        /**
         * Method to get the complete description of the service.
         *
         * @return this.strCompleteDescription: complete description of the service
         */
        @Override
        public String getCompleteDescription() {
                return this.strCompleteDescription;
        }

        /**
         * Method to get the service type.
         *
         * @return EXPANDABLE: service type
         */
        @Override
        public ServiceType getServiceType() {
                return ServiceType.EXPANDABLE;
        }

        /**
         * Method to get the category of the expandable service.
         *
         * @return oCategory: category of the expandable service
         */
        @Override
        public Category getCategory() {
                return this.oCategory;
        }

        /**
         * Method to check if two ids are the same.
         *
         * @param strId: id of the expandable
         * @return true: if both ids are equal and false if not
         */
        @Override
        public boolean hasId(String strId) {
                return this.strId.equalsIgnoreCase(strId);
        }

        /**
         * Method to check if the service has other attributes.
         *
         * @return false: expandable services dont have other attributes
         */
        @Override
        public boolean hasOtherAtributes() {
                return false;
        }

        /**
         * Method to get other attributes.
         *
         * @return IllegalArgumentException: expandable services dont have other attributes
         */
        @Override
        public String getOtherAtributes() {
                throw new IllegalArgumentException("Expandable Services don't have other atributes.");
        }

        /**
         * Method to set other attributes to the service
         * 
         * @param strOtherAtributes
         */
        @Override
        public void setOtherAtributes(String strOtherAtributes) {
                throw new IllegalArgumentException("Expandable Services don't have other atributes.");
        }

        /**
         * Method to calculate the cost of the service according to its duration.
         *
         * @param strDuration: duration of the service
         * @return result: cost of the service according to a duration
         */
        @Override
        public double getCostForDuration(String strDuration) {
                double dblDuration = Utils.convertDuration(strDuration);
                return this.dCostHour * dblDuration;
        }

        /**
         * Method to calculate the cost for every hour.
         *
         * @return cCostHour: cost of 1 hour of the service
         */
        @Override
        public String getCostHour() {
                return this.dCostHour + "€";
        }

        @Override
        public int hashCode() {
                int hash = 5;
                hash = 61 * hash + Objects.hashCode(this.strId);
                return hash;
        }

        @Override
        public boolean equals(Object o) {
                if (this == o) {
                        return true;
                }
                if (o == null) {
                        return false;
                }
                if (getClass() != o.getClass()) {
                        return false;
                }
                ExpandableService obj = (ExpandableService) o;
                return (Objects.equals(strId, obj.strId));
        }

        @Override
        public String toString() {
                return String.format("%s - %s | %s", this.strId, this.strCompleteDescription, this.getCostHour());
        }
}
