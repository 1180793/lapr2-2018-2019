/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author ZDPessoa
 */
public class ExcelExport implements Export {
    
    /**
     * File Name
     */
    private String fileName;
    /**
     * Order List
     */
    private List<ServiceOrder> orderList = new ArrayList<>();
    /**
     * Starting Date
     */
    private String dateI;
    /**
     * Ending Date
     */
    private String dateF;
    
    /**
     * Key
     */
    private int key;
    
    /**
     * Key by Default
     */
    private static final int KEY_BY_DEFAULT = 1;
    
    /**
     * Distance By Default
     */
    private static final double DIST_BY_DEFAULT = 0;
    
    /**
     * Creates an instance of ExcelExport receiving the file name, List of Service Orders, initial date and ending date
     * @param fileName Name of the File to be exported
     * @param orderList Lists contained in the file
     * @param dateI Initial Date
     * @param dateF Final date
     */
    public ExcelExport(String fileName, List<ServiceOrder> orderList,String dateI, String dateF){
        
        this.fileName = fileName;
        this.orderList = orderList;
        this.dateI = dateI;
        this.dateF = dateF;
        this.key = KEY_BY_DEFAULT;
        
    }
    
     /**
     * Return the Name of the File
     * @return File Name
     */
    @Override
    public String getFileName(){
        
        return fileName;
    }
    
    /**
     * Modifies the Name of the file
     * @param name new file name
     */
    public void setFileName(String name){
        
        fileName = name;
    }
    
    /**
     * Checks to see if the List of Orders is empty
     * @return True if it has elements | False if empty
     */
    @Override
    public boolean validate(){
        
        if(orderList.isEmpty())
                return false;
        
        return true;
    }
    
    /**
     * Exports the File with xlsx format
     * @return true if it Exports | False Otherwise
     */
    @Override
    public boolean ExportFile(){
        
        XSSFWorkbook workbook = new XSSFWorkbook(); 
  
        
        XSSFSheet sheet = workbook.createSheet(fileName); 
        
        Map<String, Object[]> data = new TreeMap<String, Object[]>(); 
        
        data.put(Integer.toString(key), new Object[]{ "Service Order Number", "Client", "Distance", "Service Category",
            "Service Type", "Date", "Hour", "Address", "Locality", "Postal Code" });
        
        for(ServiceOrder so : orderList){
            key++;
            
            data.put(Integer.toString(key), new Object[]{ so.getSequentialNumber(), so.getClient().getName(), DIST_BY_DEFAULT, so.getService().getService().getCategory().toString(),
                                           so.getService().getService().getServiceType(),so.getSchedulePreference().getDate(), so.getSchedulePreference().getHour(),
                                           so.getAddress().CSVtoString(), so.getAddress().getLocality(), so.getAddress().getPostalCode().CSVtoString() });
        }
        
        Set<String> keyset = data.keySet(); 
        int rownum = 0; 
        for (String keyS : keyset) { 
            
            Row row = sheet.createRow(rownum++); 
            Object[] objArr = data.get(keyS); 
            int cellnum = 0; 
            for (Object obj : objArr) { 
                
                Cell cell = row.createCell(cellnum++); 
                if (obj instanceof String) 
                    cell.setCellValue((String)obj); 
                else if (obj instanceof Integer) 
                    cell.setCellValue((Integer)obj); 
            } 
        }
        
         try { 
           
            FileOutputStream out = new FileOutputStream(new File("./src/main/resources/exports/" + fileName + ".xlsx")); 
            workbook.write(out); 
            out.close(); 
        } 
        catch (Exception e) { 
            System.out.println("Something went wrong wile exporting the Excel File");
            return false;
        }
        
        
        return true;
    }
    
}
