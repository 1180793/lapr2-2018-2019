package lapr.project.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import javafx.scene.control.Alert;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Availability;
import lapr.project.gpsd.model.PostalCode;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class Utils {

        public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.00");

        public static final String[] LIST_OF_DAYS = {"Sunday", "Monday", "Tuesday", "Wednesday",
                "Thursday", "Friday", "Saturday"};

        public static final String[] LIST_OF_TIMES = {"06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00",
                "09:30", "10:00", "10:30", "11:00", "11:30", "12:00", "12:30", "13:00", "13:30", "14:00", "14:30", "15:00",
                "15:30", "16:00", "16:30", "17:00", "17:30", "18:00", "18:30", "19:00", "19:30", "20:00", "20:30", "21:00",
                "21:30", "22:00", "22:30", "23:00", "23:30", "00:00"};

        public static final String[] LIST_OF_DURATIONS = {"00:30", "01:00", "01:30", "02:00", "02:30", "03:00", "03:30",
                "04:00", "04:30", "05:00", "05:30", "06:00", "06:30", "07:00", "07:30", "08:00", "08:30", "09:00", "09:30",
                "10:00", "10:30", "11:00", "11:30", "12:00"};

        public static final Integer[] LIST_OF_RATINGS = {0, 1, 2, 3, 4, 5};

        private Utils() {
        }

        /**
         * Method that opens an alert window
         *
         * @param title: title of the alert window
         * @param headerText: header text of the alert window
         * @param contentText: content of the alert window
         * @param alertType: alertType of the alert window
         */
        public static void openAlert(String title, String headerText, String contentText, Alert.AlertType alertType) {
                Alert alert = new Alert(alertType);
                alert.setTitle(title);
                alert.setHeaderText(headerText);
                alert.setContentText(contentText);
                alert.showAndWait();
        }

        /**
         * Method to convert an inserted duration
         *
         * @param strDuration: duration to be converted
         * @return firsDigit: if the duration ends in 0 or firstDigit + 0.5: if the duration ends in 30
         */
        public static double convertDuration(String strDuration) {
                String[] temp = strDuration.split(":");
                int firstDigit = Integer.parseInt(temp[0]);
                int secondDigit = Integer.parseInt(temp[1]);
                switch (secondDigit) {
                        case 0:
                                return firstDigit;
                        case 30:
                                return firstDigit + 0.5;
                        default:
                                return 0;
                }
        }

        public static String convertToDuration(String strDuration) {
                int number = Integer.parseInt(strDuration);
                int hours = number / 60;
                int minutes = number % 60;
                DecimalFormat df = new DecimalFormat("00");
                String strHours = df.format(hours);
                if (minutes == 0) {
                        return strHours + ":00";
                } else {
                        return strHours + ":30";

                }
        }

        /**
         * Method to know the distance of two different places according to the latitude and longitude
         *
         * @param lat1: latitude of the first place
         * @param lon1: longitude of the first place
         * @param lat2: latitude of the second place
         * @param lon2: longitude of the second place
         * @return d: distance in meters of the two places
         */
        public static double getDistance(double lat1, double lon1, double lat2, double lon2) {
                final double R = 6371e3;
                double theta1 = Math.toRadians(lat1);
                double theta2 = Math.toRadians(lat2);
                double deltaTheta = Math.toRadians(lat2 - lat1);
                double deltaLambda = Math.toRadians(lon2 - lon1);
                double a = Math.sin(deltaTheta / 2) * Math.sin(deltaTheta / 2)
                        + Math.cos(theta1) * Math.cos(theta2) * Math.sin(deltaLambda / 2) * Math.sin(deltaLambda / 2);
                double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
                return R * c;
        }

        public static Address getAddressFromPostalCode(String strPostalCode) {
                PostalCode oPostalCode = getPostalCode(strPostalCode);
                if (oPostalCode == null) {
                        return null;
                }
                String[] postalCode = strPostalCode.split("-");
                if (postalCode.length != 2) {
                        return null;
                }
                Scanner scanner;
                try {
                        scanner = new Scanner(new File("./src/main/resources/files/PostalCodes.csv"));
                } catch (FileNotFoundException e) {
                        return null;
                }
                while (scanner.hasNextLine()) {
                        String line = scanner.nextLine();
                        if (!line.isEmpty()) {
                                if (!line.equalsIgnoreCase("num;dd_desig;cc_desig;dd;cc;llll;LOCALIDADE;CP4;CP3;CPALF;LATITUDE;LONGITUDE;precision;foundAddress;matched")) {
                                        String[] fields = line.split(";");
                                        if ((Integer.parseInt(fields[7]) == Integer.parseInt(postalCode[0])) && (Integer.parseInt(fields[8]) == Integer.parseInt(postalCode[1]))) {
                                                scanner.close();
                                                return new Address(fields[13].split(",")[0], oPostalCode, fields[6]);
                                        }
                                }
                        }
                }
                scanner.close();
                return null;
        }

        public static PostalCode getPostalCode(String strPostalCode) {
                String[] postalCode = strPostalCode.split("-");
                if (postalCode.length != 2) {
                        return null;
                }
                Scanner scanner;
                try {
                        scanner = new Scanner(new File("./src/main/resources/files/PostalCodes.csv"));
                } catch (FileNotFoundException e) {
                        return null;
                }
                while (scanner.hasNextLine()) {
                        String line = scanner.nextLine();
                        if (!line.isEmpty()) {
                                if (!line.equalsIgnoreCase("num;dd_desig;cc_desig;dd;cc;llll;LOCALIDADE;CP4;CP3;CPALF;LATITUDE;LONGITUDE;precision;foundAddress;matched")) {
                                        String[] fields = line.split(";");
                                        if ((Integer.parseInt(fields[7]) == Integer.parseInt(postalCode[0])) && (Integer.parseInt(fields[8]) == Integer.parseInt(postalCode[1]))) {
                                                double latitude = Double.parseDouble(fields[10].replace(",", "."));
                                                double longitude = Double.parseDouble(fields[11].replace(",", "."));
                                                scanner.close();
                                                return new PostalCode(strPostalCode, latitude, longitude);
                                        }
                                }
                        }
                }
                scanner.close();
                return null;
        }

        /**
         * Method to know the week day of a certain date (dd/MM/yyyy)
         *
         * @param strDate: date to know the day of the week
         * @return weekDay: week day of the date passed through parameter
         * @throws java.text.ParseException
         */
        public static String getWeekDay(String strDate) throws ParseException {
                Calendar c = Calendar.getInstance();
                Date date = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
                c.setTime(date);
                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK) - 1;
                return LIST_OF_DAYS[dayOfWeek];
        }

        /*
        Method to add one day to a certain week day
        @param strWeekDay: day of the week 
        @return strNewWeekDay: week day after adding a day
         */
        public static String addWeekDay(String strWeekDay) {
                int index = 0;
                String strNewWeekDay;
                for (int i = 0; i < LIST_OF_DAYS.length; i++) {
                        if (strWeekDay.equalsIgnoreCase(LIST_OF_DAYS[i])) {
                                index = i;
                        }
                }
                if (index == 0 && !strWeekDay.equalsIgnoreCase(LIST_OF_DAYS[0])) {
                        return "ERROR";
                }
                try {
                        strNewWeekDay = LIST_OF_DAYS[index + 1];
                } catch (ArrayIndexOutOfBoundsException ex) {
                        strNewWeekDay = LIST_OF_DAYS[0];
                }
                return strNewWeekDay;
        }

        public static boolean isValidEmailAddress(String email) {
                boolean result = true;
                try {
                        InternetAddress emailAddr = new InternetAddress(email);
                        emailAddr.validate();
                } catch (AddressException ex) {
                        result = false;
                }
                return result;
        }

        public static boolean isValidNif(String strNif) {
                final int max = 9;
                return !(!strNif.matches("[0-9]+") || strNif.length() != max);
        }
        
        public static boolean isValidTelephone(String strTelephone) {
                final int max = 9;
                return !(!strTelephone.matches("[0-9]+") || strTelephone.length() != max);
        }

        /**
         * Method to order the list of availabilities
         *
         * @param lstAvailabilities: list of availabilities to order
         * @return lstAvailabilities: ordered list of availabilities
         */
        public static List<Availability> orderAvailabilities(List<Availability> lstAvailabilities) {
                Collections.sort(lstAvailabilities, new OrderAvailabilities());
                return lstAvailabilities;
        }

        static class OrderAvailabilities implements Comparator<Availability> {

                @Override
                public int compare(Availability o1, Availability o2) {
                        String[] sStartDate = o1.getStartDate().split("/");
                        String[] sStartHour = o1.getStartHour().split(":");
                        Date o1Date = new Date(Integer.parseInt(sStartDate[2]), Integer.parseInt(sStartDate[1]),
                                Integer.parseInt(sStartDate[0]), Integer.parseInt(sStartHour[0]),
                                Integer.parseInt(sStartHour[1]));

                        String[] sStartDate2 = o2.getStartDate().split("/");
                        String[] sStartHour2 = o2.getStartHour().split(":");
                        Date o2Date = new Date(Integer.parseInt(sStartDate2[2]), Integer.parseInt(sStartDate2[1]),
                                Integer.parseInt(sStartDate2[0]), Integer.parseInt(sStartHour2[0]),
                                Integer.parseInt(sStartHour2[1]));

                        if (o1Date.before(o2Date)) {
                                return -1;
                        } else if (o1Date.after(o2Date)) {
                                return 1;
                        } else {
                                return 0;
                        }
                }

        }
}
