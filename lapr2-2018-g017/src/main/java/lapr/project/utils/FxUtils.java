package lapr.project.utils;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import lapr.project.auth.model.Role;
import lapr.project.auth.model.RoleRegistry;
import lapr.project.auth.model.UserSession;
import lapr.project.gpsd.MainApp;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Constants;
import lapr.project.gpsd.model.ServiceProvider;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class FxUtils {
    
        private static final String TITLE = "AppGPSD";
        private static final String ERROR_WINDOW = "Error Window";
        private static final String DASHBOARD_TITLE = "Dashboard - ";
        
        private FxUtils() {}

	public static void switchScene(ActionEvent event, String fileName, String title, boolean hasTopBar) throws IOException {

		BorderPane tableViewParent = FXMLLoader.load(MainApp.class.getResource("/fxml/" + fileName + ".fxml"));
                
                if (hasTopBar) {
                        Parent topBar = FXMLLoader.load(MainApp.class.getResource("/fxml/TopBar.fxml"));
                        tableViewParent.setTop(topBar);
                }

                Scene tableViewScene = new Scene(tableViewParent);

		Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

		window.setScene(tableViewScene);
		window.setTitle(TITLE + " | " + title);
		window.setResizable(false);
		window.centerOnScreen();
		window.show();

		addWindowListener(window);
	}

	public static void switchLogin() throws IOException, InvalidAvailabilityException {
		switchSmallWindow("/fxml/LoginUI.fxml", "Login");
	}
        
        public static void switchRole() throws IOException, InvalidAvailabilityException {
                switchSmallWindow("/fxml/RolesUI.fxml", "Role");
	}

        private static void switchSmallWindow(String strResourcePath, String strTitle) throws IOException, InvalidAvailabilityException {
                Parent root;
                root = FXMLLoader.load(MainApp.class.getResource(strResourcePath));
                
                Scene scene = new Scene(root);
        
                scene.getStylesheets().add("/styles/Styles.css");
        
                Stage window = AppGPSD.getInstance().getStage();
                if (window != null) {
                        addWindowListener(window);
                        window.setTitle(TITLE + " | " + strTitle);
                        window.setResizable(false);
                        window.setScene(scene);
                        window.centerOnScreen();
                        window.show();
                } else {
                        System.err.println(ERROR_WINDOW);
                }
        }
	
	public static void switchDashboard(ActionEvent event, String strRole, String strEmail) throws IOException, InvalidAvailabilityException {
		ClientRegistry oClients = AppGPSD.getInstance().getCompany().getClientRegistry();
		RoleRegistry oRoles = AppGPSD.getInstance().getCompany().getAuthFacade().getRoleRegistry();
		Role oRole = oRoles.getRole(strRole);
		if (oRole == null) {
			return;
		}
		UserSession oSession = AppGPSD.getInstance().getCurrentSession();
		if (oRole.hasId(Constants.ROLE_CLIENT)) {
			Client oClient = oClients.getClientByEmail(strEmail);
			FxUtils.switchScene(event, "DashboardClient", DASHBOARD_TITLE + oClient.getName(), true);
		} else if (oRole.hasId(Constants.ROLE_SP)) {
			ServiceProvider oProvider = AppGPSD.getInstance().getCompany().getServiceProviderRegistry().getServiceProviderByEmail(strEmail);
			FxUtils.switchScene(event, "DashboardProvider", DASHBOARD_TITLE + oProvider.getFullName(), true);
		} else if (oRole.hasId(Constants.ROLE_ADMIN)) {
			FxUtils.switchScene(event, "DashboardAdmin", DASHBOARD_TITLE + oSession.getUserName(), true);
		} else if (oRole.hasId(Constants.ROLE_HRO)) {
                        FxUtils.switchScene(event, "DashboardHumanResources", DASHBOARD_TITLE + oSession.getUserName(), true);
		} else {
			// It should never happen
			Utils.openAlert("Error Choosing Role", "Invalid Role", "The selected Role does not exist.",
					Alert.AlertType.ERROR);
		}
	}
        
        private static void addWindowListener(Stage window) {
                window.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                        
                        alert.setTitle(TITLE);
                        alert.setHeaderText("Confirm Exit");
                        alert.setContentText("Do you want to exit AGPSD?");
                        
                        if (alert.showAndWait().get() == ButtonType.CANCEL) {
                                event.consume();
                        }
                }
        });
    }

}