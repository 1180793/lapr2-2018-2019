package lapr.project.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class MD5Utils {

        /**
         * Private constructor to avoid the public default one
         */
        private MD5Utils() {
        }

        /**
         * Method to encript password.
         *
         * @param password: password to be converted
         * @return Encripted Password
         */
        public static String convert(String password) {
                try {
                        String pass;
                        MessageDigest md;
                        md = MessageDigest.getInstance("MD5");
                        if (md != null) {
                                BigInteger hash = new BigInteger(1, md.digest(password.getBytes()));
                                pass = hash.toString(16);
                                return pass;
                        } else {
                                return null;
                        }
                } catch (NoSuchAlgorithmException ex) {
                        Logger.getLogger(MD5Utils.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
        }
}
