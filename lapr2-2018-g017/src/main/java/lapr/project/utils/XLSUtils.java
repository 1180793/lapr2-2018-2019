package lapr.project.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.exception.DuplicatedIDException;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Application;
import lapr.project.gpsd.model.Availability;
import lapr.project.gpsd.model.Category;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.GeographicArea;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.gpsd.model.PromptedServiceDescription;
import lapr.project.gpsd.model.SchedulePreference;
import lapr.project.gpsd.model.Service;
import lapr.project.gpsd.model.ServiceOrder;
import lapr.project.gpsd.model.ServiceProvider;
import lapr.project.gpsd.model.ServiceProvisionRequest;
import lapr.project.gpsd.model.ServiceType;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class XLSUtils {

        public static void loadClients(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/Clients.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                String strStreet = dataFormatter.formatCellValue(row.getCell(5));
                                                String strPostalCode = dataFormatter.formatCellValue(row.getCell(7));
                                                PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                                                String strLocality = dataFormatter.formatCellValue(row.getCell(6));
                                                Address oAddress = new Address(strStreet, oPostalCode, strLocality);
                                                Client oClient = new Client(dataFormatter.formatCellValue(row.getCell(1)),
                                                        dataFormatter.formatCellValue(row.getCell(0)),
                                                        dataFormatter.formatCellValue(row.getCell(2)),
                                                        dataFormatter.formatCellValue(row.getCell(3)), oAddress);
                                                try {
                                                        oApp.getCompany().getClientRegistry().registerClient(oClient, dataFormatter.formatCellValue(row.getCell(4)));
                                                } catch (DuplicatedIDException e) {
                                                }
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }

        public static void loadCategories(AppGPSD oApp) {
                try {
                        try (Workbook workbookCategory = WorkbookFactory.create(new File("./src/main/resources/files/Categories.xlsx"))) {
                                Sheet sheet = workbookCategory.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                String strId = dataFormatter.formatCellValue(row.getCell(0));
                                                String strDescription = dataFormatter.formatCellValue(row.getCell(1));
                                                Category oCategory = new Category(strId, strDescription);
                                                try {
                                                        oApp.getCompany().getCategoryRegistry().registerCategory(oCategory);
                                                } catch (DuplicatedIDException e) {
                                                }
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }

        public static void loadServices(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/Services.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                String strId = dataFormatter.formatCellValue(row.getCell(0));
                                                ServiceType oServiceType = oApp.getCompany().getServiceTypeRegistry().getServiceTypeById(dataFormatter.formatCellValue(row.getCell(1)));
                                                String strBriefDescription = dataFormatter.formatCellValue(row.getCell(2));
                                                String strCompleteDescription = dataFormatter.formatCellValue(row.getCell(3));
                                                double dblCostHour = Double.parseDouble(dataFormatter.formatCellValue(row.getCell(4)));
                                                Category oCategory = oApp.getCompany().getCategoryRegistry().getCategoryById(dataFormatter.formatCellValue(row.getCell(5)));
                                                Service oService = oServiceType.newService(strId, strBriefDescription, strCompleteDescription, dblCostHour, oCategory);
                                                if (oService.hasOtherAtributes()) {
                                                        oService.setOtherAtributes(Utils.convertToDuration(dataFormatter.formatCellValue(row.getCell(6))));
                                                }
                                                try {
                                                        oApp.getCompany().getServiceRegistry().registerService(oService);
                                                } catch (DuplicatedIDException e) {
                                                }
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }

        public static void loadGeographicAreas(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/GeographicAreas.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                String strDesignation = dataFormatter.formatCellValue(row.getCell(0));
                                                PostalCode oPostalCode = Utils.getPostalCode(dataFormatter.formatCellValue(row.getCell(1)));
                                                Double dblActionRange = Double.parseDouble(dataFormatter.formatCellValue(row.getCell(2)));
                                                Double dblTravelExpenses = Double.parseDouble(dataFormatter.formatCellValue(row.getCell(3)));
                                                
                                                GeographicArea oGeographicArea = new GeographicArea(strDesignation, oPostalCode, dblTravelExpenses, dblActionRange);
                                                
                                                try {
                                                        oApp.getCompany().getGeographicAreaRegistry().registerGeographicArea(oGeographicArea);
                                                } catch (DuplicatedIDException e) {
                                                }
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }

        public static void loadServiceProviders(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/ServiceProviders.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                String strPersonnelNumber = dataFormatter.formatCellValue(row.getCell(0));
                                                String strNif = dataFormatter.formatCellValue(row.getCell(1));
                                                String strAbbrevName = dataFormatter.formatCellValue(row.getCell(2));
                                                String strFullName = dataFormatter.formatCellValue(row.getCell(3));
                                                String strEmail = dataFormatter.formatCellValue(row.getCell(4));
                                                Address oAddress = Utils.getAddressFromPostalCode(dataFormatter.formatCellValue(row.getCell(5)));
                                                List<Category> lstCategories = new ArrayList<>();
                                                List<GeographicArea> lstGeographicAreas = new ArrayList<>();
                                                
                                                for (int i = 6; i < 8; i++) {
                                                        if (!dataFormatter.formatCellValue(row.getCell(i)).isEmpty()) {
                                                                Category oCategory = oApp.getCompany().getCategoryRegistry().getCategoryById(dataFormatter.formatCellValue(row.getCell(i)));
                                                                lstCategories.add(oCategory);
                                                        }
                                                }
                                                for (int i = 9; i < 10; i++) {
                                                        if (!dataFormatter.formatCellValue(row.getCell(i)).isEmpty()) {
                                                                GeographicArea oGeographicArea = oApp.getCompany().getGeographicAreaRegistry().getGeographicAreaByDesignation(dataFormatter.formatCellValue(row.getCell(i)));
                                                                lstGeographicAreas.add(oGeographicArea);
                                                        }
                                                }
                                                ServiceProvider oProvider = new ServiceProvider(strPersonnelNumber, strNif, strFullName, strAbbrevName, strEmail, lstCategories, lstGeographicAreas);
                                                String strPassword = dataFormatter.formatCellValue(row.getCell(11));
                                                try {
                                                        oApp.getCompany().getServiceProviderRegistry().registerProvider(oProvider, strPassword);
                                                } catch (DuplicatedIDException e) {
                                                }
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {

                }
        }

        public static void loadRequests(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/ServiceRequests.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                
                                                Client oClient = oApp.getCompany().getClientRegistry().getClientByNif(dataFormatter.formatCellValue(row.getCell(1)));
                                                
                                                String strStreet = dataFormatter.formatCellValue(row.getCell(2));
                                                String strPostalCode = dataFormatter.formatCellValue(row.getCell(4));
                                                PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                                                String strLocality = dataFormatter.formatCellValue(row.getCell(3));
                                                Address oAddress = new Address(strStreet, oPostalCode, strLocality);
                                                
                                                ServiceProvisionRequest oRequest = new ServiceProvisionRequest(oClient, oAddress);
                                                
                                                Service oService = oApp.getCompany().getServiceRegistry().getServiceById(dataFormatter.formatCellValue(row.getCell(6)));
                                                String strDescription = dataFormatter.formatCellValue(row.getCell(7));
                                                
                                                String strDuration;
                                                if (oService.hasOtherAtributes()) {
                                                        strDuration = oService.getOtherAtributes();
                                                } else {
                                                        strDuration = Utils.convertToDuration(dataFormatter.formatCellValue(row.getCell(8)));
                                                }
                                                
                                                PromptedServiceDescription oPromptedService = new PromptedServiceDescription(oService, strDescription, strDuration);
                                                oRequest.addServiceToRequest(oPromptedService);
                                                
                                                oRequest.addSchedule(dataFormatter.formatCellValue(row.getCell(9)), dataFormatter.formatCellValue(row.getCell(10)));
                                                if (!dataFormatter.formatCellValue(row.getCell(11)).isEmpty()) {
                                                        oRequest.addSchedule(dataFormatter.formatCellValue(row.getCell(11)), dataFormatter.formatCellValue(row.getCell(12)));
                                                }
                                                oRequest.setNumber(oApp.getCompany().getRequestRegistry().generateRequestNumber());
                                                oApp.getCompany().getRequestRegistry().getRequests().add(oRequest);
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }

        public static void loadAvailabilities(AppGPSD oApp) throws InvalidAvailabilityException {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/Availabilities.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                
                                                String strPersonnelNumber = dataFormatter.formatCellValue(row.getCell(0));
                                                ServiceProvider oProvider = oApp.getCompany().getServiceProviderRegistry().getServiceProviderByPersonnelNumber(strPersonnelNumber);
                                                
                                                if (oProvider != null) {
                                                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                                        String strStartDate = sdf.format(row.getCell(2).getDateCellValue());
                                                        String strStartHour = dataFormatter.formatCellValue(row.getCell(3));
                                                        String strEndDate = sdf.format(row.getCell(4).getDateCellValue());
                                                        String strEndHour = dataFormatter.formatCellValue(row.getCell(5));
                                                        Availability oAvailability = new Availability(strStartDate, strStartHour, strEndDate, strEndHour);
                                                        oProvider.addAvailability(oAvailability);
                                                        
                                                        if (!dataFormatter.formatCellValue(row.getCell(6)).isEmpty()) {
                                                                strStartDate = sdf.format(row.getCell(6).getDateCellValue());
                                                                strStartHour = dataFormatter.formatCellValue(row.getCell(7));
                                                                strEndDate = sdf.format(row.getCell(8).getDateCellValue());
                                                                strEndHour = dataFormatter.formatCellValue(row.getCell(9));
                                                                oAvailability = new Availability(strStartDate, strStartHour, strEndDate, strEndHour);
                                                                oProvider.addAvailability(oAvailability);
                                                        }
                                                        
                                                        if (!dataFormatter.formatCellValue(row.getCell(10)).isEmpty()) {
                                                                strStartDate = sdf.format(row.getCell(10).getDateCellValue());
                                                                strStartHour = dataFormatter.formatCellValue(row.getCell(11));
                                                                strEndDate = sdf.format(row.getCell(12).getDateCellValue());
                                                                strEndHour = dataFormatter.formatCellValue(row.getCell(13));
                                                                oAvailability = new Availability(strStartDate, strStartHour, strEndDate, strEndHour);
                                                                oProvider.addAvailability(oAvailability);
                                                        }
                                                        
                                                }
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }
        
        public static void loadApplications(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/Applications.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                String strNif = dataFormatter.formatCellValue(row.getCell(0));
                                                String strName = dataFormatter.formatCellValue(row.getCell(1));
                                                String strTel = dataFormatter.formatCellValue(row.getCell(2));
                                                String strEmail = dataFormatter.formatCellValue(row.getCell(3));
                                                String strPostalCode = dataFormatter.formatCellValue(row.getCell(6));
                                                Address oAddress;
                                                if (dataFormatter.formatCellValue(row.getCell(4)).isEmpty()) {
                                                        oAddress = Utils.getAddressFromPostalCode(strPostalCode);
                                                } else {
                                                        String strStreet = dataFormatter.formatCellValue(row.getCell(4));
                                                        String strLocality = dataFormatter.formatCellValue(row.getCell(5));
                                                        PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                                                        oAddress = new Address(strStreet, oPostalCode, strLocality);
                                                }
                                                
                                                Application oApplication = new Application(strName, strNif, strTel, strEmail, oAddress);
                                                
                                                String strDesignation = dataFormatter.formatCellValue(row.getCell(7));
                                                oApplication.addAcademicQualification(strDesignation);
                                                
                                                if (!dataFormatter.formatCellValue(row.getCell(8)).isEmpty()) {
                                                        strDesignation = dataFormatter.formatCellValue(row.getCell(8));
                                                        oApplication.addAcademicQualification(strDesignation);
                                                }
                                                if (!dataFormatter.formatCellValue(row.getCell(9)).isEmpty()) {
                                                        strDesignation = dataFormatter.formatCellValue(row.getCell(9));
                                                        oApplication.addAcademicQualification(strDesignation);
                                                }
                                                
                                                String strDescription = dataFormatter.formatCellValue(row.getCell(10));
                                                oApplication.addProfessionalQualification(strDescription);
                                                
                                                if (!dataFormatter.formatCellValue(row.getCell(11)).isEmpty()) {
                                                        strDesignation = dataFormatter.formatCellValue(row.getCell(11));
                                                        oApplication.addAcademicQualification(strDesignation);
                                                }
                                                if (!dataFormatter.formatCellValue(row.getCell(12)).isEmpty()) {
                                                        strDesignation = dataFormatter.formatCellValue(row.getCell(12));
                                                        oApplication.addAcademicQualification(strDesignation);
                                                }
                                                
                                                String strId = dataFormatter.formatCellValue(row.getCell(13));
                                                Category oCategory = oApp.getCompany().getCategoryRegistry().getCategoryById(strId);
                                                oApplication.addCategory(oCategory);
                                                if (!dataFormatter.formatCellValue(row.getCell(14)).isEmpty()) {
                                                        strId = dataFormatter.formatCellValue(row.getCell(14));
                                                        oCategory = oApp.getCompany().getCategoryRegistry().getCategoryById(strId);
                                                        oApplication.addCategory(oCategory);
                                                }
                                                if (!dataFormatter.formatCellValue(row.getCell(15)).isEmpty()) {
                                                        strId = dataFormatter.formatCellValue(row.getCell(15));
                                                        oCategory = oApp.getCompany().getCategoryRegistry().getCategoryById(strId);
                                                        oApplication.addCategory(oCategory);
                                                }
                                                
                                                oApp.getCompany().getApplicationRegistry().addApplication(oApplication);
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }
        
        public static void loadServiceOrders(AppGPSD oApp) {
                loadServiceOrder1(oApp);
                loadServiceOrder2(oApp);
                loadServiceOrder3(oApp);
        }
        
        public static void loadServiceOrder1(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/ServiceOrder_APadrao.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                
                                                String strEmail = dataFormatter.formatCellValue(row.getCell(1));
                                                Client oClient = oApp.getCompany().getClientRegistry().getClientByEmail(strEmail);
                                                
                                                String strServiceId = dataFormatter.formatCellValue(row.getCell(4));
                                                Service oService = oApp.getCompany().getServiceRegistry().getServiceById(strServiceId);
                                                
                                                ServiceProvisionRequest oRequest = oApp.getCompany().getRequestRegistry().getRequestByNumber(10000);
                                                PromptedServiceDescription oPrompted = oRequest.getRequestedService(oService);
                                                
                                                
                                                String strDate = sdf.format(row.getCell(5).getDateCellValue());
                                                String strStartHour = dataFormatter.formatCellValue(row.getCell(6));
                                                SchedulePreference oSchedule = new SchedulePreference(999, strDate, strStartHour);
                                                
                                                String strStreet = dataFormatter.formatCellValue(row.getCell(7));
                                                String strLocality = dataFormatter.formatCellValue(row.getCell(8));
                                                String strPostalCode = dataFormatter.formatCellValue(row.getCell(9));
                                                PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                                                Address oAddress = new Address(strStreet, oPostalCode, strLocality);
                                                
                                                String strProviderEmail = dataFormatter.formatCellValue(row.getCell(10));
                                                ServiceProvider oProvider = oApp.getCompany().getServiceProviderRegistry().getServiceProviderByEmail(strProviderEmail);
                                                
                                                
                                                ServiceOrder oOrder = oApp.getCompany().getServiceOrderRegistry().newServiceOrder(oSchedule, oClient, oAddress, oProvider, oPrompted);
                                                
                                                oApp.getCompany().getServiceOrderRegistry().registerServiceOrder(oOrder);
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }
        
        public static void loadServiceOrder2(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/ServiceOrder_MSilva.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                
                                                String strEmail = dataFormatter.formatCellValue(row.getCell(1));
                                                Client oClient = oApp.getCompany().getClientRegistry().getClientByEmail(strEmail);
                                                
                                                String strServiceId = dataFormatter.formatCellValue(row.getCell(4));
                                                Service oService = oApp.getCompany().getServiceRegistry().getServiceById(strServiceId);
                                                
                                                ServiceProvisionRequest oRequest = oApp.getCompany().getRequestRegistry().getRequestByNumber(10003);
                                                PromptedServiceDescription oPrompted = oRequest.getRequestedService(oService);
                                                
                                                String strDate = sdf.format(row.getCell(5).getDateCellValue());
                                                String strStartHour = dataFormatter.formatCellValue(row.getCell(6));
                                                SchedulePreference oSchedule = new SchedulePreference(999, strDate, strStartHour);
                                                
                                                String strStreet = dataFormatter.formatCellValue(row.getCell(7));
                                                String strLocality = dataFormatter.formatCellValue(row.getCell(8));
                                                String strPostalCode = dataFormatter.formatCellValue(row.getCell(9));
                                                PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                                                Address oAddress = new Address(strStreet, oPostalCode, strLocality);
                                                
                                                String strProviderEmail = dataFormatter.formatCellValue(row.getCell(10));
                                                ServiceProvider oProvider = oApp.getCompany().getServiceProviderRegistry().getServiceProviderByEmail(strProviderEmail);
                                                
                                                
                                                ServiceOrder oOrder = oApp.getCompany().getServiceOrderRegistry().newServiceOrder(oSchedule, oClient, oAddress, oProvider, oPrompted);
                                                
                                                oApp.getCompany().getServiceOrderRegistry().registerServiceOrder(oOrder);
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }
        
        public static void loadServiceOrder3(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/ServiceOrder_JSantos.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                
                                                String strEmail = dataFormatter.formatCellValue(row.getCell(1));
                                                Client oClient = oApp.getCompany().getClientRegistry().getClientByEmail(strEmail);
                                                
                                                String strServiceId = dataFormatter.formatCellValue(row.getCell(4));
                                                Service oService = oApp.getCompany().getServiceRegistry().getServiceById(strServiceId);
                                                
                                                ServiceProvisionRequest oRequest;
                                                if (row.getRowNum() == 1) {
                                                        oRequest = oApp.getCompany().getRequestRegistry().getRequestByNumber(10004);
                                                } else {
                                                        oRequest = oApp.getCompany().getRequestRegistry().getRequestByNumber(10005);
                                                }
                                                PromptedServiceDescription oPrompted = oRequest.getRequestedService(oService);
                                                
                                                String strDate = sdf.format(row.getCell(5).getDateCellValue());
                                                String strStartHour = dataFormatter.formatCellValue(row.getCell(6));
                                                SchedulePreference oSchedule = new SchedulePreference(999, strDate, strStartHour);
                                                
                                                String strStreet = dataFormatter.formatCellValue(row.getCell(7));
                                                String strLocality = dataFormatter.formatCellValue(row.getCell(8));
                                                String strPostalCode = dataFormatter.formatCellValue(row.getCell(9));
                                                PostalCode oPostalCode = Utils.getPostalCode(strPostalCode);
                                                Address oAddress = new Address(strStreet, oPostalCode, strLocality);
                                                
                                                String strProviderEmail = dataFormatter.formatCellValue(row.getCell(10));
                                                ServiceProvider oProvider = oApp.getCompany().getServiceProviderRegistry().getServiceProviderByEmail(strProviderEmail);
                                                
                                                
                                                ServiceOrder oOrder = oApp.getCompany().getServiceOrderRegistry().newServiceOrder(oSchedule, oClient, oAddress, oProvider, oPrompted);
                                                
                                                oApp.getCompany().getServiceOrderRegistry().registerServiceOrder(oOrder);
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }
        
        public static void loadRatings(AppGPSD oApp) {
                try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/Ratings.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                String strOrderNumber = dataFormatter.formatCellValue(row.getCell(0));
                                                Integer intOrderNumber = Integer.parseInt(strOrderNumber);
                                                ServiceOrder oOrder = oApp.getCompany().getServiceOrderRegistry().getServiceOrderByNumber(intOrderNumber);
                                                
                                                String strRate = dataFormatter.formatCellValue(row.getCell(1));
                                                Integer intRate = Integer.parseInt(strRate);
                                                
                                                oApp.getCompany().getServiceRatingRegistry().registerServiceRating(intRate, oOrder);
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }
        
        public static void loadWorkReports(AppGPSD oApp){
            
            try {
                        try (Workbook workbook = WorkbookFactory.create(new File("./src/main/resources/files/WorkReport.xlsx"))) {
                                Sheet sheet = workbook.getSheetAt(0);
                                DataFormatter dataFormatter = new DataFormatter();
                                Iterator<Row> rows = sheet.rowIterator();
                                while (rows.hasNext()) {
                                        Row row = rows.next();
                                        if (row.getRowNum() != 0) {
                                                String oNumber = dataFormatter.formatCellValue(row.getCell(0));
                                                Integer orderNumber = Integer.parseInt(oNumber);
                                                
                                                String problem = dataFormatter.formatCellValue(row.getCell(1));
                                                String strategy = dataFormatter.formatCellValue(row.getCell(2));
                                                
                                                String eHours = dataFormatter.formatCellValue(row.getCell(3));
                                                
                                                Integer extraHours;
                                                
                                                if((!dataFormatter.formatCellValue(row.getCell(3)).isEmpty()))
                                                    extraHours = Integer.parseInt(eHours);
                                                else
                                                    extraHours = 0;
             
                                                oApp.getCompany().getWorkReportRegistry().registerReport( oApp.getCompany().getWorkReportRegistry().newReport(orderNumber, problem, strategy, extraHours) );
                                                oApp.getCompany().getServiceOrderRegistry().setOrderDone(orderNumber);
                                        }
                                }
                        }
                } catch (IOException | InvalidFormatException | EncryptedDocumentException ex) {
                }
        }
}
