/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.utils;

import java.io.Serializable;
import java.util.Calendar;
import lapr.project.gpsd.model.Constants;

/**
 * Represents time in hour, minutes and seconds.
 * 
 * @author Bodynho Oliveira
 */
public class Time implements Comparable<Time>, Serializable {

    /**
     * The hour.
     */
    private int hour;
    
    /**
     * The minutes
     */
    private int minutes;
    
    /**
     * The seconds
     */
    private int seconds;

    /**
     * Create an instance of Time receiving the hours, minutes and seconds.
     *
     * @param hour: the hour
     * @param minutes: the minute
     * @param seconds: the second
     */
    public Time(int hour, int minutes, int seconds) {
        this.hour = hour;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    /**
     * Create an instance of Time receiving the hours, the minutes and the 
     * seconds by omission.
     *
     * @param hour: the hour
     * @param minutes: the minutes
     */
    public Time(int hour, int minutes) {
        this.hour = hour;
        this.minutes = minutes;
        seconds = Constants.DEFAULT_INT;
    }

    /**
     * Create an instance of Time receiving the hours, the minutes and 
     * seconds by omission.
     *
     * @param hour: the hour
     */
    public Time(int hour) {
        this.hour = hour;
        minutes = Constants.DEFAULT_INT;
        seconds = Constants.DEFAULT_INT;
    }

    /**
     * Create an instance of Time receiving the hours, minutes and 
     * seconds by omission.
     */
    public Time() {
        hour = Constants.DEFAULT_INT;
        minutes = Constants.DEFAULT_INT;
        seconds = Constants.DEFAULT_INT;
    }

    /**
     * Create an copie of an instance of Time
     *
     * @param anotherTime: with the same caracteristics to copie.
     */
    public Time(Time anotherTime) {
        hour = anotherTime.hour;
        minutes = anotherTime.minutes;
        seconds = anotherTime.seconds;
    }

    /**
     * Return the hour.
     *
     * @return hour of Time
     */
    public int getHour() {
        return hour;
    }

    /**
     * Return the minutes.
     *
     * @return minutes
     */
    public int getMinutes() {
        return minutes;
    }

    /**
     * Return the seconds
     *
     * @return second of Time .
     */
    public int getSeconds() {
        return seconds;
    }

    /**
     * Modify the hours
     *
     * @param hour: the new hour
     */
    public void setHour(int hour) {
        this.hour = hour;
    }

    /**
     * Modify teh minutes.
     *
     * @param minutes: the new minutes
     */
    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    /**
     * Modify the seconds
     *
     * @param seconds: the new seconds
     */
    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    /**
     * Modify the hours, the minutes and the seconds.
     *
     * @param hour: the new hour
     * @param minutes:  the new minutes
     * @param seconds:  the new seconds
     */
    public void setTempo(int hour, int minutes, int seconds) {
        this.hour = hour;
        this.minutes = minutes;
        this.seconds = seconds;
    }

    /**
     * Return textual description of Time in format: HH:MM:SS AM/PM.
     *
     * @return caraterísticas do tempo.
     */
    @Override
    public String toString() {
        return String.format("%02d:%02d:%02d %s",
                (hour == 12 || hour == 0) ? 12 : hour % 12,
                minutes, seconds, hour < 12 ? "AM" : "PM");
    }

    /**
     * Return Time in format: %02d%02d%02d.
     *
     * @return caracteristics of Time
     */
    public String toStringHHMMSS() {
        return String.format("%02d:%02d:%02d", hour, minutes, seconds);
    }

    /**
     * Compare the Time with the Object received by parameter.
     *
     * @param anotherObject: the object to Compare with the Time
     * @return true: if the object represents another time equals to the Time
     * otherwise, false
     */
    
    @Override
    public boolean equals(Object anotherObject) {
        if (this == anotherObject) {
            return true;
        }
        if (anotherObject == null || getClass() != anotherObject.getClass()) {
            return false;
        }
        Time anotherTime = (Time) anotherObject;
        return hour == anotherTime.hour && minutes == anotherTime.minutes
                && seconds == anotherTime.seconds;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + this.hour;
        hash = 89 * hash + this.minutes;
        hash = 89 * hash + this.seconds;
        return hash;
    }
    
    /**
     * Compare the time with another time received by parameter
     * 
     * @param anotherTime: time to be compared
     * @return the value 0: if the another time is equals to the time;
     *   the value -1: if the another time is after to the time;
     *   the value 1: if the another time is before to the time
     */ 
    
    @Override
    public int compareTo(Time anotherTime) {
        return (anotherTime.isBigger(this)) ? -1 : (isBigger(anotherTime)) ? 1 : 0;
    }     

    /**
     * Increase the time in one second
     */
    public void tick() {
        seconds = ++seconds % 60;
        if (seconds == 0) {
            minutes = ++minutes % 60;
            if (minutes == 0) {
                hour = ++hour % 24;
            }
        }
    }

    /**
     * Return the biggest time.
     *
     * @param anotherTime: the Time to be compared to
     * @return true: if the Time is bigger the compared one
     */
    public boolean isBigger(Time anotherTime) {
        return toSeconds() > anotherTime.toSeconds();
    }
    
    /**
     * Return the biggest time compared receiving hour, minutes and seconds
     *
     * @param hour 
     * @param minutes
     * @param seconds
     * @return true if the Time is bigger than the compared one
     */
    public boolean isBigger(int hour, int minutes, int seconds) {
        Time anotherTime = new Time(hour, minutes, seconds);
        return this.toSeconds() > anotherTime.toSeconds();
    }

    /**
     * Return the difference of 2 times to compare in seconds.
     *
     * @param anotherTime: the other time to compare in seconds
     * @return difference of 2 times to compare in seconds.
     */
    public int diferenceInSeconds(Time anotherTime) {
        return Math.abs(toSeconds() - anotherTime.toSeconds());
    }

    /**
     * Return an instance of Time that represents the difference of 2 Times.
     *
     * @param anotherTime: the another toime to compare
     * @return instance of Time that represents the difference of 2 Times.
     */
    public Time diferenceInTime(Time anotherTime) {
        int dif = diferenceInSeconds(anotherTime);
        int s = dif % 60;
        dif = dif / 60;
        int m = dif % 60;
        int h = dif / 60;
        return new Time(h, m, s);
    }
    
    /**
     * Return actual Time of System.
     * 
     * @return actual Time of System.
     */
    public static Time ActualTime() {
        Calendar now = Calendar.getInstance();
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minutes = now.get(Calendar.MINUTE);
        int seconds = now.get(Calendar.SECOND);
        return new Time(hour,minutes,seconds);
    }    
    
    /**
     * Receives a String representing a Time in the form of HH-MM-SS and converts it to "Time".
     * 
     * @param strTime Time in form of String
     * @return instance of Time
     */
    public static Time parseTime(String strTime){
        
        int hour = Integer.parseInt( strTime.split("-")[0] );
        int minute = Integer.parseInt( strTime.split("-")[1] );
        int second =  Integer.parseInt( strTime.split("-")[2] );
        
        Time timeAux = new Time(hour, minute, second);
        
        return timeAux; 
    }

     /**
     * Return the number os seconds of a Time.
     *
     * @return number os seconds of a Time.
     */
    private int toSeconds() {
        return hour * 3600 + minutes * 60 + seconds;
    }

}
