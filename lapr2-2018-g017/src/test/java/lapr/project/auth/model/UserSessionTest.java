package lapr.project.auth.model;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserSessionTest {
        
        private final User user;
        private final Role role;
        private final UserSession userSession;
        
        public UserSessionTest() {
                user = new User("Test1", "test@esoft.pt", "123");
                role = new Role("Test");
                user.addRole(role);
                userSession = new UserSession(user);
        }

        /**
         * Test of doLogout method, of class UserSession.
         */
        @Test
        public void testDoLogout() {
                System.out.println("doLogout");
                userSession.doLogout();
                assertEquals(userSession.getUser(), null);
        }

        /**
         * Test of isLoggedIn method, of class UserSession.
         */
        @Test
        public void testIsLoggedIn() {
                System.out.println("isLoggedIn");
                boolean expResult = true;
                boolean result = userSession.isLoggedIn();
                assertEquals(expResult, result);
        }

        /**
         * Test of isLoggedInWithRole method, of class UserSession.
         */
        @Test
        public void testIsLoggedInWithRole() {
                System.out.println("isLoggedInWithRole");
                String strRole = "Test";
                boolean expResult = true;
                boolean result = userSession.isLoggedInWithRole(strRole);
                assertEquals(expResult, result);
        }

        /**
         * Test of getUser method, of class UserSession.
         */
        @Test
        public void testGetUser() {
                System.out.println("getUser");
                User result = userSession.getUser();
                assertEquals(user, result);
        }

        /**
         * Test of getUserName method, of class UserSession.
         */
        @Test
        public void testGetUserName() {
                System.out.println("getUserName");
                String expResult = "Test1";
                String result = userSession.getUserName();
                assertEquals(expResult, result);
        }

        /**
         * Test of getUserEmail method, of class UserSession.
         */
        @Test
        public void testGetUserEmail() {
                System.out.println("getUserEmail");
                String expResult = "test@esoft.pt";
                String result = userSession.getUserEmail();
                assertEquals(expResult, result);
        }

        /**
         * Test of getUserRoles method, of class UserSession.
         */
        @Test
        public void testGetUserRoles() {
                System.out.println("getUserRoles");
                List<Role> expResult = new ArrayList<>();
                expResult.add(role);
                List<Role> result = userSession.getUserRoles();
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class UserSession.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = user.toString();
                String result = userSession.toString();
                assertEquals(expResult, result);
        }

}
