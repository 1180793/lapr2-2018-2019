package lapr.project.auth.model;

import lapr.project.gpsd.model.PostalCode;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class RoleTest {

        private final Role role;

        public RoleTest() {
                role = new Role("TestRole", "Role Testing");
        }

        /**
         * Test of hasId method, of class Role.
         */
        @Test
        public void testHasId() {
                System.out.println("hasId");
                String strId = "TestRole";
                boolean expResult = true;
                boolean result = role.hasId(strId);
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class Role.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                Role instance = new Role("TestRole", "Role Testing");
                boolean expResult = true;
                boolean result = role.equals(instance);
                assertEquals(expResult, result);
                result = role.equals(role);
                assertEquals(expResult, result);
                expResult = false;
                result = role.equals(null);
                assertEquals(expResult, result);
                result = role.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }

        /**
         * Test of getRole method, of class Role.
         */
        @Test
        public void testGetRole() {
                System.out.println("getRole");
                String expResult = "TestRole";
                String result = role.getRole();
                assertEquals(expResult, result);
        }

        /**
         * Test of getDescription method, of class Role.
         */
        @Test
        public void testGetDescription() {
                System.out.println("getDescription");
                String expResult = "Role Testing";
                String result = role.getDescription();
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class Role.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = -1082194711;
                int result = role.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class Role.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "TestRole - Role Testing";
                String result = role.toString();
                assertEquals(expResult, result);
        }
}
