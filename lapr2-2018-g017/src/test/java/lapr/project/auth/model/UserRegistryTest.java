package lapr.project.auth.model;

import lapr.project.auth.AuthFacade;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserRegistryTest {
        
        private final AuthFacade oAuth;
        private final UserRegistry oUsers;
        private final User user2;
        
        private UserRegistryTest() {
                oAuth = new AuthFacade();
                oUsers = oAuth.getUserRegistry();
                oAuth.registerUser("Test User", "testUser@esoft.pt", "12345");
                user2 = new User("Test 2", "testUser23@esoft.pt", "123");
        }

        /**
         * Test of newUser method, of class UserRegistry.
         */
        @Test
        public void testNewUser() {
                System.out.println("newUser");
                String strName = "Test";
                String strEmail = "testUser22@esoft.pt";
                String strPassword = "123";
                User expResult = new User(strName, strEmail, strPassword);
                User result = oUsers.newUser(strName, strEmail, strPassword);
                assertEquals(expResult, result);
        }

        /**
         * Test of addUser method, of class UserRegistry.
         */
        @Test
        public void testAddUser() {
                System.out.println("addUser");
                boolean expResult = true;
                boolean result = oUsers.addUser(this.user2);
                assertEquals(expResult, result);
        }

        /**
         * Test of removeUser method, of class UserRegistry.
         */
        @Test
        public void testRemoveUser() {
                System.out.println("removeUser");
                oUsers.addUser(this.user2);
                boolean expResult = true;
                boolean result = oUsers.removeUser(this.user2);
                assertEquals(expResult, result);
        }

        /**
         * Test of getUser method, of class UserRegistry.
         */
        @Test
        public void testGetUser() {
                System.out.println("getUser");
                String strId = "testUser23@esoft.pt";
                oUsers.addUser(this.user2);
                User expResult = user2;
                User result = oUsers.getUser(strId);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasUser method, of class UserRegistry.
         */
        @Test
        public void testHasUser_String() {
                System.out.println("hasUser");
                String strId = "testUser23@esoft.pt";
                oUsers.addUser(this.user2);
                boolean expResult = true;
                boolean result = oUsers.hasUser(strId);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasUser method, of class UserRegistry.
         */
        @Test
        public void testHasUser_User() {
                System.out.println("hasUser");
                User user = this.user2;
                oUsers.addUser(this.user2);
                boolean expResult = true;
                boolean result = oUsers.hasUser(user);
                assertEquals(expResult, result);
        }
        
}
