package lapr.project.auth.model;

import lapr.project.auth.AuthFacade;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class RoleRegistryTest {
        
        private final AuthFacade oAuth;
        private final UserRegistry oUsers;
        private final RoleRegistry oRoles;
        private final User user2;
        private final Role role;
        private final Role role2;
        
        public RoleRegistryTest() {
                oAuth = new AuthFacade();
                oUsers = oAuth.getUserRegistry();
                oRoles = oAuth.getRoleRegistry();
                oAuth.registerUser("Test User", "testUser@esoft.pt", "12345");
                user2 = new User("Test 2", "testUser23@esoft.pt", "123");
                role = new Role("Test");
                role2 = new Role("Test1", "Test2");
        }

        /**
         * Test of newRole method, of class RoleRegistry.
         */
        @Test
        public void testNewRole_String() {
                System.out.println("newRole");
                String strRole = "Test";
                Role expResult = role;
                Role result = oRoles.newRole(strRole);
                assertEquals(expResult, result);
        }

        /**
         * Test of newRole method, of class RoleRegistry.
         */
        @Test
        public void testNewRole_String_String() {
                System.out.println("newRole");
                String strRole = "Test1";
                String strDescription = "Test2";
                Role expResult = role2;
                Role result = oRoles.newRole(strRole, strDescription);
                assertEquals(expResult, result);
        }

        /**
         * Test of addRole method, of class RoleRegistry.
         */
        @Test
        public void testAddRole() {
                System.out.println("addRole");
                RoleRegistry instance = new RoleRegistry();
                boolean expResult = true;
                boolean result = instance.addRole(role);
                assertEquals(expResult, result);
                boolean expResult2 = false;
                boolean result2 = instance.addRole(null);
                assertEquals(expResult2, result2);
        }

        /**
         * Test of removeRole method, of class RoleRegistry.
         */
        @Test
        public void testRemoveRole() {
                System.out.println("removeRole");
                boolean expResult = true;
                oRoles.addRole(role);
                boolean result = oRoles.removeRole(role);
                assertEquals(expResult, result);
        }

        /**
         * Test of getRole method, of class RoleRegistry.
         */
        @Test
        public void testGetRole() {
                System.out.println("getRole");
                oRoles.addRole(role);
                String strRole = "Test";
                Role expResult = role;
                Role result = oRoles.getRole(strRole);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasRole method, of class RoleRegistry.
         */
        @Test
        public void testHasRole_String() {
                System.out.println("hasRole");
                oRoles.addRole(role);
                String strRole = "Test";
                boolean expResult = true;
                boolean result = oRoles.hasRole(strRole);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasRole method, of class RoleRegistry.
         */
        @Test
        public void testHasRole_Role() {
                System.out.println("hasRole");
                oRoles.addRole(role);
                boolean expResult = true;
                boolean result = oRoles.hasRole(role);
                assertEquals(expResult, result);
        }
        
}
