package lapr.project.auth.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.model.PostalCode;
import lapr.project.utils.MD5Utils;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UserTest {
        
        private final User user2;
        private final Role role;
        
        public UserTest() {
                user2 = new User("Test 2", "testUser23@esoft.pt", "eweew123");
                role = new Role("TestRole", "Role Testing");
        }

        /**
         * Test of hasId method, of class User.
         */
        @Test
        public void testHasId() {
                System.out.println("hasId");
                String strId = "testUser23@esoft.pt";
                boolean expResult = true;
                boolean result = user2.hasId(strId);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasPassword method, of class User.
         */
        @Test
        public void testHasPassword() {
                System.out.println("hasPassword");
                String strPwd = "eweew123";
                boolean expResult = true;
                boolean result = user2.hasPassword(strPwd);
                assertEquals(expResult, result);
        }

        /**
         * Test of addRole method, of class User.
         */
        @Test
        public void testAddRole() {
                System.out.println("addRole");
                boolean expResult = true;
                boolean result = user2.addRole(role);
                assertEquals(expResult, result);
        }

        /**
         * Test of removeRole method, of class User.
         */
        @Test
        public void testRemoveRole() {
                System.out.println("removeRole");
                user2.addRole(role);
                boolean expResult = true;
                boolean result = user2.removeRole(role);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasRole method, of class User.
         */
        @Test
        public void testHasRole_Role() {
                System.out.println("hasRole");
                user2.addRole(role);
                boolean expResult = true;
                boolean result = user2.hasRole(role);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasRole method, of class User.
         */
        @Test
        public void testHasRole_String() {
                System.out.println("hasRole");
                user2.addRole(role);
                String strRole = "TestRole";
                boolean expResult = true;
                boolean result = user2.hasRole(strRole);
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class User.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                User instance = new User("Test 2", "testUser23@esoft.pt", "eweew123");
                boolean expResult = true;
                boolean result = user2.equals(instance);
                assertEquals(expResult, result);
                result = user2.equals(user2);
                assertEquals(expResult, result);
                expResult = false;
                result = user2.equals(null);
                assertEquals(expResult, result);
                result = user2.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }

        /**
         * Test of getName method, of class User.
         */
        @Test
        public void testGetName() {
                System.out.println("getName");
                String expResult = "Test 2";
                String result = user2.getName();
                assertEquals(expResult, result);
        }

        /**
         * Test of getEmail method, of class User.
         */
        @Test
        public void testGetEmail() {
                System.out.println("getEmail");
                String expResult = "testUser23@esoft.pt";
                String result = user2.getEmail();
                assertEquals(expResult, result);
        }

        /**
         * Test of getEncriptedPassword method, of class User.
         */
        @Test
        public void testGetEncriptedPassword() {
                System.out.println("getEncriptedPassword");
                String expResult = MD5Utils.convert("eweew123");
                String result = user2.getEncriptedPassword();
                assertEquals(expResult, result);
        }

        /**
         * Test of getRoles method, of class User.
         */
        @Test
        public void testGetRoles() {
                System.out.println("getRoles");
                List<Role> expResult = new ArrayList<>();
                expResult.add(role);
                user2.addRole(role);
                List<Role> result = user2.getRoles();
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class User.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = 1465588326;
                int result = user2.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class User.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "Test 2 - testUser23@esoft.pt";
                String result = user2.toString();
                assertEquals(expResult, result);
        }
        
}
