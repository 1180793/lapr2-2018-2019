package lapr.project.auth;

import lapr.project.auth.model.Role;
import lapr.project.auth.model.RoleRegistry;
import lapr.project.auth.model.UserRegistry;
import lapr.project.auth.model.UserSession;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AuthFacadeTest {
        
        private final AuthFacade oAuth;
        private final RoleRegistry oRoles;
        private final UserRegistry oUsers;
        
        private AuthFacadeTest() {
                oAuth = new AuthFacade();
                oRoles = oAuth.getRoleRegistry();
                oUsers = oAuth.getUserRegistry();
                oAuth.registerRole("TestRole", "Role Testing");
                oAuth.registerRole("TestRole2", "Role Testing 2");
                oAuth.registerUser("Test User", "testUser@esoft.pt", "entrar123");
        }
        
        /**
         * Test of registerRole method, of class AuthFacade.
         */
        @Test
        public void testRegisterRole_String() {
                System.out.println("registerRole");
                String strRole = "Test";
                Role expResult = new Role(strRole);
                oAuth.registerRole(strRole);
                Role result = oRoles.getRole(strRole);
                assertEquals(expResult, result);
        }

        /**
         * Test of registerRole method, of class AuthFacade.
         */
        @Test
        public void testRegisterRole_String_String() {
                System.out.println("registerRole");
                String strRole = "Test1";
                String strDescription = "Test2";
                Role expResult = new Role(strRole, strDescription);
                oAuth.registerRole(strRole, strDescription);
                Role result = oRoles.getRole(strRole);
                assertEquals(expResult, result);
        }

        /**
         * Test of registerUser method, of class AuthFacade.
         */
        @Test
        public void testRegisterUser() {
                System.out.println("registerUser");
                String strName = "Test1";
                String strEmail = "test@esoft.pt";
                String strPassword = "entrar";
                boolean expResult = true;
                oAuth.registerUser(strName, strEmail, strPassword);
                boolean result = (oUsers.getUser(strEmail) != null);
                assertEquals(expResult, result);
        }

        /**
         * Test of registerUserWithRole method, of class AuthFacade.
         */
        @Test
        public void testRegisterUserWithRole() {
                System.out.println("registerUserWithRole");
                String strName = "Test2";
                String strEmail = "test2@esoft.pt";
                String strPassword = "entrar";
                String strRole = "TestRole";
                boolean expResult = true;
                boolean result = oAuth.registerUserWithRole(strName, strEmail, strPassword, strRole);
                assertEquals(expResult, result);
        }

        /**
         * Test of registerUserWithRoles method, of class AuthFacade.
         */
        @Test
        public void testRegisterUserWithRoles() {
                System.out.println("registerUserWithRoles");
                String strName = "user1";
                String strEmail = "user123@esoft.pt";
                String strPassword = "54321";
                String[] roles = {"TestRole" , "TestRole2"};
                boolean expResult = true;
                boolean result = oAuth.registerUserWithRoles(strName, strEmail, strPassword, roles);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasUser method, of class AuthFacade.
         */
        @Test
        public void testHasUser() {
                System.out.println("hasUser");
                String strId = "testUser@esoft.pt";
                boolean expResult = true;
                boolean result = oAuth.hasUser(strId);
                assertEquals(expResult, result);
        }

        /**
         * Test of doLogin method, of class AuthFacade.
         */
        @Test
        public void testDoLogin() {
                System.out.println("doLogin");
                String strId = "testUser@esoft.pt";
                String strPwd = "entrar123";
                boolean expResult = true;
                boolean result = (oAuth.doLogin(strId, strPwd) != null);
                assertEquals(expResult, result);
        }

        /**
         * Test of getCurrentSession method, of class AuthFacade.
         */
        @Test
        public void testGetCurrentSession() {
                oAuth.doLogin("testUser@esoft.pt", "entrar123");
                System.out.println("getCurrentSession");
                boolean expResult = true;
                boolean result = (oAuth.getCurrentSession() != null);
                assertEquals(expResult, result);
        }

        /**
         * Test of doLogout method, of class AuthFacade.
         */
        @Test
        public void testDoLogout() {
                System.out.println("doLogout");
                UserSession us = new UserSession(oUsers.getUser("testUser@esoft.pt"));
                oAuth.doLogout();
                boolean expResult = true;
                boolean result = (oAuth.getCurrentSession() == null);
                assertEquals(expResult, result);
                oAuth.doLogin("testUser@esoft.pt", "entrar123");
                oAuth.getCurrentSession().doLogout();
                result = oAuth.getCurrentSession().isLoggedIn();
                expResult = false;
                assertEquals(expResult, result);
        }
        
}
