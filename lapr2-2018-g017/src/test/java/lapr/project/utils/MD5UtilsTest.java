package lapr.project.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class MD5UtilsTest {

        /**
         * Test of convert method, of class MD5Utils.
         */
        @Test
        public void testConvert() {
                System.out.println("convert");
                String password = "entrar";
                String expResult = "b2eccf65385e0138e26ae97e89e88a0c";
                String result = MD5Utils.convert(password);
                assertEquals(expResult, result);
        }
        
}
