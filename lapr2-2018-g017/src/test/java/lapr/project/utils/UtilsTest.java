package lapr.project.utils;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import lapr.project.gpsd.model.Availability;
import lapr.project.gpsd.model.PostalCode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class UtilsTest {

        /**
         * Test of convertDuration method, of class Utils.
         */
        @Test
        public void testConvertDuration() {
                System.out.println("convertDuration");
                String strDuration = "05:30";
                double expResult = 5.5;
                double result = Utils.convertDuration(strDuration);
                assertEquals(expResult, result);
        }

        /**
         * Test of convertToDuration method, of class Utils.
         */
        @Test
        public void testConvertToDuration() {
                System.out.println("convertToDuration");
                String strDuration = "90";
                String expResult = "01:30";
                String result = Utils.convertToDuration(strDuration);
                assertEquals(expResult, result);
                strDuration = "120";
                expResult = "02:00";
                result = Utils.convertToDuration(strDuration);
                assertEquals(expResult, result);
        }

        /**
         * Test of getDistance method, of class Utils.
         */
        @Test
        public void testGetDistance() {
                System.out.println("getDistance");
                double lat1 = 041.09286;
                double lon1 = -008.37448;
                double lat2 = 038.43201;
                double lon2 = -009.08216;
                double expResult = 301989.71;
                double result = Utils.getDistance(lat1, lon1, lat2, lon2);
                assertEquals(expResult, result, 0.05);
        }

//        /**
//         * Test of getPostalCode method, of class Utils.
//         */
//        @Test
//        public void testGetPostalCode() {
//                System.out.println("getPostalCode");
//                PostalCode expResult = new PostalCode("4495-449", 41.3951172, -8.7526565);
//                PostalCode result = Utils.getPostalCode("4495-449");
//                assertEquals(expResult, result);
//        }

        /**
         * Test of getWeekDay method, of class Utils.
         *
         * @throws java.lang.Exception
         */
        @Test
        public void testGetWeekDay() throws Exception {
                System.out.println("getWeekDay");
                String strDate = "06/06/2019";
                String expResult = "Thursday";
                String result = Utils.getWeekDay(strDate);
                assertEquals(expResult, result);
        }

        /**
         * Test of addWeekDay method, of class Utils.
         */
        @Test
        public void testAddWeekDay() {
                System.out.println("addWeekDay");
                String strWeekDay = "Monday";
                String expResult = "Tuesday";
                String result = Utils.addWeekDay(strWeekDay);
                assertEquals(expResult, result);
        }

        /**
         * Test of orderAvailabilities method, of class Utils.
         *
         * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
         */
        @Test
        public void testOrderAvailabilities() throws InvalidAvailabilityException {
                System.out.println("orderAvailabilities");
                List<Availability> lstAvailabilities = new ArrayList<>();
                Availability av1 = new Availability("04/06/2019", "18:00", "04/06/2019","22:00");
                Availability av2 = new Availability("05/06/2019", "15:30", "05/06/2019","16:00");
                Availability av3 = new Availability("03/06/2019", "10:00", "03/06/2019","11:00");
                lstAvailabilities.add(av1);
                lstAvailabilities.add(av2);
                lstAvailabilities.add(av3);
                List<Availability> expResult = new ArrayList<>();
                expResult.add(0, av3);
                expResult.add(1, av1);
                expResult.add(2, av2);
                List<Availability> result = Utils.orderAvailabilities(lstAvailabilities);
                assertEquals(expResult, result);
        }
}
