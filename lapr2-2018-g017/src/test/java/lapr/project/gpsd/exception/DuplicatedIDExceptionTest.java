package lapr.project.gpsd.exception;

import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.gpsd.model.Address;
import lapr.project.gpsd.model.Client;
import lapr.project.gpsd.model.ClientRegistry;
import lapr.project.gpsd.model.Company;
import lapr.project.gpsd.model.PostalCode;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class DuplicatedIDExceptionTest {
        
        private final Company oCompany;
        private final ClientRegistry oClients;
        private final Client oClient;
        
        public DuplicatedIDExceptionTest() throws DuplicatedIDException {
                oCompany = new Company("Test", "Test");
                oClients = oCompany.getClientRegistry();
                oClient = new Client("Teste", "Teste", "Teste", "Teste", new Address("Street", new PostalCode("", 0, 0), "Locality"));
                oClients.registerClient(oClient, "123");
        }

        @Test
        public void testDuplicatedIDException() {
                Throwable exception = assertThrows(DuplicatedIDException.class, () -> oClients.registerClient(oClient, "123"));
                assertEquals("Email or NIF already registered.", exception.getMessage());
        }
}
