package lapr.project.gpsd.exception;

import lapr.project.gpsd.model.Availability;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class InvalidAvailabilityExceptionTest {

        @Test
        public void testInvalidAvailabilityException() {
                Throwable exception = assertThrows(InvalidAvailabilityException.class, () -> new Availability("04/06/2019", "23:00", "04/06/2019","22:00"));
                assertEquals("Invalid Availability", exception.getMessage());
        }
        
}
