package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.auth.AuthFacade;
import lapr.project.gpsd.exception.DuplicatedIDException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ClientRegistryTest {
        
        private final ClientRegistry oRegistry;
        private final Client oClient;
        private final Address oAddress;
        private final PostalCode oPostalCode;
        private final Client oClient2;
        private final Address oAddress2;
        private final PostalCode oPostalCode2;
        
        public ClientRegistryTest() throws DuplicatedIDException {
                this.oRegistry = new ClientRegistry(new AuthFacade());
                this.oPostalCode = new PostalCode("0000-000", 0, 0);
                this.oAddress = new Address("testStreet", oPostalCode, "testLocality");
                this.oClient = new Client("testName", "testNif", "testTel", "testEmail", oAddress);
                this.oPostalCode2 = new PostalCode("0000-002", 2, 2);
                this.oAddress2 = new Address("testStreet2", oPostalCode2, "testLocality2");
                this.oClient2 = new Client("testName2", "testNif2", "testTel2", "testEmail2", oAddress2);
                oRegistry.registerClient(oClient, "entrar");
        }

        /**
         * Test of getClients method, of class ClientRegistry.
         */
        @Test
        public void testGetClients() {
                System.out.println("getClients");
                List<Client> expResult = new ArrayList<>();
                expResult.add(oClient);
                List<Client> result = oRegistry.getClients();
                assertEquals(expResult, result);
        }

        /**
         * Test of newClient method, of class ClientRegistry.
         */
        @Test
        public void testNewClient() {
                System.out.println("newClient");
                String strName = "testName";
                String strNIF = "testNif";
                String strTelephone = "testTel";
                String strEmail = "testEmail";
                Client expResult = oClient;
                Client result = oRegistry.newClient(strName, strNIF, strTelephone, strEmail, oAddress);
                assertEquals(expResult, result);
        }

        /**
         * Test of addClient method, of class ClientRegistry.
         */
        @Test
        public void testAddClient() {
                System.out.println("addClient");
                boolean expResult = false;
                boolean result = oRegistry.addClient(null);
                assertEquals(expResult, result);
                expResult = true;
                result = oRegistry.addClient(oClient2);
                assertEquals(expResult, result);
        }

        /**
         * Test of getClientByEmail method, of class ClientRegistry.
         */
        @Test
        public void testGetClientByEmail() {
                System.out.println("getClientByEmail");
                String strEmail = "testEmail";
                Client expResult = oClient;
                Client result = oRegistry.getClientByEmail(strEmail);
                assertEquals(expResult, result);
                expResult = null;
                result = oRegistry.getClientByEmail("ofoofod");
                assertEquals(expResult, result);
        }

        /**
         * Test of getClientByNif method, of class ClientRegistry.
         */
        @Test
        public void testGetClientByNif() {
                System.out.println("getClientByNif");
                String strEmail = "testNif";
                Client expResult = oClient;
                Client result = oRegistry.getClientByNif(strEmail);
                assertEquals(expResult, result);
                expResult = null;
                result = oRegistry.getClientByNif("ofoofod");
                assertEquals(expResult, result);
        }

        /**
         * Test of registerClient method, of class ClientRegistry.
         */
        @Test
        public void testRegisterClient() throws Exception {
                System.out.println("registerClient");
                Throwable exception = assertThrows(DuplicatedIDException.class, () -> oRegistry.registerClient(oClient, null));
                assertEquals("Email or NIF already registered.", exception.getMessage());
                exception = assertThrows(DuplicatedIDException.class, () -> oRegistry.registerClient(oClient, "entrar"));
                assertEquals("Email or NIF already registered.", exception.getMessage());
                boolean expResult = true;
                boolean result = oRegistry.registerClient(oClient2, "entrar");
                assertEquals(expResult, result);
        }

        /**
         * Test of validateClient method, of class ClientRegistry.
         */
        @Test
        public void testValidateClient() {
                System.out.println("validateClient");
                boolean expResult = false;
                boolean result = oRegistry.validateClient(oClient, null);
                assertEquals(expResult, result);
                result = oRegistry.validateClient(oClient, "entrar");
                assertEquals(expResult, result);
                expResult = true;
                result = oRegistry.validateClient(oClient2, "entrar");
                assertEquals(expResult, result);
        }
        
}
