package lapr.project.gpsd.model;

import lapr.project.auth.AuthFacade;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class CompanyTest {
        
        private final Company oCompany;
        
        public CompanyTest() {
                this.oCompany = new Company("test", "test");
        }

        /**
         * Test of toString method, of class Company.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = String.format("Company - test %nTax Identification Number:test");;
                String result = oCompany.toString();
                assertEquals(expResult, result);
        }

        /**
         * Test of getAuthFacade method, of class Company.
         */
        @Test
        public void testGetAuthFacade() {
                System.out.println("getAuthFacade");
                AuthFacade expResult = oCompany.getAuthFacade();
                AuthFacade result = oCompany.getAuthFacade();
                assertEquals(expResult, result);
        }

        /**
         * Test of getClientRegistry method, of class Company.
         */
        @Test
        public void testGetClientRegistry() {
                System.out.println("getClientRegistry");
                ClientRegistry expResult = oCompany.getClientRegistry();
                ClientRegistry result = oCompany.getClientRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getGeographicAreaRegistry method, of class Company.
         */
        @Test
        public void testGetGeographicAreaRegistry() {
                System.out.println("getGeographicAreaRegistry");
                GeographicAreaRegistry expResult = oCompany.getGeographicAreaRegistry();
                GeographicAreaRegistry result = oCompany.getGeographicAreaRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getCategoryRegistry method, of class Company.
         */
        @Test
        public void testGetCategoryRegistry() {
                System.out.println("getCategoryRegistry");
                CategoryRegistry expResult = oCompany.getCategoryRegistry();
                CategoryRegistry result = oCompany.getCategoryRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getServiceRegistry method, of class Company.
         */
        @Test
        public void testGetServiceRegistry() {
                System.out.println("getServiceRegistry");
                ServiceRegistry expResult = oCompany.getServiceRegistry();
                ServiceRegistry result = oCompany.getServiceRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getServiceTypeRegistry method, of class Company.
         */
        @Test
        public void testGetServiceTypeRegistry() {
                System.out.println("getServiceTypeRegistry");
                ServiceTypeRegistry expResult = oCompany.getServiceTypeRegistry();
                ServiceTypeRegistry result = oCompany.getServiceTypeRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getServiceProviderRegistry method, of class Company.
         */
        @Test
        public void testGetServiceProviderRegistry() {
                System.out.println("getServiceProviderRegistry");
                ServiceProviderRegistry expResult = oCompany.getServiceProviderRegistry();
                ServiceProviderRegistry result = oCompany.getServiceProviderRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getRequestRegistry method, of class Company.
         */
        @Test
        public void testGetRequestRegistry() {
                System.out.println("getRequestRegistry");
                ServiceProvisionRequestRegistry expResult = oCompany.getRequestRegistry();
                ServiceProvisionRequestRegistry result = oCompany.getRequestRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getServiceRatingRegistry method, of class Company.
         */
        @Test
        public void testGetServiceRatingRegistry() {
                System.out.println("getServiceRatingRegistry");
                ServiceRatingRegistry expResult = oCompany.getServiceRatingRegistry();
                ServiceRatingRegistry result = oCompany.getServiceRatingRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getApplicationRegistry method, of class Company.
         */
        @Test
        public void testGetApplicationRegistry() {
                System.out.println("getApplicationRegistry");
                ApplicationRegistry expResult = oCompany.getApplicationRegistry();
                ApplicationRegistry result = oCompany.getApplicationRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getAttribuitionRegistry method, of class Company.
         */
        @Test
        public void testGetAttribuitionRegistry() {
                System.out.println("getAttribuitionRegistry");
                AttribuitionRegistry expResult = oCompany.getAttribuitionRegistry();
                AttribuitionRegistry result = oCompany.getAttribuitionRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getServiceOrderRegistry method, of class Company.
         */
        @Test
        public void testGetServiceOrderRegistry() {
                System.out.println("getServiceOrderRegistry");
                ServiceOrderRegistry expResult = oCompany.getServiceOrderRegistry();
                ServiceOrderRegistry result = oCompany.getServiceOrderRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getExportTypeRegistry method, of class Company.
         */
        @Test
        public void testGetExportTypeRegistry() {
                System.out.println("getExportTypeRegistry");
                ExportTypeRegistry expResult = oCompany.getExportTypeRegistry();
                ExportTypeRegistry result = oCompany.getExportTypeRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getWorkReportRegistry method, of class Company.
         */
        @Test
        public void testGetWorkReportRegistry() {
                System.out.println("getWorkReportRegistry");
                WorkReportRegistry expResult = oCompany.getWorkReportRegistry();
                WorkReportRegistry result = oCompany.getWorkReportRegistry();
                assertEquals(expResult, result);
        }

        /**
         * Test of getExternalService method, of class Company.
         */
        @Test
        public void testGetExternalService() {
                System.out.println("getExternalService");
                ExternalService expResult = oCompany.getExternalService();
                ExternalService result = oCompany.getExternalService();
                assertEquals(expResult, result);
        }
        
}
