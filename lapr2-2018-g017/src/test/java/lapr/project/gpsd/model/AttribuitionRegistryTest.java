package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AttribuitionRegistryTest {
        
        private final AttribuitionRegistry oRegistry;
        private final Attribuition oAttribuition;
        private final ServiceProvider oProvider;
        private final Category oCategory;
        private final Service oService;
        private final GeographicArea oGeographicArea;
        private final PostalCode oPostalCode;
        private final SchedulePreference oSchedule;
        private final PromptedServiceDescription oServiceDesc;
        
        public AttribuitionRegistryTest() {
                oRegistry = new AttribuitionRegistry();
                this.oCategory = new Category("01", "Test");
                this.oService = new LimitedService("01", "testb", "testc", 12, oCategory);
                this.oPostalCode = new PostalCode("0000-000", 0, 0);
                this.oGeographicArea = new GeographicArea("Test-1", oPostalCode, 23, 23);
                List<Category> lstCategories = new ArrayList<>();
                lstCategories.add(oCategory);
                List<GeographicArea> lstGeographicAreas = new ArrayList<>();
                lstGeographicAreas.add(oGeographicArea);
                this.oProvider = new ServiceProvider("0001", "0002", "0003", "0004", "0005", lstCategories, lstGeographicAreas);
                this.oSchedule = new SchedulePreference(1, "04/06/2019", "09:00");
                this.oServiceDesc = new PromptedServiceDescription(oService, "32323", "01:30");
                this.oAttribuition = new Attribuition(oServiceDesc, null, oProvider, oSchedule);
        }

        /**
         * Test of getAttribuitions method, of class AttribuitionRegistry.
         */
        @Test
        public void testGetAttribuitions() {
                System.out.println("getAttribuitions");
                List<Attribuition> expResult = new ArrayList<>();
                expResult.add(oAttribuition);
                oRegistry.addAttribuition(oAttribuition);
                List<Attribuition> result = oRegistry.getAttribuitions();
                assertEquals(expResult, result);
        }

        /**
         * Test of isRejected method, of class AttribuitionRegistry.
         */
        @Test
        public void testIsRejected() {
                System.out.println("isRejected");
                oRegistry.addAttribuition(oAttribuition);
                boolean expResult = false;
                boolean result = oRegistry.isRejected(oAttribuition);
                assertEquals(expResult, result);
                oAttribuition.setDecision(false);
                expResult = true;
                result = oRegistry.isRejected(oAttribuition);
                assertEquals(expResult, result);
        }

        /**
         * Test of getRejectedAttribuitions method, of class AttribuitionRegistry.
         */
        @Test
        public void testGetRejectedAttribuitions() {
                System.out.println("getRejectedAttribuitions");
                oRegistry.addAttribuition(oAttribuition);
                oAttribuition.setDecision(false);
                List<Attribuition> expResult = new ArrayList<>();
                expResult.add(oAttribuition);
                List<Attribuition> result = oRegistry.getRejectedAttribuitions();
                assertEquals(expResult, result);
        }

        /**
         * Test of addAttribuition method, of class AttribuitionRegistry.
         */
        @Test
        public void testAddAttribuition() {
                System.out.println("addAttribuition");
                boolean expResult = true;
                boolean result = oRegistry.addAttribuition(oAttribuition);
                assertEquals(expResult, result);
                expResult = false;
                result = oRegistry.addAttribuition(null);
                assertEquals(expResult, result);
        }

        /**
         * Test of validateAttribuition method, of class AttribuitionRegistry.
         */
        @Test
        public void testValidateAttribuition() {
                System.out.println("validateAttribuition");
                boolean expResult = true;
                boolean result = oRegistry.validateAttribuition(oAttribuition);
                assertEquals(expResult, result);
                oRegistry.addAttribuition(oAttribuition);
                expResult = false;
                result = oRegistry.validateAttribuition(oAttribuition);
                assertEquals(expResult, result);
        }
}
