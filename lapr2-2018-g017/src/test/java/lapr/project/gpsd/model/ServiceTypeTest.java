/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import static org.junit.Assert.*;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Simao
 */
public class ServiceTypeTest {
        
    @Test
    public void testValues() {
        System.out.println("values");
        ServiceType[] expResult = { ServiceType.STATIONARY,ServiceType.LIMITED,ServiceType.EXPANDABLE};
        ServiceType[] result = ServiceType.values();
        assertArrayEquals(expResult, result);

    }

    @Test
    public void testValueOf() {
        System.out.println("valueOf");
        String name = "STATIONARY";
        ServiceType expResult = ServiceType.STATIONARY;
        ServiceType result = ServiceType.valueOf(name);
        assertEquals(expResult, result);

    }

   @Test
   public void testNewServiceExpandable() {
       System.out.println("newServiceExpandable");
       String strId = "12";
       String strBriefDesc = "jardim";
       String strCompleteDesc = "corta relva";
       double dCost = 10.0;
       Category oCategory = new Category("02","jardinagem");
       String expResult = "Expandable";
       String result = ServiceType.EXPANDABLE.newService(strId, strBriefDesc, strCompleteDesc, dCost, oCategory).getServiceType().getId();
       assertEquals(expResult, result);
  }
      @Test
   public void testNewServiceStationary() {
       System.out.println("newServiceStationary");
       String strId = "12";
       String strBriefDesc = "jardim";
       String strCompleteDesc = "corta relva";
       double dCost = 10.0;
       Category oCategory = new Category("02","jardinagem");
       String expResult = "Stationary";
       String result = ServiceType.STATIONARY.newService(strId, strBriefDesc, strCompleteDesc, dCost, oCategory).getServiceType().getId();
       assertEquals(expResult, result);
  }
      @Test
   public void testNewServiceLimited() {
       System.out.println("newServiceLimited");
       String strId = "12";
       String strBriefDesc = "jardim";
       String strCompleteDesc = "corta relva";
       double dCost = 10.0;
       Category oCategory = new Category("02","jardinagem");
       String expResult = "Limited";
       String result = ServiceType.LIMITED.newService(strId, strBriefDesc, strCompleteDesc, dCost, oCategory).getServiceType().getId();
       assertEquals(expResult, result);
  }

    @Test
    public void testGetId() {
        System.out.println("getId");
        ServiceType instance = ServiceType.EXPANDABLE;
        String expResult = "Expandable";
        String result = instance.getId();
        assertEquals(expResult, result);
    }

    @Test
    public void testToString() {
        System.out.println("toString");
        ServiceType instance = ServiceType.EXPANDABLE;
        String expResult = "Expandable";
        String result = instance.toString();
        assertEquals(expResult, result);
    }
    
}
