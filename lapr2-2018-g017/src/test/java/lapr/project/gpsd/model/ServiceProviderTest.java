/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.exception.InvalidAvailabilityException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Lenovo
 */
public class ServiceProviderTest {

    private final List<Availability> lstAvailabilities = new ArrayList<>();
    private final Category category = new Category("111", "Cleaning");
    private final List<Category> lstCategories = new ArrayList<>();
    private final PostalCode postalCode = new PostalCode("4490-982", 0, 0);
    private List<ActsIn> lstActsIn;
    private final GeographicArea geoArea = new GeographicArea("designation", postalCode, 0, 5, lstActsIn);
    private final List<GeographicArea> lstGeographicAreas = new ArrayList<>();
    private final ExportList lstExports = new ExportList();

    ServiceProvider serviceProvider = new ServiceProvider("12345", "14413904", "Vasco Miguel Medeiros Furtado", "Vasco Furtado", "email@gmail.com", lstCategories, lstGeographicAreas);

    public ServiceProviderTest() {
    }

    /**
     * Test of getPersonnelNumber method, of class ServiceProvider.
     */
    @Test
    public void testGetPersonnelNumber() {
        System.out.println("getPersonnelNumber");
        String expResult = "12345";
        String result = serviceProvider.getPersonnelNumber();
        assertEquals(expResult, result);

    }

    /**
     * Test of getNif method, of class ServiceProvider.
     */
    @Test
    public void testGetNif() {
        System.out.println("getNif");
        String expResult = "14413904";
        String result = serviceProvider.getNif();
        assertEquals(expResult, result);

    }

    /**
     * Test of getFullName method, of class ServiceProvider.
     */
    @Test
    public void testGetFullName() {
        System.out.println("getFullName");
        String expResult = "Vasco Miguel Medeiros Furtado";
        String result = serviceProvider.getFullName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAbbreviatedName method, of class ServiceProvider.
     */
    @Test
    public void testGetAbbreviatedName() {
        System.out.println("getAbbreviatedName");
        String expResult = "Vasco Furtado";
        String result = serviceProvider.getAbbreviatedName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getEmail method, of class ServiceProvider.
     */
    @Test
    public void testGetEmail() {
        System.out.println("getEmail");
        String expResult = "email@gmail.com";
        String result = serviceProvider.getEmail();
        assertEquals(expResult, result);

    }

    /**
     * Test of getTelephone method, of class ServiceProvider.
     */
    @Test
    public void testGetTelephone() {
        System.out.println("getTelephone");
        serviceProvider.setTelephone("910961976");
        String expResult = "910961976";
        String result = serviceProvider.getTelephone();
        assertEquals(expResult, result);

    }

    /**
     * Test of getCategories method, of class ServiceProvider.
     */
    @Test
    public void testGetCategories() {
        System.out.println("getCategories");
        List<Category> expResult = new ArrayList<>();
        serviceProvider.addCategory(category);
        expResult.add(category);
        List<Category> result = serviceProvider.getCategories();
        assertEquals(expResult, result);

    }

    /**
     * Test of getGeographicAreas method, of class ServiceProvider.
     */
    @Test
    public void testGetGeographicAreas() {
        System.out.println("getGeographicAreas");

        List<GeographicArea> expResult = new ArrayList<>();
        serviceProvider.addGeographicArea(geoArea);
        expResult.add(geoArea);
        List<GeographicArea> result = serviceProvider.getGeographicAreas();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAvailabilities method, of class ServiceProvider.
     *
     * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
     */
    @Test
    public void testGetAvailabilities() throws InvalidAvailabilityException {
        System.out.println("getAvailabilities");

        Availability availability = new Availability("04/06/2019", "11:00", "04/06/2019", "14:30");
        List<Availability> expResult = new ArrayList<>();
        serviceProvider.addAvailability(availability);
        lstAvailabilities.add(availability);
        expResult.add(availability);
        List<Availability> result = serviceProvider.getAvailabilities();
        assertEquals(expResult, result);

    }

    /**
     * Test of setTelephone method, of class ServiceProvider.
     */
    @Test
    public void testSetTelephone() {
        System.out.println("setTelephone");
        String strTelephone = "919285852";
        serviceProvider.setTelephone(strTelephone);

    }

    /**
     * Test of hasEmail method, of class ServiceProvider.
     */
    @Test
    public void testHasEmail() {
        System.out.println("hasEmail");
        String strEmail = "email@gmail.com";
        boolean expResult = true;
        boolean result = serviceProvider.hasEmail(strEmail);
        assertEquals(expResult, result);

    }

    /**
     * Test of hasPersonnelNumber method, of class ServiceProvider.
     */
    @Test
    public void testHasPersonnelNumber() {
        System.out.println("hasPersonnelNumber");
        String strPersonnelNumber = "12345";
        boolean expResult = true;
        boolean result = serviceProvider.hasPersonnelNumber(strPersonnelNumber);
        assertEquals(expResult, result);

    }

    /**
     * Test of addCategory method, of class ServiceProvider.
     */
    @Test
    public void testAddCategory() {
        System.out.println("addCategory");
        boolean expResult = true;
        boolean result = serviceProvider.addCategory(category);
        assertEquals(expResult, result);

    }

    /**
     * Test of addGeographicArea method, of class ServiceProvider.
     */
    @Test
    public void testAddGeographicArea() {
        System.out.println("addGeographicArea");
        boolean expResult = true;
        boolean result = serviceProvider.addGeographicArea(geoArea);
        assertEquals(expResult, result);

    }

    /**
     * Test of addAvailability method, of class ServiceProvider.
     *
     * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
     */
    @Test
    public void testAddAvailability() throws InvalidAvailabilityException {
        System.out.println("addAvailability");
        Availability availability = new Availability("04/06/2019", "11:00", "04/06/2019", "14:30");
        boolean expResult = true;
        boolean result = serviceProvider.addAvailability(availability);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeCategory method, of class ServiceProvider.
     */
    @Test
    public void testRemoveCategory() {
        System.out.println("removeCategory");
        lstCategories.add(category);
        boolean expResult = false;
        boolean result = serviceProvider.removeCategory(category);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeGeographicArea method, of class ServiceProvider.
     */
    @Test
    public void testRemoveGeographicArea() {
        System.out.println("removeGeographicArea");
        lstGeographicAreas.add(geoArea);
        boolean expResult = false;
        boolean result = serviceProvider.removeGeographicArea(geoArea);
        assertEquals(expResult, result);

    }

    /**
     * Test of removeAvailability method, of class ServiceProvider.
     *
     * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
     */
    @Test
    public void testRemoveAvailability() throws InvalidAvailabilityException {
        System.out.println("removeAvailability");
        Availability availability = new Availability("04/06/2019", "11:00", "04/06/2019", "14:30");
        lstAvailabilities.add(availability);

        boolean expResult = false;
        boolean result = serviceProvider.removeAvailability(availability);
        assertEquals(expResult, result);

    }

    /**
     * Test of validateCategory method, of class ServiceProvider.
     */
    @Test
    public void testValidateCategory() {
        System.out.println("validateCategory");
        boolean expResult = true;
        boolean result = serviceProvider.validateCategory(category);
        assertEquals(expResult, result);

    }

    /**
     * Test of validateGeographicArea method, of class ServiceProvider.
     */
    @Test
    public void testValidateGeographicArea() {
        System.out.println("validateGeographicArea");
        boolean expResult = true;
        boolean result = serviceProvider.validateGeographicArea(geoArea);
        assertEquals(expResult, result);

    }

    /**
     * Test of validateAvailability method, of class ServiceProvider.
     *
     * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
     */
    @Test
    public void testValidateAvailability() throws InvalidAvailabilityException {
        System.out.println("validateAvailability");
        Availability availability = new Availability("04/06/2019", "11:00", "04/06/2019", "14:30");
        boolean expResult = true;
        boolean result = serviceProvider.validateAvailability(availability);
        assertEquals(expResult, result);

    }

    /**
     * Test of getEvaluation method, of class ServiceProvider.
     */
    @Test
    public void testGetEvaluation() {
        System.out.println("getEvaluation");
        ServiceProviderEvaluation expResult = ServiceProviderEvaluation.REGULAR;
        serviceProvider.setEvaluation(expResult);
        ServiceProviderEvaluation result = serviceProvider.getEvaluation();
        assertEquals(expResult, result);

    }

    /**
     * Test of setEvaluation method, of class ServiceProvider.
     */
    @Test
    public void testSetEvaluation() {
        System.out.println("setEvaluation");
        ServiceProviderEvaluation expResult = ServiceProviderEvaluation.REGULAR;
        serviceProvider.setEvaluation(expResult);
        ServiceProviderEvaluation result = serviceProvider.getEvaluation();
        assertEquals(expResult, result);
    }

}
