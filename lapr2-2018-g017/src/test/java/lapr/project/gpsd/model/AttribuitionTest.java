package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AttribuitionTest {
        
        private final Attribuition oAttribuition;
        private final ServiceProvider oProvider;
        private final Category oCategory;
        private final Service oService;
        private final GeographicArea oGeographicArea;
        private final PostalCode oPostalCode;
        private final SchedulePreference oSchedule;
        private final PromptedServiceDescription oServiceDesc;
        
        public AttribuitionTest() {
                this.oCategory = new Category("01", "Test");
                this.oService = new LimitedService("01", "testb", "testc", 12, oCategory);
                this.oPostalCode = new PostalCode("0000-000", 0, 0);
                this.oGeographicArea = new GeographicArea("Test-1", oPostalCode, 23, 23);
                List<Category> lstCategories = new ArrayList<>();
                lstCategories.add(oCategory);
                List<GeographicArea> lstGeographicAreas = new ArrayList<>();
                lstGeographicAreas.add(oGeographicArea);
                this.oProvider = new ServiceProvider("0001", "0002", "0003", "0004", "0005", lstCategories, lstGeographicAreas);
                this.oSchedule = new SchedulePreference(1, "04/06/2019", "09:00");
                this.oServiceDesc = new PromptedServiceDescription(oService, "32323", "01:30");
                this.oAttribuition = new Attribuition(oServiceDesc, null, oProvider, oSchedule);
        }

        /**
         * Test of getProvider method, of class Attribuition.
         */
        @Test
        public void testGetProvider() {
                System.out.println("getProvider");
                ServiceProvider expResult = oProvider;
                ServiceProvider result = oAttribuition.getProvider();
                assertEquals(expResult, result);
        }

        /**
         * Test of getRequest method, of class Attribuition.
         */
        @Test
        public void testGetRequest() {
                System.out.println("getRequest");
                ServiceProvisionRequest expResult = null;
                ServiceProvisionRequest result = oAttribuition.getRequest();
                assertEquals(expResult, result);
        }

        /**
         * Test of getRequestedService method, of class Attribuition.
         */
        @Test
        public void testGetRequestedService() {
                System.out.println("getRequestedService");
                PromptedServiceDescription expResult = oServiceDesc;
                PromptedServiceDescription result = oAttribuition.getRequestedService();
                assertEquals(expResult, result);
        }

        /**
         * Test of getShedule method, of class Attribuition.
         */
        @Test
        public void testGetShedule() {
                System.out.println("getShedule");
                SchedulePreference expResult = oSchedule;
                SchedulePreference result = oAttribuition.getShedule();
                assertEquals(expResult, result);
        }

        /**
         * Test of getDecision method, of class Attribuition.
         */
        @Test
        public void testGetDecision() {
                System.out.println("getDecision");
                boolean expResult = false;
                boolean result = oAttribuition.getDecision();
                assertEquals(expResult, result);
        }

        /**
         * Test of isAccepted method, of class Attribuition.
         */
        @Test
        public void testIsAccepted() {
                System.out.println("isAccepted");
                boolean expResult = false;
                boolean result = oAttribuition.isAccepted();
                assertEquals(expResult, result);
        }

        /**
         * Test of setDecision method, of class Attribuition.
         */
        @Test
        public void testSetDecision() {
                System.out.println("setDecision");
                boolean decision = true;
                oAttribuition.setDecision(decision);
                boolean result = oAttribuition.isAccepted();
        }
        
}
