/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Simao
 */
public class ServiceTypeRegistryTest {
    
 
    @Test
    public void testGetServiceTypeByIdTrue() {
        System.out.println("getServiceTypeById");
        String strId = "Stationary";
        ServiceTypeRegistry instance = new ServiceTypeRegistry();
        ServiceType expResult = ServiceType.STATIONARY;
        ServiceType result = instance.getServiceTypeById(strId);
        assertEquals(expResult, result);
    }

     
    @Test
    public void testGetServiceTypeByIdFalse() {
        System.out.println("getServiceTypeById");
        String strId = "Expandable";
        ServiceTypeRegistry instance = new ServiceTypeRegistry();
        ServiceType expResult = ServiceType.STATIONARY;
        ServiceType result = instance.getServiceTypeById(strId);
        assertNotEquals(expResult,result);
    }
      @Test
    public void testGetServiceTypeByIdNull() {
        System.out.println("getServiceTypeById");
        String strId = "nothing";
        ServiceTypeRegistry instance = new ServiceTypeRegistry();
        ServiceType expResult = null;
        ServiceType result = instance.getServiceTypeById(strId);
        assertEquals(expResult,result);
    }
    
    @Test
    public void testGetServiceTypes() {
        System.out.println("getServiceTypes");
        ServiceTypeRegistry instance = new ServiceTypeRegistry();
        List<ServiceType> expResult = new ArrayList<>();
        expResult.add(ServiceType.STATIONARY);
        expResult.add(ServiceType.LIMITED);
        expResult.add(ServiceType.EXPANDABLE);
        List<ServiceType> result = instance.getServiceTypes();
        assertEquals(expResult, result);
    }
    
}
