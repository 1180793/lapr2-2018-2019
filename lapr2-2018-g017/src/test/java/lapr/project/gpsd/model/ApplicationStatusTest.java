package lapr.project.gpsd.model;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ApplicationStatusTest {
        
        
        
        public ApplicationStatusTest() {
        }

        /**
         * Test of values method, of class ApplicationStatus.
         */
        @Test
        public void testValues() {
                System.out.println("values");
                ApplicationStatus[] expResult = {ApplicationStatus.PENDING, ApplicationStatus.ACCEPTED, ApplicationStatus.REJECTED};
                ApplicationStatus[] result = ApplicationStatus.values();
                assertArrayEquals(expResult, result);
        }

        /**
         * Test of valueOf method, of class ApplicationStatus.
         */
        @Test
        public void testValueOf() {
                System.out.println("valueOf");
                String name = "REJECTED";
                ApplicationStatus result = ApplicationStatus.valueOf(name);
                assertEquals(ApplicationStatus.REJECTED, result);
        }

        /**
         * Test of getStatus method, of class ApplicationStatus.
         */
        @Test
        public void testGetStatus() {
                System.out.println("getStatus");
                String expResult = "Verified and Rejected";
                String result = ApplicationStatus.REJECTED.getStatus();
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class ApplicationStatus.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "Verified and Rejected";
                String result = ApplicationStatus.REJECTED.toString();
                assertEquals(expResult, result);
        }
        
}
