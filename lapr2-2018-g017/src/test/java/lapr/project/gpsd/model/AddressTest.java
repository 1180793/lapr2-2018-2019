/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Lenovo
 */
public class AddressTest {

        PostalCode oPostalCode = new PostalCode("4490-952", 0, 0);
        Address instance = new Address("Street number 3", oPostalCode, "Povoa de Varzim");

        /**
         * Test of getStreet method, of class Address.
         */
        @Test
        public void testGetStreet() {
                System.out.println("getStreet");
                String expResult = "Street number 3";
                String result = instance.getStreet();
                assertEquals(expResult, result);

        }

        /**
         * Test of getLocality method, of class Address.
         */
        @Test
        public void testGetLocality() {
                System.out.println("getLocality");
                String expResult = "Povoa de Varzim";
                String result = instance.getLocality();
                assertEquals(expResult, result);

        }

        /**
         * Test of getPostalCodeString method, of class Address.
         */
        @Test
        public void testGetPostalCodeString() {
                System.out.println("getPostalCodeString");
                String expResult = "4490-952";
                String result = instance.getPostalCodeString();
                assertEquals(expResult, result);

        }

        /**
         * Test of getPostalCode method, of class Address.
         */
        @Test
        public void testGetPostalCode() {
                System.out.println("getPostalCode");
                PostalCode expResult = oPostalCode;
                PostalCode result = instance.getPostalCode();
                assertEquals(expResult, result);

        }

        /**
         * Test of hashCode method, of class Role.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = -252316420;
                int result = instance.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class Address.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                Address instance2 = new Address("Street number 3", oPostalCode, "Povoa de Varzim");
                boolean expResult = true;
                boolean result = instance.equals(instance2);
                assertEquals(expResult, result);
                result = instance.equals(instance);
                assertEquals(expResult, result);
                expResult = false;
                result = instance.equals(null);
                assertEquals(expResult, result);
                result = instance.equals(new ProfessionalQualification("tttt"));
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class Address.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "Street number 3 | 4490-952 - Povoa de Varzim";
                String result = instance.toString();
                assertEquals(expResult, result);

        }

}
