package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.exception.DuplicatedIDException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class CategoryRegistryTest {
        
        private final CategoryRegistry oCategories;
        private final Category oCategory;
        private final Category oCategory2;
        
        private CategoryRegistryTest() throws DuplicatedIDException {
                this.oCategories = new CategoryRegistry();
                this.oCategory = new Category("01", "Test Category");
                this.oCategory2 = new Category("02", "Test Category 2");
                oCategories.registerCategory(oCategory);
        }
        
        /**
         * Test of newCategory method, of class CategoryRegistry.
         */
        @Test
        public void testNewCategory() {
                System.out.println("newCategory");
                String strId = "01";
                String strDescription = "Test Category";
                Category expResult = oCategory;
                Category result = oCategories.newCategory(strId, strDescription);
                assertEquals(expResult, result);
        }

        /**
         * Test of getCategories method, of class CategoryRegistry.
         */
        @Test
        public void testGetCategories() {
                System.out.println("getCategories");
                List<Category> expResult = new ArrayList<>();
                expResult.add(oCategory);
                List<Category> result = oCategories.getCategories();
                assertEquals(expResult, result);
        }

        /**
         * Test of getCategoryById method, of class CategoryRegistry.
         */
        @Test
        public void testGetCategoryById() {
                System.out.println("getCategoryById");
                String strId = "01";
                Category expResult = oCategory;
                Category result = oCategories.getCategoryById(strId);
                assertEquals(expResult, result);
        }

        /**
         * Test of registerCategory method, of class CategoryRegistry.
         * @throws lapr.project.gpsd.exception.DuplicatedIDException
         */
        @Test
        public void testRegisterCategory() throws DuplicatedIDException {
                System.out.println("registerCategory");
                boolean expResult = true;
                boolean result = oCategories.registerCategory(oCategory2);
                assertEquals(expResult, result);
                Throwable exception = assertThrows(DuplicatedIDException.class, () -> oCategories.registerCategory(oCategory));
                assertEquals("Duplicated Category", exception.getMessage());
                assertEquals(expResult, result);
        }
        
        /**
         * Test of validateCategory method, of class CategoryRegistry.
         */
        @Test
        public void testValidateCategory() {
                System.out.println("validateCategory");
                boolean expResult = false;
                boolean result = oCategories.validateCategory(oCategory);
                assertEquals(expResult, result);
                expResult = true;
                result = oCategories.validateCategory(oCategory2);
                assertEquals(expResult, result);
        }
        
}
