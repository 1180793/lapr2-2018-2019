package lapr.project.gpsd.model;

import lapr.project.gpsd.exception.InvalidAvailabilityException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AvailabilityTest {
        
        private Availability oAvailability;
        
        public AvailabilityTest() throws InvalidAvailabilityException {
                this.oAvailability = new Availability("07/06/2019", "09:00", "08/06/2019", "19:00");
        }

        /**
         * Test of getStartDate method, of class Availability.
         */
        @Test
        public void testGetStartDate() {
                System.out.println("getStartDate");
                String expResult = "07/06/2019";
                String result = oAvailability.getStartDate();
                assertEquals(expResult, result);
        }

        /**
         * Test of getStartHour method, of class Availability.
         */
        @Test
        public void testGetStartHour() {
                System.out.println("getStartHour");
                String expResult = "09:00";
                String result = oAvailability.getStartHour();
                assertEquals(expResult, result);
        }

        /**
         * Test of getEndDate method, of class Availability.
         */
        @Test
        public void testGetEndDate() {
                System.out.println("getEndDate");
                String expResult = "08/06/2019";
                String result = oAvailability.getEndDate();
                assertEquals(expResult, result);
        }

        /**
         * Test of getEndHour method, of class Availability.
         */
        @Test
        public void testGetEndHour() {
                System.out.println("getEndHour");
                String expResult = "19:00";
                String result = oAvailability.getEndHour();
                assertEquals(expResult, result);
        }

        /**
         * Test of validateAvailability method, of class Availability.
         */
        @Test
        public void testValidateAvailability() {
                System.out.println("validateAvailability");
                String strStartDate = "07/06/2019";
                String strStartHour = "09:00";
                String strEndDate = "08/06/2019";
                String strEndHour = "19:00";
                boolean expResult = true;
                boolean result = Availability.validateAvailability(strStartDate, strStartHour, strEndDate, strEndHour);
                assertEquals(expResult, result);
                expResult = false;
                result = Availability.validateAvailability(strEndDate, strStartHour, strStartDate, strEndHour);
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class Availability.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = 734210817;
                int result = oAvailability.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class Availability.
         * @throws lapr.project.gpsd.exception.InvalidAvailabilityException
         */
        @Test
        public void testEquals() throws InvalidAvailabilityException {
                System.out.println("equals");
                Availability instance2 = new Availability("07/06/2019", "09:00", "08/06/2019", "19:00");
                boolean expResult = true;
                boolean result = oAvailability.equals(instance2);
                assertEquals(expResult, result);
                result = oAvailability.equals(oAvailability);
                assertEquals(expResult, result);
                expResult = false;
                result = oAvailability.equals(null);
                assertEquals(expResult, result);
                result = oAvailability.equals(new ProfessionalQualification("tttt"));
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class Availability.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "07/06/2019 | 09:00 - 08/06/2019 | 19:00";
                String result = oAvailability.toString();
                assertEquals(expResult, result);
        }
        
}
