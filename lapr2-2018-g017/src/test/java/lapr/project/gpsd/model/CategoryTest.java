package lapr.project.gpsd.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Vasco Furtado <1180837@isep.ipp.pt>
 */
public class CategoryTest {

        private final Category oCategory;

        public CategoryTest() {
                this.oCategory = new Category("01", "Test Category");
        }

        /**
         * Test of hasId method, of class Category.
         */
        @Test
        public void testHasId() {
                System.out.println("hasId");
                String strId = "01";
                boolean expResult = true;
                boolean result = oCategory.hasId(strId);
                assertEquals(expResult, result);

        }

        /**
         * Test of getId method, of class Category.
         */
        @Test
        public void testGetId() {
                System.out.println("getId");
                String expResult = "01";
                String result = oCategory.getId();
                assertEquals(expResult, result);
        }

        /**
         * Test of getDescription method, of class Category.
         */
        @Test
        public void testGetDescription() {
                System.out.println("getDescription");
                String expResult = "Test Category";
                String result = oCategory.getDescription();
                assertEquals(expResult, result);

        }

        /**
         * Test of equals method, of class Category.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                Category category = new Category("01", "Test Category");
                boolean expResult = true;
                boolean result = oCategory.equals(category);
                assertEquals(expResult, result);
                result = oCategory.equals(oCategory);
                assertEquals(expResult, result);
                expResult = false;
                result = oCategory.equals(null);
                assertEquals(expResult, result);
                result = oCategory.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class StationaryService.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = 1740;
                int result = oCategory.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class StationaryService.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "01 | Test Category";
                String result = oCategory.toString();
                assertEquals(expResult, result);
        }

}
