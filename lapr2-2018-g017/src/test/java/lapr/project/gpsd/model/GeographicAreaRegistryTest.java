package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.gpsd.exception.DuplicatedIDException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class GeographicAreaRegistryTest {
        
        private final GeographicAreaRegistry oGeographicAreas;
        private final GeographicArea oGeographicArea;
        private final GeographicArea oGeographicArea2;
        private final PostalCode oPostalCode;
        private final PostalCode oPostalCode2;
        
        private GeographicAreaRegistryTest() throws DuplicatedIDException {
                this.oGeographicAreas = new GeographicAreaRegistry();
                this.oPostalCode = new PostalCode("4490-641", 41.3998301, -8.7668682);
                this.oPostalCode2 = new PostalCode("4495-449", 41.3951172, -8.7526565);
                this.oGeographicArea = new GeographicArea("Test", oPostalCode, 5.3, 10);
                this.oGeographicArea2 = new GeographicArea("Test2", oPostalCode2, 5.3, 10);
                this.oGeographicAreas.registerGeographicArea(oGeographicArea);
        }

        /**
         * Test of getGeographicAreas method, of class GeographicAreaRegistry.
         */
        @Test
        public void testGetGeographicAreas() {
                System.out.println("getGeographicAreas");
                List<GeographicArea> expResult = new ArrayList<>();
                expResult.add(oGeographicArea);
                List<GeographicArea> result = oGeographicAreas.getGeographicAreas();
                assertEquals(expResult, result);
        }

        /**
         * Test of getGeographicAreaById method, of class GeographicAreaRegistry.
         */
        @Test
        public void testGetGeographicAreaById() {
                System.out.println("getGeographicAreaById");
                String strId = "4490-641";
                GeographicArea expResult = oGeographicArea;
                GeographicArea result = oGeographicAreas.getGeographicAreaById(strId);
                assertEquals(expResult, result);
                expResult = null;
                result = oGeographicAreas.getGeographicAreaById("3232-000");
                assertEquals(expResult, result);
        }
        
        /**
         * Test of getGeographicAreaByDesignation method, of class GeographicAreaRegistry.
         */
        @Test
        public void testGetGeographicAreaByDesignation() {
                System.out.println("getGeographicAreaByDesignation");
                String strId = "Test";
                GeographicArea expResult = oGeographicArea;
                GeographicArea result = oGeographicAreas.getGeographicAreaByDesignation(strId);
                assertEquals(expResult, result);
                expResult = null;
                result = oGeographicAreas.getGeographicAreaByDesignation("erer");
                assertEquals(expResult, result);
        }

        /**
         * Test of registerGeographicArea method, of class GeographicAreaRegistry.
         * @throws lapr.project.gpsd.exception.DuplicatedIDException
         */
        @Test
        public void testRegisterGeographicArea() throws DuplicatedIDException {
                System.out.println("registerGeographicArea");
                boolean expResult = true;
                boolean result = oGeographicAreas.registerGeographicArea(oGeographicArea2);
                assertEquals(expResult, result);
                Throwable exception = assertThrows(DuplicatedIDException.class, () -> oGeographicAreas.registerGeographicArea(oGeographicArea));
                assertEquals("Duplicated Geographic Area", exception.getMessage());
                assertEquals(expResult, result);
        }

        /**
         * Test of validateGeographicArea method, of class GeographicAreaRegistry.
         */
        @Test
        public void testValidateGeographicArea() {
                System.out.println("validateGeographicArea");
                boolean expResult = false;
                boolean result = oGeographicAreas.validateGeographicArea(oGeographicArea);
                assertEquals(expResult, result);
                expResult = true;
                result = oGeographicAreas.validateGeographicArea(oGeographicArea2);
                assertEquals(expResult, result);
        }

        /**
         * Test of getClosestGeographicArea method, of class GeographicAreaRegistry.
         */
        @Test
        public void testGetClosestGeographicArea() {
                System.out.println("getClosestGeographicArea");
                GeographicArea expResult = oGeographicArea;
                GeographicArea result = oGeographicAreas.getClosestGeographicArea(oPostalCode2);
                assertEquals(expResult, result);
        }
        
}
