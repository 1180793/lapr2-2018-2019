package lapr.project.gpsd.model;

import java.util.List;
import lapr.project.gpsd.controller.AppGPSD;
import lapr.project.utils.Utils;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Lenovo
 */
public class ServiceProvisionRequestTest {

    PostalCode postalCode = new PostalCode("4490-952", 0, 0);
    Address address = new Address("Street number 5", postalCode, "Povoa de varzim");
    Client client = new Client("Vasco Furtado", "1331204", "919285852", "email@gmail.com", address);
    ServiceProvisionRequest instance = new ServiceProvisionRequest(client, address);
    SchedulePreference schedule = new SchedulePreference(1, "2019-7-27", "10:00");

    public ServiceProvisionRequestTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of setReady method, of class ServiceProvisionRequest.
     */
    @Test
    public void testSetReady() {
        System.out.println("setReady");
        boolean bReady = false;

        instance.setReady(bReady);

    }

    /**
     * Test of isReady method, of class ServiceProvisionRequest.
     */
    @Test
    public void testIsReady() {
        System.out.println("isReady");
        boolean expResult = false;
        boolean result = instance.isReady();
        assertEquals(expResult, result);

    }

    /**
     * Test of addServiceToRequest method, of class ServiceProvisionRequest.
     */
    @Test
    public void testAddServiceToRequest_3args() {
        System.out.println("addServiceToRequest");
        Service oService = null;
        String strDescription = "Clean windows";
        String strDuration = "2:30";

        boolean expResult = true;
        boolean result = instance.addServiceToRequest(oService, strDescription, strDuration);
        assertEquals(expResult, result);

    }

    /**
     * Test of addServiceToRequest method, of class ServiceProvisionRequest.
     */
    @Test
    public void testAddServiceToRequest_PromptedServiceDescription() {
        System.out.println("addServiceToRequest");
        Service oService = null;
        PromptedServiceDescription oRequestedService = new PromptedServiceDescription(oService, "Clean windows", "Povoa de varzim");
        boolean expResult = true;
        boolean result = instance.addServiceToRequest(oRequestedService);
        assertEquals(expResult, result);
    }

    

    /**
     * Test of addSchedule method, of class ServiceProvisionRequest.
     */
    @Test
    public void testAddSchedule_String_String() {
        System.out.println("addSchedule");
        String strDate = "16/06/2019";
        String strHour = "11:30";
        boolean expResult = true;
        boolean result = instance.addSchedule(strDate, strHour);
        assertEquals(expResult, result);
    }

    /**
     * Test of addSchedule method, of class ServiceProvisionRequest.
     */
    @Test
    public void testAddSchedule_SchedulePreference() {
        System.out.println("addSchedule");
        SchedulePreference oSchedule = new SchedulePreference(1, "20/07/2019", "11:00");
        boolean expResult = true;
        boolean result = instance.addSchedule(oSchedule);
        assertEquals(expResult, result);

    }

    /**
     * Test of getClient method, of class ServiceProvisionRequest.
     */
    @Test
    public void testGetClient() {
        System.out.println("getClient");
        Client expResult = client;
        Client result = instance.getClient();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAddress method, of class ServiceProvisionRequest.
     */
    @Test
    public void testGetAddress() {
        System.out.println("getAddress");
        Address expResult = address;
        Address result = instance.getAddress();
        assertEquals(expResult, result);

    }

    /**
     * Test of getStatus method, of class ServiceProvisionRequest.
     */
    @Test
    public void testGetStatus() {
        System.out.println("getStatus");
        String expResult = "Waiting for Service Provider Atribuition";
        String result = instance.getStatus();
        assertEquals(expResult, result);

    }

    /**
     * Test of getNumber method, of class ServiceProvisionRequest.
     */
    @Test
    public void testGetNumber() {
        System.out.println("getNumber");
        int expResult = 0;
        int result = instance.getNumber();
        assertEquals(expResult, result);

    }

    /**
     * Test of getAddressString method, of class ServiceProvisionRequest.
     */
    @Test
    public void testGetAddressString() {
        System.out.println("getAddressString");
        String expResult = "Street number 5 | 4490-952 - Povoa de varzim";
        String result = instance.getAddressString();
        assertEquals(expResult, result);

    }

    

    

    /**
     * Test of setNumber method, of class ServiceProvisionRequest.
     */
    @Test
    public void testSetNumber() {
        System.out.println("setNumber");
        int intNumber = 0;
        instance.setNumber(intNumber);

    }

    /**
     * Test of hasClient method, of class ServiceProvisionRequest.
     */
    @Test
    public void testHasClient() {
        System.out.println("hasClient");
        boolean expResult = true;
        boolean result = instance.hasClient(client);
        assertEquals(expResult, result);

    }

    /**
     * Test of setServiceStatusDone method, of class ServiceProvisionRequest.
     */
    @Test
    public void testSetServiceStatusDone() {
        System.out.println("setServiceStatusDone");
        Service serv = null;
        boolean expResult = false;
        boolean result = instance.setServiceStatusDone(serv);
        assertEquals(expResult, result);

    }

    /**
     * Test of toString method, of class ServiceProvisionRequest.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        String expResult = "0";
        String result = instance.toString();
        assertEquals(expResult, result);

    }

    /**
     * Test of validateServiceRequest method, of class ServiceProvisionRequest.
     */
    @org.junit.Test
    public void testValidateServiceRequest() {
        System.out.println("validateServiceRequest");
        PromptedServiceDescription oRequestedService = null;
        boolean expResult = false;
        boolean result = instance.validateServiceRequest(oRequestedService);
        assertEquals(expResult, result);

    }

        /**
         * Test of createSchedule method, of class ServiceProvisionRequest.
         */
        @org.junit.Test
        public void testCreateSchedule() {
                System.out.println("createSchedule");
                String strDate = "";
                String strHour = "";
                ServiceProvisionRequest instance = null;
                SchedulePreference expResult = null;
                SchedulePreference result = instance.createSchedule(strDate, strHour);
                assertEquals(expResult, result);
                // TODO review the generated test code and remove the default call to fail.
                fail("The test case is a prototype.");
        }

        /**
         * Test of getServiceDesciprionList method, of class ServiceProvisionRequest.
         */
        @org.junit.Test
        public void testGetServiceDesciprionList() {
                System.out.println("getServiceDesciprionList");
                ServiceProvisionRequest instance = null;
                List<PromptedServiceDescription> expResult = null;
                List<PromptedServiceDescription> result = instance.getServiceDesciprionList();
                assertEquals(expResult, result);
                // TODO review the generated test code and remove the default call to fail.
                fail("The test case is a prototype.");
        }

        /**
         * Test of getRequestedServices method, of class ServiceProvisionRequest.
         */
        @org.junit.Test
        public void testGetRequestedServices() {
                System.out.println("getRequestedServices");
                ServiceProvisionRequest instance = null;
                List<PromptedServiceDescription> expResult = null;
                List<PromptedServiceDescription> result = instance.getRequestedServices();
                assertEquals(expResult, result);
                // TODO review the generated test code and remove the default call to fail.
                fail("The test case is a prototype.");
        }

        /**
         * Test of getSchedules method, of class ServiceProvisionRequest.
         */
        @org.junit.Test
        public void testGetSchedules() {
                System.out.println("getSchedules");
                ServiceProvisionRequest instance = null;
                List<SchedulePreference> expResult = null;
                List<SchedulePreference> result = instance.getSchedules();
                assertEquals(expResult, result);
                // TODO review the generated test code and remove the default call to fail.
                fail("The test case is a prototype.");
        }

        /**
         * Test of getDoneServices method, of class ServiceProvisionRequest.
         */
        @org.junit.Test
        public void testGetDoneServices() {
                System.out.println("getDoneServices");
                ServiceProvisionRequest instance = null;
                List<PromptedServiceDescription> expResult = null;
                List<PromptedServiceDescription> result = instance.getDoneServices();
                assertEquals(expResult, result);
                // TODO review the generated test code and remove the default call to fail.
                fail("The test case is a prototype.");
        }

}
