package lapr.project.gpsd.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class StationaryServiceTest {

        private final Category oCategory;
        private final StationaryService oService;

        public StationaryServiceTest() {
                oCategory = new Category("01", "Test Category");
                oService = new StationaryService("01", "Test", "Test Service", 6.2, oCategory);
                oService.setOtherAtributes("02:30");
        }
        
        /**
         * Test for StationaryService constructor.
         */
        @Test
        public void testStationaryService() {
                System.out.println("StationaryService");
                Throwable exception = assertThrows(IllegalArgumentException.class, () -> new StationaryService("", "", "", -1, null));
                assertEquals("Service arguments can't be empty or null.", exception.getMessage());
                Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> new StationaryService(null, null, null, -1, null));
                assertEquals("Service arguments can't be empty or null.", exception2.getMessage());
        }

        /**
         * Test of getId method, of class StationaryService.
         */
        @Test
        public void testGetId() {
                System.out.println("getId");
                String expResult = "01";
                String result = oService.getId();
                assertEquals(expResult, result);
        }

        /**
         * Test of getBriefDescription method, of class StationaryService.
         */
        @Test
        public void testGetBriefDescription() {
                System.out.println("getBriefDescription");
                String expResult = "Test";
                String result = oService.getBriefDescription();
                assertEquals(expResult, result);
        }

        /**
         * Test of getCompleteDescription method, of class StationaryService.
         */
        @Test
        public void testGetCompleteDescription() {
                System.out.println("getCompleteDescription");
                String expResult = "Test Service";
                String result = oService.getCompleteDescription();
                assertEquals(expResult, result);
        }
        
        /**
         * Test of getServiceType method, of class StationaryService.
         */
        @Test
        public void testGetServiceType() {
                System.out.println("getServiceType");
                ServiceType expResult = ServiceType.STATIONARY;
                ServiceType result = oService.getServiceType();
                assertEquals(expResult, result);
        }

        /**
         * Test of getCategory method, of class StationaryService.
         */
        @Test
        public void testGetCategory() {
                System.out.println("getCategory");
                Category expResult = oCategory;
                Category result = oService.getCategory();
                assertEquals(expResult, result);
        }

        /**
         * Test of hasId method, of class StationaryService.
         */
        @Test
        public void testHasId() {
                System.out.println("hasId");
                String strId = "01";
                boolean expResult = true;
                boolean result = oService.hasId(strId);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasOtherAtributes method, of class StationaryService.
         */
        @Test
        public void testHasOtherAtributes() {
                System.out.println("hasOtherAtributes");
                boolean expResult = true;
                boolean result = oService.hasOtherAtributes();
                assertEquals(expResult, result);
        }

        /**
         * Test of getOtherAtributes method, of class StationaryService.
         */
        @Test
        public void testGetOtherAtributes() {
                System.out.println("getOtherAtributes");
                String expResult = "02:30";
                String result = oService.getOtherAtributes();
                assertEquals(expResult, result);
        }

        /**
         * Test of setOtherAtributes method, of class StationaryService.
         */
        @Test
        public void testSetOtherAtributes() {
                System.out.println("setOtherAtributes");
                oService.setOtherAtributes("03:30");
                String expResult = "03:30";
                String result = oService.getOtherAtributes();
                assertEquals(expResult, result);
        }

        /**
         * Test of getCostForDuration method, of class StationaryService.
         */
        @Test
        public void testGetCostForDuration() {
                System.out.println("getCostForDuration");
                String strDuration = "02:30";
                double expResult = 15.5;
                double result = oService.getCostForDuration(strDuration);
                assertEquals(expResult, result, 0.1);
        }

        /**
         * Test of getCostHour method, of class StationaryService.
         */
        @Test
        public void testGetCostHour() {
                System.out.println("getCostHour");
                String expResult = "6.2€";
                String result = oService.getCostHour();
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class StationaryService.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                StationaryService service = new StationaryService("01", "Test", "Test Service", 6.2, oCategory);
                boolean expResult = true;
                boolean result = oService.equals(service);
                assertEquals(expResult, result);
                result = oService.equals(oService);
                assertEquals(expResult, result);
                expResult = false;
                result = oService.equals(null);
                assertEquals(expResult, result);
                result = oService.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class StationaryService.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = 2160;
                int result = oService.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class StationaryService.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "01 - Test Service | 6.2€";
                String result = oService.toString();
                assertEquals(expResult, result);
        }
}
