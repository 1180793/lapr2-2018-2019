package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ApplicationTest {
        
        private final Application oApplication;
        private final Address oAddress;
        private final Category oCategory;
        
        public ApplicationTest() {
                this.oAddress = new Address("TestStreet", new PostalCode("0000-000", 0, 0), "TestLocality");
                this.oApplication = new Application("TestName", "TestNif", "TestTel", "TestEmail", oAddress);
                this.oCategory = new Category("02", "Teste2");
                oApplication.addCategory(oCategory);
        }

        /**
         * Test of getName method, of class Application.
         */
        @Test
        public void testGetName() {
                System.out.println("getName");
                String expResult = "TestName";
                String result = oApplication.getName();
                assertEquals(expResult, result);
        }

        /**
         * Test of getEmail method, of class Application.
         */
        @Test
        public void testGetEmail() {
                System.out.println("getEmail");
                String expResult = "TestEmail";
                String result = oApplication.getEmail();
                assertEquals(expResult, result);
        }

        /**
         * Test of getNif method, of class Application.
         */
        @Test
        public void testGetNif() {
                System.out.println("getNif");
                String expResult = "TestNif";
                String result = oApplication.getNif();
                assertEquals(expResult, result);
        }

        /**
         * Test of getTelephone method, of class Application.
         */
        @Test
        public void testGetTelephone() {
                System.out.println("getTelephone");
                String expResult = "TestTel";
                String result = oApplication.getTelephone();
                assertEquals(expResult, result);
        }

        /**
         * Test of getStatus method, of class Application.
         */
        @Test
        public void testGetStatus() {
                System.out.println("getStatus");
                ApplicationStatus expResult = ApplicationStatus.PENDING;
                ApplicationStatus result = oApplication.getStatus();
                assertEquals(expResult, result);
        }

        /**
         * Test of hasNif method, of class Application.
         */
        @Test
        public void testHasNif() {
                System.out.println("hasNif");
                String strNIF = "TestNif";
                boolean expResult = true;
                boolean result = oApplication.hasNif(strNIF);
                assertEquals(expResult, result);
        }

        /**
         * Test of newAddress method, of class Application.
         */
        @Test
        public void testNewAddress() {
                System.out.println("newAddress");
                String strStreet = "TestStreet";
                PostalCode oPostalCode = new PostalCode("0000-000", 0, 0);
                String strLocality = "TestLocality";
                Address expResult = oAddress;
                Address result = oApplication.newAddress(strStreet, oPostalCode, strLocality);
                assertEquals(expResult, result);
        }

        /**
         * Test of getAddress method, of class Application.
         */
        @Test
        public void testGetAddress() {
                System.out.println("getAddress");
                Address expResult = oAddress;
                Address result = oApplication.getAddress();
                assertEquals(expResult, result);
        }

        /**
         * Test of addAcademicQualification method, of class Application.
         */
        @Test
        public void testAddAcademicQualification() {
                System.out.println("addAcademicQualification");
                String strDesignation = "Test";
                boolean expResult = true;
                boolean result = oApplication.addAcademicQualification(strDesignation);
                assertEquals(expResult, result);
                expResult = false;
                result = oApplication.addAcademicQualification(strDesignation);
                assertEquals(expResult, result);
        }

        /**
         * Test of validateAcademicQualification method, of class Application.
         */
        @Test
        public void testValidateAcademicQualification() {
                System.out.println("validateAcademicQualification");
                AcademicQualification oAcademicQualification = new AcademicQualification("Test");
                boolean expResult = true;
                boolean result = oApplication.validateAcademicQualification(oAcademicQualification);
                assertEquals(expResult, result);
                oApplication.addAcademicQualification("Test");
                expResult = false;
                result = oApplication.validateAcademicQualification(oAcademicQualification);
                assertEquals(expResult, result);
        }

        /**
         * Test of addProfessionalQualification method, of class Application.
         */
        @Test
        public void testAddProfessionalQualification() {
                System.out.println("addProfessionalQualification");
                String strDescription = "Test";
                boolean expResult = true;
                boolean result = oApplication.addProfessionalQualification(strDescription);
                assertEquals(expResult, result);
                expResult = false;
                result = oApplication.addProfessionalQualification(strDescription);
                assertEquals(expResult, result);
        }

        /**
         * Test of validateProfessionalQualification method, of class Application.
         */
        @Test
        public void testValidateProfessionalQualification() {
                System.out.println("validateProfessionalQualification");
                ProfessionalQualification oProfessionalQualification = new ProfessionalQualification("Test");
                boolean expResult = true;
                boolean result = oApplication.validateProfessionalQualification(oProfessionalQualification);
                assertEquals(expResult, result);
                oApplication.addProfessionalQualification("Test");
                expResult = false;
                result = oApplication.validateProfessionalQualification(oProfessionalQualification);
                assertEquals(expResult, result);
        }

        /**
         * Test of getCategory method, of class Application.
         */
        @Test
        public void testGetCategory() {
                System.out.println("getCategory");
                List<Category> expResult = new ArrayList<>();
                expResult.add(oCategory);
                List<Category> result = oApplication.getCategories();
                assertEquals(expResult, result);
        }
        
        /**
         * Test of addCategory method, of class Application.
         */
        @Test
        public void testAddCategory() {
                System.out.println("addCategory");
                Category cate = new Category("01", "TestCategory");
                boolean expResult = true;
                boolean result = oApplication.addCategory(cate);
                assertEquals(expResult, result);
                expResult = false;
                result = oApplication.addCategory(oCategory);
                assertEquals(expResult, result);
        }

        /**
         * Test of validateCategory method, of class Application.
         */
        @Test
        public void testValidateCategory() {
                System.out.println("validateCategory");
                Category oCategory = new Category("01", "TestCategory");
                boolean expResult = true;
                boolean result = oApplication.validateCategory(oCategory);
                assertEquals(expResult, result);
                Category cat2  = new Category("02", "Teste2");
                expResult = false;
                result = oApplication.validateCategory(cat2);
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class Application.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                Application application = new Application("TestName", "TestNif", "TestTel", "TestEmail", oAddress);
                boolean expResult = true;
                boolean result = oApplication.equals(application);
                assertEquals(expResult, result);
                result = oApplication.equals(oApplication);
                assertEquals(expResult, result);
                expResult = false;
                result = oApplication.equals(null);
                assertEquals(expResult, result);
                result = oApplication.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class Application.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = -2091551432;
                int result = oApplication.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class Application.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "TestName - TestNif - TestTel - TestEmail";
                String result = oApplication.toString();
                assertEquals(expResult, result);
        }
        
}
