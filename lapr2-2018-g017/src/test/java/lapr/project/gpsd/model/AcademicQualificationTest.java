package lapr.project.gpsd.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class AcademicQualificationTest {
        
        private final AcademicQualification oAcademicQualification;
        
        public AcademicQualificationTest() {
                this.oAcademicQualification = new AcademicQualification("Test");
        }

        /**
         * Test of getDescription method, of class ProfessionalQualification.
         */
        @Test
        public void testGetDescription() {
                System.out.println("getDesignation");
                String expResult = "Test";
                String result = oAcademicQualification.getDesignation();
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class ProfessionalQualification.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                AcademicQualification qualification = new AcademicQualification("Test");
                boolean expResult = true;
                boolean result = oAcademicQualification.equals(qualification);
                assertEquals(expResult, result);
                result = oAcademicQualification.equals(oAcademicQualification);
                assertEquals(expResult, result);
                expResult = false;
                result = oAcademicQualification.equals(null);
                assertEquals(expResult, result);
                result = oAcademicQualification.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class ProfessionalQualification.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = 2603345;
                int result = oAcademicQualification.hashCode();
                assertEquals(expResult, result);
        }
        
        /**
         * Test of toString method, of class AcademicQualification.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "Test";
                String result = oAcademicQualification.toString();
                assertEquals(expResult, result);
        }
        
}
