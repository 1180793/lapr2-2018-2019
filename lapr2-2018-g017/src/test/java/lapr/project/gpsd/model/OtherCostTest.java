package lapr.project.gpsd.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class OtherCostTest {
        
        private final OtherCost oOtherCost;
        
        public OtherCostTest() {
                this.oOtherCost = new OtherCost("Test", 13);
        }

        /**
         * Test of getDesignation method, of class OtherCost.
         */
        @Test
        public void testGetDesignation() {
                System.out.println("getDesignation");
                String expResult = "Test";
                String result = oOtherCost.getDesignation();
                assertEquals(expResult, result);
        }

        /**
         * Test of getCost method, of class OtherCost.
         */
        @Test
        public void testGetCost() {
                System.out.println("getCost");
                double expResult = 13.0;
                double result = oOtherCost.getCost();
                assertEquals(expResult, result, 0.01);
        }

        /**
         * Test of toString method, of class OtherCost.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = String.format("Test - %.2f", oOtherCost.getCost());;
                String result = oOtherCost.toString();
                assertEquals(expResult, result);
        }
        
}
