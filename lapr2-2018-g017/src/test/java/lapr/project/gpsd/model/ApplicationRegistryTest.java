package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ApplicationRegistryTest {
        
        private final ApplicationRegistry oApplications;
        private final Application oApplication;
        private final Address oAddress;
        
        private ApplicationRegistryTest() {
                this.oApplications = new ApplicationRegistry();
                this.oAddress = new Address("TestStreet", new PostalCode("0000-000", 0, 0), "TestLocality");
                this.oApplication = new Application("TestName", "TestNif", "TestTel", "TestEmail", oAddress);
                this.oApplications.addApplication(oApplication);
        }

        /**
         * Test of getApplications method, of class ApplicationRegistry.
         */
        @Test
        public void testGetApplications() {
                System.out.println("getApplications");
                List<Application> expResult = new ArrayList<>();
                expResult.add(oApplication);
                List<Application> result = oApplications.getApplications();
                assertEquals(expResult, result);
        }

        /**
         * Test of newApplications method, of class ApplicationRegistry.
         */
        @Test
        public void testNewApplication() {
                System.out.println("newApplication");
                Application expResult = oApplication;
                Application result = oApplications.newApplication("TestName", "TestNif", "TestTel", "TestEmail", oAddress);
                assertEquals(expResult, result);
        }

        /**
         * Test of addApplication method, of class ApplicationRegistry.
         */
        @Test
        public void testAddApplication() {
                System.out.println("addApplication");
                String strName2 = "TestName2";
                String strNIF2 = "TestNif2";
                String strTelephone2 = "TestTel2";
                String strEmail2 = "TestEmail2";
                Address oAddress2 = new Address("TestStreet2", new PostalCode("0002-000", 0, 0), "TestLocality2");
                Application oApplication2 = new Application(strName2, strNIF2, strTelephone2, strEmail2, oAddress2);
                boolean expResult = true;
                boolean result = oApplications.addApplication(oApplication2);
                assertEquals(expResult, result);
                oApplications.getApplications().remove(oApplication2);
                expResult = false;
                result = oApplications.addApplication(null);
                assertEquals(expResult, result);
        }

        /**
         * Test of getApplicationByNif method, of class ApplicationRegistry.
         */
        @Test
        public void testGetApplicationByNif() {
                System.out.println("getApplicationByNif");
                String strNif = "TestNif";
                Application expResult = oApplication;
                Application result = oApplications.getApplicationByNif(strNif);
                assertEquals(expResult, result);
                strNif = "NullNif";
                expResult = null;
                result = oApplications.getApplicationByNif(strNif);
                assertEquals(expResult, result);
        }

        /**
         * Test of validateApplication method, of class ApplicationRegistry.
         */
        @Test
        public void testValidateApplication() {
                System.out.println("validateApplication");
                System.out.println("addApplication");
                String strName2 = "TestName2";
                String strNIF2 = "TestNif2";
                String strTelephone2 = "TestTel2";
                String strEmail2 = "TestEmail2";
                Address oAddress2 = new Address("TestStreet2", new PostalCode("0002-000", 0, 0), "TestLocality2");
                Application oApplication2 = new Application(strName2, "TestNif", strTelephone2, strEmail2, oAddress2);
                boolean expResult = false;
                boolean result = oApplications.validateApplication(oApplication2);
                assertEquals(expResult, result);
                oApplication2 = new Application(strName2, strNIF2, strTelephone2, "TestEmail", oAddress2);
                result = oApplications.validateApplication(oApplication2);
                assertEquals(expResult, result);
                oApplication2 = new Application(strName2, strNIF2, strTelephone2, strEmail2, oAddress2);
                expResult = true;
                result = oApplications.validateApplication(oApplication2);
                assertEquals(expResult, result);    
        }
        
}
