package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Goncalo
 */
public class ClientTest {
        
        private final Client oClient;
        private final Address oAddress;
        private final PostalCode oPostalCode;
        
        public ClientTest() {
                this.oPostalCode = new PostalCode("0000-000", 0, 0);
                this.oAddress = new Address("testStreet", oPostalCode, "testLocality");
                this.oClient = new Client("testName", "testNif", "testTel", "testEmail", oAddress);
        }

        /**
         * Test of getName method, of class Client.
         */
        @Test
        public void testGetName() {
                System.out.println("getName");
                String expResult = "testName";
                String result = oClient.getName();
                assertEquals(expResult, result);
        }

        /**
         * Test of getEmail method, of class Client.
         */
        @Test
        public void testGetEmail() {
                System.out.println("getEmail");
                String expResult = "testEmail";
                String result = oClient.getEmail();
                assertEquals(expResult, result);
        }

        /**
         * Test of getNif method, of class Client.
         */
        @Test
        public void testGetNif() {
                System.out.println("getNif");
                String expResult = "testNif";
                String result = oClient.getNif();
                assertEquals(expResult, result);
        }

        /**
         * Test of getTelephone method, of class Client.
         */
        @Test
        public void testGetTelephone() {
                System.out.println("getTelephone");
                String expResult = "testTel";
                String result = oClient.getTelephone();
                assertEquals(expResult, result);
        }

        /**
         * Test of hasEmail method, of class Client.
         */
        @Test
        public void testHasEmail() {
                System.out.println("hasEmail");
                String strEmail = "testEmail";
                boolean expResult = true;
                boolean result = oClient.hasEmail(strEmail);
                assertEquals(expResult, result);
                expResult = false;
                result = oClient.hasEmail("34erf");
                assertEquals(expResult, result);
        }

        /**
         * Test of hasNif method, of class Client.
         */
        @Test
        public void testHasNif() {
                System.out.println("hasNif");
                String strEmail = "testNif";
                boolean expResult = true;
                boolean result = oClient.hasNif(strEmail);
                assertEquals(expResult, result);
                expResult = false;
                result = oClient.hasNif("34erf");
                assertEquals(expResult, result);
        }

        /**
         * Test of getAddresses method, of class Client.
         */
        @Test
        public void testGetAddresses() {
                System.out.println("getAddresses");
                List<Address> expResult = new ArrayList<>();
                expResult.add(oAddress);
                List<Address> result = oClient.getAddresses();
                assertEquals(expResult, result);
        }
        
        /**
         * Test of getAddressString method, of class Client.
         */
        @Test
        public void testGetAddressString() {
                System.out.println("getAddressString");
                String expResult = oAddress.toString();
                String result = oClient.getAddressString();
                assertEquals(expResult, result);
        }

        /**
         * Test of registerAddress method, of class Client.
         */
        @Test
        public void testRegisterAddress() {
                System.out.println("registerAddress");
                boolean expResult = false;
                boolean result = oClient.registerAddress(oAddress);
                assertEquals(expResult, result);
                Address add2 = new Address("testStreet2", oPostalCode, "testLocality2");
                expResult = true;
                result = oClient.registerAddress(add2);
                assertEquals(expResult, result);
        }

        /**
         * Test of removeAddress method, of class Client.
         */
        @Test
        public void testRemoveAddress() {
                System.out.println("removeAddress");
                boolean expResult = true;
                boolean result = oClient.removeAddress(oAddress);
                assertEquals(expResult, result);
                expResult = false;
                result = oClient.removeAddress(null);
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class Client.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = -1204346165;
                int result = oClient.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class Client.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                Client cli2 = new Client("testName", "testNif", "testTel", "testEmail", oAddress);
                boolean expResult = true;
                boolean result = oClient.equals(cli2);
                assertEquals(expResult, result);
                result = oClient.equals(oClient);
                assertEquals(expResult, result);
                expResult = false;
                result = oClient.equals(null);
                assertEquals(expResult, result);
                result = oClient.equals(new ProfessionalQualification("tttt"));
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class Client.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "testName - testNif - testTel - testEmailtestStreet | 0000-000 - testLocality";
                String result = oClient.toString();
                assertEquals(expResult, result);
        }
        
}
