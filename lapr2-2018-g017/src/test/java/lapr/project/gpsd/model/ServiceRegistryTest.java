/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import org.junit.jupiter.api.Test;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import lapr.project.gpsd.exception.DuplicatedIDException;
import static org.junit.Assert.*;

/**
 *
 * @author Simao
 */
public class ServiceRegistryTest {
    
       private final Category oCategory;
       private final StationaryService oService;
       private final ServiceRegistry oRegistry;
       
    public ServiceRegistryTest() throws DuplicatedIDException {
           oCategory = new Category("01", "Test Category");
           oService = new StationaryService("01", "Test", "Test Service", 6.2, oCategory);
           oService.setOtherAtributes("02:30");
           oRegistry = new ServiceRegistry();
           oRegistry.registerService(oService);
    }
    
    @Test
    public void testGetServices() {
        System.out.println("getServices");
        boolean expResult = true;
        boolean result = oRegistry.getServices().contains(oService);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetServiceById() {
        System.out.println("getServiceById");
        String strId = "01";
        Service expResult = oService;
        Service result = oRegistry.getServiceById(strId);
        assertEquals(expResult, result);
    }

    @Test
    public void testRegisterService(){
        System.out.println("registerService");
        boolean expResult = false;
        boolean result = true;
           try {
               result = oRegistry.registerService(oService);
           } catch (DuplicatedIDException ex) {
               result = false;
           }
        assertEquals(expResult, result);
    }

    @Test
    public void testValidateServiceTrue() {
        System.out.println("validateService");
        boolean expResult = false;
        boolean result = oRegistry.validateService(oService);
        assertEquals(expResult, result);
    }

        @Test
        public void testValidateServiceFalse() {
        System.out.println("validateService");
        oRegistry.getServices().add(oService);
        boolean expResult = false;
        boolean result = oRegistry.validateService(oService);
        oRegistry.getServices().remove(oService);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testGetServicesByCategory_String() {
        System.out.println("getServicesByCategory");
        String strCategory = "01";
        boolean expResult = true;
        boolean result = oRegistry.getServicesByCategory(strCategory).contains(oService);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetServicesByCategory_Category() {
        System.out.println("getServicesByCategory");
        boolean expResult = true;
        boolean result = oRegistry.getServicesByCategory(oCategory).contains(oService);
        assertEquals(expResult, result);
    }
    
}
