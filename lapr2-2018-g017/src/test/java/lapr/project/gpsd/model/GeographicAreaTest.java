package lapr.project.gpsd.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class GeographicAreaTest {
        
        private final PostalCode oPostalCode;
        private final PostalCode oPostalCode2;
        private final GeographicArea oGeographicArea;
        private final Address oAddress;
        
        public GeographicAreaTest() {
                this.oPostalCode = new PostalCode("4490-641", 41.3998301, -8.7668682);
                this.oPostalCode2 = new PostalCode("4495-449", 41.3951172, -8.7526565);
                this.oGeographicArea = new GeographicArea("Test", oPostalCode, 5.3, 10);
                this.oAddress = new Address("R. do Vale Maior", oPostalCode2, "Póvoa de Varzim");
        }

        /**
         * Test of getDesignation method, of class GeographicArea.
         */
        @Test
        public void testGetDesignation() {
                System.out.println("getDesignation");
                String expResult = "Test";
                String result = oGeographicArea.getDesignation();
                assertEquals(expResult, result);
        }

        /**
         * Test of getPostalCode method, of class GeographicArea.
         */
        @Test
        public void testGetPostalCode() {
                System.out.println("getPostalCode");
                PostalCode expResult = oPostalCode;
                PostalCode result = oGeographicArea.getPostalCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of getTravelExpenses method, of class GeographicArea.
         */
        @Test
        public void testGetTravelExpenses() {
                System.out.println("getTravelExpenses");
                double expResult = 5.3;
                double result = oGeographicArea.getTravelExpenses();
                assertEquals(expResult, result, 0.01);
        }

        /**
         * Test of getActionRange method, of class GeographicArea.
         */
        @Test
        public void testGetActionRange() {
                System.out.println("getActionRange");
                double expResult = 10;
                double result = oGeographicArea.getActionRange();
                assertEquals(expResult, result, 0.1);
        }

        /**
         * Test of actsIn method, of class GeographicArea.
         */
        @Test
        public void testActsIn() {
                System.out.println("actsIn");
                boolean expResult = true;
                boolean result = oGeographicArea.actsIn(oAddress);
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class GeographicArea.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = 2106299975;
                int result = oGeographicArea.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class GeographicArea.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                GeographicArea geoArea = new GeographicArea("Test", oPostalCode, 5.3, 10);
                boolean expResult = true;
                boolean result = oGeographicArea.equals(geoArea);
                assertEquals(expResult, result);
                result = oGeographicArea.equals(oGeographicArea);
                assertEquals(expResult, result);
                expResult = false;
                result = oGeographicArea.equals(null);
                assertEquals(expResult, result);
                result = oGeographicArea.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }
}
