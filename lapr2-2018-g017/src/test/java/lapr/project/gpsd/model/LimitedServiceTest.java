package lapr.project.gpsd.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class LimitedServiceTest {

        private final Category oCategory;
        private final LimitedService oService;

        public LimitedServiceTest() {
                oCategory = new Category("01", "Test Category");
                oService = new LimitedService("01", "Test", "Test Service", 6.2, oCategory);
        }

        /**
         * Test for LimitedService constructor.
         */
        @Test
        public void testLimitedService() {
                System.out.println("LimitedService");
                Throwable exception = assertThrows(IllegalArgumentException.class, () -> new LimitedService("", "", "", -1, null));
                assertEquals("Service arguments can't be empty or null.", exception.getMessage());
                Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> new LimitedService(null, null, null, -1, null));
                assertEquals("Service arguments can't be empty or null.", exception2.getMessage());
        }

        /**
         * Test of getId method, of class LimitedService.
         */
        @Test
        public void testGetId() {
                System.out.println("getId");
                String expResult = "01";
                String result = oService.getId();
                assertEquals(expResult, result);
        }

        /**
         * Test of getBriefDescription method, of class LimitedService.
         */
        @Test
        public void testGetBriefDescription() {
                System.out.println("getBriefDescription");
                String expResult = "Test";
                String result = oService.getBriefDescription();
                assertEquals(expResult, result);
        }

        /**
         * Test of getCompleteDescription method, of class LimitedService.
         */
        @Test
        public void testGetCompleteDescription() {
                System.out.println("getCompleteDescription");
                String expResult = "Test Service";
                String result = oService.getCompleteDescription();
                assertEquals(expResult, result);
        }

        /**
         * Test of getServiceType method, of class LimitedService.
         */
        @Test
        public void testGetServiceType() {
                System.out.println("getServiceType");
                ServiceType expResult = ServiceType.LIMITED;
                ServiceType result = oService.getServiceType();
                assertEquals(expResult, result);
        }

        /**
         * Test of getCategory method, of class LimitedService.
         */
        @Test
        public void testGetCategory() {
                System.out.println("getCategory");
                Category expResult = oCategory;
                Category result = oService.getCategory();
                assertEquals(expResult, result);
        }

        /**
         * Test of hasId method, of class LimitedService.
         */
        @Test
        public void testHasId() {
                System.out.println("hasId");
                String strId = "01";
                boolean expResult = true;
                boolean result = oService.hasId(strId);
                assertEquals(expResult, result);
        }

        /**
         * Test of hasOtherAtributes method, of class LimitedService.
         */
        @Test
        public void testHasOtherAtributes() {
                System.out.println("hasOtherAtributes");
                boolean expResult = false;
                boolean result = oService.hasOtherAtributes();
                assertEquals(expResult, result);
        }

        /**
         * Test of getOtherAtributes method, of class LimitedService.
         */
        @Test
        public void testGetOtherAtributes() {
                System.out.println("getOtherAtributes");
                Throwable exception = assertThrows(IllegalArgumentException.class, () -> oService.getOtherAtributes());
                assertEquals("Limited Services don't have other atributes.", exception.getMessage());
        }

        /**
         * Test of setOtherAtributes method, of class LimitedService.
         */
        @Test
        public void testSetOtherAtributes() {
                System.out.println("setOtherAtributes");
                Throwable exception = assertThrows(IllegalArgumentException.class, () -> oService.setOtherAtributes("test"));
                assertEquals("Limited Services don't have other atributes.", exception.getMessage());
        }

        /**
         * Test of getCostForDuration method, of class LimitedService.
         */
        @Test
        public void testGetCostForDuration() {
                System.out.println("getCostForDuration");
                String strDuration = "02:30";
                double expResult = 15.5;
                double result = oService.getCostForDuration(strDuration);
                assertEquals(expResult, result, 0.1);
        }

        /**
         * Test of getCostHour method, of class LimitedService.
         */
        @Test
        public void testGetCostHour() {
                System.out.println("getCostHour");
                String expResult = "6.2€";
                String result = oService.getCostHour();
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class LimitedService.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                LimitedService service = new LimitedService("01", "Test", "Test Service", 6.2, oCategory);
                boolean expResult = true;
                boolean result = oService.equals(service);
                assertEquals(expResult, result);
                result = oService.equals(oService);
                assertEquals(expResult, result);
                expResult = false;
                result = oService.equals(null);
                assertEquals(expResult, result);
                result = oService.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class LimitedService.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = 1842;
                int result = oService.hashCode();
                assertEquals(expResult, result);
        }

        /**
         * Test of toString method, of class LimitedService.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "01 - Test Service | 6.2€";
                String result = oService.toString();
                assertEquals(expResult, result);
        }
}
