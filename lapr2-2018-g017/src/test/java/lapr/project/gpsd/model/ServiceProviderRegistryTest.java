/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lapr.project.gpsd.model;

import java.util.ArrayList;
import java.util.List;
import lapr.project.auth.AuthFacade;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 *
 * @author Lenovo
 */
public class ServiceProviderRegistryTest {

    private final Category category = new Category("111", "Cleaning");
    private final List<Category> lstCategories = new ArrayList<>();
    private final PostalCode postalCode = new PostalCode("4490-982", 0, 0);
    private List<ActsIn> lstActsIn;
    private final GeographicArea geoArea = new GeographicArea("designation", postalCode, 0, 5, lstActsIn);
    private final List<GeographicArea> lstGeographicAreas = new ArrayList<>();
    private final ExportList lstExports = new ExportList();

    ServiceProvider serviceProvider = new ServiceProvider("12345", "14413904", "Vasco Miguel Medeiros Furtado", "Vasco Furtado", "email@gmail.com", lstCategories, lstGeographicAreas);

    private final AuthFacade oAuth = null;
    ServiceProviderRegistry serviceProviderRegistry = new ServiceProviderRegistry(oAuth);
    private final List<ServiceProvider> lstProviders = new ArrayList<>();

    public ServiceProviderRegistryTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    /**
     * Test of getProviders method, of class ServiceProviderRegistry.
     */
    @Test
    public void testGetProviders() {
        System.out.println("getProviders");
        List<ServiceProvider> expResult = new ArrayList<>();
        serviceProviderRegistry.addProvider(serviceProvider);
        expResult.add(serviceProvider);
        List<ServiceProvider> result = serviceProviderRegistry.getProviders();
        assertEquals(expResult, result);

    }

    /**
     * Test of newProvider method, of class ServiceProviderRegistry.
     */
    @Test
    public void testNewProvider() {
        System.out.println("newProvider");
        String strPersonnelNumber = "12345";
        String strNif = "14413904";
        String strFullName = "Miguel Castro Ferreira";
        String strAbbrevName = "Miguel Ferreira";
        String strEmail = "email@gmail.com";
        String strTelephone = "919285852";

        ServiceProvider expResult = new ServiceProvider("12345", "14413904", "Miguel Castro Ferreira", "Miguel Ferreira", "email@gmail.com", lstCategories, lstGeographicAreas);
        expResult.setTelephone("919285852");
        ServiceProvider result = serviceProviderRegistry.newProvider(strPersonnelNumber, strNif, strFullName, strAbbrevName, strEmail, strTelephone, lstCategories, lstGeographicAreas);
        assertEquals(expResult, result);

    }

    /**
     * Test of addProvider method, of class ServiceProviderRegistry.
     */
    @Test
    public void testAddProvider() {
        System.out.println("addProvider");

        boolean expResult = true;
        boolean result = serviceProviderRegistry.addProvider(serviceProvider);
        assertEquals(expResult, result);

    }

    /**
     * Test of getServiceProviderByEmail method, of class
     * ServiceProviderRegistry.
     */
    @Test
    public void testGetServiceProviderByEmail() {
        System.out.println("getServiceProviderByEmail");
        String strEmail = "email@gmail.com";
        lstProviders.add(serviceProvider);
        serviceProviderRegistry.addProvider(serviceProvider);

        ServiceProvider expResult = new ServiceProvider("12345", "14413904", "Miguel Castro Ferreira", "Miguel Ferreira", "email@gmail.com", lstCategories, lstGeographicAreas);
        ServiceProvider result = serviceProviderRegistry.getServiceProviderByEmail(strEmail);
        assertEquals(expResult, result);

    }

    /**
     * Test of registerProvider method, of class ServiceProviderRegistry.
     */
    @org.junit.Test
    public void testRegisterProvider() throws Exception {
        System.out.println("registerProvider");
        ServiceProvider oProvider = new ServiceProvider("12345", "14413904", "Miguel Castro Ferreira", "Miguel Ferreira", "email@gmail.com", lstCategories, lstGeographicAreas);
        String strPwd = "teste111";
        boolean expResult = false;
        boolean result = serviceProviderRegistry.registerProvider(oProvider, strPwd);
        assertEquals(expResult, result);

    }

    /**
     * Test of validateProvider method, of class ServiceProviderRegistry.
     */
    @org.junit.Test
    public void testValidateProvider() {
        System.out.println("validateProvider");
        ServiceProvider oProvider = new ServiceProvider("12345", "14413904", "Miguel Castro Ferreira", "Miguel Ferreira", "email@gmail.com", lstCategories, lstGeographicAreas);
        String strPwd = "123445";

        boolean expResult = false;
        boolean result = serviceProviderRegistry.validateProvider(oProvider, strPwd);
        assertEquals(expResult, result);

    }

}
