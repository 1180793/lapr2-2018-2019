package lapr.project.gpsd.model;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * @author Gonçalo Corte Real <1180793@isep.ipp.pt>
 */
public class ProfessionalQualificationTest {
        
        private final ProfessionalQualification oProfessionalQualification;
        
        public ProfessionalQualificationTest() {
                this.oProfessionalQualification = new ProfessionalQualification("Test");
        }

        /**
         * Test of getDescription method, of class ProfessionalQualification.
         */
        @Test
        public void testGetDescription() {
                System.out.println("getDescription");
                String expResult = "Test";
                String result = oProfessionalQualification.getDescription();
                assertEquals(expResult, result);
        }

        /**
         * Test of equals method, of class ProfessionalQualification.
         */
        @Test
        public void testEquals() {
                System.out.println("equals");
                ProfessionalQualification qualification = new ProfessionalQualification("Test");
                boolean expResult = true;
                boolean result = oProfessionalQualification.equals(qualification);
                assertEquals(expResult, result);
                result = oProfessionalQualification.equals(oProfessionalQualification);
                assertEquals(expResult, result);
                expResult = false;
                result = oProfessionalQualification.equals(null);
                assertEquals(expResult, result);
                result = oProfessionalQualification.equals(new PostalCode("0000-000", 0, 0));
                assertEquals(expResult, result);
        }

        /**
         * Test of hashCode method, of class ProfessionalQualification.
         */
        @Test
        public void testHashCode() {
                System.out.println("hashCode");
                int expResult = 2603277;
                int result = oProfessionalQualification.hashCode();
                assertEquals(expResult, result);
        }
        
        /**
         * Test of toString method, of class AcademicQualification.
         */
        @Test
        public void testToString() {
                System.out.println("toString");
                String expResult = "Test";
                String result = oProfessionalQualification.toString();
                assertEquals(expResult, result);
        }
        
}
